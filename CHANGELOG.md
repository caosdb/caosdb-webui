# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.15.2] - 2025-02-28 ##

### Fixed ###

* [#271](https://gitlab.com/linkahead/linkahead-webui/-/issues/271)
  Removed an unused parameter redefinition from query.xsl.

## [0.15.1] - 2024-12-20 ##

### Fixed ###

- [#254](https://gitlab.com/linkahead/linkahead-webui/-/issues/254) "Download files referenced in the table" failed
- [#230](https://gitlab.com/linkahead/linkahead-webui/-/issues/230)
  `make doc` is now compatible with npm >= 9.0.0.

## [0.15.0] - 2024-10-01 ##

### Added ###

* `BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING` build variable that
  determines whether reference dropdowns in the edit mode are filled
  automatically or only after manually clicking on "edit this
  property". Default is `ENABLED`, i.e., don't load the dropdown
  automatically.
* `caosdb-v-property-not-yet-lazy-loaded` class for reference
  properties in the edit mode, the reference dropdown menu of which
  hasn't been initialized yet.

### Changed ###

* [#262](https://gitlab.com/linkahead/linkahead-webui/-/issues/262)
  Reference dropdowns in the edit mode don't load automatically by
  default anymore but have to be initialized manually and per property
  by default. Set `BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING=DISABLED` to
  restore the previous behavior.

### Fixed ###

* `form_elements.make_scripting_submission_button` respects context root.

## [0.14.0] - 2024-07-25 ##

### Added ###

- GRPC Entity Service can be enabled with
  `BUILD_MODULE_EXT_GRPC_ENTIY_SERVICE=ENABLED`. Defaults to
  `DISABLED`. More info on this module under
  `src/doc/extension/grpc-entity-service.rst`. <https://docs.indiscale.com/caosdb-webui/extension/grpc-entity-service.html>

### Fixed ###

- [#252](https://gitlab.com/linkahead/linkahead-webui/-/issues/252) Make
  different link to parent dir more visible.
- [#255](https://gitlab.com/linkahead/linkahead-webui/-/issues/255) Remove
  disabled scroll bar under property names that would show on some browsers.
- [#258](https://gitlab.com/linkahead/linkahead-webui/-/issues/258)
  Ignore `auto_focus` when `create_show_form_callback` is called on an
  element without HTML `form` elements.

## [0.13.3 - 2024-04-23] ##

### Fixed ###

- Using correct (dynamic) base path for file upload.
- CSS with scrolling for long tables and entity names.

## [0.13.2 - 2024-02-20] ##

### Fixed ###

* [#247](https://gitlab.com/linkahead/linkahead-webui/-/issues/247) Linkify absorbs commas
  etc. (, . : ;) into link adress
* Query shortcuts are now displayed when using the new query panel.
* [249](https://gitlab.com/linkahead/linkahead-webui/-/issues/249) only the
  first ≈100 options were shown in edit-mode reference dropdown menus in case
  of `BUILD_MAX_EDIT_MODE_DROPDOWN_OPTIONS=-1`.

## [0.13.1 - 2023-11-17] ##

### Fixed ###

* [#242](https://gitlab.com/linkahead/linkahead-webui/-/issues/242) Missing
  LinkAhead logo in footer

## [0.13.0 - 2023-10-11] ##

### Added ###

* `form_elements` module: New `pattern` option for form fields of `type="text"`.
* `form_panel` new `auto_focus` parameter for the `create_show_form_callback`.
* The file-upload module from the [CaosDB WebUI Legacy
  Adapter](https://gitlab.com/caosdb/caosdb-webui-legacy-adapter) has been
  added. The new file-upload add a "+" button to the file-system view for
  uploading files into the current directory and new subdirectories. The module
  can be disabled by setting the build property
  `BUILD_MODULE_EXT_FILE_UPLOAD=DISABLED`.
* `BUILD_MODULE_LEGACY_MAP={DISABLED,ENABLED}`, which is enabled by default.
  This options prepares the web for the upcomming new map implementation (see
  below).
* The new map from the [CaosDB WebUI Legacy
  Adapter](https://gitlab.com/caosdb/caosdb-webui-legacy-adapter) has been
  added. It is disabled by default because it depends not only on the server's
  GRPC API but also on a proxy translating WebGRPC/HTTP1.1 (browser) requests
  to GRPC/HTTP2 (server) and vice versa.
  The new map should work as a drop-in for the legacy map with the exact
  functionality of the old map and the same config files. However, the new map
  also implements a new behavior for the query button: The query will be
  executed immediately, see
  [caosdb-webui#205](https://gitlab.com/caosdb/caosdb-webui/-/issues/205).
  You can enable the new map by disabling the old one AND placing a valid
  configuration at `conf/ext/json/ext_map.json`. A suitable proxy is envoy and
  a suitable envoy.yaml can be found in `misc/envoy.yaml`. Please have a look
  at that file for more information.


### Changed ###

* Update to bootstrap 5.3.1
* Update to bootstrap-icons 1.10.5

### Fixed ###

* The new query panel can't be deactivated.
  [caosdb-webui#226](https://gitlab.com/caosdb/caosdb-webui/-/issues/226)
* Fixed broken download of referenced files in the export of select statements.
  https://gitlab.com/linkahead/linkahead-webui/-/issues/238
* Do not send unpaged queries during Edit Mode usage. Fixes
  [caosdb-webui#217](https://gitlab.com/linkahead/linkahead-webui/-/issues/217)
* Fixed breaking timestamps due to whitespace
  [caosdb-webui#239](https://gitlab.com/caosdb/caosdb-webui/-/issues/239)

### Documentation ###

* Added `tools/query_template_descrtiption.yml` for easy set-up of query
  template datamodel.

## [0.12.0] - 2023-07-07 ##

### Added ###

* `navbar.hideElementforRoles` function that hides a given element for certain
  roles.

### Fixed ###

* Infinite loop in query panel auto-completion.
  [caosdb-webui#225](https://gitlab.com/caosdb/caosdb-webui/-/issues/225)
* Missing `autocomplete="off"` in query form.
  [caosdb-webui#223](https://gitlab.com/caosdb/caosdb-webui/-/issues/223)

## [0.11.1] - 2023-05-31 ##

### Fixed ###

* Styling problems introduced by a corrupted merge in the previous release

## [0.11.0] - 2023-05-31

### Added

* `caosdb-v-property-linkified` css class to denote properties that have been
  linkified already.
* `caosdb-f-property-datetime-value` css class for special handling of datetime properties.  Also
  there is now very basic customization (via variable `BUILD_MODULE_EXT_COSMETICS_CUSTOMDATETIME`)
  for how datetime values can be displayed, which comes along with the new css classes
  `caosdb-v-property-datetime-customized` and `caosdb-v-property-datetime-customized-newvalue`.
* `form_elements.make_form_modal` and
  `form_elements.make_scripting_submission_button` functions to create a form
  modal and an SSS submission button, respectively.
* Allow to supply a function that creates the form instead of a config to
  create_show_form_callback
* `BUILD_MODULE_EXT_ENTITY_ACL_USER_MANAGEMENT_BUTTON` build variable with which
  a button to the user management is added to the navbar for users with role
  administration if `BUILD_MODULE_EXT_ENTITY_ACL=ENABLED`.
* [#202](https://gitlab.com/caosdb/caosdb-webui/-/issues/202) - handle long
  text properties. Introcution of the
  `BUILD_LONG_TEXT_PROPERTY_THRESHOLD_SINGLE` and
  `BUILD_LONG_TEXT_PROPERTY_THRESHOLD_LIST` build variables. Any numeric
  value>0 will cause text properties with values longer than the threshold to
  be put into a `details` tag with a shortened summary. `DISABLED` will disable
  this feature. Defaults: 140 characters for single values, 40 for list values.
* Query filters that can be configured via a `query-form-tabs.json` (see [CaosDB
  WebUI Legacy Adapter](https://gitlab.com/caosdb/caosdb-webui-legacy-adapter))
  when the new query panel is used.

### Changed

* Changed name from LinkAhead to LinkAhead.

* The new query panel from the [CaosDB WebUI Legacy
  Adapter](https://gitlab.com/caosdb/caosdb-webui-legacy-adapter) is now the
  default. Change to the old query panel by setting
  `BUILD_MODULE_LEGACY_QUERY_FORM=ENABLED` (see below).

### Deprecated

* `query_form` module. Enable/disable via build property
  `BUILD_MODULE_LEGACY_QUERY_FORM={ENABLED,DISABLED}`. To be removed when
  caosdb-webui-core-components are included into this webui permanently.

### Fixed

* [#207](https://gitlab.com/caosdb/caosdb-webui/-/issues/207) - Error message
  is present but invisible.
* [#212](https://gitlab.com/caosdb/caosdb-webui/-/issues/212) - Searching for
  double values in scientific notation fails
* [#199](https://gitlab.com/caosdb/caosdb-webui/-/issues/199) - Linkify creates
  additional links after showing previews of referenced entities

### Documentation

* DOC: Extended documentation for Edit Mode.

## [0.10.1] - 2023-02-14

### Fixed

* [#194](https://gitlab.com/caosdb/caosdb-webui/-/issues/194) - Properties
  remain hidden in previews of referenced entities

## [0.10.0] - 2022-12-19
(Florian Spreckelsen)

### Added

* [#191](https://gitlab.com/caosdb/caosdb-webui/-/issues/191) - "Configure the
  RecordType which is searched by the simple search."
  A list of entity roles and names can be specified via the newly added
  `BUILD_FREE_SEARCH_ROLE_NAME_FACET_OPTIONS` build variable. See the docstring
  of `queryForm.initFreeSearch` for more information.
* [#188](https://gitlab.com/caosdb/caosdb-webui/-/issues/188) Properties can be
  hidden/shown only for certain users/roles.
* [#189](https://gitlab.com/caosdb/caosdb-webui/-/issues/189) The order in which
  the properties of (Records of) a RecordType are displayed can be configured.

### Changed

* Version bump of caosdb_map module (0.5.0):
  * Added configurable entityLayers
  * Changed name of the icon option to icon_options, because that name better
    distiguished the options from the result icon object.
* New behavior of the "Enter" key in the query input text field: When pressed
  when the autocompletion drop-down is open, the enter key selects an option
  from the autocompletion (this is as it was before). But when the "Enter" key
  is pressed, when the autocompletion drop-down is not open, the query is being
  submitted.

## [0.9.0] - 2022-11-02
(Florian Spreckelsen)

### Changed

* ext_references: names will be shown instead of file names if they exist

### Fixed

* Uncaught exception in dropdown menus of the edit mode when pressing arrow
  down or arrow up.
* [#182](https://gitlab.com/caosdb/caosdb-webui/-/issues/182) - Quotes breake
  "googly" search
* Hidden boolean select in edit mode.
* Missing latitude or longitude does no longer cause a problem when the element
  is on the current page

## [0.8.0] - 2022-07-12
(Timm Fitschen)

### Added

* Cache for queries in the edit_mode.

### Changed

* The "google query" now splits the given string at the spaces and creates an
  AND query (e.g., `john gibson` will result in `FIND ENTITY WHICH HAS A
  PROPERTY LIKE '*john*' AND A PROPERTY LIKE '*gibson*'`, not in `FIND ENTITY
  WHICH HAS A PROPERTY LIKE '*john gibson*'` as before).

### Fixed

* Styling of the selects in the edit_mode.

## [0.7.0] - 2022-05-31
(Florian Spreckelsen)

### Added

* [#172](https://gitlab.com/caosdb/caosdb-webui/-/issues/172) - Map can handle
  geo locations in list of references.

### Fixed

* [#276](https://gitlab.indiscale.com/caosdb/src/caosdb-webui/-/issues/276)
  documentation couldn't be built because of a too long module name.

## [0.6.0] - 2022-05-03
(Daniel Hornung)

### Changed

* Renamed the person reference resolver.

### Fixed

* [webui#170](https://gitlab.com/caosdb/caosdb-webui/-/issues/170)
  Autocompletion for "IS REFERENCED BY" leads to query syntax error

## [0.5.0] - 2022-03-25
(Timm Fitschen)

### Added

* entity_acl module which adds a button to each entity which links to the
  webui-acm module. Enable via BUILD_MODULE_EXT_ENTITY_ACL=ENABLED.
* A `#version_history` URI fragment which can be used to directly open the modal
  with the full version history of the first entity on the page.
* `BUILD_MODULE_SHOW_ID_IN_LABEL` build variable with which the showing of
  entity ids together with their names if it is enabled (disabled by default).
* Introduced `caosdb-f-form-required-marker` and `caosdb-f-form-required-label`
  css classes for the markers of required fields in LinkAhead form elements.
* The navbar has now an html id `caosdb-navbar-full`

### Changed

* Added `show` option to caosdb_map.MapConfig for showing the map initially and
  storing the state accross page reloads. Defaults to `false`. This also bumps
  the version of the map to 0.4.1 as it changes the behavior but the change is
  backwards-compatible for the map config. Clients need to update their
  version string in their config file, tho.

### Removed

* globla `setNameID` function (replaced by `_setDescriptionNameID` which should
  only be used be the non-public XML-serialization functions.)

### Fixed

* Fixed saving of text properties that were changed in the Source-Editing mode
  of the WYSIWYG editor.
* [#266](https://gitlab.indiscale.com/caosdb/src/caosdb-webui/-/issues/266)
  Fixed an issue whereby missing property descriptions in the edit mode would
  lead to wrongly detected entity updates in the server

## [0.4.2] - 2021-12-06

### Added (for new features, dependecies etc.)

* Documentation link in standard footer
* Build properties for footer elements:
  * `BUILD_FOOTER_CONTACT_HREF`
  * `BUILD_FOOTER_IMPRINT_HREF`
  * `BUILD_FOOTER_DOCS_HREF`
  * `BUILD_FOOTER_SOURCES_HREF`
  * `BUILD_FOOTER_LICENCE_HREF`
  See `build.properties.d/00_default.properties` for more information
* Start editing an entity/creating a new record directly by adding an `#edit` or
  `#new_record` URI fragment, respectively.
* Optional WYSIWYG editor with markdown output for text properties. Controled by
  the `BUILD_MODULE_EXT_EDITMODE_WYSIWYG_TEXT` build variable which is set do
  `DISABLED` by default.
* Added button to version history panel that allows restoring old versions

### Changed

* Default footer elements contain invalid links for imprint, contact, and data-policy now

### Removed

* Build property `BUILD_CUSTOM_IMPRINT`. Please use BUILD_FOOTER_IMPRINT_HREF
  and link a document instead. You can always put a html page to
  `src/ext/html/` and link to that.

### Fixed
- #156
- #251

## [0.4.1] - 2021-11-04

### Added

* `form_panel` module for conveniently creating a panel for web forms.
* `restore_old_version` function to base functionality (caosdb.js)
* buttons to the version history modal that allow restoring older versions

### Changed

* Default footer elements contain invalid links for imprint, contact, and data-policy now

### Removed

* Build property `BUILD_CUSTOM_IMPRINT`. Please use BUILD_FOOTER_IMPRINT_HREF
  and link a document instead. You can always put a html page to
  `src/ext/html/` and link to that.

### Fixed

* Auto-completion and edit_mode can handle entity names with empty spaces better now
* [#251](https://gitlab.indiscale.com/caosdb/src/caosdb-webui/-/issues/251) - Data loss when editing Entities with URL-like properties

## [0.4.0] - 2021-10-28

### Added

* Module `ext_qrcode` which generates a QR Code for an entity (pointing to the
  the head or the exact version).
* Optional functionality to bookmark all query results. Note that too many
  bookmarks will result in the URI being too lang and bookmarks will have to be
  cleared manually.

### Removed

* `getEntityId`, a former duplicate of `getEntityID` which must be used instead.

### Documentation

## [v0.4.0-rc1] - 2021-06-16

### Added

- `ext_applicable` module for building fancy features which append
  functionality to entities (EXPERIMENTAL).
- `ext_cosmetics` module which converts http(s) uris in property values into
  clickable links (with tests)
- Added a menu/toc for the tour
- Added a previous and next buttons for pages in the tour
- Added warnings to inform about minimum width when accessing tour and
  edit mode on small screens.
- Added a tutorial for the edit mode to the documentation
- Documentation on how to customize reference resolving

### Changed

- Updated from bootstrap 3 to bootstrap 5. This is a major change which will
  possibly break existing custom implementations (e.g. a custom welcome page)
  since a lot of classes where renamed by bootstrap an other classes have been
  dropped entirely (e.g. "jumbotron"). Please have a look at
    * https://getbootstrap.com/docs/5.0/migration/
    * https://getbootstrap.com/docs/4.6/migration/
- Moved the resolving of references to Person Records to separate
  example which can be disabled

### Deprecated

- css-class `.caosdb-property-text-value`. Please use
  `.caosdb-f-property-text-value` or `.caosdb-v-property-text-value` instead.

### Removed

* `#subnav` element from navbar which was previously used for spacing
* `caosdb.form.ready` event

### Fixed

- #212 - form_elements: Drop-down menu shows wrong value after clicking "None"
- #202 - Make filter fields in edit mode toolbox visible
- #117 - Reload data model after adding an RT or a Property
- #214 - Paging panel is hidden.
- Displaying issues with long lists in property values
- An issue whereby a grey container would appear above the map when
  changing the map view.
- #200 - Re-enabled the file-upload button

## [v0.3.1] - 2021-06-15

This is the last Bootstrap-3 compatible release.

### Added

* Displaying and interacting with the entity state.
* Change password functionality for users of the internal user source. Disable
  with `BUILD_MODULE_USER_MANAGEMENT=DISABLED` and set the user realm with
  `BUILD_MODULE_USER_MANAGEMENT_CHANGE_OWN_PASSWORD_REALM=CAOSDB`.
* Visually highlighted drop zones for properties and parents in the edit_mode.
* two new field types for the form_elements module, `file` and `select`. See
  the module documentation for more information.

### Changed
- The heading attributes datatype, path, checksum and size are now placed
  in a `details` html element.

### Deprecated

* Any bootstrap-3 dependencies. Please prepare upgrading to bootstrap-5 with the next release.

### Removed

* `ext_revisions` module. This module was only a work-around which had been
  used for versioning functionality before the native versioning was
  implemented. Also, the `BUILD_MODULE_EXT_REVISIONS` is no longer used and can
  be removed from the config files in `build.properties.d/`

### Fixed

* #156 - Edit mode for Safari 11
* #160 - Entity preview for Safari 11
* Several minor cosmetic flaws
* Fixed edit mode for Safari 11.

## [v0.3.0] - 2021-02-10

### Added

- The versioning model has a new styling and can show and tsv-export the full
  version history now.
- Module `ext_bookmarks` which allows users to bookmark entities, store them in
  a link or export them to TSV.
- table previews in the bottom line module
- added preview for tif images
* new function `form_elements.make_alert` which generates a proceed/cancel
  dialog which intercepts a function call and asks the user for confirmation.
* Deleting entities prompts for user confirmation after hitting the "Delete"
  button (edit_mode).
* Plotly preview has an additional parameter for a config object,
  e.g., for disabling the plotly logo
- The map can now show entities that have no geo location but are related to
  entities that have one. This also effects the search from the map.
* `getPropertyValues` function which generates a table of property values from
  a xml representation of entities.
* After a SELECT statement now also all referenced files can be downloaded.
* Automated documentation builds: `make doc`
- documentation on queries

### Changed

* ext_map version bumped to 0.4
- enabled and enhanced autocompletion
* Login form is hidden behind another button.

### Fixed

- #144 (Select with ANY VERSION OF).
- #136 (adding reference properties to entities in edit mode)
- exclude configuration files when reading files from build.properties.d
- summaries when opening preview
- #125 special characters like "\t", \"n", "#" are replaced in table
  download
- #158 show preview if the entity is too large for the viewport if
  bottom line is in view.

## [v0.2.1] - 2020-09-07

### Added

* `ext_jupyterdrag` (v0.1) module for dragging entities into jupyter notebooks.

## [v0.2] - 2020-09-02

### Added

* Build variable `EXT_REFERENCES_CUSTOM_REFERENCE_RESOLVER`. The value of this
  variable must be module which has at least a `resolve(id)` function, which
  returns a `reference_info` object for further processing by the
  `resolve_references` module.
* `ext_sss_markdown` module for pretty display of server-side scripting stdout.
  See module docstring for more information.
* `ext_trigger_crawler_form` module which generates a form for triggering the
  crawler. See module docstring for more info.
* Support for deeply nested selectors in SELECT queries.
* edit_mode supports reference properties when inserting/updating properties
  (See [#83](https://gitlab.com/caosdb/caosdb-webui/-/issues/83) and
  [#55](https://gitlab.com/caosdb/caosdb-webui/-/issues/55)).
* new `navbar.add_tool` method which adds a button to submenu of the navbar.
* new CSS classes for the styling of the default image and video preview of the
  `ext_bottom_line` module: `caosdb-v-bottom-line-image-preview` and
  `caosdb-v-bottom-line-video-preview`.
* basic support for entity versioning:
    * entity.xsl generates a versioning button which opens a versioning info modal.
    * `preview` also works for versioned references
    * `edit_mode` prevents the user from editing old versions of an entity.

### Changed
- The `navbar.add_button` signatur changed to `add_button(button, options)`.
  See the doc string of that method for more information.
- added a layout argument to the create_plot function of ext_bottom_line

### Deprecated

* css class `caosdb-property-text-value` is deprecated because different
  functionality interpreted it differently and most of the uses of this class
  have already been removed and replaced by specialized classes.

### Fixed

* #101 - loading of A LOT of references in `ext_references` slows down the
  webui or even freezes the brower.
* Fixed a bug where the Tour would lose its state upon page reload.

## [v0.2-rc.1] - 2020-04-10

### Added

* ext_bottom_line module (v0.1 - Experimental)
    * A module which adds a preview section where any kind of summarizing
      information (like plots, video players, thumbnails...) can be shown.
    * Enable with build property `BUILD_MODULE_EXT_BOTTOM_LINE=ENABLED`.
    * More documentation in `src/core/js/ext_bottom_line.js`.
* ext_revisions (v0.1 - EXPERIMENTAL)
    * Creates a backup copy of each entity which is updated via the edit_mode.
    * Enable via the build property `BUILD_MODULE_EXT_REVISIONS=ENABLED`.
    * Needs two special entities. A RecordType "Obsolete" and a reference
      property "revisionOf" with data type "Obsolete".
* Map (v0.3)
    * Adds a button to the navbar which toggles a map in the top of the main
      panel. The map currently shows all known entities which have geolocation
      properties and highlights those which are on the current page. Users can
      select an area in the map and generate a query filter for this area which
      is can be openend in the query panel.
    * See the caosdb_map module in  [ext_map.js](./src/core/js/ext_map.js)
    * The default configuration is located in the module itself.
    * The module needs an additional [ext_map.json](./conf/ext/json/ext_map.json)
      which defines at least the tiling server. The tiling server is not
      configured in the default config because this would require the caosdb
      maintainers to enforce the respective usage policies of the tiling server
      providers.
    * Test data for your server can be generated with
      [map_test_data.py](./misc/map_test_data.py).
    * The map module supports different map projections, e.g. Spherical
      Mercartor and projections for the polar regions. The active projection
      can changed by the user with a button.
* navbar module (v0.1)
    * A collection of helper functions for adding stuff to the navbar.
* caosdb_utils module (v0.1)
    * A collection of helper functions for multiple purposes, especially for
      checking a functions parameters (to mimic type savety).
* New Dependency: leaflet-1.5-1
    * for the map
* New Dependency: loglevel-1.6.4
    * Our new logging framework. Any new logging should be done with this
      framework.
* File Upload (v0.1 - EXPERIMENTAL)
    Works only for non-LIST properties
* Edit Mode - switch the LISTishness of entity properties back and forth.

### Changed

* The old `caosdb-property-row` CSS class has been replaced by
  `caosdb-v-property-row` for styling and `caosdb-f-property` for functional
  needs.
* The old `caosdb-property-value` and `caosdb-property-edit-value` CSS classes
  have been merged into a new `caosdb-f-property-value` class.
* In [ext_xls_download.js](./src/core/js/ext_xls_download.js): Complete rewrite
  of the module. The generation of the TSV table is done in this module now,
  instead of generating it with xsl (in [query.xsl](./src/core/xsl/query.xsl)).
  Also it is generated on demand.
* In [ext_references.js](./src/core/js/ext_references.js): Updated
  ext_references to v0.2. The new version can also generate and show summaries
  of `LIST` properties.
* In [entity.xsl](./src/core/xsl/entity.xsl): an emtpy property value (a `NULL`
  property) did not produce any `<span>` element with class
  `caosdb-property-text-value`. This caused the current implementation of
  `getPropertyFromElement` in [caosdb.js](./src/core/js/caosdb.js) to return
  the unit of the property as the value. The new implementation produces an
  empty `<span>` (no child text node) which is more appropriate and also fixes
  the buggy `getPropertyFromElement` without touching it.
* In [webcaosdb.js](./src/core/js/webcaosdb.js), `markdown` module: The
  markdown module is very generell and small now. The logic for converting
  comments (aka CommentAnnotations) to markdown is implemented in the
  `annotation` module now (which uses the `markdown` module as back-end, tho).
* updated QUnit test framework to 2.9.2

### Deprecated

* Image Preview in the FileSystem. The functionality is to be replaced by real
  thumbnails, which cover also non-image data-formats. The thumbnails resource
  is part of the new file system API of the LinkAhead Server which is currently
  under development.

### Removed

* Removed non-informative tests for webcaosdb.css
* Hard-coded image and video preview in the entity panel. The preview of images
  and videos is controlled by the `ext_bottom_line` module now.

### Fixed

* #95 - Edit Mode removes property values of reference properties when server
  response for possible reference targets is empty.
* Bug in `getPropertyFromElement` (see above) which returned the unit as the
  property value if the actual value was an empty string.
