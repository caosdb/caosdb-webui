<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2019 IndiScale GmbH (info@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
# Build and install the Web Interface

Here, we document how to install and build the LinkAhead Web Interface. If you are
only interested in how to use it, please continue [here](tutorials/index).

## Folder Structure

* The `src` folder contains all source code for the web interface.
* The `libs` folder contains all necessary third-party libraries as zip files.
* The `test` folder contains the unittests for the web interface.
* The `ext` folder contains extension for the web interface. The make file will
  copy all javascript files from `ext/js/` into the public folder and links the
  javascript in the `public/xsl/main.xsl`.
* The `misc` folder contains a simple http server which is used for running the
  unit tests.
* The `build.properties.d/` folder contains configuration files for the build.


## Build Configuration

The default configuration is defined in
`build.properties.d/00_default.properties`.

This file defines default variables which can be used in source files and 
will be replaced with the defined values during the build. 

All files in that directory will be sourced during `make install` and `make test`.
Thus any customized configuration can also be added to that folder by just placing
files in there which override the default values from `00_default.properties`.

See `build.properties.d/00_default.properties` for more information.

## Setup

* Run `make install` to compile/copy the web interface to a newly created
  `public` folder.
* Also, `make install` will copy the scripts from `src/server_side_scripting/`
  to `sss_bin/`. If you want to make the server-side scripts callable for the
  server as server-side scripts you need to include the `sss_bin/` directory
  into the server property `SERVER_SIDE_SCRIPTING_BIN_DIRS`.

## Test

* See [DEPENDENCIES](DEPENDENCIES#for-unit-testing) for the requirements to run the unit tests.
* Run `make test` to compile/copy the web interface and the tests to a newly
  created `public` folder.
* Run `make run-test-server` to start a python http server.
* The test suite can be started with `firefox http://localhost:8000/`.
* *On WSL (as of Feb 2024, WSL v2.0.14.0)*, port 8000 is sometimes not properly forwarded to the host
  resulting in an unreachable test suite. Switching to a different port can be done easily
  by using the `TEST_PORT` environment variable, e.g. with `TEST_PORT=8111 make
  run-test-server`. Using the X11 `firefox` from within WSL always works.
* To run the test suite non-interactively (as in CI/CD) use `make -d run-qunit`

## Clean

* Run `make clean` to clean up everything.

## Build the documentation #

Install the below requirements and build documentation in `build/`
with `make doc`. You may need to set the environment variables used in
`src/doc/Makefile`, especially `JSDOC_BIN` in case you didn't install
jsdoc globally.

### Requirements ##

- sphinx
- sphinx-autoapi
- jsdoc (`npm install jsdoc`)
- @indiscale/jsdoc-sphinx  (`npm install @indiscale/jsdoc-sphinx`)
- sphinx-js
- recommonmark
