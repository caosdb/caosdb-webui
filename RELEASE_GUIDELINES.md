# Release Guidelines for the LinkAhead Web Interface

This document specifies release guidelines in addition to the general release
guidelines of the LinkAhead Project
([RELEASE_GUIDELINES.md](https://gitlab.com/linkahead/linkahead-meta/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* CHANGELOG.md is up-to-date (insert version number and remove unpublished)
* DEPENDENCIES.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Check all general prerequisites.

3. Update the version:
   - Update `src/doc/conf.py` version and check that the correct linkahead-server
     version is listed in `DEPENDENCIES.md`.
   - `CITATION.cff` (update version and date)

4. Merge the release branch into the main branch.

5. Tag the latest commit of the main branch with `v<VERSION>`.

6. Create gitlab releases on gitlab.indiscale.com and on gitlab.com for new
   tag. Add most recent section of the changelog to release description.

7. Delete the release branch.

8. Merge the main branch back into the dev branch.

9. Prepare for next release cycle:
   * `CHANGELOG.md`: "Unreleased" section
   * `src/doc/conf.py`: Bump to next version number and `x.y.z-SNAPSHOT` for the `release` variable.
