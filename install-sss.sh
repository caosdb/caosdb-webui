SRC_DIR=$1
INSTALL_DIR=$2

mkdir -p $INSTALL_DIR

# from here on do your module-wise installing

# ext_table_preview
if [ "${BUILD_MODULE_EXT_BOTTOM_LINE_TABLE_PREVIEW}" == "ENABLED" ]; then
    mkdir -p $INSTALL_DIR/ext_table_preview
    cp $SRC_DIR/ext_table_preview/*.py $INSTALL_DIR/ext_table_preview/
    echo "installed all server-side scripts for ext_table_preview"
fi
# ext_file_download; should always be installed - No build variable
mkdir -p $INSTALL_DIR/ext_file_download
cp $SRC_DIR/ext_file_download/*.py $INSTALL_DIR/ext_file_download/
echo "installed all server-side scripts for ext_file_download"
