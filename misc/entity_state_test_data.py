#!/usr/bin/env python3
import sys
import linkahead as db


_PASSWORD = "Password1!"


def teardown():
    d = db.execute_query("FIND ENTITY WITH ID > 99")
    if len(d) > 0:
        d.delete(flags={"forceFinalState": "true"})


def setup_users():
    for role in ["publisher", "normal", "external"]:
        try:
            db.administration._delete_user(name=role+"_user")
        except BaseException:
            pass
    for role in ["publisher", "normal", "external"]:
        try:
            db.administration._delete_role(name=role)
        except BaseException:
            pass
    for role in ["publisher", "normal", "external"]:
        db.administration._insert_role(name=role, description="A test role")

        username = role + "_user"
        db.administration._insert_user(
            name=username,
            password=_PASSWORD,
            status="ACTIVE")
        db.administration._set_roles(username=username, roles=[role])

    db.administration._set_permissions(
        role="external", permission_rules=[
            db.administration.PermissionRule(
                "Grant", "TRANSACTION:RETRIEVE"),
        ])

    db.administration._set_permissions(
        role="normal", permission_rules=[
            db.administration.PermissionRule(
                "Grant", "TRANSACTION:*"),
            db.administration.PermissionRule(
                "Grant", "ACM:USER:UPDATE_PASSWORD:?REALM?:?USERNAME?"),
            db.administration.PermissionRule(
                "Grant", "STATE:TRANSITION:Edit"),
            db.administration.PermissionRule(
                "Grant", "STATE:TRANSITION:Start Review"),
            db.administration.PermissionRule(
                "Grant", "STATE:ASSIGN:Publish Life-cycle"),
        ])

    db.administration._set_permissions(
        role="publisher", permission_rules=[
            db.administration.PermissionRule(
                "Grant", "ACM:USER:UPDATE_PASSWORD:?REALM?:?USERNAME?"),
            db.administration.PermissionRule(
                "Grant", "TRANSACTION:*"),
            db.administration.PermissionRule(
                "Grant", "STATE:*"),
        ])


def freeze_and_hide(entity):
    """ nobody owns this entity and nobody has any permissions"""
    entity.acl = db.ACL()
    entity.acl.deny(role="?OTHER?", permission="*")
    entity.insert()


def setup_state_data_model():
    freeze_and_hide(db.RecordType("State"))
    freeze_and_hide(db.RecordType("StateModel"))
    freeze_and_hide(db.RecordType("Transition"))
    freeze_and_hide(db.Property(name="from", datatype="State"))
    freeze_and_hide(db.Property(name="to", datatype="State"))
    freeze_and_hide(db.Property(name="initial", datatype="State"))
    freeze_and_hide(db.Property(name="final", datatype="State"))
    freeze_and_hide(db.Property(name="color", datatype=db.TEXT))


def setup_state_model():
    unpublished_acl = db.ACL()
    unpublished_acl.grant(role="publisher", permission="*")
    unpublished_acl.grant(role="normal", permission="UPDATE:*")
    unpublished_acl.grant(role="normal", permission="RETRIEVE:ENTITY")
    unpublished_acl = db.State.create_state_acl(unpublished_acl)

    unpublished_state = db.Record(
        "Unpublished",
        description="Unpublished entries are only visible to the team and may be edited by any team member."
    ).add_parent("State").add_property(
        "color",
        "#5bc0de")
    unpublished_state.acl = unpublished_acl
    unpublished_state.insert()

    review_acl = db.ACL()
    review_acl.grant(role="publisher", permission="*")
    review_acl.grant(role="normal", permission="RETRIEVE:ENTITY")

    review_state = db.Record(
        "Under Review",
        description="Entries under review are not publicly available yet, but they can only be edited by the members of the publisher group."
    ).add_parent("State").add_property(
        "color",
        "#FFCC33")
    review_state.acl = db.State.create_state_acl(review_acl)
    review_state.insert()

    published_acl = db.ACL()

    published_state = db.Record(
        "Published",
        description="Published entries are publicly available and cannot be edited unless they are unpublished again."
    ).add_parent("State").add_property(
        "color",
        "#333333")
    published_state.acl = db.State.create_state_acl(published_acl)
    published_state.insert()

    # 1->2
    db.Record(
        "Start Review",
        description="This transitions denies the permissions to edit an entry for anyone but the members of the publisher group. However, the entry is not yet publicly available."
    ).add_parent("Transition").add_property(
        "from",
        "unpublished").add_property(
            "to",
        "under review").add_property(
        "color",
        "#FFCC33").insert()

    # 2->3
    db.Record(
        "Publish",
        description="Published entries are visible for the public and cannot be changed unless they are unpublished again. Only members of the publisher group can publish or unpublish entries."
    ).add_parent("Transition").add_property(
        "from", "under review").add_property(
            "to", "published").add_property(
        "color",
        "red").insert()

    # 3->1
    db.Record("Unpublish", description="Unpublish this entry to hide it from the public. Unpublished entries can be edited by any team member.").add_parent(
        "Transition").add_property("from", "published").add_property("to", "unpublished").insert()

    # 2->1
    db.Record("Reject", description="Reject the publishing of this entity.  Afterwards, the entity is editable for any team member again.").add_parent(
        "Transition").add_property("from", "under review").add_property("to", "unpublished").insert()

    # 1->1
    db.Record("Edit", description="Edit this entity. The changes are not publicly available until this entity will have been reviewed and published.").add_parent(
        "Transition").add_property(
        "from",
        "unpublished").add_property(
            "to",
        "unpublished").insert()

    db.Record("Publish Life-cycle", description="The publish life-cycle is a quality assurance tool. Database entries can be edited without being publicly available until the changes have been reviewed and explicitely published by an eligible user.").add_parent("StateModel").add_property(
        "Transition",
        datatype=db.LIST("Transition"),
        value=[
            "Edit",
            "Start Review",
            "Reject",
            "Publish",
            "Unpublish",
        ]).add_property(
        "initial",
        "Unpublished").add_property(
        "final",
        "Unpublished").insert()


def setup_test_data():
    # any record of this type will have the unpublished state
    rt = db.RecordType("TestRT")
    rt.state = db.State(model="Publish Life-cycle", name="Unpublished")
    rt.acl = db.ACL()
    rt.acl.grant(role="normal", permission="RETRIEVE:ENTITY")
    rt.acl.grant(role="normal", permission="USE:AS_PARENT")
    rt.insert()

    prop = db.Property("TestProperty", datatype=db.TEXT)
    prop.acl = db.ACL()
    prop.acl.grant(role="normal", permission="RETRIEVE:ENTITY")
    prop.acl.grant(role="normal", permission="USE:AS_PROPERTY")
    prop.insert()

    rec = db.Record().add_parent("TestRT")
    rec.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."
    rec.add_property("TestProperty", "TestValue")
    rec.insert()


if __name__ == "__main__":
    for call in sys.argv[1:]:
        if call == "setup_test_data":
            setup_test_data()
        elif call == "setup_state_data_model":
            setup_state_data_model()
        elif call == "setup_state_model":
            setup_state_model()
        elif call == "setup_users":
            setup_users()
        elif call == "teardown":
            teardown()
        elif call == "all":
            setup_users()
            setup_state_data_model()
            setup_state_model()
            setup_test_data()
        else:
            print("unknown parameter")
