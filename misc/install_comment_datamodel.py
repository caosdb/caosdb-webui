#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

import sys
import linkahead as db


if len(db.execute_query("FIND RecordType CommentAnnotation")) > 0:
    print("RecordType CommentAnnotation does exist")
    sys.exit(1)

comment = db.Property("comment", datatype=db.TEXT).insert()
annotationOf = db.Property("annotationOf", datatype=db.REFERENCE).insert()
par = db.RecordType("Annotation")
par.add_property(annotationOf, importance=db.OBLIGATORY)
par.insert()
rt = db.RecordType("CommentAnnotation").add_parent("Annotation",
                                                   inheritance=db.OBLIGATORY)
rt.add_property(comment, importance=db.OBLIGATORY)
rt.insert()
