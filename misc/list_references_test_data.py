#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
(C) Copyright 2019-2020 IndiScale GmbH <info@indiscale.com>
(C) Copyright 2019-2020 Timm Fitschen <t.fitschen@indiscale.com>

Test entities for the ext_references module.
"""

import linkahead
import random

d = linkahead.execute_query("FIND TestReferenc*")
if len(d) > 0:
    d.delete()

# data model
datamodel = linkahead.Container()
datamodel.extend([
    linkahead.RecordType("TestReferenced"),
    linkahead.RecordType(
        "TestReferencing"
    ).add_property("TestReferenced", datatype=linkahead.LIST("TestReferenced")),
])

datamodel.insert()


# test data
testdata = linkahead.Container()

for i in range(100):
    testdata.append(
        linkahead.Record("TestReferenceObject-{}".format(i)
                         ).add_parent("TestReferenced")
    )

testdata.insert()
linkahead.Record().add_parent(
    "TestReferencing"
).add_property("TestReferenced",
               datatype=linkahead.LIST("TestReferenced"),
               value=testdata
               ).insert()
