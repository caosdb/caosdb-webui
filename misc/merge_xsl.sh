#!/bin/bash
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH
# Copyright (C) 2020 Timm Fitschen (t.fitschen@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# This file can be used to merge xsl files together. It replaces include
# directives in the webcaosdb.xsl with the content of the included xsl file,
# resulting in one, big(ish) stand-alone xsl file.
#
# The resulting file can be tested (least for syntax) with
# `xsltproc public/webcaosdb.xsl test/core/xml/test_case_preview_entities.xml`.


SOURCE_DIR=public/
MERGE="xsl/jsheader.xsl xsl/footer.xsl xsl/filesystem.xsl xsl/entity.xsl xsl/query.xsl xsl/messages.xsl xsl/navbar.xsl xsl/main.xsl xsl/welcome.xsl xsl/common.xsl"
TARGET=public/webcaosdb.xsl


function _merge () {
    _HREF=$1
    _SOURCE=$2
    _TARGET=$3

    echo "merging ${_SOURCE} into ${_TARGET} by replacing xsl:include directive with href=${_HREF} by the content of ${_SOURCE}."

    # inject bash-style env-var name into xsl
    _PLACEHOLDER="  <!-- COPIED FROM ${_HREF} (START)-->\n  \$\{_CONTENT_XSL\}\n  <!-- COPIED FROM ${_HREF} (END)-->"
    sed "s|^.*xsl:include.*${_HREF}.*|${_PLACEHOLDER}|g" -i ${_TARGET}

    # remove header from source file
    csplit -f ".tmp_source" "${_SOURCE}" /xsl:stylesheet/
    rm .tmp_source00

    # remove root and other things
    sed "/xsl:stylesheet/d" -i .tmp_source01
    sed "/xsl:output/d" -i .tmp_source01

    # replace env-var in tmp results
    export _CONTENT_XSL=$(cat .tmp_source01)
    envsubst '$_CONTENT_XSL' < "${_TARGET}" > .tmp_results.xsl
    rm .tmp_source01

    # replace target file with newly generated one
    mv .tmp_results.xsl ${_TARGET}
}

for HREF in $MERGE ; do
    _SOURCE=${SOURCE_DIR}${HREF}
    _merge $HREF ${_SOURCE} $TARGET
done

