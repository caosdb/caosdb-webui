#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

import linkahead


d = linkahead.execute_query("FIND Test*")
if len(d) > 0:
    d.delete()

rt_house = linkahead.RecordType("TestHouse").insert()
rt_house.description = "A House"
linkahead.RecordType("TestWindow").insert()
rt_person = linkahead.RecordType("TestPerson").insert()
linkahead.RecordType("TestParty").insert()
linkahead.Property("TestHeight", datatype=linkahead.DOUBLE, unit="ft").insert()
linkahead.Property("TestDate", datatype=linkahead.DATETIME).insert()

window = linkahead.Record().add_parent("TestWindow")
window.add_property("TestHeight", 20.5, unit="ft")
window.insert()

owner = linkahead.Record("The Queen").add_parent("TestPerson").insert()

house = linkahead.Record("Buckingham Palace")
house.description = "A rather large house"
house.add_parent("TestHouse")
house.add_property(rt_person, name="TestOwner", value=owner)
house.add_property("TestWindow", window).insert()

g1 = linkahead.Record().add_parent("TestPerson").insert()
g2 = linkahead.Record().add_parent("TestPerson").insert()
g3 = linkahead.Record().add_parent("TestPerson").insert()

party = linkahead.Record("Diamond Jubilee of Elizabeth II").add_parent("TestParty")
party.add_property(rt_house, name="Location", value=house)
party.add_property("TestDate", "2012-02-06")
party.add_property(rt_person, datatype=linkahead.LIST(rt_person), name="Guests",
                   value=[g1, g2, g3])
party.insert()
