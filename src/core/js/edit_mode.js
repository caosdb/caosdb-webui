/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 Henrik tom Wörden
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

"use strict";

/**
 * Edit mode module
 */
var edit_mode = new function() {

    var logger = log.getLogger("edit_mode");

    /**
     * Fired by the new property element *after* it has been added to the
     * entity.
     */
    this.property_added = new Event("caosdb.edit_mode.property_added");

    /**
     * Fired by the entity element after it has been set up for editing.
     */
    this.start_edit = new Event("caosdb.edit_mode.start_edit")

    /**
     * Fired by the entity element after editing (canceled or saved).
     */
    this.end_edit = new Event("caosdb.edit_mode.end_edit")

    /**
     * Fired by a list-property when an (input) element is added to the list during editing.
     */
    this.list_value_input_added = new Event("caosdb.edit_mode.list_value_input_added");

    /**
     * Fired by a list-property when an (input) element is removed to the list during editing.
     */
    this.list_value_input_removed = new Event("caosdb.edit_mode.list_value_input_removed");


    /**
     * Fired by a property when the data type changes.
     */
    this.property_data_type_changed = new Event("caosdb.edit_mode.property_data_type_changed");

    /**
     * Fired when a reference property is editted. Only relevant if
     * BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING=ENABLED.
     */
    this.property_edit_reference_value = new Event("caosdb.edit_mode.property_edit_reference_value");

    /**
     * Initialize this module
     */
    this.init = function() {
        if (isAuthenticated()) {
            this._init();
        } else {
            window.localStorage.removeItem("edit_mode");
        }
    }

    this.has_edit_fragment = function() {
        const fragment = window.location.hash.substr(1);
        return fragment === "edit";
    }

    this.has_new_record_fragment = function() {
        const fragment = window.location.hash.substr(1);
        return fragment === "new_record";
    }

    this._init = function() {
        var target = $("#top-navbar").find("ul").first();
        this.add_edit_mode_button(target, edit_mode.toggle_edit_mode);

        var after_setup_callback = () => {} // do nothing
        if (this.has_edit_fragment()) {
            // find first entity
            const first_entity = $(".caosdb-entity-panel")[0];
            if (first_entity) {
                window.localStorage["edit_mode"] = true;
                after_setup_callback = () => {
                    logger.debug("Edit this entity after #edit in the uri", first_entity);
                    edit_mode.edit(first_entity);
                }
            }
        } else if (this.has_new_record_fragment()) {
            for (let entity of $(".caosdb-entity-panel")) {
                // find first record type
                if (getEntityRole(entity) === "RecordType") {
                    window.localStorage["edit_mode"] = true;
                    const new_record = edit_mode.create_new_record(getEntityID(entity));
                    after_setup_callback = () => {
                        logger.debug("Create a new record after #new_record in the uri", entity);
                        new_record.then((new_record) => {
                            edit_mode.app.newEntity(new_record);
                        }, edit_mode.handle_error);
                    }
                    break;
                }
            }
        }

        // intialize the edit mode panel and add all necessary buttons if the edit mode is 
        if (this.is_edit_mode()) {

            edit_mode.enter_edit_mode().then(after_setup_callback);
            edit_mode.toggle_edit_panel();
            // This is for the very specific case of reloading the
            // page while the edit mode is active on small screens
            $(".caosdb-edit-min-width-warning").removeClass("d-none");
            $(".caosdb-edit-min-width-warning").addClass("d-block");
        }
        $('.caosdb-f-edit').css("transition", "top 1s");

        // add drag-n-drop listener (needed for the edit_mode toolbox).
        edit_mode.init_dragable();
    }


    this.dragstart = function(e) {
        e.dataTransfer.setData("text/plain", e.target.id);
    }

    this.dragleave = function(e) {
        edit_mode.unhighlight();
    }

    this.dragover = function(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "copy";
        edit_mode.highlight(this);
    }

    /**
     * Add a property element to the entity's properties section and make it editable.
     *
     * @param entity
     * @param new_prop
     * @param make_property_editable_cb
     */
    this.add_new_property = function(entity, new_prop, make_property_editable_cb = edit_mode.make_property_editable) {
        if (typeof entity === "undefined" || !(entity instanceof HTMLElement)) {
            throw new TypeError("entity must instantiate HTMLElement");
        }
        if (typeof new_prop === "undefined" || !(new_prop instanceof HTMLElement)) {
            throw new TypeError("new_prop must instantiate HTMLElement");
        }
        const drop_zone = $(entity).find(".caosdb-properties").find(".caosdb-f-edit-mode-property-dropzone");
        drop_zone.before(new_prop);
        make_property_editable_cb(new_prop);
        new_prop.dispatchEvent(edit_mode.property_added);
    }


    /**
     * Add a dropped property to the entity.
     *
     * @param {Event} e - the drop event.
     * @param {HTMLElement} entity - the entity.
     */
    this.add_dropped_property = function(e, entity) {
        var propsrcid = e.dataTransfer.getData("text/plain");
        var tmp_id = propsrcid.split("-");
        var prop_id = tmp_id[tmp_id.length - 1];
        var entity_type = tmp_id[tmp_id.length - 2];
        if (entity_type == "p") {
            retrieve_dragged_property(prop_id).then(new_prop_doc => {
                edit_mode.add_new_property(entity, new_prop_doc.firstChild);
            }, edit_mode.handle_error);
        } else if (entity_type == "rt") {
            var name = $("#" + propsrcid).text();
            var dragged_rt = str2xml('<Response><Property id="' + prop_id + '" name="' + name + '" datatype="' + name + '"></Property></Response>');
            transformation.transformProperty(dragged_rt).then(new_prop_doc => {
                edit_mode.add_new_property(entity, new_prop_doc.firstChild);
            }, edit_mode.handle_error);
        }
    }


    /**
     * Add a dropped parent to the entity.
     *
     * @param {Event} e - the drop event.
     * @param {HTMLElement} entity - the entity.
     */
    this.add_dropped_parent = function(e, entity) {
        var propsrcid = e.dataTransfer.getData("text/plain");
        var parent_list = entity.getElementsByClassName("caosdb-f-parent-list")[0]
        var tmp_id = propsrcid.split("-");
        var prop_id = tmp_id[tmp_id.length - 1];
        var entity_type = tmp_id[tmp_id.length - 2];
        if (entity_type == "p") {
            logger.warn("SHOULD not happend")
        } else if (entity_type == "rt") {
            var name = $("#" + propsrcid).text();
            var dragged_rt = str2xml('<Response><RecordType id="' + prop_id + '" name="' + name + '"></RecordType></Response>');
            transformation.transformParent(dragged_rt).then(new_prop => {
                parent_list.appendChild(new_prop);
                edit_mode.add_parent_delete_buttons(entity);
            }, edit_mode.handle_error);

        }
    }

    this.property_drop_listener = function(e) {
        edit_mode._drop_listener.call(this, e, edit_mode.add_dropped_property);
    }

    this.parent_drop_listener = function(e) {
        edit_mode._drop_listener.call(this, e, edit_mode.add_dropped_parent);
    }

    this._drop_listener = function(e, add_cb) {
        e.preventDefault();
        edit_mode.unhighlight();

        const app = edit_mode.app;
        const state = app.state;

        if (state === "initial") {
            var entity = $(this).parent();
            app.startEdit(entity[0]);
        } else if (state !== "changed") {
            // return unless in states `initial` or `changed`.
            return false;
        }
        add_cb(e, app.entity);
        return true;
    }

    // Dropping a RecordType in the heading will add it as a parent.
    // This is done by this function. 
    this.parent_drop = function(e) {
        logger.assert(edit_mode.app.state === "changed", "state should be changed. Current state: ", edit_mode.app.state, edit_mode.app, e);
        e.preventDefault();
        edit_mode.unhighlight();
        edit_mode.add_dropped_parent(e, this);
    }


    this.set_entity_dropable = function(entity, dragover, dragleave, parent_drop, property_drop) {
        if (getEntityRole(entity) === "Property") {
            // currently no parents and subproperties for properties.
            return;
        }
        var rts = entity.getElementsByClassName("caosdb-entity-panel-body");
        for (var rel of rts) {
            rel.addEventListener("dragleave", dragleave);
            rel.addEventListener("dragover", dragover);
            rel.addEventListener("drop", property_drop);
        }
        var headings = entity.getElementsByClassName("caosdb-entity-panel-heading");
        for (var rel of headings) {
            rel.addEventListener("dragleave", dragleave);
            rel.addEventListener("dragover", dragover);
            rel.addEventListener("drop", parent_drop);
        }

    }

    this.unset_entity_dropable = function(entity, dragover, dragleave, parent_drop, property_drop) {
        var rts = entity.getElementsByClassName("caosdb-entity-panel-body");
        for (var rel of rts) {
            rel.removeEventListener("dragleave", dragleave);
            rel.removeEventListener("dragover", dragover);
            rel.removeEventListener("drop", property_drop);
        }
        var headings = entity.getElementsByClassName("caosdb-entity-panel-heading");
        for (var rel of headings) {
            rel.removeEventListener("dragleave", dragleave);
            rel.removeEventListener("dragover", dragover);
            rel.removeEventListener("drop", parent_drop);
        }
    }

    this.remove_save_button = function(ent) {
        $(ent).find('.caosdb-f-entity-save-button').remove();
    }

    this.add_save_button = function(ent, callback) {
        var save_btn = $('<button class="btn btn-link caosdb-update-entity-button caosdb-f-entity-save-button">Save</button>');

        $(ent).find(".caosdb-f-edit-mode-entity-actions-panel").append(save_btn);

        $(save_btn).click(callback);
    }

    /*
     * This is unused functionality which might be added as an expert option later on.
     *
    this.add_edit_xml_button = function(entity) {
        var button = $('<button class="btn btn-link caosdb-update-entity-button" title="Edit the XML representation of this entity."><span class="glyphicon glyphicon-pencil"/> Edit XML</button>');
        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        var callback = (e) => {
            e.stopPropagation();
            transaction.update.initUpdate(entity); 
        }

        button.click(callback);
    }
    */

    /**
     * Remove all .caosdb-f-parent-trash-button in the entity header.
     * Then add a trash button to all parents if there are more than one
     * parents or if this is not a Record. Reason: Records must have at least
     * one parent whereas Properties and RecordTypes may come without parents.
     *
     * The newly created trash button removes the corresponding parent and
     * calls this function again to reset the trash buttons (and remove the
     * last trash button for the last parent if the header belongs to a
     * record).
     */
    this.add_parent_delete_buttons = function(header) {
        $(header).find(".caosdb-f-parent-trash-button").remove();
        var parents = $(header).find(".caosdb-parent-item");
        if ((parents.length > 1) || getEntityRole(header) != "Record") {
            for (var par of parents) {
                edit_mode.add_parent_trash_button($(par).find('.caosdb-f-parent-actions-panel')[0], par, () => {
                    edit_mode.add_parent_delete_buttons(header);
                });
            }
        }
    }

    /**
     * Append a trash button with class "caosdb-f-parent-trash-button", bind a
     * remove on the deletable, and bind a callback function to click.
     */
    this.add_parent_trash_button = function(appendable, deletable, callback = undefined) {
        edit_mode.add_trash_button(appendable, deletable, "caosdb-f-parent-trash-button", callback);
    }

    /**
     * Append a trash button with class "caosdb-f-property-trash-button" and
     * bind a remove on the deletable.
     */
    this.add_property_trash_button = function(appendable, deletable) {
        edit_mode.add_trash_button(appendable, deletable, "caosdb-f-property-trash-button", undefined, "Remove this property");
    }

    /**
     * Append a trash button which removes the deletable and calls an optional
     * callback function.
     *
     * @param {HTMLElement} appendable - add the button to this element.
     * @param {HTMLElement} deletable - delete this element on click.
     * @param {string} className - a class name for the button element.
     * @param {function} [callback] - optional parameterless callback function
     *     which is called on click.
     * @param {string} [title] - optional title for the button element.
     * @return {undefined}
     */
    this.add_trash_button = function(appendable, deletable, className, callback = undefined, title = undefined) {
        var button = $('<button class="btn btn-link p-0 ' + className + ' caosdb-f-entity-trash-button"><i class="bi-trash"></i></button>');
        if (title) {
            button.attr("title", title);
        }
        $(appendable).append(button);
        button.click((e) => {
            e.stopPropagation();
            deletable.remove();
            if (typeof callback == 'function') {
                callback();
            }
        });
    }


    /**
     * Pure converter from a datatype in string representation to a json
     * objects.
     *
     * E.g. passing "LIST<Person>" results in
     *     { atomic_datatype: "REFERENCE",
     *       reference_scope: "Person",
     *       is_list: true 
     *     }
     *
     * @param {str} datatype
     * @returns {object}
     */
    this.get_datatype_from_string = function(datatype) {

        var atomic = datatype;
        const is_list = edit_mode.isListDatatype(datatype);
        if (is_list) {
            // unwrap the datatype from LIST<...>
            atomic = edit_mode.unListDatatype(datatype);
        }

        var ref = undefined;
        if (edit_mode._known_atomic_datatypes.indexOf(atomic) == -1) {
            // `atomic` not in `known_atomic_datatypes` (e.g. "Person") -> this
            // is a reference and the `atomic` is actually the reference's
            // scope entity.
            ref = atomic;
            atomic = "REFERENCE";
        }

        return {
            "atomic_datatype": atomic,
            "reference_scope": ref,
            "is_list": is_list
        };
    }

    /**
     * Pure converter from a datatype json objects to a string representation
     * of the datatype.
     *
     * E.g. passing
     *     { atomic_datatype: "REFERENCE",
     *       reference_scope: "Person",
     *       is_list: true 
     *     }
     * results in "LIST<Person>".
     *
     * @param {object} datatype
     * @returns {str}
     */
    this.get_datatype_str = function(datatype) {
        var result = datatype["atomic_datatype"];

        const ref = datatype["reference_scope"];
        if (ref !== null && typeof ref !== "undefined") {
            result = ref;
        }

        const is_list = datatype["is_list"];
        if (is_list === "on" || is_list === true) {
            result = `LIST<${result}>`;
        }

        return result;
    }


    /**
     * Insert entities.
     *
     * @param {(HTMLElement|HTMLElement[])} ent_elements - a single or an array
     *        of entities in HTML representation.
     * @returns {XMLDocument} the unprocessed server response.
     */
    this.insert_entity = async function(ent_elements) {
        ent_elements = caosdb_utils.assert_array(ent_elements, "param `ent_elements`", true);
        var xmls = [];
        for (const ent_element of ent_elements) {
            xmls.push(edit_mode.form_to_xml(ent_element));
        }
        return await insert(xmls);
    }

    /**
     * Generate an XML from the form of the edited/newly created entity.
     *
     * @param {HTMLElement} entity_form - FORM of new/changed entity.
     * @returns {Document|DocumentFragment} - An xml document containing the
     *     entity in XML representation.
     */
    this.form_to_xml = function(entity_form) {
        const obj = form_elements.form_to_object($(entity_form).find("form")[0])[0];
        var entityRole = getEntityRole(entity_form);
        var file_path = undefined;
        var file_checksum = undefined;
        var file_size = undefined;
        if (entityRole.toLowerCase() == "file") {
            file_path = getEntityPath(entity_form);
            file_checksum = getEntityChecksum(entity_form);
            file_size = getEntitySize(entity_form);
        }
        return createEntityXML(
            entityRole,
            getEntityName(entity_form),
            getEntityID(entity_form),
            edit_mode.getProperties(entity_form),
            getParents(entity_form),
            true,
            edit_mode.get_datatype_str(obj),
            getEntityDescription(entity_form),
            obj.unit,
            file_path, file_checksum, file_size
        );
    }

    /**
     * TODO merge with getPropertyFromElement in caosdb.js
     */
    this.getPropertyFromElement = function(element) {
        var editfield = $(element).find(".caosdb-f-property-value");
        var property = getPropertyFromElement(element);

        var _parse_single_datetime = function(field) {
            let time = $(field).find(":input[type='time']").val()
            let date = $(field).find(":input[type='date']").val();
            // subseconds are stored in the subsec attribue of the input element
            let subsec = $(field).find(":input[type='time']").attr('subsec');
            if (time) {
                return input2caosdbDate(date, time, subsec);
            } else {
                return date;
            }
        }

        // The property is a reference, but wasn't editted (the
        // dropdown options were never loaded, so leave unchanged).
        if (property.reference && ("${BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING}" == "ENABLED") && $(element).hasClass("caosdb-v-property-not-yet-lazy-loaded")) {
            return property;
        }
        // LISTs need to be handled here
        if (property.list == true) {
            property.value = [];
            if (["TEXT", "DOUBLE", "INTEGER"].includes(property.listDatatype)) {
                // LOOP over elements of editfield.find(":input")
                for (var singleelement of $(editfield).find(":not(.caosdb-f-list-item-button):input:not(.caosdb-unit)")) {
                    property.value.push($(singleelement).val());
                }
            } else if (property.listDatatype == "DATETIME") {
                for (let singleelement of $(editfield).find("li")) {
                    property.value.push(_parse_single_datetime(singleelement));
                }
            } else if (property.reference || property.listDatatype == "BOOLEAN") {
                // LOOP over elements of editfield.find("select")
                for (var singleelement of $(editfield).find("select")) {
                    property.value.push($(singleelement).val());
                }
            } else {
                throw ("This property's data type is not supported by the webui. Please issue a feature request for support for `" + property.datatype + "`.");
            }
        } else {
            if (["TEXT", "DOUBLE", "INTEGER"].includes(property.datatype)) {
                property.value = editfield.find(":input:not(.caosdb-unit)").val();
            } else if (property.datatype == "DATETIME") {
                property.value = _parse_single_datetime(editfield);
            } else if (property.reference || property.datatype == "BOOLEAN") {
                property.value = $(editfield).find("select").val();
            } else {
                throw ("This property's data type is not supported by the webui. Please issue a feature request for support for `" + property.datatype + "`.");
            }
        }
        property.unit = editfield.find(".caosdb-unit").val();
        return property;
    }

    /**
     * Get a list of js objects representing the properties of the entity.
     * Calls getPropertyFromElement on each property row found.
     *
     * TODO merge with caosdb.js->getProperties
     *
     * ent_element : {HTMLElement} entity in view mode
     */
    this.getProperties = function(ent_element) {
        const properties = [];
        if (ent_element) {
            const prop_elements = getPropertyElements(ent_element);
            for (var element of prop_elements) {

                var property = edit_mode.getPropertyFromElement(element);
                properties.push(property);
            }
        }
        return properties;

    }

    this.update_entity = async function(ent_element) {
        var xml = edit_mode.form_to_xml(ent_element);
        return await edit_mode.update(xml);
    }

    this.update = update;

    this.add_edit_mode_button = function(target, toggle_function) {
        var edit_mode_li = $('<li class="nav-item"><a class="nav-link caosdb-f-btn-toggle-edit-mode" role="button">Edit Mode</a></li>');
        $(target).append(edit_mode_li);
        $(".caosdb-f-btn-toggle-edit-mode").click(toggle_function);

        var min_width_warning = $('<div class="alert alert-warning caosdb-edit-min-width-warning d-lg-none d-none" role="alert"><strong>Warning</strong> The edit mode is optimized for screens wider than 992px. If you have trouble using it, please	try accessing it on a larger screen.</div>');
        $(".navbar").append(min_width_warning);

        return edit_mode_li[0];
    }

    this.toggle_edit_mode = async function() {
        edit_mode.toggle_edit_panel();
        if (edit_mode.is_edit_mode()) {
            await edit_mode.leave_edit_mode();
        } else {
            await edit_mode.enter_edit_mode();
        }
    }

    /**
     * To be overridden by an instance of `leave_edit_mode_template` during the
     * `enter_edit_mode()` execution.
     */
    this.leave_edit_mode = function() {}


    /**
     * Initializes the edit mode and loads the tool box.
     */
    this.enter_edit_mode = async function(editApp = undefined) {
        window.localStorage.edit_mode = "true";

        try {
            $(".caosdb-f-btn-toggle-edit-mode").text("Leave Edit Mode");

            edit_mode.init_tool_box();

            var nextEditApp = editApp;
            if (typeof nextEditApp == "undefined") {
                nextEditApp = edit_mode.init_edit_app();
            }

            edit_mode.init_new_buttons(nextEditApp);
            edit_mode.leave_edit_mode = function() {
                edit_mode.leave_edit_mode_template(nextEditApp);
            };

            return nextEditApp;
        } catch (err) {
            edit_mode.handle_error(err);
        }
    }

    /**
     * (Re-)load the toolbox, i.e. retrieve Properties and RecordTypes
     */
    this.load_edit_mode_toolbox = async function() {}

    /**
     * Retrieve all properties and record types and convert them into
     * a web page using a specialized XSLT named entity palette.
     *
     * @return {Promise<XMLDocument>} An array of entities in xml representation.
     */
    this.retrieve_data_model = async function() {
        const props_prom = connection.get("Entity/?query=FIND Property");
        const rts_prom = connection.get("Entity/?query=FIND RecordType");

        const [props, rts] = await Promise.all([props_prom, rts_prom]);

        // add all properties to rts
        for (var p of props.children[0].children) {
            rts.documentElement.appendChild(p.cloneNode());
        }
        return rts;
    }

    /**
     * @param {Promise<XMLElement[]>} xml - an array of XMLElements.
     * @return {Promise<HTMLDocument>} an HTMLElements.
     */
    this.transform_entity_palette = async function _tME(xml) {
        var xsl = transformation.retrieveXsltScript("entity_palette.xsl");
        let html = await asyncXslt(xml, xsl);
        return html;
    }



    /**
     * Remove all warnings and set the HTML of the toolbox panel to
     * the model given by model.
     */
    this.init_tool_box = async function() {

        // remove previously added model
        $(".caosdb-f-edit-mode-existing").remove()

        const editPanel = $(edit_mode.get_edit_panel());
        removeAllWaitingNotifications(editPanel[0]);
        editPanel.append(createWaitingNotification("Please wait."));
        const model = await edit_mode.transform_entity_palette(edit_mode.retrieve_data_model());

        removeAllWaitingNotifications(editPanel[0]);
        editPanel.children()[0].appendChild(model);

        if (edit_mode.app && edit_mode.app.entity && edit_mode.app.entity.parentElement && (edit_mode.app.state === "changed")) {
            // an entity is being editted
            $(".caosdb-f-edit-mode-existing").toggleClass("d-none", false);
            $(".caosdb-f-edit-mode-create-buttons").toggleClass("d-none", true);
        }
    }


    /**
     * Add an actions panel which will be used by the edit mode and hide the
     * original actions panel. The original actions-panel can be restored with
     * {@link edit_mode.reset_actions_panels}.
     *
     * @param {HTMLElement} entity
     */
    this.init_actions_panels = function(entity) {
        this.reset_actions_panels([entity]);
        $(entity).find(".caosdb-entity-actions-panel").each(function(index) {
            var clone = $(this).clone(true)[0];
            $(clone).removeClass("caosdb-entity-actions-panel").addClass("caosdb-f-edit-mode-entity-actions-panel").insertBefore(this);
            $(clone).children().remove();
            $(this).hide();
        });
    }

    /**
     * Remove the edit_mode action panel and restore the original action panel.
     *
     * @param {HTMLElements[]} array of entities in HTML representation.
     */
    this.reset_actions_panels = function(entities) {
        $(entities).find(".caosdb-f-edit-mode-entity-actions-panel").remove();
        $(entities).find(".caosdb-entity-actions-panel").show();
    }

    this._init_select = function(select) {
        $(select).selectpicker();
    }

    this.make_header_editable = function(entity) {
        var header = $(entity).find('.caosdb-entity-panel-heading');
        var roleElem = $(header).find('.caosdb-f-entity-role');
        roleElem.detach();
        var parentsElem = $(header).find('.caosdb-f-parent-list');
        parentsElem.detach();
        const parentsSection = $(`
      <div class="row">
        <div class="col-2 text-end">
          <label class="col-form-label">parents</label>
        </div>
        <div class="col caosdb-f-parents-form-element">
        </div>
      </div>
      `);
        parentsSection.find("div.caosdb-f-parents-form-element").append(parentsElem);

        header.attr("title", "Drop parents from the right panel here.");
        header.data("toggle", "tooltip");

        // create inputs
        var inputs = [
            roleElem,
            parentsSection,
            this.make_input("name", getEntityName(entity)),
            this.make_input("description", getEntityDescription(entity)),
        ];
        if (getEntityRole(roleElem[0]) == "Property") {
            const current_datatype = getEntityDatatype(entity);
            inputs.push(edit_mode.make_datatype_input(current_datatype));

            const current_unit = getEntityUnit(entity);
            inputs.push(edit_mode.make_unit_input(current_unit));

            // TODO currently no parents for properties. Why not?
            parentsSection.hide();
            header.attr("title", "");
        } else if (getEntityRole(roleElem[0]) == "File") {
            inputs.push(this.make_input("path", getEntityPath(entity)));
            inputs.push(this.make_input("checksum", getEntityChecksum(entity)));
            inputs.push(this.make_input("size", getEntitySize(entity)));
        }
        // remove other stuff
        header.children().remove();
        const form = $('<form class="form-horizontal"></form>').append(inputs);
        header.append(form);
        // selectpicker is based on bootstrap-select and must be initializes
        // AFTER it had been added to the dom tree.
        edit_mode._init_select(form.find(".selectpicker"));
        edit_mode.add_parent_dropzone(entity);

        edit_mode.make_datatype_input_logic(form[0]);
        edit_mode.add_parent_delete_buttons(header[0]);
    }

    this.isListDatatype = function(datatype) {
        return (typeof datatype !== 'undefined' && datatype.substring(0, 5) == "LIST<");
    }

    this.unListDatatype = function(datatype) {
        return datatype.substring(5, datatype.length - 1);
    }

    /**
     * Append listeners to all input elements depending on the datatype.
     *
     * The relevant input elements are those for the unit and the
     * reference_scope.
     *
     * Only when the atomic_datatype is either "DOUBLE" or "INTEGER" the unit
     * field should be visible and enabled. Only when the atomic_datatype is
     * "REFERENCE", the reference_scope field shoudl be visible and enabled.
     *
     * The listener `on_datatype_change` is added to the datatype field and
     * triggered right away for the first time.
     *
     * @param {HTMLElement} form - The form containing the input fields.
     */
    this.make_datatype_input_logic = function(form) {
        const datatype = form_elements.get_fields(form, "atomic_datatype");

        $(datatype).find("select").change(function() {
            const new_type = $(this).val();
            logger.debug(`datatype changed to ${new_type}.`);
            edit_mode.on_datatype_change(form, new_type);
        });

        // trigger for the first time
        edit_mode.on_datatype_change(form, $(datatype).find("select").val());
    }

    /**
     * The listener which is added by `make_datatype_input_logic`.
     *
     * @param {HTMLElement} form - The form containing the datatype and unit
     *     input elements.
     * @param {string} new_type - the new datatype which is used by this
     *     listener to determine which fields need to be enabled or disabled.
     */
    this.on_datatype_change = function(form, new_type) {
        logger.trace('enter on_datatype_change', form, new_type);

        if (new_type === "REFERENCE") {
            form_elements.enable_name(form, "reference_scope");
        } else {
            form_elements.disable_name(form, "reference_scope");
        }

        if (new_type === "DOUBLE" || new_type === "INTEGER") {
            form_elements.enable_name(form, "unit");
        } else {
            form_elements.disable_name(form, "unit");
        }
    }


    /**
     * Generate a text input element for the unit of an abstract property.
     *
     * @param {string} unit - the initial value of the input element.
     * @returns {HTMLElement} - a labeled form field.
     */
    this.make_unit_input = function(unit) {
        const unit_input = $(form_elements
            .make_text_input({
                name: "unit",
                label: "unit",
                value: unit,
            }));
        unit_input.toggleClass("form-control", true);
        unit_input.find(".col-sm-3").toggleClass("col-sm-2", true).toggleClass("col-sm-3", false);
        unit_input.find(".col-sm-9").toggleClass("col-sm-2", true).toggleClass("col-sm-9", false);
        return unit_input[0];
    }

    this._known_atomic_datatypes = [
        "TEXT",
        "DOUBLE",
        "INTEGER",
        "DATETIME",
        "BOOLEAN",
        "FILE",
        "REFERENCE",
    ];

    /**
     * Make three input elements which contain all necessary parts of a datatype.
     *
     * The three input elements are wrapped in a single DIV.form-control.
     *
     * @param {string} [datatype] - defaults to TEXT if undefined.
     * @returns {HTMLElement}
     */
    this.make_datatype_input = function(datatype) {
        var _datatype = datatype || "TEXT";

        // split/convert datatype string into more practical variables.
        const datatype_obj = edit_mode.get_datatype_from_string(_datatype);
        const atomic_datatype = datatype_obj["atomic_datatype"];
        const reference_scope = datatype_obj["reference_scope"];
        const is_list = datatype_obj["is_list"];


        // generate select input for the atomic_datatype
        const select = $('<select name="atomic_datatype" class="selectpicker form-control"></select>');
        for (const dt of edit_mode._known_atomic_datatypes) {
            select.append(`<option value="${dt}">${dt}</option>`);
        }
        select.val(atomic_datatype)

        const datatype_config = {
            name: "atomic_datatype",
            label: "datatype",
        };
        const datatype_selector = $(form_elements
                ._make_field_wrapper(datatype_config.name))
            .append(form_elements._make_input_label_str(datatype_config))
            .append($('<div class="col-sm-3"/>').append(select));

        // generate select input for the RecordTypes which are the reference's
        // scope.
        const reference_config = {
            name: "reference_scope",
            label: "reference to",
            value: reference_scope,
            query: "SELECT name FROM RECORDTYPE",
            make_desc: getEntityName,
            make_value: getEntityName,
        };
        const ref_selector = form_elements
            .make_reference_drop_down(reference_config);


        // generate the checkbox ([ ] list)
        const list_checkbox_config = {
            name: "is_list",
            label: "list",
            checked: is_list,
        }
        const list_checkbox = form_elements
            .make_checkbox_input(list_checkbox_config);

        // styling
        //$(list_checkbox).children().toggleClass("col-sm-3",false).toggleClass("col-sm-9", false).toggleClass("col-sm-1", true);
        $(list_checkbox).find(".caosdb-f-property-value").toggleClass("my-auto", true)
        const form_group = $('<div class="">').append([datatype_selector, ref_selector, list_checkbox]);
        form_group.find(".col-sm-3").toggleClass("text-end", true).toggleClass("col-sm-2", true).toggleClass("col-sm-3", false);
        form_group.find(".col-sm-9").toggleClass("col-sm-3", true).toggleClass("col-sm-9", false);


        return form_group[0];
    }

    this.make_input = function(label, value) {
        return $('<div class="row"> <div class="col-2 text-end"> <label class="col-form-label">' + label + ' </label> </div> <div class="col caosdb-f-parents-form-element"> <input type="text" class="form-control caosdb-f-entity-' + label + '" value="' + (typeof value == 'undefined' ? "" : value) + '"></input> </div> </div>')[0];
    }

    this.smooth_replace = function(from, to) {
        $(to).hide();
        $(from).fadeOut();
        $(from).after(to);
        $(from).detach();
        $(to).fadeIn();
    }

    /**
     * Create an input element for a single property's value.
     *
     * @param {object} property - entity property object.
     * @param {HTMLElements[]} [options] - an array of OPTION elements
     *     which represent possible candidates for reference values. The
     *     options will be appended to the SELECT input. This parameter is
     *     optional and only used for reference properties. This parameter
     *     might as well be a Promise for such an array.
     */
    this.createElementForProperty = function(property, options) {
        var result;
        if (property.datatype == "TEXT") {
            result = `<textarea>${property.value || ""}</textarea>`;
        } else if (property.datatype == "DATETIME") {
            var dateandtime = [""];
            if (property.value) {
                dateandtime = caosdb2InputDate(property.value);
            }
            let date = dateandtime[0];
            if (dateandtime.length > 1) {
                let time = dateandtime[1];
                // subseconds are stored in the subsec attribue of the input element
                result = "<span><input type='date' value='" + date + "'/>" +
                    "<input type='time' subsec='" + dateandtime[2] + "' value='" + time + "'/></span>";
            } else if (property.value && ((property.name || "").toLowerCase() == "year" || (date.match(/-/g) || []).length == 0)) {
                // Year
                result = "<input type='number' value='" + date + "'/>";
            } else if (property.value && (date.match(/-/g) || []).length == 1) {
                // Year-Month
                result = "<input type='text' value='" + date + "'/>";
            } else {
                result = "<input type='date' value='" + date + "'/>";
            }
        } else if (property.datatype == "DOUBLE") {
            result = "<input type='number' step='any' value='" + property.value + "'></input>";
        } else if (property.datatype == "INTEGER") {
            result = "<input type='number' value='" + property.value + "'></input>";
        } else if (property.datatype == "BOOLEAN" || property.datatype == "FILE" || property.reference) {
            result = $('<div/>');
            var css = {
                "min-height": "38px",
                "width": "80%",
                "display": "inline-block",
            };
            result.css(css);

            if (property.datatype == "BOOLEAN") {
                const select = $(`<select data-container="body" class="selectpicker form-control caosdb-list-${property.datatype}"><option value=""></option><option>FALSE</option><option>TRUE</option></select>`);
                if (property.value) {
                    select.val(property.value);
                }
                result.append(select);
            } else { // references and files
                result.append(`<select style="display: none"><option selected>${property.value}</option></select>`);
                result.append(createWaitingNotification(property.value));

                const select = $('<select data-container="body" data-virtual-scroll="100" data-window-padding="15" data-live-search="true" class="selectpicker form-control caosdb-list-' + property.datatype + '" data-resolved="false"><option value=""></option></select>');
                options.then((_options) => {
                    // The options might not include all entites (if there are
                    // many). Include the property value if it is missing.
                    if (property.value != "" && -1 == _options.map((e) => {
                            return e.value
                        }).indexOf(property.value)) {
                        _options = _options.concat([$(`<option value="${property.value}">ID: ${property.value}</option>`)[0]])
                    }
                    edit_mode.fill_reference_drop_down(select[0], _options, property.value);
                    result.empty();
                    result.append(select);
                    // value=NA means that the first element is the warning that not all entities are provided
                    if (_options.length > 0 && _options[0].getAttribute('value') == "NA") {
                        // The button to show the input field for manual ID insertion
                        var manualInsertButton = $('<button title="Insert Entity Id manually." class="btn btn-link caosdb-update-entity-button caosdb-f-list-item-button"><i class="bi-pencil"></i></button>');
                        $(manualInsertButton).click(function() {
                            // toggle input
                            // forward:
                            if ($(result).find('input.caosdb-f-manual-id-insert').is(':hidden')) {
                                // show ID input; hide dropdown
                                $(result).find('.dropdown').hide();
                                $(result).find('input.caosdb-f-manual-id-insert').show();
                                $('.caosdb-f-entity-save-button').hide()
                            } else {
                                // backward:
                                $(result).find('input.caosdb-f-manual-id-insert')[0].dispatchEvent(new KeyboardEvent('keyup', {
                                    'key': 'Enter'
                                }));
                            }
                        });
                        // Add input for manual ID insertion
                        var idinput = $("<input type='number' class='caosdb-f-manual-id-insert' value=''></input>")
                        idinput.hide()
                        // Add callback for pressing Enter: the insertion is completed
                        idinput.on('keyup', function(e) {
                            if (e.key === 'Enter' || e.keyCode === 13) {
                                // hide the input, show the dropdown again and append the value that
                                // was entered to the candidates and select that item
                                $(result).find('input.caosdb-f-manual-id-insert').hide();
                                var sel_id = $(result).find('input.caosdb-f-manual-id-insert')[0].value
                                if (sel_id != "") {
                                    if ($(result).find(`option[value="${sel_id}"]`).length == 0) {
                                        select.append(`<option value="${sel_id}">ID: ${sel_id}</option>`);
                                        // workaround for selectpicker bug (options are
                                        // doubled otherwise)
                                        select.selectpicker('destroy');
                                        select.selectpicker();
                                    }
                                    select.selectpicker('val', `${sel_id}`);
                                }
                                $(result).find('.dropdown').show();
                                $('.caosdb-f-entity-save-button').show()
                            }
                        });
                        result.append(idinput);
                        result.parent().append(manualInsertButton);
                    }
                    edit_mode._init_select(select);
                });
            }
        } else {
            throw ("Unsupported data type: `" + property.datatype + "`. Please issue a feature request.");
        }
        return $(result)[0];
    }

    /**
     * Return a span which contains plus button and trash button.
     *
     * This panel is used to insert and remove elements from list properties.
     *
     * @param {object} property - a property object.
     * @returns {HTMLElement} a SPAN element.
     */
    this.generate_list_item_control_panel = function(property, options) {
        // Add list delete buttons:
        var deleteButton = $('<button title="Delete this list element." class="btn btn-link caosdb-update-entity-button caosdb-f-list-item-button"><i class="bi-trash"></i></button>');
        $(deleteButton).click(function() {
            var ol = this.parentElement.parentElement.parentElement;

            $(this.parentElement.parentElement).remove();
            ol.dispatchEvent(edit_mode.list_value_input_removed);
        });


        // Add list insert buttons:
        var insertButton = $('<button title="Insert a new list element before this element." class="btn btn-link caosdb-update-entity-button caosdb-f-list-item-button"><i class="bi-plus"></i></button>');
        $(insertButton).click(function() {
            // TODO MERGE WITH OTHER PLACES WHERE THIS STRING APPEARS
            var proptemp = {
                list: false,
                reference: property.reference,
                value: "",
                unit: property.unit,
                name: undefined,
                id: undefined,
                datatype: property.listDatatype
            };
            $("<li/>")
                .append(edit_mode.createElementForProperty(proptemp, options))
                .append(edit_mode.generate_list_item_control_panel(property, options))
                .insertBefore(this.parentElement.parentElement);

            // dispatch on <ol> element
            this.parentElement.parentElement.parentElement.dispatchEvent(edit_mode.list_value_input_added);
        });

        return $('<span class="caosdb-v-edit-value-list-buttons"></span>')
            .append(deleteButton)
            .append(insertButton)[0];
    }


    /**
     *
     */
    this.create_unit_field = function(unit) {
        return $(`<input class='caosdb-unit' title='unit' style='width: 60px;' placeholder='unit' value='${unit||""}' type='text'></input>`)[0];
    }


    /**
     * Create input elements for a property's value.
     *
     * The created input element's type depends on the data type of the
     * property, e.g. 'textarea' for TEXT properties, 'select' for LISTs and so
     * on.
     *
     * @param {object} property - a property object.
     * @return {HTMLElement[]}
     */
    this.create_value_inputs = function(property) {
        logger.trace("enter create_value_inputs", arguments);

        var result = [];
        if (!property.list) {
            var options = property.reference ? edit_mode.retrieve_datatype_list(property.datatype) : undefined;
            result.push(edit_mode.createElementForProperty(property, options));
            if (["DOUBLE", "INTEGER"].includes(property.datatype)) {
                result.push(edit_mode.create_unit_field(property.unit));
            }
        } else {
            var options = property.reference ? edit_mode.retrieve_datatype_list(property.listDatatype) : undefined;
            var inputs = $('<ol style="list-style-type: none; padding: 0; margin:0;"/>');

            // unit_field
            if (["DOUBLE", "INTEGER"].includes(property.listDatatype)) {
                result.push(edit_mode.create_unit_field(property.unit));
            }

            for (var i = 0; i < property.value.length; i++) {
                // TODO MERGE WITH OTHER PLACES WHERE THIS STRING APPEARS
                var proptemp = {
                    list: false,
                    reference: property.reference,
                    value: property.value[i],
                    name: undefined,
                    id: undefined,
                    datatype: property.listDatatype
                };
                var new_element = $("<li/>")
                    .append(edit_mode.createElementForProperty(proptemp, options))
                    .append(edit_mode.generate_list_item_control_panel(property, options));

                inputs.append(new_element);
            }

            // PLUS-button for appending inputs to the list.
            var insertButton = $('<button title="Append a new field at the end." class="btn btn-link caosdb-update-entity-button caosdb-f-list-item-button"><i class="bi-plus"></i></button>');
            $(insertButton).click(function() {
                // TODO MERGE WITH OTHER PLACES WHERE THIS STRING APPEARS
                var proptemp = {
                    list: false,
                    reference: property.reference,
                    value: "",
                    name: undefined,
                    id: undefined,
                    datatype: property.listDatatype
                };

                var new_element = $("<li/>")
                    .append(edit_mode.createElementForProperty(proptemp, options))
                    .append(edit_mode.generate_list_item_control_panel(property, options));

                inputs.append(new_element);
                inputs[0].dispatchEvent(edit_mode.list_value_input_added);

            });

            result = result.concat([inputs[0],
                $("<span>Insert element at the end of the list: </span>")
                .append(insertButton)[0]
            ]);
        }
        return result;

    }


    /**
     * Return a property object for the property element where the LISTishness
     * is toggled.
     *
     * This function returns `undefined` if the property already has the
     * desired state.
     *
     * @param {HTMLElement} element - entity property in HTML representation.
     * @param {boolean} toList - switch the LISTishness of the property on/off.
     * @returns {object} property object when the switch was successful,
     *     or `undefined` if the property was already in the desired state.
     *
     * @throws {Error} if the LIST property has more than one element and is
     *     about to be converted to a non-LIST property (which is not allowed,
     *     because only the other elements would not fit into a non-LIST
     *     property).
     *
     * @see {@link _toggle_list_property} which is the most prominent callee.
     */
    this._toggle_list_property_object = function(element, toList) {
        const property = edit_mode.getPropertyFromElement(element);

        // property.list XAND toList
        if (property.list ? toList : !toList) {
            // already in desired state.
            return undefined;
        }
        property.list = toList;
        let datatype = toList ? `LIST<${property.datatype}>` : property.listDatatype || property.datatype;
        property.listDatatype = toList ? property.datatype : undefined;
        property.datatype = datatype;

        // transform value
        if (!toList && $.isArray(property.value) && property.value.length < 2) {
            property.value = property.value[0] || "";
        } else if (toList && !$.isArray(property.value)) {
            property.value = [property.value]
        } else {
            throw new Error(`Could not toggle to list=${toList} with value=${property.value}.`);
        }

        return property;
    }

    /**
     * Change the data type and the input fields of an entity property from the
     * LIST version to the atomic version of the same data type back and forth.
     *
     * @param {HTMLElement} element - entity property in HTML representation.
     * @param {boolean} toList - toggle list on or off.
     * @returns {HTMLElement|HTMLElement[]} the generated inputs (for testing
     *     purposes). Returns `undefined` when no changes.
     *
     * @see {@link _toggle_list_property_object} which does the actual
     * conversion work.
     */
    this._toggle_list_property = function(element, toList) {
        logger.trace("enter _toggle_list_property", arguments);

        let property = edit_mode._toggle_list_property_object(element, toList);

        if (!property) {
            // already in desired state.
            return;
        }

        let inputs = edit_mode
            .create_value_inputs(property);

        // remove old content if present
        var editfield = $(element).find(".caosdb-f-property-value");
        editfield.children().remove();
        editfield.append(inputs);
        // selectpicker is based on bootstrap-select and must be initializes
        // AFTER it had been added to the dom tree.
        edit_mode._init_select(editfield.find(".selectpicker"));

        edit_mode.change_property_data_type(element, property.datatype);

        return inputs;
    }


    this.change_property_data_type = function(element, datatype) {
        $(element).find(".caosdb-property-datatype").text(datatype);
        element.dispatchEvent(edit_mode.property_data_type_changed);
    }

    /**
     * Add a checkbox 'LIST [ ]' which switches between list and non-list datatype.
     *
     * @param {HTMLElement} element - entity property in HTML representation.
     * @param {boolean} list - whether the property is a list, initially.
     * @param {string} datatype - the initial data type of the property.
     */
    this.add_toggle_list_checkbox = function(element, list, datatype) {
        var editfield = $(element).find(".caosdb-f-property-value");
        var label = $('<label>List</label>');
        var checkbox = $('<input type="checkbox" class="form-check-input caosdb-f-entity-is-list"/>');
        $(element).find(".caosdb-property-edit").prepend(label.append(checkbox));

        checkbox.prop("checked", list);

        // bind _toggle_list_property to checkbox
        checkbox.change(() => {
            try {
                edit_mode._toggle_list_property(element, $(checkbox).prop("checked"));
            } catch (err) {
                globalError(err);
            }
        });

        // disable checkbox when list has more than 1 element
        let disabled = editfield.find("li").length > 1;
        checkbox.prop("disabled", disabled);
        const listDatatype = list ? datatype.substring(5, datatype.length - 1) : datatype;
        if (disabled) {
            checkbox.attr("title", `You must remove all elements of the list but one by clicking the trash buttons (to the left) before you can toggle the LIST<${listDatatype}>`);
        } else {
            checkbox.attr("title", `Toggle LIST<${listDatatype}> data type of this property.`);
        }

        // disable the checkbox when elements are added to the list and the
        // list has more than one element.
        editfield[0].addEventListener(
            edit_mode.list_value_input_added.type, (e) => {
                let disabled = editfield.find("li").length > 1;
                checkbox.prop("disabled", disabled);
                if (disabled) {
                    checkbox.attr("title", `You must remove all elements of the list but one by clicking the trash buttons (to the left) before you can toggle the LIST<${listDatatype}>`);
                }
            }, true);

        // enable the checkbox when elements removed to the list and the number
        // of elements is smaller than 2.
        editfield[0].addEventListener(
            edit_mode.list_value_input_removed.type, (e) => {
                let disabled = editfield.find("li").length > 1;
                checkbox.prop("disabled", disabled);
                if (!disabled) {
                    checkbox.attr("title", `Toggle LIST<${listDatatype}> data type of this property.`);
                }
            }, true);
    }

    /**
     * Hide the read-only property value and create an input element for the
     * property value.
     *
     * The new input element for the property value reflects the special needs
     * of each data type, e.g. drop-down menues for REFERENCE properties, an
     * ordered list with buttons for inserting and deleting elements for
     * LIST<?> properties, text areas for TEXT properties and so on.
     *
     * Also, a checkbox `LIST[ ]`, and a trash button are added. The former is
     * for toggling the data type of the property back and forth from
     * LIST<Person> to Person or similar. The trash button removes the property
     * from the entity.
     *
     * @param {HTMLElement} element - entity property in HTML representation.
     */
    this.make_property_editable = function(element) {
        caosdb_utils.assert_html_element(element, "param 'element'");

        var editfield = $(element).find(".caosdb-f-property-value")
            .removeClass("col-sm-6")
            .removeClass("col-md-8")
            .addClass("col-sm-4")
            .addClass("col-md-6")
            .addClass("caosdb-v-property-value-inputs")
            .after(`<div class="col-sm-2 col-sm-2 caosdb-v-property-other-inputs caosdb-property-edit" style="text-align: right;"/>`);
        var property = getPropertyFromElement(element);


        // create inputs
        if (property.reference && ("${BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING}" == "ENABLED")) {
            // Only add button to load reference options lazily.
            const editValueButton = $(`<button title="Edit this property" class="btn btn-link caosdb-f-edit-reference-property-button"><i class="bi-pencil"/></button>`);
            // Set data-lazy-loaded to false to indicate that it hasn't been initialized.
            $(element).addClass("caosdb-v-property-not-yet-lazy-loaded");
            $(editValueButton).click(() => {
                // Remove edit and trash button
                $(element).find(".caosdb-f-edit-reference-property-button").remove();
                $(element).find(".caosdb-f-property-trash-button").remove();
                // Create the input
                edit_mode._addPropertyEditInputs(element, property, editfield);
                element.dispatchEvent(edit_mode.property_edit_reference_value);
                $(element).removeClass("caosdb-v-property-not-yet-lazy-loaded");
            });
            // append button
            $(element).find(".caosdb-property-edit").append(editValueButton);
            // Trash button can exist without having to load all options
            edit_mode.add_property_trash_button($(element).find(".caosdb-property-edit")[0], element);
            // No preview of references in edit mode (might mess up the update)
            $(element).find(".caosdb-show-preview-button").remove();
        } else {
            edit_mode._addPropertyEditInputs(element, property, editfield);
        }
    }

    /**
     * Add input elements for value, units, list toggling, and
     * deletion to a property row.
     */
    this._addPropertyEditInputs = function(element, property, editfield) {
        var inputs = edit_mode.create_value_inputs(property);
        editfield.children().remove();
        editfield.append(inputs);
        // selectpicker is based on bootstrap-select and must be initializes
        // AFTER it had been added to the dom tree.
        edit_mode._init_select(editfield.find(".selectpicker"));

        // CHECKBOX `List [ ]`
        edit_mode.add_toggle_list_checkbox(element, property.list, property.datatype);

        // TRASH BUTTON
        edit_mode.add_property_trash_button($(element).find(".caosdb-property-edit")[0], element);
    }


    /**
     * Create a new Record using an existing RecordType.
     * The RecordType can be specified using recordtype_id.
     *
     * Currently name is ignored. TODO: check whether that behavior is intended.
     */
    this.create_new_record = async function(recordtype_id, name = undefined) {
        var rt = await retrieve(recordtype_id);
        var newrecord = createEntityXML("Record", undefined, undefined,
            getProperties(rt[0]),
            [{
                name: getEntityName(rt[0])
            }], true);
        var doc = str2xml("<Response/>");
        doc.firstElementChild.appendChild(newrecord.firstElementChild);
        // TODO I dunno whats wrong here: xml -> str -> xml ???
        var x = await transformation.transformEntities(str2xml(xml2str(doc)));
        $(x[0]).addClass("caosdb-v-entity-being-edited");
        return x[0];
    }

    /**
     * All draggable elements (that are those which have a css class
     * caosdb-f-edit-drag are added the correct drag listener and the attribute draggable.
     *
     * This is now done using bubbling so that the listeners are also dynamically updated.
     */
    this.init_dragable = function() {
        // Bubbling: Add the listener to the document and check whether the class is present.
        document.addEventListener("dragstart", function(event) {
            if (event.target.classList.contains("caosdb-f-edit-drag")) {
                edit_mode.dragstart(event);
            }
        });
    }

    /**
     * This function treats the deletion of entities, i.e. when the "delete"
     * button is clicked.
     */
    this.delete_action = async function(entity) {
        var app = edit_mode.app;

        // show waiting notification
        edit_mode.smooth_replace(entity, app.waiting);
        // send delete request
        var response = await transaction.deleteEntities([getEntityID(entity)]);
        // transform the delete response
        var entities = await transformation.transformEntities(response);
        // error of delete
        // continue with transformed entities
        edit_mode.smooth_replace(app.waiting, entities[0]);
        app.entity = entities[0];
        // treating error msg of delete
        if (edit_mode.has_errors(app.entity)) {
            hintMessages.hintMessages(app.entity);

            edit_mode.init_actions_panels(app.entity);
            // only add edit button, because a deletion would fail again
            edit_mode.add_start_edit_button(app.entity, () => {
                app.startEdit(app.entity)
            });
            if (getEntityRole(app.entity) == "RecordType") {
                edit_mode.add_new_record_button(app.entity, () => {
                    edit_mode.create_new_record(getEntityID(app.entity)).then((entity) => {
                        app.newEntity(entity);
                    }, edit_mode.handle_error);
                });
            }
        } else {
            // no error msg of delete
            // removes trash bin etc
            $(app.entity).find('.caosdb-f-edit-mode-entity-actions-panel').remove();
            // creates an ok button, which removes
            // deleted entity on click
            var closeButton = $(app.entity).find('.alert-info .close');
            closeButton.text("Ok");
            closeButton.click((e) => {
                $(app.entity).remove();
            });
        }
    }


    this.disable_new_buttons = function() {
        var new_buttons = $('.caosdb-f-edit-panel-new-button');
        new_buttons.attr("disabled", true);
    }

    this.enable_new_buttons = function() {
        var new_buttons = $('.caosdb-f-edit-panel-new-button');
        new_buttons.attr("disabled", false);
    }

    this.init_new_buttons = function(app) {
        var new_buttons = $('.caosdb-f-edit-panel-new-button');

        // handler for new property button
        // calls newEntity transition of state machine
        new_buttons.filter('.new-property').click(() => {
            edit_mode.create_new_entity("Property").then(entity => {
                app.newEntity(entity);
            }, edit_mode.handle_error);
        });

        // handler for new record type button
        // calls newEntity transition of state machine
        new_buttons.filter('.new-recordtype').click(() => {
            edit_mode.create_new_entity("RecordType").then(entity => {
                app.newEntity(entity);
            }, edit_mode.handle_error);
        });
    }


    /**
     * Initialize the edit mode and create a state machine.
     *
     * O -- init -> [INITIAL] -- newEntity --> [CHANGED] -.
     *              ^   ^   ^                     |        |
     *              |   |   *--- cancel ---------<         |
     *              |   |                         |        |
     *              |    *------ startEdit --> [CHANGED]   |
     *              |                             |        |
     *              |                 .- update -*         |
     *              |                 V                    |
     *               * showResults -- [WAIT] <--- insert -*
     *
     * [*ANY STATE*] -- finish --> [FINAL]
     *
     *
     * The initialization (`init`) is triggered when the edit mode is started.
     *
     * `newEntity` is triggered by the user clicking on a 'new Property' or
     * 'new RecordType' button
     *
     * `startEdit` is triggered by the user dropping an entity from the
     * Tool Box on an entity in the main panel.
     *
     * `update` and `insert` are triggered by the user clicking on the 'save' button and both have slightly different behavior. That effectively means, that the `CHANGED` state in both cases are not exactly the same and maybe should be called `CHANGE(update)` and `CHANGE(insert)` depending on the allowed transformations.
     *
     * `showResults` is triggered by the response of the server on the insert or update request.
     *
     * `cancel` is triggered by the user clicking 'cancel'.
     *
     * `finish` is triggered automatically when leaving the edit mode and does a little cleanup.
     */
    this.init_edit_app = function() {


        var app = new StateMachine({
            transitions: [{
                name: "init",
                from: 'none',
                to: "initial"
            }, {
                name: "newEntity",
                from: "initial",
                to: "changed"
            }, {
                name: "startEdit",
                from: "initial",
                to: "changed"
            }, {
                name: "insert",
                from: 'changed',
                to: 'wait'
            }, {
                name: "update",
                from: 'changed',
                to: 'wait'
            }, {
                name: "showResults",
                from: 'wait',
                to: 'initial'
            }, {
                name: "cancel",
                from: 'changed',
                to: 'initial'
            }, {
                name: "finish",
                from: '*',
                to: 'final'
            }],
        });


        var init_drag_n_drop = function() {
            const state = app.state;
            $('.caosdb-entity-panel').each(function(index) {
                let entity = this;
                if ($(entity).is("[data-version-successor]")) {
                    // not the latest version -> ignore
                    return;
                }

                // remove listeners from all entities
                edit_mode.unset_entity_dropable(entity, edit_mode.dragover, edit_mode.dragleave, edit_mode.parent_drop_listener, edit_mode.property_drop_listener);

                if (state === "initial") {
                    // add initial listener to all entities
                    edit_mode.set_entity_dropable(entity, edit_mode.dragover, edit_mode.dragleave, edit_mode.parent_drop_listener, edit_mode.property_drop_listener);
                }
            });
            if (state === "changed") {
                // add changed listener to editable entity
                let entity = app.entity;
                edit_mode.set_entity_dropable(entity, edit_mode.dragover, edit_mode.dragleave, edit_mode.parent_drop_listener, edit_mode.property_drop_listener);
            }
        }

        // Define the error handler for the state machine.
        app.errorHandler = function(fn) {
            try {
                fn();
            } catch (e) {
                edit_mode.handle_error(e);
            }
        };
        app.onAfterTransition = function(e) {
            init_drag_n_drop();
        }
        app.onEnterInitial = async function(e) {

            $(".caosdb-f-edit-mode-existing").toggleClass("d-none", true);
            $(".caosdb-f-edit-mode-create-buttons").toggleClass("d-none", false);
            app.old = undefined;
            app.errorHandler(() => {
                // make entities dropable and freezable
                edit_mode.enable_new_buttons();
                $('.caosdb-entity-panel').each(function(index) {
                    let entity = this;
                    if ($(entity).is("[data-version-successor]")) {
                        // not the latest version -> ignore
                        return;
                    }
                    if (typeof getEntityID(entity) == "undefined" || getEntityID(entity) == '') {
                        // no id -> insert
                        edit_mode.init_actions_panels(entity);
                        edit_mode.add_start_edit_button(entity, () => {
                            app.newEntity(entity)
                        });
                    } else {
                        // has id -> delete, edit, create RT
                        edit_mode.init_actions_panels(entity);
                        edit_mode.add_delete_button(entity);
                        edit_mode.add_start_edit_button(entity, () => {
                            app.startEdit(entity)
                        });
                        if (getEntityRole(entity) == "RecordType") {
                            edit_mode.add_new_record_button(entity, () => {
                                edit_mode.create_new_record(getEntityID(entity)).then((entity) => {
                                    app.newEntity(entity);
                                }, edit_mode.handle_error);
                            });
                        }
                    }
                });
            });
        };
        app.onLeaveInitial = function(e) {
            app.errorHandler(() => {
                $('.caosdb-entity-panel').each(function(index) {
                    // add the save button an so on
                    edit_mode.remove_start_edit_button(this);
                    edit_mode.remove_new_record_button(this);
                    edit_mode.remove_delete_button(this);
                });
            });
            edit_mode.disable_new_buttons();
        };
        app.onBeforeStartEdit = function(e, entity) {
            edit_mode.unhighlight();
            app.old = entity;
            app.entity = $(entity).clone(true)[0];
            // remove preview stuff
            $(app.entity).find(".caosdb-preview-container").remove();
            // add class for styling the entity that's being edited
            $(app.entity).addClass("caosdb-v-entity-being-edited");
            edit_mode.smooth_replace(app.old, app.entity);

            edit_mode.add_save_button(app.entity, () => app.update(app.entity));
            edit_mode.add_cancel_button(app.entity, () => app.cancel(app.entity));

            edit_mode.freeze_but(app.entity);
        };
        app.onBeforeCancel = function(e) {
            edit_mode.smooth_replace(app.entity, app.old);
            edit_mode.unfreeze();
        };
        app.onUpdate = function(e, entity) {
            edit_mode.update_entity(entity).then(response => {
                return transformation.transformEntities(response);
            }, edit_mode.handle_error).then(entities => {
                edit_mode.smooth_replace(app.entity, entities[0]);
                app.entity = entities[0];
                app.showResults();
            }, edit_mode.handle_error);
        };
        app.onEnterChanged = function(e) {

            // show existing entities in toolbox
            $(".caosdb-f-edit-mode-existing").toggleClass("d-none", false);
            $(".caosdb-f-edit-mode-create-buttons").toggleClass("d-none", true);

            edit_mode.unhighlight();
            hintMessages.removeMessages(app.old);
            edit_mode.make_header_editable(app.entity);

            // set listener for editable entity.
            hintMessages.hintMessages(app.entity);
            $(app.entity).find('.caosdb-annotation-section').remove();

            const prop_elements = getPropertyElements(app.entity);
            for (var element of prop_elements) {
                edit_mode.make_property_editable(element);
            }
            if (getEntityRole(app.entity) != "Property") {
                edit_mode.add_property_dropzone(app.entity);
            }
            app.entity.dispatchEvent(edit_mode.start_edit);
        }
        app.onEnterWait = function(e) {
            edit_mode.smooth_replace(app.entity, app.waiting);
        }
        app.onLeaveWait = function(e) {
            edit_mode.smooth_replace(app.waiting, app.entity);
        }
        app.onBeforeNewEntity = function(e, entity) {
            if (typeof entity == "undefined") {
                throw new TypeError("entity is undefined");
            }

            edit_mode.freeze_but(entity);

            app.entity = entity;


            // hide, append, fade-in - if we dont hide, it appears, disappears
            // and fades in.
            $(entity).hide();
            if ($('.caosdb-f-main-entities .caosdb-entity-panel').length > 0) {
                $('.caosdb-f-main-entities .caosdb-entity-panel').first().before(entity);
            } else {
                $('.caosdb-f-main-entities').append(entity);
            }
            $(entity).fadeIn();

            edit_mode.init_actions_panels(entity);
            edit_mode.add_save_button(entity, () => app.insert(entity));
            edit_mode.add_cancel_button(entity, () => app.cancel(entity));

            $(window).scrollTop(0);

            app.old = $('<div/>')[0];
        }
        app.onInsert = function(e, entity) {
            edit_mode.insert_entity(entity).then(response => {
                return transformation.transformEntities(response);
            }, edit_mode.handle_error).then(entities => {
                if (entities.length === 0) {
                    edit_mode.handle_error("The insertion did not return any entities");
                } else {
                    edit_mode.smooth_replace(app.entity, entities[0]);
                    app.entity = entities[0];
                }
                app.showResults();
            }, edit_mode.handle_error);
        }
        app.onFinish = function(e) {
            // this transition is triggerd on leaving the edit mode.
            edit_mode.unhighlight();
            if (app.old) {
                edit_mode.smooth_replace(app.entity, app.old);
            }
            edit_mode.unfreeze();
        }
        app.onShowResults = function(e) {
            // this transition is triggered by the response of the server on a
            // previous insert/update request.

            if (!edit_mode.has_errors(app.entity)) {
                app.old = false;
            }
            hintMessages.hintMessages(app.entity);
            edit_mode.unfreeze();
            app.entity.dispatchEvent(edit_mode.end_edit);
            resolve_references.init();
            preview.init();
            edit_mode.init_tool_box();
        }
        app.waiting = createWaitingNotification("Please wait.");
        $(app.waiting).hide();
        app.init();
        // TODO: We need some refactoring: there ist the variable editApp which is
        // set with this return value. I think app is a bad name as it contains
        // the state machine. Alos, the state machine should be either passed
        // around or be global (edit_mode.app).
        edit_mode.app = app
        return app;
    }

    this.has_errors = function(entity) {
        return $(entity).find(".alert.alert-danger").length > 0;
    }

    this.freeze_but = function(element) {
        $('.caosdb-f-main-entities').children().each(function(index) {
            if (element != this) {
                edit_mode.freeze_entity(this);
            }
        });
    }

    this.add_property_dropzone = function(entity) {
        $(entity).find("ul.caosdb-properties")
            .append('<li class="caosdb-v-edit-mode-dropzone caosdb-f-edit-mode-property-dropzone caosdb-v-edit-mode-property-dropzone">Drag and drop Properties and RecordTypes from the Edit Mode Toolbox here.</li>');
    }

    this.add_parent_dropzone = function(entity) {
        $(entity).find(".caosdb-f-parent-list")
            .addClass("caosdb-v-edit-mode-parent-dropzone")
            .addClass("caosdb-v-edit-mode-dropzone")
            .prepend('<div>Drag and drop RecordTypes from the Edit Mode Toolbox here.</div>');
    }

    this.unfreeze = function() {
        $('.caosdb-f-main-entities').children().each(function(index) {
            edit_mode.unfreeze_entity(this);
        });
    }


    this._create_reference_options = function(entities) {
        var results = [];

        for (var i = 0; i < entities.length; i++) {
            var eli = entities[i];
            var prlist = getProperties(eli);
            var prdict = [];
            // The name is not included in the property list.
            var entname = getEntityName(eli);
            if (entname.length > 0) {
                prdict.push("Name: " + entname);
            }
            for (var j = 0; j < prlist.length; j++) {
                prdict.push(prlist[j].name + ": " + prlist[j].value);
            }
            prdict.push("LinkAhead ID: " + getEntityID(eli));
            results.push($(`<option value="${getEntityID(eli)}"/>`).text(prdict.join(", "))[0]);
        }

        return results;
    }

    /**
     * Fill the reference drop down menu of an entity property with options.
     *
     * The options are the entities which are within the scope of the reference
     * property.
     *
     * @param {HTMLElement} drop_down - A SELECT element which will be filled.
     * @param {HTMLElement[]} options - array of option elements, ready for
     *     being appended to a SELECT element. This parameter might as well be
     *     a Promise for such an array.
     * @param {String} [current_value] - The value which is to be selected.
     */
    this.fill_reference_drop_down = function(drop_down, options, current_value) {
        drop_down = $(drop_down);

        drop_down.append($(options).clone());
        if (current_value) {
            drop_down.val(current_value);
        }
    }

    /**
     * Retrieve all candidates for reference values depending on the datatype
     * of a property and populate the value's drop down menu with options.
     *
     * E.g retrieve all "Person" records for a property with data type `Person`
     * or `LIST<Person>`.
     *
     * @async
     * @param {string} datatype - the data type of the reference property.
     * @returns {HTMLElement[]} array of OPTION element, representing an entity
     *     which can be referenced by the property.
     */
    this.retrieve_datatype_list = async function(datatype) {
        const find_entity = ["FILE", "REFERENCE"].includes(datatype) ? "" : `"${datatype}"`;
        const max_options = parseInt("${BUILD_MAX_EDIT_MODE_DROPDOWN_OPTIONS}"); //for each query; there might be more candidates in total

        const query_suffix = max_options != -1 ? `&P=0L${max_options}` : "";
        if (max_options != -1) {
            var n_entities = datatype !== "FILE" ? await edit_mode.query(`COUNT Record ${find_entity}`, true) : 0;
            var n_files = await edit_mode.query(`COUNT File ${find_entity}`, true);
        }
        const entities = datatype !== "FILE" ? edit_mode.query(`FIND Record ${find_entity}${query_suffix}`, true) : [];
        const files = edit_mode.query(`FIND File ${find_entity}${query_suffix}`, true);

        await Promise.all([entities, files])

        var options = edit_mode
            ._create_reference_options(await entities)
            .concat(edit_mode
                ._create_reference_options(await files));

        if (max_options != -1 && (n_entities > max_options || n_files > max_options)) {
            // add notification that not all entities are shown
            options = [$(`<option disabled=true value="NA"/>`).text(
                `More than ${max_options} possible options; showing only a subset. You may need to enter the id manually.`
            )[0]].concat(options);
        }
        return options;
    }

    this.highlight = function(entity) {
        $(entity).find(".caosdb-v-edit-mode-dropzone")
            .addClass("caosdb-v-edit-mode-highlight");
    }

    this.unhighlight = function() {
        $('.caosdb-v-edit-mode-highlight')
            .removeClass("caosdb-v-edit-mode-highlight");
    }

    this.handle_error = function(err) {
        globalError(err);
    }

    this.get_edit_panel = function() {
        return $('.caosdb-f-edit-panel-body')[0];
    }

    this.toggle_edit_panel = function() {
        //$(".caosdb-f-main").toggleClass("container-fluid").toggleClass("container");
        $(".caosdb-f-main-entities").toggleClass("caosdb-f-main-entities-edit");
        $(".caosdb-f-edit").toggle(); //.toggleClass("col-xs-4");
        this._toggle_min_width_warning();
    }

    this._toggle_min_width_warning = function() {
        // Somewhat counter-intuitive, but when we're not in edit mode
        // and toggle the panel, we're entering and the warning should
        // be shown on small screens and vice-versa.
        $(".caosdb-edit-min-width-warning").toggleClass("d-none", edit_mode.is_edit_mode());
        $(".caosdb-edit-min-width-warning").toggleClass("d-block", !(edit_mode.is_edit_mode()));
    }

    this.leave_edit_mode_template = function(app) {
        app.finish();
        $(".caosdb-f-btn-toggle-edit-mode").text("Edit Mode");
        edit_mode.reset_actions_panels($(".caosdb-f-main").toArray());
        window.localStorage.removeItem("edit_mode");
    }

    this.is_edit_mode = function() {
        return window.localStorage.edit_mode !== undefined;
    }

    this.add_cancel_button = function(ent, callback) {
        var cancel_btn = $('<button class="btn btn-link caosdb-update-entity-button caosdb-f-entity-cancel-button">Cancel</button>');

        $(ent).find(".caosdb-f-edit-mode-entity-actions-panel").append(cancel_btn);

        $(cancel_btn).click(callback);
    }

    this.create_new_entity = async function(role) {
        var empty_entity = str2xml('<Response><' + role + '/></Response>');
        var ent_element = await transformation.transformEntities(empty_entity);
        $(ent_element).addClass("caosdb-v-entity-being-edited");
        return ent_element[0];
    }

    this.remove_cancel_button = function(entity) {
        $(entity).find('.caosdb-f-entity-cancel-button').remove()
    }

    /**
     * entity : HTMLElement
     */
    this.freeze_entity = function(entity) {
        $(entity).css("pointer-events", "none").css("filter", "blur(10px)");
    }

    /**
     * entity : HTMLElement
     */
    this.unfreeze_entity = function(entity) {
        $(entity).css("pointer-events", "").css("filter", "");
    }

    /**
     * this function is called in entity_palette.xsl
     */
    this.filter = function(ent_type) {
        var text = $("#caosdb-f-filter-" + ent_type).val();
        if (ent_type == "properties") {
            var short_type = "p";
        } else if (ent_type == "recordtypes") {
            var short_type = "rt";
        } else {
            globalError("unknown type");
        }

        var keywords = text.toLowerCase().split(" ");
        var elements = document.getElementsByClassName("caosdb-f-edit-drag");
        for (var el of elements) {
            if (el.id.includes("caosdb-f-edit-" + short_type)) {
                var name = el.innerText.toLowerCase();
                var found_kw = false;
                for (var kw of keywords) {
                    if (name.includes(kw)) {
                        found_kw = true;
                        break;
                    }
                }
                if (found_kw) {
                    $(el).show()
                } else {
                    $(el).hide()
                }
            }
        }
    }

    /**
     * Programatically start editing an entity.
     *
     * Throws an error if the action cannot be performed (this or another
     * entity is already being edited or the edit_mode is not active).
     *
     * @param {HTMLElement} entity - the entity which is to be changed.
     * @return {HTMLElement} the entity form
     */
    this.edit = function(entity) {
        if (!this.is_edit_mode()) {
            throw Error("edit_mode is not active");
        }
        if (!edit_mode.app.can("startEdit")) {
            throw Error("edit_mode.app does not allow to start the edit");
        }
        edit_mode.app.startEdit(entity);
        return edit_mode.app.entity;
    }

    /**
     * List of all permissions which indicate that the edit button should be
     * visible.
     */
    const UPDATE_PERMISSIONS = [
        "UPDATE:DESCRIPTION",
        "UPDATE:VALUE",
        "UPDATE:ROLE",
        "UPDATE:PARENT:REMOVE",
        "UPDATE:PARENT:ADD",
        "UPDATE:PROPERTY:REMOVE",
        "UPDATE:PROPERTY:ADD",
        "UPDATE:NAME",
        "UPDATE:DATA_TYPE",
        "UPDATE:FILE:REMOVE",
        "UPDATE:FILE:ADD",
        "UPDATE:FILE:MOVE",
        "UPDATE:QUERY_TEMPLATE_DEFINITION",
    ];

    /**
     * Add a button labeled "Edit" to the entity which opens the edit form for
     * this entity.
     *
     * The button is added only when any of the `UPDATE:...` permissions are
     * there.
     *
     * @param {HTMLElement} entity - the entity which gets the button.
     * @parma {function} callback - the function which initializes and opens
     *     the edit form.
     */
    this.add_start_edit_button = function(entity, callback) {
        var has_any_update_permission = false;
        for (let permission of UPDATE_PERMISSIONS) {
            if (hasEntityPermission(entity, permission)) {
                has_any_update_permission = true;
                break;
            }
        }
        if (!has_any_update_permission) {
            return;
        }
        edit_mode.remove_start_edit_button(entity);
        var button = $('<button title="Edit this ' + getEntityRole(entity) + '." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-start-edit-button">Edit</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(callback);
    }

    this.remove_start_edit_button = function(entity) {
        $(entity).find(".caosdb-f-entity-start-edit-button").remove();
    }


    this.add_new_record_button = function(entity, callback) {
        if (!hasEntityPermission(entity, "USE:AS_PARENT")) {
            return;
        }
        edit_mode.remove_new_record_button(entity);
        var button = $('<button title="Create a new Record from this RecordType." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-new-record-button">+Record</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(callback);
    }

    this.remove_new_record_button = function(entity) {
        $(entity).find(".caosdb-f-entity-new-record-button").remove();
    }

    this.add_delete_button = function(entity, callback) {
        if (!hasEntityPermission(entity, "DELETE")) {
            return;
        }
        edit_mode.remove_delete_button(entity);
        var button = $('<button title="Delete this ' + getEntityRole(entity) + '." class="btn btn-link caosdb-update-entity-button caosdb-f-entity-delete-button">Delete</button>');

        $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").append(button);

        $(button).click(() => {
            // hide other elements
            const _alert = form_elements.make_alert({
                title: "Warning",
                message: "You are going to delete this entity permanently. This cannot be undone.",
                proceed_callback: () => {
                    edit_mode.delete_action(entity)
                },
                cancel_callback: () => {
                    $(_alert).remove();
                    $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").show();
                },
                proceed_text: "Yes, delete!",
                remember_my_decision_id: "delete_entity",
            });
            $(_alert).addClass("text-end");

            $(entity).find(".caosdb-f-edit-mode-entity-actions-panel").after(_alert).hide();

        });
    }

    this.remove_delete_button = function(entity) {
        $(entity).find(".caosdb-f-entity-delete-button").remove();
    }

    const query_cache = {};

    this.query = async (str, use_cache) => {
        if (use_cache && query_cache[str]) {
            return query_cache[str];
        }
        var result;
        if (str.toUpperCase().startsWith('COUNT')) {
            const res = await connection.get(`Entity/?query=${str}`);
            result = $(res).find('Query').attr('results')
        } else {
            result = query(str);
        }
        if (use_cache) {
            query_cache[str] = result;
        }
        return result;
    }

}()
/**
 * Add the extensions to the webui.
 */
$(document).ready(function() {
    edit_mode.init();
});
