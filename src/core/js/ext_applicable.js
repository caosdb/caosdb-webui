/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * Useful helpers for is_applicable functionality.
 */
var _helpers = function(getEntityPath) {

  /**
   * Check if an entity has a path attribute and one of a set of extensions.
   *
   * Note: the array of extensions must contain only lower-case strings.
   *
   * @param {HTMLElement} entity
   * @param {string[]} extensions - an array of file extesions, e.g. `jpg`.
   * @return {boolean} true iff the entity has a path with one of the
   *     extensionss.
   */
  const path_has_file_extension = function(entity, extensions) {
      const path = getEntityPath(entity);
      if (path) {
          for (var ext of extensions) {
              if (path.toLowerCase().endsWith(ext)) {
                  return true;
              }
          }
      }
      return false;
  }

  return {
    path_has_file_extension: path_has_file_extension
  };
}(getEntityPath)

var ext_applicable = function($, logger, is_in_view_port, load_config, getEntityPath, connection, helpers, createWaitingNotification) {

  const version = "0.1";

  /**
   * Run through all creators and call the "create" function of the first
   * creator which returns true from is_applicable(entity).
   *
   * @param {HTMLElement} entity
   * @param {Creator[]} creators - list of creators
   * @return {HTMLElement|String} content - the result of the first matching
   *     creator.
   */
  const root_creator = async function (entity, creators) {
    for (let c of creators) {
      var is_applicable = false;
      try {
        is_applicable = await c.is_applicable(entity);
      } catch (err) {
        logger.error(`error in is_applicable function of creator`, c, err);
        continue;
      }
      if (is_applicable) {
        const content = await c.create(entity);
        return content;
      }
    }
    return undefined;
  }

  var _set_content = function (container, content) {
    const _container = $(container);
    _container.empty();

    if (content) {
        _container.append(content);
    }
  }

  /**
   * Replace the old content (if any) of the container by the new the content
   * (created by the creators).
   *
   * Dispatch contentReadyEvent or noContentEvent if given.
   *
   * @param {HTMLElement} container
   * @param {HTMLElement|string} [content]
   * @param {Event} [contentReadyEvent]
   * @param {Event} [noContentEvent]
   */
  const set_content = async function (container, content, contentReadyEvent, noContentEvent) {
    try {
      const wait = createWaitingNotification("Please wait...");
      _set_content(container, wait);
      const result = await content;
      _set_content(container, result);
      if (result && contentReadyEvent) {
        container.dispatchEvent(contentReadyEvent);
      } else if (!result && noContentEvent) {
        container.dispatchEvent(noContentEvent);
      }

    } catch (err) {
      logger.error(err);
      const err_msg = "An error occured while loading this content.";
      _set_content(container, err_msg);
    }
  }


  /**
   * @param {HTMLElement} entity
   * @param {get_container_cb} get_container_cb
   * @param {Creator[]} creators
   * @param {Event} [contentReadyEvent]
   * @param {Event} [noContentEvent]
   */
  const _root_handler = function (entity, get_container_cb, creators, contentReadyEvent, noContentEvent) {
    const _container = get_container_cb(entity);
    if (_container) {
      const content = root_creator(entity, creators);
      set_content(_container, content, contentReadyEvent, noContentEvent);
    }
  }


  /**
   * The root handler trigger call the root_handler callback on every
   * .caosdb-entity-panel and every .caosdb-entity-preview in the viewport.
   *
   * @param {function} root_handler - the root handler callback.
   */
  var root_handler_trigger = function(root_handler) {
    var entities = $(".caosdb-entity-panel,.caosdb-entity-preview");
    for (let entity of entities) {

      // TODO viewport + 1000 px for earlier loading
      if (is_in_view_port(entity)) {
        root_handler(entity);
      }
    }
  }


  /**
   * Initialize the scroll watcher which listens on the scroll event of the
   * window and triggers the root handler with a delay after the last
   * scroll event.
   *
   * @param {integer} delay - timeout in milliseconds after the last scroll
   *     event. After this timeout the trigger is called.
   * @param {function} trigger - the root handler callback which is called.
   */
  var init_watcher = function(delay, trigger) {
    var scroll_timeout = undefined;
    $(window).scroll(() => {
        if (scroll_timeout) {
            clearTimeout(scroll_timeout);
        }
        scroll_timeout = setTimeout(trigger, delay);
    });

    var preview_timeout = undefined;

    // init watcher on newly loaded entity previews.
    window.addEventListener(
      preview.previewReadyEvent.type,
      () => {
        if (preview_timeout) {
          clearTimeout(preview_timeout);
        }
        preview_timeout = setTimeout(trigger, delay);
      },
      true);

    // trigger for the first time
    trigger();
  };

  /**
   * @callback get_container_cb
   * @param {HTMLElement} entity - the entity for which this callback shall
   *     return the is_applicable container.
   * @returns {HTMLElement} child of entity which is a container for the
   *     is_applicable_app
   */

  /**
   * @type {IsApplicableApp}
   * @property {IsApplicableConfig} config
   * @property {Creator[]} creators
   * @property {function} root_handler
   */

  /**
   * @type {IsApplicableConfig}
   * @property {string|HTMLElement|function} fallback - Fallback content or
   *     callback if none of the creators are applicable.
   * @property {string} version - the version of the configuration which must
   *     match this module's version.
   * @property {CreatorConfig[]} creators - an array of creators.
   */

  /**
   * make a fallback creator
   *
   * @return {Creator}
   */
  const _make_fallback_creator = function(fallback) {
    if (fallback) {
      return {
        id: "_generated_fallback_creator",
        is_applicable: (entity) => true, // always applicable
        create: typeof fallback === "function" ? fallback : (entity) => fallback,
      };
    }
    return undefined;
  }

  const _make_creator = function (c) {
    return {
      id: c.id,
      is_applicable: typeof c.is_applicable === "function" ?
                         c.is_applicable : eval(c.is_applicable),
      create: typeof c.create === "function" ? c.create : eval(c.create)
    };
  }


  /**
   * @param {IsApplicableConfig}
   * @return {Creator[]} creators
   */
  const _make_creators = function(config) {
    const creators = [];
    for (let c of config.creators) {
      creators.push(_make_creator(c));
    }
    const fallback_creator = _make_fallback_creator(config.fallback);
    if (fallback_creator) {
      creators.push(fallback_creator);
    }
    return creators;
  }

  /**
   * @type {CreatorConfig}
   * @property {string} [id] - a unique id for the creator. optional, for
   *     debuggin purposes.
   * @property {function|string} is_applicable - If this is a string this has
   *     to be valid javascript! An asynchronous function which accepts one
   *     parameter, an entity in html representation, and which returns true
   *     iff this creator is applicable for the given entity.
   * @property {string} create - This has to be valid javascript! An
   *     asynchronous function which accepts one parameter, an entity in html
   *     representation. It returns a HTMLElement or text node which will be
   *     shown in the bottom line container iff the creator is applicable.
   */

  /**
   * @param {string} app_name - the name of this app.
   * @param {get_container_cb} get_container_cb
   * @return {get_container_cb} wrapped function which also checks if the
   *     container has already been filled with the created content.
   */
  const _make_get_container_wrapper = function (get_container_cb, app_name) {
    const _wrapper = function (entity) {
      const container = get_container_cb(entity);
      const app_done = $(container).data(app_name);
      if(!app_done) {
        // mark container as used
        $(container).data(app_name, "done");
        return container
      }
      // don't return the container if already used by this app
      return undefined;
    }
    return _wrapper;
  }

  /**
   * @param {string} that_version - a version string.
   * @throws {Error} if that_version doesn't match this modules version.
   */
  const _check_version = function(that_version) {
    if(that_version != version) {
      throw new Error(`Wrong version in config. Was '${that_version}', should be '${version}'.`);
    }
  }

  /**
   * @param {string} app_name
   * @param {IsApplicableConfig} config
   * @param {get_container_cb} get_container
   * @param {Event} [contentReadyEvent]
   * @param {Event} [noContentEvent]
   * returns {IsApplicableApp}
   */
  const create_is_applicable_app = function(app_name, config, get_container, contentReadyEvent, noContentEvent) {
    logger.debug("create_is_applicable_app", config, get_container);
    _check_version(config["version"])
    const creators = _make_creators(config)
    const get_container_wrapper = _make_get_container_wrapper(get_container, app_name);

    const root_handler = (entity) => _root_handler(entity, get_container_wrapper, creators, contentReadyEvent, noContentEvent);

    if (config.init_watcher) {
      init_watcher(config.delay || 500, () => {root_handler_trigger(root_handler);});
    }
    return {
      config: config,
      creators: creators,
      root_handler: root_handler,
    }
  }

  return {
    create_is_applicable_app: create_is_applicable_app,
    root_handler_trigger: root_handler_trigger,
    init_watcher: init_watcher,
    helpers: helpers,
    version: version,
  };

}($, log.getLogger("ext_applicable"), resolve_references.is_in_viewport_vertically, load_config, getEntityPath, connection, _helpers, createWaitingNotification);
