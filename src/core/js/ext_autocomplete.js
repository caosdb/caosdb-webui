/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH, Henrik tom Wörden
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/*
 * This module uses and extends bootstrap-autocomplete to provide
 * autocompletion to the query field.
 * Currently, completion exists for names that are stored in LinkAhead
 */
'use strict';

var ext_autocomplete = new function () {
    this.CQL_WORDS = [
        "FIND",
        "FILE",
        "ENTITY",
        "SELECT",
        "COUNT",
        "RECORD",
        "PROPERTY",
        "RECORDTYPE",
        "REFERENCES",
        "REFERENCED BY",
        "WHICH",
        "WITH",
        "CREATED BY",
        "CREATED BY ME",
        "CREATED AT",
        "CREATED ON",
        "CREATED IN",
        "CREATED BEFORE",
        "CREATED UNTIL",
        "CREATED AFTER",
        "CREATED SINCE",
        "SOMEONE",
        "STORED AT",
        "HAS A PROPERTY",
        "HAS BEEN",
        "ANY VERSION OF",
        "FROM",
        "INSERTED AT",
        "INSERTED ON",
        "INSERTED IN",
        "INSERTED BY",
        "INSERTED BY ME",
        "INSERTED BEFORE",
        "INSERTED UNTIL",
        "INSERTED AFTER",
        "INSERTED SINCE",
        "UPDATED AT",
        "UPDATED ON",
        "UPDATED IN",
        "UPDATED BY",
        "UPDATED BY ME",
        "UPDATED BEFORE",
        "UPDATED UNTIL",
        "UPDATED AFTER",
        "UPDATED SINCE",
        "SINCE",
        "BEFORE",
        "ON",
        "IN",
        "AFTER",
        "UNTIL",
        "AT",
        "BY",
        "BY ME",
    ];
    this.version = "0.1";


    /**
     * configure logging
     */
    var logger = log.getLogger("ext_autocomplete");

    /**
     * Initialize this module.
     *
     * Fetch candidates and prepare bootstrap-autocomplete.
     */
    this.init = async function () {
        this.candidates = await this.retrieve_names();
        this.switch_on_completion();
    };


    /**
     * creates a list of names from the server resource Entity/names
     */
    this.retrieve_names = async function () {
        var response = $(await connection.get(transaction.generateEntitiesUri(
            ["names"]))).find("Property[name],RecordType[name],Record[name]")

        response = response.toArray().map(x => $(x).attr("name"));
        response = response.concat(ext_autocomplete.CQL_WORDS);

        return response
    };

    /**
     * case insensitive filter that returns elements that start with qry
     */
    this.starts_with_filter = function (x, qry) {
        return x.toLowerCase().startsWith(qry.toLowerCase())
    };

    /**
     * Overwrites the default search function of bootstrap-autocomplete
     * uses candidates and filters the list using filter
     */
    this.search = async function (qry, callback, origJQElement) {
        callback(ext_autocomplete.candidates.filter(
            x => ext_autocomplete.starts_with_filter(x, qry)));
    };

    /**
     * Overwrites the default typed function of bootstrap-autocomplete
     * This assures that only the word before the cursor is used for completion
     */
    this.typed = function (newValue, origJQElement) {
        var cursorpos = origJQElement[0].selectionEnd;
        var beginning_of_word = newValue.slice(0, cursorpos).lastIndexOf(" ");
        var word = newValue.slice(beginning_of_word + 1, cursorpos);
        return word;
    };

    /**
     * Overwrites the default searchPost function of bootstrap-autocomplete
     * This assures that only the word before the cursor is replaced.
     * It is achieved by returning not a simple list, but an object where each
     * element has "text" and "html" fields, where "html" is what is visible
     * in the dropdown and "text" will be inserted.
     */
    this.searchPost = function (resultsFromServer, origJQElement) {
        var cursorpos = origJQElement[0].selectionEnd;
        var newValue = origJQElement[0].value
        var beginning_of_word = newValue.slice(0, cursorpos).lastIndexOf(
            " ");
        var start = newValue.slice(0, beginning_of_word + 1);
        var end = origJQElement[0].value.slice(cursorpos);
        var result = resultsFromServer.map(x => {
            var x_quoted = x;
            if (ext_autocomplete.CQL_WORDS.indexOf(x) == -1 && x.indexOf(" ") > -1) {
              if(x.indexOf("\"") > -1) {
                x_quoted = `'${x}'`;
              } else {
                x_quoted = `"${x}"`;
              }
            }
            return {
                text: start + x_quoted + end,
                html: x
            }
        });
        return result;
    };

    /**
     * enables autocompletion in the query field
     */
    this.switch_on_completion = function () {
        var field = $("#caosdb-query-textarea");

        // submit on "enter" when the drop-down menu is not visible.
        field.on("keydown", (e) => {
            if(e.originalEvent.keyCode == 13) { // Enter
                if($(e.target).siblings(".bootstrap-autocomplete.show").length == 0) {
                    $(".caosdb-search-btn").click();
                } else {
                    // don't submit - the user is just selecting something
                    e.originalEvent.preventDefault();
                }
            }
        });

        field.attr("autocomplete", "off");
        field.toggleClass("basicAutoComplete", true);
        field.autoComplete({
            events: {
                search: this.search,
                typed: this.typed,
                searchPost: this.searchPost,
            },
            noResultsText: 'No autocompletion suggestions',
            bootstrapVersion: "4",
            /* this should be updated to 5 when
                     bootstrap-autocomplete releases official support for bootstrap 5.
                     Until then: let's hope the best.*/

        });

        return field;
    };
};

// this will be replaced by require.js in the future.
$(document).ready(function () {
    if ("${BUILD_MODULE_EXT_AUTOCOMPLETE}" == "ENABLED") {
        caosdb_modules.register(ext_autocomplete);
    }
});
