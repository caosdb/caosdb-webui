/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * @typedef {BottomLineConfig}
 * @property {string|HTMLElement} fallback - Fallback content if none of
 *     the creators are applicable.
 * @property {string} version - the version of the configuration which must
 *     match this module's version.
 * @property {CreatorConfig[]} creators - an array of creators.
 */

/**
 * @typedef {CreatorConfig}
 * @property {string} [id] - a unique id for the creator. optional, for
 *     debuggin purposes.
 * @property {function|string} is_applicable - If this is a string this has
 *     to be valid javascript! An asynchronous function which accepts one
 *     parameter, an entity in html representation, and which returns true
 *     iff this creator is applicable for the given entity.
 * @property {string} create - This has to be valid javascript! An
 *     asynchronous function which accepts one parameter, an entity in html
 *     representation. It returns a HTMLElement or text node which will be
 *     shown in the bottom line container iff the creator is applicable.
 */

/**
 * Add a special section to each entity one the current page where a thumbnail,
 * a video preview or any other kind of summary for the entity is shown.
 *
 * Apart from some defaults, the content for the bottom line has to be
 * configured in the file `conf/ext/json/ext_bottom_line.json` which must
 * contain a {@link BottomLineConfig}. An example is located at
 * `conf/core/json/ext_bottom_line.json`.
 *
 * @module ext_bottom_line
 * @version 0.1
 *
 * @requires jQuery (library)
 * @requires log (singleton from loglevel library)
 * @requires resolve_references.is_in_viewport_vertically (funtion from ext_references.js)
 * @requires load_config (function from caosdb.js)
 * @requires getEntityPath (function from caosdb.js)
 * @requires connection (module from webcaosdb.js)
 * @requires UTIF (from utif.js library)
 * @requires ext_table_preview (module from ext_table_preview.js)
 */

/**
  * Helper function analogous to ext_references isOutOfViewport
  *
  * Check whether the bottom of an entity is within the viewport.
  * Returns true when this is the case and false otherwise.
  *
  */
function is_bottom_in_viewport(entity) {
    var bounding = entity.getBoundingClientRect();
    return bounding.bottom > 0 && bounding.bottom < (window.innerHeight ||
                                                     document.documentElement.clientHeight);
}

var ext_bottom_line = function($, logger, is_in_view_port, load_config, getEntityPath, connection, UTIF, ext_table_preview) {

    /**
     * @property {string|function} create - a function with one parameter
     *     (entity) Note: This property can as well be a
     *     javascript string which evaluates to a function.
     */


    /**
     * Check if an entity has a path attribute and one of a set of extensions.
     *
     * Note: the array of extensions must contain only lower-case strings.
     *
     * @param {HTMLElement} entity
     * @param {string[]} extensions - an array of file extesions, e.g. `jpg`.
     * @return {boolean} true iff the entity has a path with one of the
     *     extensionss.
     */
    const _path_has_file_extension = function(entity, extensions) {
        const path = getEntityPath(entity);
        if (path) {
            for (var ext of extensions) {
                if (path.toLowerCase().endsWith(ext)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Create a preview for videos.
     *
     * @param {HTMLElement} entity
     * @return {HTMLElement} a VIDEO element.
     */
    const _create_video_preview = function(entity) {
        var path = connection.getFileSystemPath() + getEntityPath(entity);
        return $(`<div class="caosdb-v-bottom-line-video-preview">
          <video controls="controls"><source src="${path}"/></video></div>`)[0];
    }

    /**
     * Error class which has the special to_html method.
     *
     * The to_html method creates a html representation of the error which is
     * intended for displaying in the bottom_line container.
     */
    const BottomLineError = function(arg) {
        this._is_bottom_line_error = true;

        if (arg.message) {
            // arg is an Error object
            this.message = arg.message;
            this.stack = arg.stack;
        } else {
            this.message = arg;
        }

        this.to_html = function() {
            return $(`<div><p>An error occured while loading this preview.<p>${
              this.message}<div>`)[0];
        }
    }

    const BottomLineWarning = function (arg) {
        this._is_bottom_line_error = true;

        if (arg.message) {
            // arg is an Error object
            this.message = arg.message;
            this.stack = arg.stack;
        } else {
            this.message = arg;
        }

        this.to_html = function() {
            return $(`<div>${this.message}<div>`)[0];
        }
    }

    /**
     * Create a preview for tiff files.
     *
     * Tiff files are decompressed if necessary and converted into png by UTIF library.
     *
     * @param {HTMLElement} entity
     * @return {Promise | HTMLElement} Promise for an IMG element.
     */
    const _create_tiff_preview = function(entity) {
        const path = getEntityPath(entity).split("/").map(encodeURIComponent).join("/");
        const result = $(`<div class="caosdb-v-bottom-line-image-preview"></div>`);
        const img = $(`<img src="${connection.getFileSystemPath() + path}"/>`)[0];
        result.append(img);

        /**
         * Promise which retrieves the tiff file and calls the UTIF library for
         * decompression and conversion into png.
         */
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            UTIF._xhrs.push(xhr);
            UTIF._imgs.push(img);
            xhr.open("GET", path);
            xhr.responseType = "arraybuffer";
            xhr.onload = (e) => {
                try {
                  // decompress and convert tiff file
                  UTIF._imgLoaded(e);

                  // return the result if no error occured.
                  resolve(result);
                } catch(err) {
                  // throw errors from UTIF to the awaiting caller.
                  reject(new BottomLineError(err));
                }
            }
            // throw http errors to the awaiting caller.
            xhr.onerror = reject;

            // this finally triggers the retrieval
            xhr.send();
        });
    }

    /**
     * Create a preview for pictures.
     *
     * @param {HTMLElement} entity
     * @return {HTMLElement} an IMG element.
     */
    const _create_picture_preview = function(entity) {
        var path = connection.getFileSystemPath() + getEntityPath(entity)
          .split("/").map(encodeURIComponent).join("/");
        return $(`<div class="caosdb-v-bottom-line-image-preview"><img src="${path}"/></div>`)[0];
    }

    var fallback_preview = undefined;

    const _tiff_preview_enabled = "${BUILD_MODULE_EXT_BOTTOM_LINE_TIFF_PREVIEW}" == "ENABLED";

    /**
     * Default creators.
     *
     * @member {Creator[]}
     */
    const _default_creators = [{ // pictures
            id: "_default_creators.pictures",
            is_applicable: (entity) => _path_has_file_extension(
                entity, ["jpg", "png", "gif", "svg"]),
            create: _create_picture_preview,
        }, {
            id: "_default_creators.tiff_images",
            is_applicable: (entity) => _tiff_preview_enabled && _path_has_file_extension(
                entity, ["tif", "tiff","dng","cr2","nef"]),
            create: _create_tiff_preview,
        }, { // videos
            id: "_default_creators.videos",
            is_applicable: (entity) => _path_has_file_extension(
                entity, ["mp4", "mov", "webm"]),
            create: _create_video_preview,
        }, { // tables
            id: "_default_creators.table_preview",
            is_applicable: (e) => ext_table_preview.is_table(e),
            create: (e) => ext_table_preview.get_preview(e),
        }, { // fallback
            id: "_default_creators.fallback",
            is_applicable: (entity) => true,
            create: (entity) => fallback_preview,
        }

    ];


    const previewShownEvent = new Event("ext_bottom_line.preview.shown");
    const previewReadyEvent = new Event("ext_bottom_line.preview.ready");
    const previewHiddenEvent = new Event("ext_bottom_line.preview.hidden");

    const _css_class_preview_container = "caosdb-f-ext_bottom_line-container";
    const _css_class_preview_container_resolvable = "caosdb-f-ext_bottom_line-container-resolvable";
    const _css_class_preview_container_button = "caosdb-f-ext_bottom_line-container-button";

    /**
     * Store the list of creators.
     *
     * @member {Creator[]}
     */
    const _creators = [];

    /**
     * Return the preview container of the entity.
     *
     * @param {HTMLElement} entity - An entity in HTML representation.
     * @returns {HTMLElement} the preview container or `undefined` if the entity
     *     doesn't have any.
     */
    const get_preview_container = function(entity) {
        return $(entity).children(`.${_css_class_preview_container}`)[0];
    }

    /**
     * Add the element to the entity's preview container and remove all other
     * content.
     *
     * This method appends the element (usually a preview, a waiting
     * notification or an error message) to the HTMLElement with class
     * `caosdb-f-ext_bottom_line-container` which was added to the entity by the
     * {@link root_preview_handler}.
     *
     * If the preview container cannot be found an error is logged, but not
     * thrown.
     *
     * @param {HTMLElement|String} element - A preview, a waiting notification,
     *     an error message or similar.
     * @param {HTMLElement} entity - An entity in HTML Representation which
     *     must have a (deep) child with class `caosdb-f-ext_bottom_line-container`.
     */
    const set_preview_container = function(entity, element) {
        const preview_container = $(get_preview_container(entity));
        if (preview_container[0]) {
            preview_container.empty();
            var buttons = preview_container.siblings(`.${_css_class_preview_container_button}`);
            if (element) {
                buttons.toggleClass("d-none", false);
                preview_container.append(element);
            } else {
                buttons.toggleClass("d-none", true);
            }
        } else {
            logger.error(new Error("Could not find the preview container."));
        }
    }

    /**
     * Append a preview to the entity and removes any pre-existing preview.
     *
     * If the preview is Promise for a preview a waiting notification is added
     * to the entity instead and the actual preview is added after the Promise
     * is resolved. If the Promise is rejected, a correspondig error is shown
     * instead.
     *
     * @see root_preview_handler
     *
     * @async
     * @param {HTMLElement} entity
     * @param {string|HTMLElement|Promise} preview - A preview for an entity or
     *     a Promise for a preview (which resolves as a string or an HTMLElement as well).
     */
    var set_preview = async function(entity, preview) {
        try {
            const wait = "Please wait...";
            set_preview_container(entity, wait);
            const result = await preview;
            set_preview_container(entity, result);
            if (result) {
                entity.dispatchEvent(previewReadyEvent);
            }
        } catch (err) {
            logger.error(err);
            if (!err._is_bottom_line_error) {
              err = new BottomLineError(err);
            }
            set_preview_container(entity, err.to_html());
        }
    }

    /**
     * Create and return a preview for a given entity.
     *
     * This root_preview_creator iterates over all the registered creators and
     * uses the first match, i.e. the first creator object which return true
     * for the `is_applicable(entity)` method of the creator object.
     *
     * If a creator throws an error during checking whether it `is_applicable`
     * or during the `create` the error is logged and the creator is treated as
     * if it were not applicable.
     *
     * @async
     * @param {HTMLElement} entity - the entity for which the preview is to be
     *     created.
     * @returns {String|HTMLElement|Promise} A preview which can be added to
     *     the entity DOM representation or a Promise for such a preview.
     */
    var root_preview_creator = async function(entity) {
        for (let c of _creators) {
            try {
                if (await c.is_applicable(entity)) {
                    return c.create(entity);
                }
            } catch (err) {
                logger.error(err);
            }
        }
        return undefined;
    };

    /**
     * Add a preview container to the entity.
     *
     * The preview container is a HTMLElement with class {@link
     * _css_class_preview_container}. It is intended to contain the preview
     * after it has been created.
     *
     * The presence of a preview container is also the marker that an entity
     * has been visited by the root_preview_handler yet and that a preview is
     * loaded and added to the entity or on its way.
     *
     * Depending on the implementation the actual preview container might be
     * wrapped into other elements which can be used for styling or other
     * purposes.
     *
     * @param {HTMLElement} entity - An entity in HTML representation.
     * @return {HTMLElement} the newly created container.
     */
    var add_preview_container = function(entity) {
        const button_show = $('<button class="btn btn-sm card-footer"><i class="bi bi-chevron-down"></i> Show Preview</button>')
            .css({
                width: "100%",
                padding: "0",
            })
            .addClass(_css_class_preview_container_button)
        const button_hide = $('<button class="btn btn-sm card-footer"><i class="bi bi-chevron-up"></i> Hide Preview</button>')
            .css({
                width: "100%",
                padding: "0",
            })
            .addClass(_css_class_preview_container_button)
            .hide();
        const style = {
            padding: "0px 10px"
        };
        const container = $(`<div class="collapse"/>`)
            .addClass(_css_class_preview_container)
            .addClass(_css_class_preview_container_resolvable)
            .css(style);
        const show = function() {
            button_show.hide();
            button_hide.show();
            container.collapse("show");
        }
        const hide = function() {
            button_hide.hide();
            button_show.show();
            container.collapse("hide");
        }
        button_show.click(show);
        button_hide.click(hide);
        container.on("shown.bs.collapse", () => {
            container[0].dispatchEvent(previewShownEvent);
        });
        container.on("hidden.bs.collapse", () => {
            container[0].dispatchEvent(previewHiddenEvent);
        });
        $(entity).append(container);
        $(entity).append(button_show);
        $(entity).append(button_hide);

        return container[0];
    }

    /**
     * Create a preview for the entity and append it to the entity.
     *
     * The root_preview_handler calls the root_preview_creator which returns a
     * preview which is to be added to the entity.
     *
     * @async
     * @param {HTMLElement} entity - the entity for which the preview is to
     *     created.
     */
    var root_preview_handler = async function(entity) {
        var container = $(get_preview_container(entity) || add_preview_container(entity));
        if (container.hasClass(_css_class_preview_container_resolvable)) {
            container.removeClass(_css_class_preview_container_resolvable);
            const preview = root_preview_creator(entity);
            await set_preview(entity, preview);
        }
    }

    /**
     * Trigger the root_preview_handler for all entities within the view port
     * when the view port.
     */
    var root_preview_handler_trigger = function() {
        var entities = $(".caosdb-entity-panel,.caosdb-entity-preview");
        for (let entity of entities) {

            // TODO viewport + 1000 px for earlier loading
            if (is_in_view_port(entity)) {
                root_preview_handler(entity);
            }
        }
    }

    /**
     * Intialize the scroll watcher which listens on the scroll event of the
     * window and triggers the preview loading with a delay after the last
     * scroll event.
     *
     * @param {integer} delay - timeout in milliseconds after the last scroll
     *     event. After this timeout the trigger is called.
     * @param {function} trigger - the callback which is called.
     */
    var init_watcher = function(delay, trigger) {
        var scroll_timeout = undefined;
        $(window).scroll(() => {
            if (!scroll_timeout) {
                clearTimeout(scroll_timeout);
            }
            scroll_timeout = setTimeout(trigger, delay);
        });

        var preview_timeout = undefined;
        window.addEventListener(
            preview.previewReadyEvent.type,
            () => {
                if (!preview_timeout) {
                    clearTimeout(scroll_timeout);
                }
                scroll_timeout = setTimeout(trigger, 100);
                return true;
            },
            true);
    };


    /**
     * Configure the creators.
     *
     * @param {BottomLineConfig} config
     */
    var configure = async function(config) {
        logger.debug("configure", config);
        if (config.version != "0.1") {
            throw new Error("Wrong version in config.");
        }

        fallback_preview = config.fallback || fallback_preview;

        // append/load creators
        _creators.splice(0, _creators.length);
        for (let c of config.creators) {
            _creators.push({
                id: c.id,
                is_applicable: typeof c.is_applicable === "function" ? c.is_applicable : eval(c.is_applicable),
                create: typeof c.create === "function" ? c.create : eval(c.create)
            });
        }

        // append default creators
        for (let c of _default_creators) {
            _creators.push(c);
        }
    };


    /**
     * Initialize this module.
     *
     * I.e. configure the list of creators and setup the scroll listener which
     * triggers the root_preview_handler.
     *
     * @property {BottomLineConfig} [config] - an optional config. Per default, the
     *     configuration is fetched from the server.
     */
    const init = async function(config) {
        logger.info("ext_bottom_line initialized");

        try {
            let _config = config || await load_config("json/ext_bottom_line.json");
            await configure(_config);

            init_watcher(_config.delay || 500, root_preview_handler_trigger);

            // trigger the whole thing for the first time
            root_preview_handler_trigger();

        } catch (err) {
            logger.error(err);
        }

    }

    /**
     * @exports ext_bottom_line
     */
    return {
        previewShownEvent: previewShownEvent,
        previewReadyEvent: previewReadyEvent,
        init: init,
        init_watcher: init_watcher,
        configure: configure,
        add_preview_container: add_preview_container,
        root_preview_handler: root_preview_handler,
        _creators: _creators,
        _css_class_preview_container,
        _css_class_preview_container_button,
        _css_class_preview_container_resolvable,
        BottomLineError: BottomLineError,
        BottomLineWarning: BottomLineWarning,
    }
}($, log.getLogger("ext_bottom_line"),
  is_bottom_in_viewport, load_config, getEntityPath,
  connection, UTIF, ext_table_preview);


/**
 * Helper for plotly
 */
var plotly_preview = function(logger, ext_bottom_line, plotly) {

    /**
     * Create a plot with plotly.
     *
     * The layout and any other plotly options are set by this function. The
     * only parameters are `data` and `layout` which are the respective
     * parameters of the `Plotly.newPlot` factory.
     *
     * Hence the full documentation of the `data` parameter is available at
     * https://plotly.com/javascript/plotlyjs-function-reference/#plotlynewplot
     *
     *
     * @param {object[]} data - array of objects containing the data which is
     *     to be plotted.
     * @param {object[]} layout - dictionary of settings defining the layout of
     *     the plot.
     * @param {object[]} settings - object containing additional
     *     settings for the plot.
     * @returns {HTMLElement} the element which contains the plot.
     */
    const create_plot = function(data,
				 layout = {
				     margin: {
					 t: 0
				     },
				     height: 400,
				     widht: 400
				 },
				 settings = {
				     responsive: true
				 }) {
        var div = $('<div/>')[0];
        plotly.newPlot(div, data, layout, settings);
        return div;
    }

    const resize_plots_event_handler = function(e) {
        var plots = $(e.target).find(".js-plotly-plot");
        for (let plot of plots) {
            plotly.Plots.resize(plot);
        }
    }

    const init = function() {
        window.addEventListener(ext_bottom_line.previewReadyEvent.type,
            resize_plots_event_handler, true);
        window.addEventListener(ext_bottom_line.previewShownEvent.type,
            resize_plots_event_handler, true);
    }

    return {
        create_plot: create_plot,
        init: init,
    };

}(log.getLogger("plotly_preview"), ext_bottom_line, Plotly);


// this will be replaced by require.js in the future.
$(document).ready(function() {
    if ("${BUILD_MODULE_EXT_BOTTOM_LINE}" == "ENABLED") {
        caosdb_modules.register(plotly_preview);
        caosdb_modules.register(ext_bottom_line);
    }
});
