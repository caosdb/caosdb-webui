/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

/**
 * Replaces textareas in the edit mode by a wysiwyg editor
 *
 * @module ext_editmode_wysiwyg_text
 * @version 0.1
 *
 * @param jQuery - well-known library.
 * @param log - singleton from loglevel library or javascript console.
 * @param {class} ClassicEditor - ClassicEditor class from ckEditor
 * @param {module} edit_mode - LinkAhead's edit-mode module
 * @param {function} getPropertyElements - LinkAhead's function to extract the
 *     property HTML elements from an entity HTML element
 * @param {function} getPropertyDatatype - LinkAhead's function to extract the
 *     data type from a property HTML element
 * @param {function} getPropertyName - LinkAhead's function to extract the
 *     name from a property HTML element
 */
var ext_editmode_wysiwyg_text = function ($, logger, ClassicEditor, edit_mode, getPropertyElements, getPropertyDatatype, getPropertyName) {

    var _callOnSave = [];

    var insertEditorInProperty = async function (prop) {
        if (!(getPropertyDatatype(prop) === 'TEXT')) {
            // Ignore anything that isn't a list property, even LIST<TEXT>
            return;
        }

        try {
            const editor = await ClassicEditor
                .create(prop.querySelector('textarea'), {
                    // use all plugins since we built the editor dependency to
                    // contain only those we need.
                    plugins: ClassicEditor.builtinPlugins,
                    // Markdown needs a header row so enforce this
                    table: {
                        defaultHeadings: {
                            rows: 1,
                            columns: 0
                        },
                    },
                })
            logger.debug('Initialized editor for ' + getPropertyName(prop));
            // Manually implement saving the data since edit mode is not
            // a form to be submitted.
            _callOnSave.push(() => {
                editor.updateSourceElement();
            });
        } catch (error) {
            logger.error(error.stack);
        }
    }

    const proxySaveMethod = function (original) {
        const result = function (entity) {
            _callOnSave.forEach(cb => {
                cb();
            });
            if (typeof original === "function") {
                return original(entity);
            }
            return undefined;
        }
        return result;
    }

    var replaceTextAreas = function (entity) {
        // on save, call callbacks
        edit_mode.app.onBeforeInsert = proxySaveMethod(edit_mode.app.onBeforeInsert);
        edit_mode.app.onBeforeUpdate = proxySaveMethod(edit_mode.app.onBeforeUpdate);
        const properties = getPropertyElements(entity);
        for (let prop of properties) {
            // TODO(fspreck): This will be replaced by a whitelist of properties
            // in the future.
            if (getPropertyName(prop)) {
                insertEditorInProperty(prop);
            }
        }
    }

    var init = function () {
        if (edit_mode.app && edit_mode.app.entity) {
            // replace text areas if we're already in the edit mode and all
            // events did fire already.
            replaceTextAreas(edit_mode.app.entity);
        }

        // Insert an editor into all TEXT properties of the entity which is
        // being edited.
        document.body.addEventListener(edit_mode.start_edit.type, (e) => {
            logger.debug('Replacing text areas ...');
            ext_editmode_wysiwyg_text.replaceTextAreas(e.target);
        }, true);

        // Listen to added properties and replace the textarea if necessary
        document.body.addEventListener(edit_mode.property_added.type, (e) => {
            logger.debug('Replacing textarea in ' + getPropertyName(e.target));
            ext_editmode_wysiwyg_text.insertEditorInProperty(e.target);
        }, true)

        // Listen to properties, the data type of which has changed. Mainly
        // because of change from list to scalar and vice versa.
        document.body.addEventListener(edit_mode.property_data_type_changed.type, (e) => {
            logger.debug('Re-rendering ' + getPropertyName(e.target));
            ext_editmode_wysiwyg_text.insertEditorInProperty(e.target);
        }, true);

        // Clear list of saving callbacks when leaving the edit mode (regardless
        // of saving or cancelling)
        document.body.addEventListener(edit_mode.end_edit.type, (e) => {
            this._callOnSave = [];
        }, true);
    };

    return {
        init: init,
        replaceTextAreas: replaceTextAreas,
        insertEditorInProperty: insertEditorInProperty,
    };
}($, log.getLogger("ext_editmode_wysiwyg_text"), ClassicEditor, edit_mode, getPropertyElements, getPropertyDatatype, getPropertyName);

$(document).ready(() => {
    if ("${BUILD_MODULE_EXT_EDITMODE_WYSIWYG_TEXT}" == "ENABLED") {
        caosdb_modules.register(ext_editmode_wysiwyg_text);
    }
});
