/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

const ext_entity_state = function ($, logger, edit_mode, update_state, getEntityXML) {

  const entity_state_transition_ready_event = new Event("entity_state.transition.ready");

  /**
   * Return an xml representation of an entity's state
   *
   * @param {State} state - the entity state
   * @return {XMLElement}
   */
  const state_to_xml = function (state) {
    logger.trace("enter state_to_xml", state);
    const name = state.name ? ` name="${state.name}"` : "";
    const model = state.model ? ` model="${state.model}"` : "";
    const id = state.id ? ` id="${state.id}"` : "";
    return str2xml(`<State${name}${model}${id}/>`).firstElementChild;
  }

  /**
   * Generate a function which replaces the edit_mode.form_to_xml function and
   * includes the entity state into the output xml.
   *
   * Internally, the function calls the original function (proxy pattern).
   *
   * @pararm {function} original - the original edit_mode.form_to_xml function.
   * @return {function}
   */
  const create_edit_mode_form_to_xml_function = function (original) {
    const result = function (entity_form) {
      const entity_xml = original(entity_form);
      const state = ext_entity_state.get_entity_state(entity_form);
      if (state) {
        const state_xml = ext_entity_state.state_to_xml(state);
        entity_xml.firstElementChild.appendChild(state_xml);
      }
      return entity_xml;
    }
    return result;
  }

  /**
   * Patch the edit mode to include the entity state (unchanged) into the xml
   * when inserting or updating an entity.
   */
  const init_edit_mode_patch = function () {
    edit_mode.form_to_xml = create_edit_mode_form_to_xml_function(
      edit_mode.form_to_xml);
  }

  /**
   * Return the state of an entity.
   *
   * @param {HTMLElement} entity - an entity in HTML representation.
   * @return {State}
   */
  const get_entity_state = function (entity) {
    const result = {
      "id": entity.getAttribute("data-state-id"),
      "name": entity.getAttribute("data-state-name"),
      "model": entity.getAttribute("data-state-model"),
    };
    if (result.id || (result.name && result.model)) {
      return result;
    }
    return undefined;
  }

  /**
   * Represents an entity state.
   *
   * @typedef {Object} State
   * @property {string} id - the entity id of the state.
   * @property {string} name - the name of the state.
   * @property {string} model - the name of the state model.

  /**
   * Set a new entity state
   *
   * @param {HTMLElement} entity - an entity in HTML representation.
   * @param {State} state - the new state
   */
  const set_entity_state = function (entity, state) {
    entity.removeAttribute("data-state-name");
    entity.removeAttribute("data-state-model");
    entity.removeAttribute("data-state-id");
    if (state.id)
      entity.setAttribute("data-state-id", state.id);
    if (state.name)
      entity.setAttribute("data-state-name", state.name);
    if (state.model)
      entity.setAttribute("data-state-model", state.model);
  }

  /**
   * Perform or start a state transition.
   *
   * If the transition is the (special) 'Edit' transition this function
   * triggers the edit_mode and opens the entity in the edit_mode form.
   *
   * Otherwise, the entity is set to the new entity state and then the update
   * requests is performed. Apart from that, the entity is left unchanged.
   *
   * For a reinitialization of relevant clients after the transition, the
   * `entity_state_transition_ready_event` is dispatched after the transition
   * has been performed.
   *
   * @param {HTMLElement} entity - entity in HTML representation.
   * @param {Transition} transition - the transition
   */
  const perform_transition = async function (entity, transition) {
    logger.trace("enter perform_transition", entity, transition);
    const next_state = ext_entity_state.get_entity_state(entity);
    next_state.id = undefined;
    next_state.name = transition.to_state;
    if (transition.name.toLowerCase() == "edit") {
      if(!edit_mode.is_edit_mode()) {
        // switch on edit_mode
        await edit_mode.toggle_edit_mode();
      }
      try {
        const entity_form = edit_mode.edit(entity);
        ext_entity_state.set_entity_state(entity_form, next_state);
      } catch (err) {
        logger.warn(err);
      }
    } else {
      const entity_xml = getEntityXML(entity);
      const state_xml = ext_entity_state.state_to_xml(next_state);
      entity_xml.firstElementChild.appendChild(state_xml);
      const response = await ext_entity_state.update_state(entity_xml);
      const updated_entity = await transformation.transformEntities(response);
      // remove warnings and info messages
      $(updated_entity).find(".alert-warning, .alert-info").remove();
      edit_mode.smooth_replace(entity, updated_entity[0]);
      updated_entity[0].dispatchEvent(entity_state_transition_ready_event);
      resolve_references.init();
      preview.init();
    }
  }

  /**
   * Represents a entity state transition
   *
   * @typedef {object} Transition
   * @property {string} name - name of the transition.
   * @property {string} to_state - name of the target state of the transition.

  /**
   * Return the transition which a button stands for.
   *
   * @param {HTMLElement} button - a transition button from the state's modal
   *     popover.
   * @return {Transition}
   *
   */
  const get_transition = function (button) {
    return {
      "name": button.getAttribute("data-transition-name"),
      "to_state": button.getAttribute("data-to-state"),
    };
  }

  /**
   * @param {HTMLElement} entity
   */
  const init_state_transitions = function (entity) {
    $(entity)
      .find(".caosdb-f-entity-state-transition-button")
      .click(function() {
      const transition = ext_entity_state.get_transition(this);
      ext_entity_state.perform_transition(entity, transition);
      $(entity).find(".caosdb-f-entity-state-info").modal("hide");
    });

  }

  const init = async function () {
    logger.info("init ext_entity_state");
    const entities = $(".caosdb-entity-panel");

    document.body.addEventListener(edit_mode.end_edit.type, (e) => {
      // reinitialization after an entity has been changed in the edit mode
      init_state_transitions(e.target);
    }, true);

    document.body.addEventListener(entity_state_transition_ready_event.type, (e) => {
      // reinitialization after a state transition has been performed
      init_state_transitions(e.target);
    }, true);

    for (let entity of entities) {
      // initialize all entities on the page which have a state
      init_state_transitions(entity);
    }

    init_edit_mode_patch();
  }

  return {
    init: init,
    state_to_xml: state_to_xml,
    get_entity_state: get_entity_state,
    perform_transition: perform_transition,
    get_transition: get_transition,
    update_state: update_state,
  }
}($, log.getLogger("ext_entity_state"), edit_mode, update, getEntityXML);

$(document).ready(function () {
      caosdb_modules.register(ext_entity_state);
});
