/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * The ext_file_download module provides a very basic preview for table files.
 *
 * The preview is generated using a server side script.
 *
 * @module ext_file_download 
 * @version 0.1
 *
 * @requires jQuery
 * @requires log
 */
var ext_file_download  = function ($, logger) {

    /**
     * collect the file ids that will be passed to the zip script
     *
     * @return {string[]} array of entity ids
     */
    const collect_ids = function(){
        var properties = $(".caosdb-f-property-value").find(
            ".caosdb-id.caosdb-f-property-single-raw-value");
        var id_list = properties.toArray().map(x=>x.textContent);
        var entities = $("tr[data-entity-id]")
        id_list = id_list.concat(entities.toArray().map(
            x=>x.attributes['data-entity-id'].value)); 
        return id_list
    };

    /**
     * chunk list to smaller pieces.
     *
     * @param {string[]} list - array of (usually) ids
     * @param {Number} size - chunk size (use integer)
     * @return {string[][]} array of array of string from the original list.
     */
    const chunk_list = function(list, size){
        size = size || 20;
        var pieces = [];
        var index = 0;
        while (index < list.length){
            pieces.push(list.slice(index,index+size));
            index+=size;
        }

        return pieces
    };


    /**
     * create select statement to find files.
     *
     * @param {string[]} id_list - array of ids
     * @return {string} a query string
     */
    const query_files_str = function(id_list){
        var povs = id_list.map(x=>` id=${x} `);
        const query_str="SELECT ID FROM FILE WITH " + povs.join("or");
        return query_str;
    };

    /**
     * Reduce id list to files. Throw away all ids which do not belong to a
     * file entity.
     *
     * @param {string[]} id_list - array of ids
     * @return {string[]} array of file ids.
     */
    const reduce_ids = async function(id_list){
        var file_ids = []
        for (var part of chunk_list(id_list)) {
            // query for files
            var result = await query(query_files_str(part));
            file_ids=file_ids.concat(result.map(x => getEntityID(x)));
        }
        return file_ids;
    };


    /**
     * Callback function for the download files link.
     *
     * Collects all the file entities and sends them to a server-side script
     * which then puts the files into a zip.
     *
     * @param {HTMLElement} zip_link - the link element which triggered this
     *     call and which will be disabled during the execution of this
     *     function in order to prevent the user from triggering the process
     *     twice.
     */
    const download_files = async function (zip_link) {
        const onClickValue = zip_link.getAttribute("onClick");
        try {
            // remove click event handler which called this function in the first place
            zip_link.removeAttribute("onClick");

            // add loading info. TODO make an animated one
            $("#downloadModal").find(".caosdb-f-modal-footer-left").append(
                createWaitingNotification("Collecting files...")
            );

            var ids = collect_ids();
            ids = await reduce_ids(ids);
            if (ids.length == 0){
                alert ("There are no file entities in this table.");
                return;
            }

            var table = await ext_bookmarks.get_export_table(ids, "", "\t", "\n");

            const result = await connection.runScript(
                "ext_file_download/zip_files.py",
                {
                  "-p0": ids,
                  "-p1": table,
                }
            );
            const code = result.getElementsByTagName("script")[0].getAttribute("code");
            if (parseInt(code) > 0) {
                throw ("An error occurred during execution of the server-side script:\n"
                    + result.getElementsByTagName("script")[0].outerHTML);
            }
            const filename = result.getElementsByTagName("stdout")[0].textContent;
            if (filename.length == 0) {
                throw("Server-side script produced no file or did not return the file name: \n"
                    + result.getElementsByTagName("script")[0].outerHTML);
            }


            // trigger download of generated file
            caosdb_table_export.go_to_script_results(filename);

            //close modal
            $("#downloadModal").find(".modal-footer").find(".btn")[0].click();
        } catch (e) {
            globalError(e);
        } finally {
            removeAllWaitingNotifications($("#downloadModal")[0]);
            // restore the old click handler - hence a new file is generated with each click.
            zip_link.setAttribute("onClick", onClickValue);
        }

    };

    const init = function () {
        // only enable when init is being called
        logger.info("init ext_file_download");
        if (userIsAnonymous()) {
            $("#caosdb-f-query-select-files").parent().hide();
        }
    };

    return {
        init: init,
        download_files: download_files,
        collect_ids: collect_ids,
        chunk_list: chunk_list,
    };

}($, log.getLogger("ext_file_download"));

// This module is registered by caosdb_table_export.
