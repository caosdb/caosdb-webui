/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 Alexander Schlemmer <alexander.schlemmer@ds.mpg.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * The ext_jupyterdrag module adds dragstart listeners to the WebUI which allow
 * dragging single entities into Jupyter notebooks and other text editors (for
 * python code) which implement drop gestures for text data.
 *
 * @module ext_jupyterdrag
 * @version 0.1
 *
 * @requires jQuery
 * @requires log
 * @requires getEntityRole
 * @requires getEntityID
 */
var ext_jupyterdrag = function($, logger, getEntityRole, getEntityID) {


    /**
     * Initialize the ext_jupyterdrag module.
     *
     */
    var init = async function() {
        $(".caosdb-entity-panel").find(
            ".caosdb-entity-panel-heading").attr("draggable",
            "true");
        $(".caosdb-entity-panel").find(
            ".caosdb-entity-panel-heading").on("dragstart",
            function(ev) {
                logger.trace("dragstart", ev);
                var eel = this.parentElement;
                var role = getEntityRole(eel);
                var entid = getEntityID(eel);
                ev.originalEvent.dataTransfer.setData(
                    "text/plain",
                    "db." + role + "(id=" + entid + ")");
            });
    }



    return {
        // public members, part of the API
        init: init,
        _logger: logger,
    }
}($, log.getLogger("ext_jupyterdrag"), getEntityRole, getEntityID);


$(document).ready(function() {
    if ("${BUILD_MODULE_EXT_JUPYTERDRAG}" == "ENABLED") {
        caosdb_modules.register(ext_jupyterdrag);
    }
});
