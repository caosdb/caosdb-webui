/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

/**
 * Adds QR-Code generation to entities.
 *
 * @author Timm Fitschen
 */
var ext_qrcode = function ($, connection, getEntityVersion, getEntityID, QRCode, logger) {

    const _buttons_list_class = "caosdb-v-entity-header-buttons-list";
    const _qrcode_button_class = "caosdb-f-entity-qrcode-button";
    const _qrcode_canvas_container = "caosdb-f-entity-qrcode";
    const _qrcode_link_container = "caosdb-f-entity-qrcode-link";
    const _qrcode_icon = `<i class="bi bi-upc"></i>`;

    /**
     * Create a new QR Code and a caption with a link, either linking to the
     * entity head or to the exact version of the entity, based on the selected
     * radio buttons and insert it into the modal.
     *
     * @param {HTMLElement} modal
     * @param {string} entity_id
     * @param {string} entity_version
     */
    var update_qrcode = function (modal, entity_id, entity_version) {
        modal = $(modal);
        const uri = modal.find("input[name=entity-qrcode-versioned]:checked").val();
        var display_version = "";
        if (uri.indexOf("@") > -1) {
            display_version = `@${entity_version.substring(0,8)}`;
        }
        const description = `Entity <a href="${uri}">${entity_id}${display_version}</a>`;
        modal.find(`.${_qrcode_canvas_container}`).empty();
        modal.find(`.${_qrcode_link_container}`).empty().append(description);
        QRCode.toCanvas(uri, {
            "scale": 6
        }).then((canvas) => {
            modal.find(`.${_qrcode_canvas_container}`).empty().append(canvas);
        }).catch(logger.error);
    }

    /**
     * Create modal which shows the QR Code and a form where the user can choose
     * whether the QR Code links to the entity head or the exact version of the
     * entity.
     *
     * @param {string} modal_id - id of the resulting HTMLElement
     * @param {string} entity_id
     * @param {string} entity_version
     * @return {HTMLElement} the resulting modal.
     */
    var create_qrcode_modal = function (modal_id, entity_id, entity_version) {
        const uri = `${connection.getEntityUri([entity_id])}`;
        const short_version = entity_version.substring(0, 8);
        const modal = $(`<div class="modal fade" id="${modal_id}" tabindex="-1" aria-labelledby="${modal_id}-label" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="${modal_id}-label">QR Code</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body text-center">
            <div class="${_qrcode_canvas_container}"></div>
            <div class="${_qrcode_link_container}"></div>
          </div>
          <div class="modal-footer justify-content-start">
            <form>
            <div class="form-check">
              <label class="form-check-label">
                <input value="${uri}" class="form-check-input" type="radio" name="entity-qrcode-versioned" checked>
                Link to this entity.
              </label>
            </div>
            <div class="form-check">
              <label class="form-check-label" for="flexRadioDefault1">
                <input value="${uri}@${entity_version}" class="form-check-input" type="radio" name="entity-qrcode-versioned">
                Link to this exact version of this entity.
              </label>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>`);
        modal.find("form").change(() => {
            update_qrcode(modal, entity_id, entity_version);
        });
        return modal[0];
    }

    /**
     * Click handler of the QR Code button. The click event opens a modal showing
     * the QR Code and a form where the user can choose whether the QR Code links
     * to the entity head or the exact version of the entity.
     *
     * @param {string} entity_id
     * @param {string} entity_version
     */
    var qrcode_button_click_handler = function (entity_id, entity_version) {
        const modal_id = `qrcode-modal-${entity_id}-${entity_version}`;
        var modal_element = document.getElementById(modal_id);
        if (modal_element) {
            // toggle modal
            const modal = bootstrap.Modal.getInstance(modal_element);
            modal.toggle();
        } else {
            modal_element = create_qrcode_modal(modal_id, entity_id, entity_version);
            update_qrcode(modal_element, entity_id, entity_version);
            $("body").append(modal_element);
            const options = {};
            const modal = new bootstrap.Modal(modal_element, options);
            modal.show();
        }
    }

    /**
     * Create a button which opens the QR Code modal on click.
     *
     * @param {string} entity_id
     * @param {string} entity_version
     * @return {HTMLElement} the newly created button.
     */
    var create_qrcode_button = function (entity_id, entity_version) {
        const button = $(`<button title="Create QR Code" type="button" class="${_qrcode_button_class} caosdb-v-entity-qrcode-button btn">${_qrcode_icon}</button>`);
        button.click(() => {
            qrcode_button_click_handler(entity_id, entity_version);
        });
        return button[0];
    }

    /**
     * Add a qrcode button to a given entity.
     * @param {HTMLElement} entity
     */
    var add_qrcode_to_entity = function (entity) {
        const entity_id = getEntityID(entity);
        const entity_version = getEntityVersion(entity);

        $(entity).find(`.${_buttons_list_class}`).append(create_qrcode_button(entity_id, entity_version));
    }

    var remove_qrcode_button = function (entity) {
        $(entity).find(`.${_buttons_list_class} .${_qrcode_button_class}`).remove();
    }

    var _init = function () {
        for (let entity of $(".caosdb-entity-panel")) {
            remove_qrcode_button(entity);
            add_qrcode_to_entity(entity);
        }
    }

    /**
     * Initialize this module and append a QR Code button to all entities panels on the page.
     *
     * Removes all respective buttons if present before adding a new one.
     */
    var init = function () {
        _init();

        // edit-mode-listener
        document.body.addEventListener(edit_mode.end_edit.type, _init, true);
    };

    return {
        update_qrcode: update_qrcode,
        add_qrcode_to_entity: add_qrcode_to_entity,
        remove_qrcode_button: remove_qrcode_button,
        create_qrcode_button: create_qrcode_button,
        create_qrcode_modal: create_qrcode_modal,
        qrcode_button_click_handler: qrcode_button_click_handler,
        init: init
    };

}($, connection, getEntityVersion, getEntityID, QRCode, console);

$(document).ready(function () {
    if ("${BUILD_MODULE_EXT_QRCODE}" == "ENABLED") {
        caosdb_modules.register(ext_qrcode);
    }
});