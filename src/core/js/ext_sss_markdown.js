/**
 * @module ext_sss_markdown
 * @version 0.1
 *
 * Transforms the STDOUT of server-side scripting responses from plain text to
 * HTML. The STDOUT is interpreted as Markdown.
 *
 * Module has to be enabled via setting the build property
 * `BUILD_MODULE_EXT_SSS_MARKDOWN=ENABLED`.
 */
var ext_sss_markdown = function() {

    var logger = log.getLogger("ext_sss_markdown");

    var init = function() {
        logger.trace("enter init");
        const stdout_container = $("#caosdb-stdout");
        if (stdout_container.length == 0) {
            // nothing to do
            return;
        }

        // get text content of the STDOUT container
        const text = stdout_container[0].textContent;
        logger.debug("transform", text);
        // interpret as markdown.
        const html = markdown.textToHtml(text);

        // a little styling
        stdout_container.css({padding: 10});

        // replace plain markdown text with formatted html
        stdout_container.empty();
        stdout_container.append(html);

        // hide error output container if the STDERR was empty.
        var errortext = $("#caosdb-stderr").text();
        if (errortext.length == 0) {
            $("#caosdb-container-stderr").hide();
        }
    }

    return {
        init: init,
        logger: logger,
    };

}();


$(document).ready(function() {
    if ("${BUILD_MODULE_EXT_SSS_MARKDOWN}" == "ENABLED") {
        caosdb_modules.register(ext_sss_markdown);
    }
});
