/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * The ext_table_preview module provides a very basic preview for table files.
 *
 * The preview is generated using a server side script.
 *
 * @module ext_table_preview 
 * @version 0.1
 *
 * @requires jQuery
 * @requires log
 * @requires getEntityPath
 * @requires getEntityID
 * @requires markdown
 */
var ext_table_preview  = function ($, logger, connection, getEntityPath, getEntityID, markdown) {

    const get_preview = async function (entity) {
        try {
            const script_result = await connection.runScript("ext_table_preview/pandas_table_preview.py",
                {"-p0": getEntityID(entity)}
            );

            const code = script_result.getElementsByTagName("script")[0].getAttribute("code");
            if (parseInt(code) > 1) {
                return script_result.getElementsByTagName("stderr")[0]
            } else if (parseInt(code) != 0) {
                throw ("An error occurred during execution of the server-side "
                    + "script:\n"
                    + script_result.getElementsByTagName("stderr")[0]);
            } else {
                const tablecontent = script_result.getElementsByTagName("stdout")[0];
                const unformatted = markdown.textToHtml(tablecontent.textContent)
                const formatted = $('<div/>').append(unformatted);
                formatted.find("table").addClass("table table-responsive table-bordered table-sm").removeAttr("border");
                return formatted[0];
            }
        } catch (err) {
            if (err.message && err.message.indexOf && err.message.indexOf("HTTP status 403") > -1) {
                throw new ext_bottom_line.BottomLineWarning("You are not allowed to generate the table preview. Please log in.");
            } else {
                throw err;
            }
        }
    };

    const is_table = function (entity) {
        const path = getEntityPath(entity);
        return path && (path.toLowerCase().endsWith('.xls') 
            || path.toLowerCase().endsWith('.xlsx') 
            || path.toLowerCase().endsWith('.csv') 
            || path.toLowerCase().endsWith('.tsv'));
    };

    const init = function () {
        // only enable when init is being called
        ext_table_preview.is_table = is_table;
    };

    return {
        init: init,
        get_preview: get_preview,
        is_table: () => false,
    };

}($, log.getLogger("ext_table_preview"), connection, getEntityPath, getEntityID, markdown);

// this will be replaced by require.js in the future.
$(document).ready(function () {
    if ("${BUILD_MODULE_EXT_BOTTOM_LINE_TABLE_PREVIEW}" == "ENABLED") {
        caosdb_modules.register(ext_table_preview);
    }
});
