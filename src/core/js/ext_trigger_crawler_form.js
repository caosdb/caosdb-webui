/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * @module ext_trigger_crawler_form
 * @version 0.1
 *
 * Adds a button to a configurable navbar toolbox which opens a form for
 * triggering the crawler as a server-side scripting job.
 *
 * The form has two fields. The text field `Path` sets the directory tree which
 * is searched by the crawler. The checkbox `Suppress Warnings` can be checked
 * to suppress the crawlers warnings.
 *
 * The module has to be enabled via the
 * `BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM=ENABLED` build variable.
 *
 * The toolbox where the button is added can be configured via the build
 * variable `BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM_TOOLBOX`. The default is
 * `Tools`.
 */
var ext_trigger_crawler_form = function ($, form_elements) {

    var init = function (toolbox) {
        const _toolbox = toolbox || "${BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM_TOOLBOX}";

        const script = "crawl.py"
        const button_name = "Trigger Crawler";
        const title = "The LinkAhead-Crawler will run over the filesystem and make necessary updates.";

        const crawler_form = make_scripting_caller_form(
            script, button_name);
        const modal = form_elements.make_form_modal(crawler_form, "Trigger the crawler", "Crawl the selected path");


        navbar.add_tool(button_name, _toolbox, {
            title: title,
            callback: () => {
                $(modal).modal("toggle");
            }
        });
    }

    /**
     * Create the trigger crawler form.
     */
    var make_scripting_caller_form = function (script, button_name) {
        const path_field = form_elements.make_text_input({
            name: "-p1",
            value: "/",
            label: "Path"
        });
        const warning_checkbox = form_elements.make_checkbox_input({
            name: "-Osuppress",
            label: "Suppress Warnings"
        });
        $(warning_checkbox).find("input").attr("value", "TRUE");

        const scripting_caller = form_elements.make_scripting_submission_button(script, button_name);

        scripting_caller.prepend(warning_checkbox).prepend(path_field);

        return scripting_caller[0];
    }

    return {
        init: init,
    };

}($, form_elements);

$(document).ready(function () {
    if ("${BUILD_MODULE_EXT_TRIGGER_CRAWLER_FORM}" === "ENABLED") {
        caosdb_modules.register(ext_trigger_crawler_form);
    }
});
