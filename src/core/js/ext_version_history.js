/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019-2022 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


/**
 * This module provides the functionality to load the full version history (for
 * privileged users) and export it to tsv.
 *
 * @module version_history
 */
var version_history = new function () {

    const logger = log.getLogger("version_history");
    this.logger = logger;

    this._has_version_fragment = function () {
        const fragment = window.location.hash.substr(1);
        return fragment === 'version_history';
    }

    this._get = connection.get;
    /**
     * Retrieve the version history of an entity and return a table with the
     * history.
     *
     * @function retrieve_history
     * @param {string} entity - the entity id with or without version id.
     * @return {HTMLElement} A table with the version history.
     */
    this.retrieve_history = async function (entity) {
        const xml = this._get(transaction
            .generateEntitiesUri([entity]) + "?H");
        const html = (await transformation.transformEntities(xml))[0];
        const history_table = $(html).find(".caosdb-f-entity-version-history");
        return history_table[0];
    }

    /**
     * Initalize the buttons for loading the version history.
     *
     * The buttons are visible when the entity has only the normal version info
     * attached and the current user has the permissions to retrieve the
     * version history.
     *
     * The buttons trigger the retrieval of the version history and append the
     * version history to the version info modal.
     *
     * @function init_load_history_buttons
     */
    this.init_load_history_buttons = function () {
        for (let entity of $(".caosdb-entity-panel")) {
            const is_permitted = hasEntityPermission(entity, "RETRIEVE:HISTORY");
            if (!is_permitted) {
                continue;
            }
            const entity_id_version = getEntityIdVersion(entity);
            const version_info = $(entity)
                .find(".caosdb-f-entity-version-info");
            const button = $(version_info)
                .find(".caosdb-f-entity-version-load-history-btn");
            button.show();
            button
                .click(async () => {
                    button.prop("disabled", true);
                    const wait = createWaitingNotification("Retrieving full history. Please wait.");
                    const sparse = $(version_info)
                        .find(".caosdb-f-entity-version-history");
                    sparse.find(".modal-body *").replaceWith(wait);

                    const history_table = await version_history
                        .retrieve_history(entity_id_version);
                    sparse.replaceWith(history_table);
                    version_history.init_export_history_buttons(entity);
                    version_history.init_restore_version_buttons(entity);
                });
        }
    }

    /**
     * Transform the HTML table with the version history to tsv.
     *
     * @function get_history_tsv
     * @param {HTMLElement} history_table - the HTML representation of the
     *     version history.
     * @return {string} the version history as downloadable tsv string,
     *     suitable for the href attribute of a link or window.location.
     */
    this.get_history_tsv = function (history_table) {
        const rows = [];
        for (let row of $(history_table).find("tr")) {
            const cells = $(row).find(".export-data").toArray().map(x => x.textContent);
            rows.push(cells);
        }
        return caosdb_utils.create_tsv_table(rows);
    }

    /**
     * Initialize the export buttons of `entity`.
     *
     * The buttons are only visible when the version history is visible and
     * trigger a download of a tsv file which contains the version history.
     *
     * The buttons trigger the download of a tsv file with the version history.
     *
     * @function init_export_history_buttons
     * @param {HTMLElement} [entity] - if undefined, the export buttons of all
     *     page entities are initialized.
     */
    this.init_export_history_buttons = function (entity) {
        entity = entity || $(".caosdb-entity-panel");
        for (let version_info of $(entity)
                .find(".caosdb-f-entity-version-info")) {
            $(version_info).find(".caosdb-f-entity-version-export-history-btn")
                .click(() => {
                    const html_table = $(version_info).find("table")[0];
                    const history_tsv = this.get_history_tsv(html_table);
                    version_history._download_tsv(history_tsv);
                });
        }
    }

    /**
     * Initialize the restore old version buttons of `entity`.
     *
     * The buttons are only visible when the user is allowed to update the
     * entity.
     *
     * The causes a retrieve of the specified version of the entity and then an
     * update that restores that version.
     *
     * @function init_restore_version_buttons
     * @param {HTMLElement} [entity] - if undefined, the export buttons of all
     *     page entities are initialized.
     */
    this.init_restore_version_buttons = function (entity) {
        var entities = [entity] || $(".caosdb-entity-panel");

        for (let _entity of entities) {
            // initialize buttons only if the user is allowed to update the entity
            if (hasEntityPermission(_entity, "UPDATE:*") || hasEntityPermission(_entity, "UPDATE:DESCRIPTION")) {
                for (let version_info of
                        $(_entity).find(".caosdb-f-entity-version-info")) {
                    // find the restore button
                    $(version_info).find(".caosdb-f-entity-version-restore-btn")
                        .toggleClass("d-none", false) // show button
                        .click(async (eve) => {
                            // the version id is stored in the restore button's
                            // data-version-id attribute
                            const versionid = eve.delegateTarget.getAttribute("data-version-id")
                            const reload = () => {
                                window.location.reload();
                            }
                            const _alert = form_elements.make_alert({
                                title: "Warning",
                                message: "You are going to restore this version of the entity.",
                                proceed_callback: async () => {
                                    try {
                                        await restore_old_version(versionid);
                                        $(_alert).remove();
                                        // reload after sucessful update
                                        $(version_info).find(".modal-body").prepend(
                                            $(`<div class="alert alert-success" role="alert">Restore successful! <p>You are being forwarded to the latest version of this entity or you can click <a href="#" onclick="window.location.reload()">here</a>.</p></div>`));
                                        setTimeout(reload, 5000);
                                    } catch (e) {
                                        logger.error(e);
                                        // print errors in an alert div
                                        $(version_info).find(".modal-body").prepend(
                                            $(`<div class="alert alert-danger alert-dismissible " role="alert"> <button class="btn-close" data-bs-dismiss="alert" aria-label="close"></button> Restore failed! <p>${e.message}</p></div>`));

                                    }
                                },
                                cancel_callback: () => {
                                    // do nothing
                                    $(_alert).remove();
                                    $(version_info).find("table").show();
                                },
                                proceed_text: "Yes, restore!",
                                remember_my_decision_id: "restore_entity",
                            });

                            $(version_info).find("table").after(_alert).hide();
                            $(_alert).addClass("text-end");
                        });
                }
            }
        }
    }

    this._download_tsv = function (tsv_link) {
        window.location.href = tsv_link;
    }


    /**
     * @function init
     */
    this.init = function () {
        this.init_load_history_buttons();
        this.init_export_history_buttons();
        this.init_restore_version_buttons();

        // check for the version_history fragment and open the modal if present.
        if (this._has_version_fragment()) {
            const first_entity = $(".caosdb-entity-panel")[0];
            if (first_entity && hasEntityPermission(first_entity, "RETRIEVE:HISTORY")) {
                logger.debug("Showing full version modal for first entity");
                const version_button = $(first_entity).find(".caosdb-f-entity-version-button");
                version_button.click();
                const full_version_history_button = $(first_entity).find(".caosdb-f-entity-version-load-history-btn");
                full_version_history_button.click();
            }
        }
    }
}

caosdb_modules.register(version_history);
