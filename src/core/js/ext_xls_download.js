/**
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

/**
 * @module caosdb_table_export
 * @version 0.2
 *
 * Convert Entities for TSV and XLS export.
 *
 * The XLS part depends on a server-side executable script.
 *
 * Dependency: webcaosdb
 */
var caosdb_table_export = new function () {

    var logger = log.getLogger("caosdb_table_export");

    /**
     * Hide "Download XLS File" link if the user is not authenticted (i.e.
     * has the `anonymous` role).
     */
    this.init = function() {
        logger.info("init caosdb_table_export");
        if (userIsAnonymous()) {
            $(".caosdb-v-query-select-data-xsl").parent().hide();
        }
    }


    /**
     * Convert the entities of the select-table to a tsv string.
     *
     * @param {boolean} raw - if true, the raw entity ids are put into the
     *     cells. Otherwise, the displayed data is used instead.
     * @return {string}
     */
    this.get_tsv_string = function(raw) {
        const columns = this._get_column_header();
        const entities = this._get_table_content();
        return this._create_tsv_string(entities, columns, raw);
    }

    /**
     * In order to create a valid tsv table, characters that have a special
     * meaning (line break and tab) need to be removed.
     *
     * Both characters will be replaced by a single white space
     *
     * @param {string} raw - the cell content.
     * @return {string} cleaned up content
     */
    this._clean_cell = function(raw) {
        return raw.replace(/\t/g," ").replace(/\n/g," ").replace(/\r/g," ").replace(/\x1E/g," ").replace(/\x15/g," ")
    }

    /**
     * Finds the table in the DOM tree and returns the column names as array
     *
     * @return {string[]}
     */
    this._get_column_header = function() {
        const table = $('.caosdb-select-table');
        return table.find("th").toArray()
            .map(e => caosdb_table_export._clean_cell(e.textContent))
            .filter(e => e.length > 0 && e.toLowerCase() != "id" && e.toLowerCase() != "version");
    }

    /**
     * Finds the table in the DOM tree and returns the table content as array
     *
     * @return {HTMLElement[]}
     */
    this._get_table_content = function() {
        const table = $('.caosdb-select-table');
        // TODO use entity-panel class in table as well (change in query.xsl
        // and then here)
        return table.find("tbody tr").toArray();
    }

    /**
     * Encode tsv string
     *
     * @param {string} main - the tsv table string.
     * @return {string} the same with prefix and URL encoded
     */
    this._encode_tsv_string = function(main) {
        const preamble = "data:text/csv;charset=utf-8,";
        main = encodeURIComponent(main);
        return `${preamble}${main}`;
    }

    /**
     * Convert all entities to an encoded tsv string with the given columns.
     *
     * TODO merge with caosdb_utils.create_tsv_table.
     *
     * @param {HTMLElement[]} entities - entities which are converted to rows
     *     of the tsv string.
     * @param {string[]} columns - array of property names.
     * @param {boolean} raw - if true, the raw entity ids are put into the
     *     cells. Otherwise, the displayed data is used instead.
     * @return {string}
     */
    this._create_tsv_string = function (entities, columns, raw) {
        logger.trace("enter _create_tsv_string ", entities, columns);
        var header = "ID\tVersion\t" + columns.join("\t") + "\n"
        var rows = [];
        for (const table_row of entities) {
            rows.push(caosdb_table_export._get_entity_row(table_row, columns, raw).join("\t"));
        }
        var rows = rows.join("\n");
        logger.trace("leave _create_tsv_string ", rows);
        return `${header}${rows}`;
    }

    /**
     * Return an array of cells, one per column, which contain a string
     * representation of the value of the properties with the same name (as the
     * column).
     *
     * @param {HTMLElement} entity - entity from which the cells are extracted.
     * @param {string[]} columns - array of property names.
     * @param {boolean} raw - if true, the raw entity ids are put into the
     *     cells. Otherwise, the displayed data is used instead.
     * @return {string[]}
     */
    this._get_entity_row = function (entity, columns, raw) {
        var cells = [getEntityID(entity), getEntityVersion(entity)];
        var properties = getProperties(entity);

        for (const column of columns) {
            var cell = "";
            for (const property of properties) {
                if(property.name.toLowerCase() === column.toLowerCase()) {
                    var value = caosdb_table_export
                        ._get_property_value(property.html);
                    if (raw) {
                        cell = value.raw;
                    } else if (value.summary) {
                        cell = value.summary;
                    } else if (value.pretty) {
                        cell = value.pretty;
                    } else {
                        cell = value.raw;
                    }
                }
            }
            cells.push(caosdb_table_export._clean_cell(cell));
        }

        logger.trace("leave _get_entity_row", cells);
        return cells;
    }

    /**
     * Return different string representations of the property's value.
     *
     * Returns an object with three string properties: raw, pretty, summary
     *
     * `raw` is just the raw property value string representation which is
     * returned by the server. In case of list properties, this is a
     * comma-separated list of the raw strings.
     *
     * `pretty` is used only for references and lists of references. It is the
     * string which is generated by the ext-references module as a replacement
     * for the entity id. If there is no such replacement, `pretty` is
     * undefined.
     *
     * `summary` is used only for lists of references. It is the string summary
     * of lists of references which is generated by the ext-references module.
     * If there is no such replacement, `summary` is undefined.
     *
     * @param {HTMLElement} property
     * @return {object}
     */
    this._get_property_value = function(property) {
        const value_element = $(property)
            .find(".caosdb-f-property-value")
            .first();
        const raw_value = value_element
            .find(".caosdb-f-property-single-raw-value")
            .toArray();
        const pretty_value = value_element
            .find(".caosdb-resolve-reference-target")
            .toArray();
        var summary_value = value_element
            .find(".caosdb-resolve-reference-summary")
            .find(".caosdb-f-resolve-reference-summary-plain")
            .toArray();
        if (summary_value.length === 0) {
            summary_value = value_element
                .find(".caosdb-resolve-reference-summary").toArray();
        }

        return {
            "raw": caosdb_table_export._to_string_value(raw_value),
            "pretty": caosdb_table_export._to_string_value(pretty_value),
            "summary": caosdb_table_export._to_string_value(summary_value),
        };
    }

    /**
     * Convert an array of property value elements to string.
     *
     * Empty arrays result in an emtpy string.
     *
     * One-element-arrays result in the text content of the element.
     *
     * N-element-arrays result in the a comma-separated list of the text
     * content of the elements.
     *
     * @param {HTMLElement[]} value_elements
     * @return {string}
     */
    this._to_string_value = function(value_elements) {
        if (value_elements.length === 0) {
            return "";
        } else if (value_elements.length === 1) {
            return $(value_elements[0])
                .text();
        } else {
            return value_elements
                .map(e => $(e).text())
                .join(", ");
        }
    }

    /**
     * Open the resulting xls file by setting href to the location of the resulting
     * file in the server's `Shared` resource and imitate a click.
     */
    this.go_to_script_results = function (filename) {
        window.location.href = connection.getBasePath() + "Shared/" + filename;
    }
}

/**
 * This function is called on click by the link button which says "Download TSV
 * File".
 *
 * It sets the href attribute of the link to a string which gerenates a
 * downloadable file. All entities of the select-table are included in the
 * resulting file.
 */
function downloadTSV(tsv_link) {
    const raw = $("input#caosdb-table-export-raw-flag-tsv").is(":checked");
    var tsv_string = caosdb_table_export.get_tsv_string(raw);
    tsv_string = caosdb_table_export._encode_tsv_string(tsv_string);

    $(tsv_link).attr("href", tsv_string);
    return true;
}
/**
 * This function is called on click by the link button which says "Download XLS
 * File".
 *
 * It calls the server-side script `xls_from_csv.py` and generate a XLS file
 * from TSV string content. All entities of the select-table are included in the
 * resulting file.
 */
async function downloadXLS(xls_link) {
    const onClickValue = xls_link.getAttribute("onClick");
    try {
        // remove click event handler which called this function in the first place
        xls_link.removeAttribute("onClick");

        // add loading info. TODO make an animated one
        $("#downloadModal").find(".caosdb-f-modal-footer-left").append(
            createWaitingNotification("Exporting table...")
        );

        const raw = $("input#caosdb-table-export-raw-flag-xls").is(":checked");
        const tsv_string = caosdb_table_export.get_tsv_string(raw);
        // TODO This existed previously. I do not know why.
        if (!tsv_string) {
            tsv_string = undefined;
        }
        const xls_result = await connection.runScript("xls_from_csv.py",
            {"-p0": {"filename": "selected.tsv", "blob": new Blob([tsv_string], {type: "text/tab-separated-values;charset=utf-8"})}});
        const code = xls_result.getElementsByTagName("script")[0].getAttribute("code");
        if (parseInt(code) > 0) {
            throw ("An error occurred during execution of the server-side script:\n"
                   + xls_result.getElementsByTagName("script")[0].outerHTML);
        }
        const filename = xls_result.getElementsByTagName("stdout")[0].textContent;
        if (filename.length == 0) {
            throw("Server-side script produced no file or did not return the file name: \n"
                  + xls_result.getElementsByTagName("script")[0].outerHTML);
        }

        // trigger download of generated file
        caosdb_table_export.go_to_script_results(filename);


    } catch (e) {
        globalError(e);
    } finally {
        removeAllWaitingNotifications($("#downloadModal")[0]);
        // restore the old click handler - hence a new file is generated with each click.
        xls_link.setAttribute("onClick", onClickValue);
    }

    return true;
}


$(document).ready(function () {
    caosdb_modules.register(caosdb_table_export);
    caosdb_modules.register(ext_file_download);
});
