/*
 * ** header v3.0 This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics, Max-Planck-Institute
 * for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

var fileupload = new function() {

    // TODO * action to config * upload-path id -> class * message configurable
    // * style path input
    const _modal_str = `
<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">File Upload</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <div class="modal-body">
                <form action="${connection.getBasePath()}Entity/" class="dropzone dz-clickable" >
                    <label>path</label>
                    <input id="upload-path" type="text" value="/"/>
                    <div class="dz-message">
                        Drag'n'drop files to this area or click to upload.
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary caosdb-f-file-upload-submit-button">Ok</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>`;

    /** Create a dropzone.js form for the file upload.
     */
    this.create_dropzone = function(form_template, dz_config, path,
        success_handler, error_handler) {
        // TODO return form_element create_file_upload_modal(form_element)
    }


    /** Create a bootstrap 'modal' (a pop-up-like window) which contains the
     * file-upload form element (governed by the third-party JS module
     * 'dropzone.js').
     *
     * The form has field(s) for the files and a `path` field for the target
     * directory of the upload files in the LinkAhead Server's internal file
     * system. The path is set to a default value, initially.
     *
     * The asyncronous success_handler function is an event listener to the
     * 'load' event of the XHR.
     *
     * The asyncronous error_handler function is an event listener to the
     * 'error' event of the XHR. and must accept an event parameter.
     *
     * @param {object} dz_config- the configuration of the dropzone.
     * @param {string} path - the default path.
     * @param {function} success_handler - the async function which handles the
     *     server's respone in case of a 'success' http response.
     * @param {function} error_handler - the async function which handles the
     *     server's respone in case of a 'error' http response.
     * @param {string} atom_par - atomic datatype (not LIST<atom_par>).
     *
     * @return {HTMLElement} the bootstrap 'modal'
     */
    this.create_fileupload_modal = function(dz_config, path, success_handler,
        error_handler, atom_par) {
        var modal = $(_modal_str);
        $(document.body).append(modal);

        var dropzone = new Dropzone(modal.find("form")[0], dz_config);
        modal.find("input#upload-path").val(path);
        dropzone.on("sending", function(e, xhr, formData) {
            // before the request, add the caosdb-specific form field with the
            // properties and so on...
            var request = "<Request>";
            for (const f of dropzone.getUploadingFiles()) {
                request = request + '<File upload="' + f.name + '" name="' + f.name + '" path="' + $("input#upload-path").val() + f.name + '">';
                if (typeof atom_par !== "undefined" && atom_par !== "FILE") {
                    // add parent
                    request = request + '<Parent name="' + atom_par + '" />';
                }
                request = request + '</File>';
            }
            request = request + "</Request>";

            formData.append("FileRepresentation", request);

            // add the success and error handlers 
            xhr.addEventListener("load", success_handler);
            xhr.addEventListener("error", error_handler);
        });

        dropzone.on("queuecomplete", function(e) {
            // after everything is done, close modal and reset dropzone
            modal.modal("hide");
            dropzone.removeAllFiles(true);
        });

        modal.find(".caosdb-f-file-upload-submit-button").on("click",
            // TODO call dropzone.processQueue when the save button of the
            // edit_mode is pressed.
            function(e) {
                dropzone.processQueue();
            });

        return modal[0];
    }

    this.create_error_handler = function(property) {
        var input = $(property).find(".caosdb-f-property-value input");
        return function(event, error, xhr) {
            if (xhr.status == "401") {
                // add error message
                input.after(`<div class="alert alert-danger alert-dismissible"
    role="alert">
    <button type="button" class="btn-close" data-bs-dismiss="alert"
        aria-label="Close"></button>
    <strong>Error!</strong> You are not logged in!.</div>`);
            } else {
                globalError(event, error, xhr);
            }

        }
    }

    /** Create a success handler function for the server's response which
     * appends the returned file elements to the target property.
     *
     * @param {HTMLElement} property - the target property
     *
     * @return {function} a callback function.
     */
    this.create_success_handler = function(property) {

        // get property-value input element (in case of FILE property)
        var input = $(property).find(".caosdb-f-property-value input");
        var set_value = function(entity) {
            input.val(getEntityID(entity));
        }

        if (input.length == 0) {
            // no input means there is drop-down select input instead (this is
            // the case for other REFERENCE properties)
            input = $(property).find(".caosdb-f-property-value select");

            set_value = function(entity) {
                var option = $('<option selected="selected" value="' + getEntityID(entity) + '" >' + getEntityName(entity) + "</option>");
                input.append(option);
            }
        }

        /**
         * Add the inserted files as value to the target property. Remove
         * upload button.
         *
         * @param {Event} e - the 'load' event of the XHR.
         */
        return async function(e) {
            var xhr = e.target;

            var array = await transformation.transformEntities(xhr.responseXML);
            if (array.len > 1) {
                // TODO this olny works for a single entity
                throw new Error("Only single files can be processed. Feel free to make a feature request.");
            }
            var entity = array[0];

            // has errors? 
            if (edit_mode.has_errors(entity)) {
                input.after($(entity).find(".alert"));
                return;
            }

            // remove upload button
            $(property).find(".caosdb-f-file-upload-button").remove();
            set_value(entity);
            input.hide();

            // add success message
            input.after(`<div class="alert alert-success alert-dismissible"
  role="alert">
  <button type="button" class="btn-close" data-bs-dismiss="alert"
    aria-label="Close"></button>
  <strong>Success!</strong>
  The file <code class="caosdb-f-file-upload-file-name">` +
                getEntityName(entity) + `</code> has been uploaded.</div>`);

            input.after(`<a class="btn btn-secondary btn-sm"
  href="` + connection.getEntityUri([getEntityID(entity)]) + `" target= "_blank">` +
                getEntityName(entity) + `</a>`);

        };
    }

    /**
     * Return a file-upload button.
     *
     * @return {HTMLElement} a button element.
     */
    this.create_small_icon_button = function() {
        var button = $('<button class="caosdb-f-file-upload-button btn btn-link navbar-btn" ><i aria-hidden="true" class="bi-upload"></button>');

        return button[0];
    };


    /**
     * Add the file-upload button to the target and append the function (which
     * supposedly toggles the file-upload dialog) as a 'click' event listener.
     *
     * @param {HTMLElement} target - where to append the button.
     * @param {HTMLElement} button - the button for the file-upload.
     * @param {function} func - the click event listener.
     */
    this.add_file_upload_button = function(target, button, func) {
        $(button).on("click", func);
        $(target).append(button);
    }

    /**
     * Stolen from https://gist.github.com/jed/982883
     *
     * LICENCE:
     * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
     *                    Version 2, December 2004
     *
     * Copyright (C) 2011 Jed Schmidt <http://jed.is>
     *
     * Everyone is permitted to copy and distribute verbatim or modified
     * copies of this license document, and changing it is allowed as long
     * as the name is changed.
     *
     *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
     * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
     *
     *   0. You just DO WHAT THE FUCK YOU WANT TO.
     */
    this.uuidv4 = function b(a) {
        return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, b)
    }

    this.init = function() {
        if ("${BUILD_EDIT_MODE_LAZY_DROPDOWN_LOADING}" == "ENABLED") {
            // If we have to manually enable editting of reference
            // properties, we only want to add the file-upload button
            // **after** we started editing the particular
            // property. So no global event listener and not
            // immediatly after adding a new reference property.
            document.body.addEventListener(edit_mode.property_edit_reference_value.type, function(e) {
                fileupload.create_upload_app(e.target);
            }, true);
        } else {
            // add global listener for start_edit event
            document.body.addEventListener(edit_mode.start_edit.type, function(e) {
                $(e.target).find(".caosdb-properties .caosdb-f-entity-property").each(function(idx) {
                    fileupload.create_upload_app(this);
                });
            }, true);

            // add global listener for property_added event
            document.body.addEventListener(edit_mode.property_added.type, function(e) {
                fileupload.create_upload_app(e.target);
            }, true);

        }

        // add global listener for data_type_changed event
        document.body.addEventListener(edit_mode.property_data_type_changed.type, function(e) {
            fileupload.create_upload_app(e.target);
        }, true);

    }

    this.debug = function(message) {
        console.log(message);
    }

    /**
     * Append a button to the target property which will open a file upload
     * dialog. The file upload dialog lets the user choose and upload a file
     * from the her local file system. On succes the id of newly inserted file
     * is inserted into the target properties value field.
     *
     * @param {HTMLElement} target - the target property.
     */
    this.create_upload_app = function(target) {
        const non_file_datatypes = ["TEXT", "DOUBLE", "BOOLEAN", "INTEGER", "DATETIME"];
        var par = getPropertyDatatype(target);
        var atom_par = par && par.startsWith("LIST<") && par.endsWith(">") ? par.substring(5, par.length - 1) : par;

        if (non_file_datatypes.indexOf(atom_par) !== -1) {
            return;
        }
        if (par != atom_par) {
            // TODO implement for LIST<...>
            return;
        }
        var default_path = this.get_default_path();
        var button = this.create_small_icon_button();
        var success_handler = this.create_success_handler(target);
        var error_handler = this.create_error_handler(target);
        var edit_menu = $(target).find(".caosdb-f-property-value")[0];

        var dropzone_config = {
            "maxFiles": 1,
            "autoProcessQueue": false,
        };
        var modal = fileupload.create_fileupload_modal(dropzone_config,
            default_path,
            success_handler,
            error_handler,
            atom_par);
        var toggle_function = function() {
            $(modal).modal("toggle");
        };

        this.add_file_upload_button(edit_menu, button, toggle_function);
    }


    /**
     * Return a default target directory for the files.
     *
     * Pattern: '/uploaded.by/{Realm}/{Username}/{UUIDv4}/'
     * Example: '/uploaded.by/PAM/me/0d1e4241-3185-4300-8825-31f5383088eb/'
     *
     * @return {string} the default target path.
     */
    this.get_default_path = function() {
        var by = "unauthenticated/";
        if (isAuthenticated()) {
            by = getUserRealm() + "/" + getUserName() + "/";
        }
        return "/uploaded.by/" + by + fileupload.uuidv4() + "/";
    }

}

$(document).ready(fileupload.init);
