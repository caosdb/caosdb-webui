/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * form_elements module for reusable form elemenst which already have a basic
 * css styling.
 *
 * IMPORTANT CONCEPTS
 *
 * - FIELD - an HTMLElement which wraps a LABEL element (the fields name) and the
 *   INPUT element together. By default, FIELDS are a flat and stringy list of
 *   key-value pairs.  Use SUBFORMS for nesting FIELDS. FIELDS come in the
 *   following flavours:
 *   - INTEGER
 *   - DOUBLE
 *   - DATE
 *   - RANGE
 *   - REFERENCE_DROP_DOWN
 *
 * - GROUP - a set of FIELDS and SUBFORMS. FIELDS and SUBFORMS can belong to
 * multiple groups. The group can be used to disable or enable a set of FIELDS
 * or SUBFORMS at once. Therefore, GROUPS are mainly used for the look and feel
 * of the form. However, disabled fields are also ignored in the
 * json-conversion of a form.
 *
 * - SUBFORM - an HTMLElement which contains FIELDS and other SUBFORMS. SUBFORMS
 * can be used to nest FIELDS, which is not supported by HTML5 but allows only
 * for flat key-value pairs.
 *
 * @version 0.2
 * @exports form_elements
 */
var form_elements = new function () {
    /**
     * Config for an alert
     *
     * @typedef {object} AlertConfig
     * @property {string} [title] - an optional title for the alert.
     * @property {string} [severity="danger"] - a bootstrap class suffix. Other
     *     examples: warning, info
     * @property {string} message - informs the user what they are about to do.
     * @property {function} proceed_callback - the function which is called
     *     then the user hits the "Proceed" button.
     * @property {function} [cancel_callback] - a callback which is called then
     *     the cancel button is clicked. By default, only the alert is being
     *     closed an nothing happens.
     * @property {string} [proceed_text="Proceed"] - the text on the proceed button.
     * @property {string} [cancel_text="Cancel"] - the text on the cancel button.
     * @property {string} [remember_my_decision_id] - if this parameter is
     *     present, a checkbox is appended to the alert ("Don't ask me
     *     again."). If the checkbox is checked the next time the make_alert
     *     function is called with the same remember_my_decision_id is created,
     *     the alert won't show up and the proceed_callback is called without
     *     any user interaction.
     * @property {string} [remember_my_decision_text="Don't ask me again."] -
     *     label text for the checkbox.
     * @property {HTMLElement} [proceed_button] - an optional custom proceed
     *     button.
     * @property {HTMLElement} [cancel_button] - an optional custom cancel
     *     button.
     */


    /**
     * The configuration for double, integer, date input elements.
     *
     * There are several specializations of this configuration object.
     * {@link ReferenceDropDownConfig}, {@link RangeFieldConfig}, {@link SelectFieldConfig}, {@link FileFieldConfig}
     *
     * @typedef {object} FieldConfig
     *
     * @property {string} name
     * @property {string} type
     * @property {string} [label]
     * @property {string} [help]
     * @property {boolean} [required=false]
     * @property {boolean} [cached=false]
     */

    this.version = "0.1";
    this.dependencies = ["log", "caosdb_utils", "markdown", "bootstrap"];
    this.logger = log.getLogger("form_elements");
    /**
     * Event. On form cancel.
     */
    this.cancel_form_event = new Event("caosdb.form.cancel");
    /**
     * Event. On form submit.
     */
    this.submit_form_event = new Event("caosdb.form.submit");
    /**
     * Event. On field change.
     */
    this.field_changed_event = new Event("caosdb.field.changed");
    /**
     * Event. On field enabled.
     */
    this.field_enabled_event = new Event("caosdb.field.enabled");
    /**
     * Event. On field disabled.
     */
    this.field_disabled_event = new Event("caosdb.field.disabled");
    /**
     * Event. On field ready (e.g. for reference drop downs)
     */
    this.field_ready_event = new Event("caosdb.field.ready");
    /**
     * Event. On field error (e.g. for reference drop downs)
     */
    this.field_error_event = new Event("caosdb.field.error");
    /**
     * Event. Form submitted successfully.
     */
    this.form_success_event = new Event("caosdb.form.success");
    /**
     * Event. Error after form was submitted.
     */
    this.form_error_event = new Event("caosdb.form.error");


    this.get_cache_key = function (form, field) {
        var form_key = $(form).prop("name");
        var field_key = $(field).attr("data-field-name");
        return "form_elements.cache." + form_key + "." + field_key;
    }

    this.get_cache_value = function (field) {
        var ret = $(field)
            .find(":input")
            .val();
        if (!ret) {
            return null;
        } else {
            return ret;
        }
    }

    this.cache_form = function (cache, form) {
        this.logger.trace("enter cache_form", cache, form);
        $(form)
            .find(".caosdb-f-field.caosdb-f-form-field-cached")
            .each(function (index, field) {
                var value = form_elements.get_cache_value(field);
                const key = form_elements.get_cache_key(form, field);
                if (value !== null) {
                    form_elements.logger.trace("cache form field", key, value);
                    cache[key] = value;
                } else {
                    delete cache[key];
                }
            });
    }


    this.set_cached = function (field) {
        $(field).toggleClass("caosdb-f-form-field-cached", true);
    }


    this.set_cached_value = async function (field, value) {
        this.logger.trace("enter set_cached_value", field, value);
        caosdb_utils.assert_html_element(field, "parameter `field`");
        caosdb_utils.assert_string(value, "parameter `value`");
        await form_elements.field_ready(field);
        const old_value = form_elements.get_cache_value(field);

        if (old_value !== value) {
            form_elements.logger.trace("loaded from cache", field, value);
            if (typeof $().selectpicker === "function" &&
                $(field).find(".selectpicker").length > 0) {
                $(field).find(".selectpicker").selectpicker("val", value);
            } else {
                $(field).find(":input").val(value);
            }
            field.dispatchEvent(form_elements.field_changed_event);
        }
    }

    this.is_set = function (field) {
        var value = $(field).find(":input").val();
        return value && value.length > 0;
    }

    this.load_cached = function (cache, form) {
        this.logger.trace("enter load_cached", cache, form);
        $(form)
            .find(".caosdb-f-field.caosdb-f-form-field-cached")
            .each(function (index, field) {
                var key = form_elements.get_cache_key(form, field);
                var value = cache[key] || null;
                if (value !== null) {
                    try {
                        form_elements.set_cached_value(field, value);
                    } catch (err) {
                        form_elements.logger.error(err);
                    }
                }
            });
    }


    this._get_alert_decision = function (key) {
        return localStorage["form_elements.alert_decision." + key];
    }

    this._set_alert_decision = function (key, val) {
        localStorage["form_elements.alert_decision." + key] = val;
    }

    /**
     * Make an alert, that is a dialog which can intercept a function call and
     * asks the user to proceed or cancel.
     *
     * @param {AlertConfig} config
     * @return {HTMLElement}
     */
    this.make_alert = function (config) {
        caosdb_utils.assert_string(config.message, "config param `message`");
        caosdb_utils.assert_type(config.proceed_callback, "function",
            "config param `proceed_callback`");

        // define some defaults.
        const title = config.title ? `<h4>${config.title}</h4>` : "";
        const proceed_text = config.proceed_text || "Proceed";
        const cancel_text = config.cancel_text || "Cancel";
        const severity = config.severity || "danger";
        const remember = !!config.remember_my_decision_id || false;

        // check if alert should be created at all
        if (remember) {
            var result = this._get_alert_decision(config.remember_my_decision_id);
            if (result == "proceed") {
                // call callback asyncronously and return
                (async function () {
                    config.proceed_callback();
                })();
                return undefined;
            }
        }

        // create the alert
        const _alert = $(`<div class="alert alert-${severity}
              alert-dismissible caosdb-f-form-elements-alert" role="alert">${title}
            <p>${config.message}</p>
        </div>`);

        // create the "Don't ask me again" checkbox
        var checkbox = undefined;
        if (remember) {
            const remember_my_decision_text = config.remember_my_decision_text ||
                "Don't ask me again.";
            checkbox = $(`<p class="form-check"><label>
              <input type="checkbox"/> ${remember_my_decision_text}</label></p>`);
            _alert.append(checkbox);
        }


        // create buttons ...
        const cancel_button = config.cancel_button || $(`<button type="button" class="btn btn-secondary caosdb-f-btn-alert-cancel">${cancel_text}</button>`);
        const proceed_button = config.proceed_button || $(`<button type="button" class="btn btn-${severity} caosdb-f-btn-alert-proceed">${proceed_text}</button>`);
        _alert.append($("<p/>").append([proceed_button, cancel_button]));


        // ... and bind callbacks to the buttons.
        cancel_button.click(() => {
            var alert = bootstrap.Alert.getInstance(_alert[0]);
            alert.close()
            if (typeof config.cancel_callback == "function") {
                config.cancel_callback();
            }
        });
        proceed_button.click(() => {
            if (remember && checkbox.find("input[type='checkbox']").is(":checked")) {
                // store this decision.
                form_elements._set_alert_decision(config.remember_my_decision_id,
                    "proceed");
            }
            var alert = bootstrap.Alert.getInstance(_alert[0]);
            alert.close()
            config.proceed_callback();
        });

        new bootstrap.Alert(_alert[0]);

        return _alert[0];
    }


    this.init = function () {
        this.logger.trace("enter init");
    }

    /**
     * Return an OPTION element with entity reference.
     *
     * The OPTION element for a SELECT form input shows a short
     * summary/description of an entity and has the entity's id as value.
     *
     * If the `desc` parameter is undefined, the entity_id is shown
     * instead.
     *
     * @param {string} entity_id - the entity's id.
     * @param {string} [desc] - the description for the entity.
     * @returns {HTMLElement} OPTION element.
     */
    this.make_reference_option = function (entity_id, desc) {
        caosdb_utils.assert_string(entity_id, "param `entity_id`");
        if (typeof desc == "undefined") {
            desc = entity_id;
        }
        return form_elements._make_option(entity_id, desc);
    }

    /**
     * Return an `option` element for a `select`.
     *
     * @param {string} value - the actual value of the option element.
     * @param {string} label - the string which is shown for this option in the
     *     drop-down menu of the select input.
     * @return {HTMLElement}
     */
    this._make_option = function (value, label) {
        const opt_str = '<option value="' + value + '">' + label +
            "</option>";
        return $(opt_str)[0];
    }

    /**
     * (Re-)set this module's functions to standard implementation.
     */
    this._init_functions = function () {

        /**
         * Return SELECT form element with entity references.
         *
         * The OPTIONS' values are generated by the `make_value` call-back
         * function from the entities. If `make_value` is undefined the
         * entities' ids are used as values. The description which is generated
         * by a `make_desc` call-back function. If `make_desc` is undefined,
         * the ids are shown instead.
         *
         * @param {HTMLElement[]} entities - an array with entity elements.
         * @param {function} [make_desc] - a call-back function with one
         *      parameter which is an entity in HTML representation.
         * @param {function} [make_value] - a call-back function with one
         *      parameter which is an entity in HTML representation.
         * @param {boolean} [multiple] - whether the select allows multiple
         *      options to be selected.
         * @param {string} name - the name of the select element 
         * @returns {HTMLElement} SELECT element with entity options.
         */
        this.make_reference_select = async function (entities, make_desc,
            make_value, name, multiple) {
            caosdb_utils.assert_array(entities, "param `entities`", false);
            if (typeof make_desc !== "undefined") {
                caosdb_utils.assert_type(make_desc, "function",
                    "param `make_desc`");
            }
            if (typeof make_value !== "undefined") {
                caosdb_utils.assert_type(make_value, "function",
                    "param `make_value`");
            }
            const ret = $(form_elements._make_select(multiple, name));
            for (let entity of entities) {
                this.logger.trace("add option", entity);
                let entity_id = getEntityID(entity);
                let desc = typeof make_desc == "function" ? await make_desc(entity) :
                    entity_id;
                let value = typeof make_value == "function" ? await make_value(entity) : entity_id;
                let option = this.make_reference_option(value, desc);
                ret.append(option);
            }
            return ret[0];
        }

        /**
         * Return a new select element.
         *
         * This function is mainly used by other factory functions, e.g. {@link
         * make_reference_select} and {@link make_select_input}.
         *
         * @param {boolean} multiple - the `multiple` attribute of the select element.
         * @param {string} name - the name of the select element.
         * @return {HTMLElement}
         */
        this._make_select = function (multiple, name) {
            const ret = $(`<select class="selectpicker form-control" name="${name}" title="Nothing selected"/>`);
            if (typeof name !== "undefined") {
                caosdb_utils.assert_string(name, "param `name`");
                ret.attr("name", name);
            }
            if (multiple) {
                ret.attr("multiple", "");
            } else {
                ret.append('<option style="display: none" selected="selected" value="" disabled="disabled"></option>');
            }
            return ret[0];
        }

        /**
         * Configuration object for a drop down menu for selecting references.
         * `make_reference_drop_down` generates such a drop down menu using a
         * SELECT input with the references as its OPTION elements.
         *
         * The `query` parameter contains a query which is executed. The
         * resulting entities are used to generate the OPTIONs.
         *
         * The OPTIONs' values are generated by the `make_value` call-back
         * function from the entities. If `make_value` is undefined the
         * entities' ids are used as values. The description which is generated
         * by a `make_desc` call-back function. If `make_desc` is undefined,
         * the ids are shown instead.
         *
         * The generated HTMLElements also contain a LABEL tag showing the text
         * defined by `label`. If the `label` property is undefined, the `name`
         * is shown instead.
         *
         * @typedef {option} ReferenceDropDownConfig
         *
         * @augments FieldConfig
         * @property {string} name - The name of the select input.
         * @property {string} query - Query for entities.
         * @property {function} [make_value] - Call-back for the generation of
         *     the OPTIONs' values from the entities.
         * @property {function} [make_desc] - Call-back for the generation of
         *     the OPTIONs' description from the entities.
         * @property {boolean} [multiple] - Whether it is possible to select
         *     multiple options at once.
         * @property {string} [value] - Pre-selected value of the SELECT.
         * @property {string} [label] - LABEL text.
         * @property {string} [type] - This should be "reference_drop_down" or
         *     undefined. This property is used by `make_form_field` to decide
         *     which type of field is to be generated.
         */

        this._query = async function (q) {
            const result = await query(q);
            this.logger.debug("query returned", result);
            return result;
        }

        /**
         * Call a server-side script with the content of the given form and
         * return the results.
         *
         * Note that the form should be one generated by this form_elements
         * module. Otherwise it cannot be guaranteed that the form will be
         * serialized (to json) correctly.
         *
         * @param {string} script - the path of the script
         * @param {HTMLElements} form - a form generated by this module.
         * @return {ScriptingResult} the results of the call.
         */
        this._run_script = async function (script, form) {
            const form_objects = form_elements.form_to_object(form[0]);
            const json_str = JSON.stringify(form_objects[0]);

            // append non-file form fields to the request
            const params = {
                "-p0": {
                    "filename": "form.json",
                    "blob": new Blob([json_str], {
                        type: "application/json"
                    })
                }
            };

            // append files to the request
            const files = form_objects[1];
            for (let i = 0; i < files.length; i++) {
                params[`file_${i}`] = {
                    "filename": `${files[i]["fieldname"]}_${files[i]["filename"]}`,
                    "blob": files[i]["blob"]
                };
            }

            const result = await connection.runScript(script, params);
            this.logger.debug("server-side script returned", result);
            return this.parse_script_result(result);
        }

    }

    /**
     * @typedef {object} ScriptingResult
     * @property {string} code
     * @property {string} call
     * @property {string} stdout
     * @property {string} stderr
     */

    /**
     * Convert the reponse of a server-side scripting call into a {@link
     * ScriptingResult} object.
     *
     * @param {XMLDocument} result
     * @return {ScriptingResult}
     */
    this.parse_script_result = function (result) {
        const scriptNode = result.evaluate("/Response/script", result, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        const code = result.evaluate("@code", scriptNode, null, XPathResult.STRING_TYPE, null).stringValue;

        const call = result.evaluate("call", scriptNode, null, XPathResult.STRING_TYPE, null).stringValue;
        const stderr = result.evaluate("stderr", scriptNode, null, XPathResult.STRING_TYPE, null).stringValue;
        const stdout = result.evaluate("stdout", scriptNode, null, XPathResult.STRING_TYPE, null).stringValue;

        const ret = {
            "code": code,
            "call": call,
            "stdout": stdout,
            "stderr": stderr
        };

        return ret;
    }

    /**
     * Search and retrieve entities and create a SELECT from element.
     *
     * @param {ReferenceDropDownConfig} config - all necessary parameters
     *     for the configuration.
     * @returns {HTMLElement} SELECT element.
     */
    this.make_reference_drop_down = function (config) {
        let ret = $(this._make_field_wrapper(config.name));
        let label = this._make_input_label_str(config);
        let loading = $(createWaitingNotification("loading..."))
            .addClass("caosdb-f-field-not-ready");
        let input_col = $('<div class="caosdb-f-property-value col-sm-9"/>');

        input_col.append(loading);
        this._query(config.query).then(async function (entities) {
            let select = $(await form_elements.make_reference_select(
                entities, config.make_desc, config.make_value, config.name, config.multiple,
                config.value));
            loading.remove();
            input_col.append(select);
            form_elements.init_select_picker(ret[0], config.value);
            ret[0].dispatchEvent(form_elements.field_ready_event);
            select.change(function () {
                ret[0].dispatchEvent(form_elements.field_changed_event);
            });
        }).catch(err => {
            form_elements.logger.error(err);
            loading.remove();
            input_col.append(err);
            ret[0].dispatchEvent(form_elements.field_error_event);
        });

        return ret.append(label, input_col)[0];
    }


    /**
     * Test 16
     */
    this.init_select_picker = function (field, value) {
        caosdb_utils.assert_html_element(field, "parameter `field`");
        const select = $(field).find("select")[0];
        const select_picker_options = {};
        if ($(select).prop("multiple")) {
            select_picker_options["actionsBox"] = true;
        }
        if ($(select).find("option").length > 8) {
            select_picker_options["liveSearch"] = true;
            select_picker_options["liveSearchNormalize"] = true;
            select_picker_options["liveSearchPlaceholder"] = "search...";
        }
        $(select).selectpicker(select_picker_options);
        $(select).selectpicker("val", value);
        this.init_actions_box(field);
    }


    /**
     * Test 17
     */
    this.init_actions_box = function (field) {
        this.logger.trace("enter init_actions_box", field);
        caosdb_utils.assert_html_element(field, "parameter `field`");
        const select = $(field).find("select");
        var actions_box = select.siblings().find(".bs-actionsbox");
        if (actions_box.length === 0) {
            actions_box = $(`<div class="bs-actionsbox">
                        <div class="btn-group btn-group-sm d-grid">
                            <button type="button" class="actions-btn bs-deselect-all btn btn-light">None</button>
                        </div>
                    </div>`)
                .hide();

            select
                .siblings(".dropdown-menu")
                .prepend(actions_box);

            field.addEventListener(
                form_elements.field_changed_event.type,
                (e) => {
                    if (form_elements.is_set(field)) {
                        actions_box.show();
                    } else {
                        actions_box.hide();
                    }
                }, true);

            actions_box
                .find(".bs-deselect-all")
                .click((e) => {
                    select
                        .selectpicker("val", null);
                    select
                        .selectpicker("render");
                    select.dropdown("hide");
                    select[0].dispatchEvent(form_elements.field_changed_event);
                });
        }
    }

    /**
     * Return a promise which resolves with the field when the field is ready.
     *
     * This function is especially useful if the caller can not be sure if
     * the field_ready_event has been dispatched already and the field is
     * ready or if the fields creation is still pending.
     *
     * @param {HTMLElement} field
     * @return {Promise} the field-ready promise
     */
    this.field_ready = function (field) {
        // TODO add support for field name (string) as field parameter
        // TODO check type of param field (not an array!)
        caosdb_utils.assert_html_element(field, "parameter `field`");
        return new Promise(function (resolve, reject) {
            try {
                if (!$(field).hasClass("caosdb-f-field-not-ready") && $(field).find(".caosdb-f-field-not-ready").length === 0) {
                    resolve(field);
                } else {
                    field.addEventListener(form_elements.field_ready_event.type,
                        (e) => resolve(e.target), true);
                }
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * Generate a java script object representation of a form and extract the
     * files from the form.
     *
     * The property names (aka keys) are the names of the form fields and
     * subforms. The values are single strings or arrays of strings. If the
     * field was had a file-type input, the value is a string identifying the
     * file blob which belongs to this key.
     *
     * Subforms lead to nested objects of the same structure.
     *
     * @param {HTMLElement} form - a form generated by this module.
     * @return {object[]} - an array of length 2. The first element is an
     *     object representing the fields of the form. The second contains a
     *     list of file blobs resulting from file inputs in the form.
     */
    this.form_to_object = function (form) {
        this.logger.trace("entity form_to_json", form);
        caosdb_utils.assert_html_element(form, "parameter `form`");

        const _to_json = (element, data, files) => {
            this.logger.trace("enter element_to_json", element, data, files);

            for (const child of element.children) {
                // ignore disabled fields and subforms
                if ($(child).hasClass("caosdb-f-field-disabled")) {
                    continue;
                }
                const name = $(child).attr("name");
                const is_subform = $(child).hasClass("caosdb-f-form-elements-subform");
                if (is_subform) {
                    const subform = $(child).data("subform-name");
                    // recursive
                    var subform_obj = _to_json(child, {}, files)[0];
                    if (typeof data[subform] === "undefined") {
                        data[subform] = subform_obj;
                    } else if (Array.isArray(data[subform])) {
                        data[subform].push(subform_obj);
                    } else {
                        data[subform] = [data[subform], subform_obj]
                    }
                } else if (name && name !== "") {
                    // input elements
                    const not_checkbox = !$(child).is(":checkbox");
                    const is_file = $(child).is("input:file");
                    if (is_file) {
                        var fileList = child.files;
                        if (fileList.length > 0) {
                            for (let i = 0; i < fileList.length; i++) {
                                // generate an identifyer for the file(s) of this input
                                value = name + "_" + fileList[i].name;
                                if (typeof data[name] === "undefined") {
                                    // first and possibly only value
                                    data[name] = value
                                } else if (Array.isArray(data[name])) {
                                    data[name].push(value);
                                } else {
                                    // there is a value present yet - convert to array.
                                    data[name] = [data[name], value]
                                }
                                files.push({
                                    "fieldname": name,
                                    "filename": fileList[i].name,
                                    "blob": fileList[i]
                                });
                            }
                        }
                    } else if (not_checkbox || $(child).is(":checked")) {
                        // checked or not a checkbox
                        var value = $(child).val();
                        if (typeof data[name] === "undefined") {
                            data[name] = value;
                        } else if (Array.isArray(data[name])) {
                            data[name].push(value);
                        } else {
                            data[name] = [data[name], value]
                        }
                    } else {
                        // TODO checkbox
                    }
                } else if (child.children.length > 0) {
                    // recursive
                    _to_json(child, data, files);
                }
            }

            this.logger.trace("leave element_to_json", element, data, files);
            return [data, files];
        };

        const ret = _to_json(form, {}, []);
        this.logger.trace("leave form_to_json", ret);
        return ret;
    }

    this.make_submit_button = function () {
        var ret = $('<button class="caosdb-f-form-elements-submit-button btn btn-primary" type="submit">Submit</button>');
        return ret[0];
    }

    this.make_cancel_button = function (form) {
        var ret = $('<button class="caosdb-f-form-elements-cancel-button btn btn-primary" type="button">Cancel</button>');
        ret.on("click", e => {
            this.logger.debug("cancel form", e, form);
            form.dispatchEvent(this.cancel_form_event);
        });
        return ret[0];
    }

    /**
     * Return a new form field (or a subform).
     *
     * This function is intended to be called by make_form and recursively by
     * other make_* functions which create subforms or other deeper structured
     * form fields.
     *
     * This function also configures the caching, whether a form field is
     * 'required' or not, and the help for each field.
     *
     * @param {FieldConfig} config - the configuration of the form field
     * @return {HTMLElement}
     */
    this.make_form_field = function (config) {
        caosdb_utils.assert_type(config, "object", "param `config`");
        caosdb_utils.assert_string(config.type, "`config.type` of param `config`");

        var field = undefined;
        const type = config.type;
        if (type === "date") {
            field = this.make_date_input(config);
        } else if (type === "file") {
            field = this.make_file_input(config);
        } else if (type === "checkbox") {
            field = this.make_checkbox_input(config);
        } else if (type === "text") {
            field = this.make_text_input(config);
        } else if (type === "double") {
            field = this.make_double_input(config);
        } else if (type === "integer") {
            field = this.make_integer_input(config);
        } else if (type === "range") {
            field = this.make_range_input(config);
        } else if (type === "reference_drop_down") {
            field = this.make_reference_drop_down(config);
        } else if (type === "select") {
            field = this.make_select_input(config);
        } else if (type === "subform") {
            return this.make_subform(config);
        } else {
            throw new TypeError("undefined field type `" + type + "`");
        }

        if (config.required) {
            this.set_required(field);
        }
        if (config.cached) {
            this.set_cached(field);
        }
        if (config.help) {
            this.add_help(field, config.help);
        }

        return field;
    }


    this.add_help = function (field, config) {
        var help_button = $('<a tabindex="0" role="button" data-bs-trigger="focus" data-bs-toggle="popover"><i class="caosdb-f-form-help float-end bi-info-circle-fill"></i></a>')
            .css({
                "cursor": "pointer"
            });

        if (typeof config === "string" || config instanceof String) {
            help_button.attr("data-bs-content", config);
            help_button.popover();
        } else {
            help_button.popover(config);
        }


        var label = $(field).children("label");
        if (label.length > 0) {
            help_button.css({
                "margin-left": "4px"
            });
            label.first().append(help_button);
        } else {
            $(field).append(help_button);
        }
    }

    this.make_heading = function (config) {
        if (typeof config.header === "undefined") {
            return;
        } else if (typeof config.header === "string" || config.header instanceof String) {
            return $('<div class="caosdb-f-form-header h3">' + config.header + '</div>')[0];
        }
        caosdb_utils.assert_html_element(config.header, "member `header` of parameter `config`");
        return config.header;
    }

    this.make_form_wrapper = function (form, config) {
        var wrapper = $('<div class="caosdb-f-form-wrapper"/>');


        var cancel = (e) => {
            form_elements.logger.trace("cancel form", e);
            wrapper.remove();
        };

        wrapper[0].addEventListener(this.cancel_form_event.type, cancel, true);


        var header = this.make_heading(config);
        wrapper.append(header);
        wrapper.append(form);

        return wrapper[0];
    }

    /**
     * Configuration objects which are passed to {@link make_form}.
     *
     * Note: either the `script` or the `name` property must be defined. If the former is defined, the latter will be overriden.
     *
     * @typedef {object} FormConfig
     *
     * @property {FieldConfig[]} fields - array of fields. The order is the
     *     order in which they appear in the resulting form.
     * @property {string} [script] - if present the form will call a
     *     server-side script on submission.
     * @property {string} [name] - The name of the form. This is being
     *     overridden by the `script` parameter if present.
     * @property {function} [submit] - a callback which handles the submission
     *     of the form. This parameter is being overridden if the `script`
     *     parameter is present.
     */

    /**
     * Create a form.
     *
     * The returned element is a container which contains a HTML form element.
     * The fields are ready or they will emit a {@link field_ready_event} when
     * they are.
     *
     * @param {FormConfig} config
     * @return {HTMLElement}
     */
    this.make_form = function (config) {
        var form = undefined;

        if (config.script) {
            form = this.make_script_form(config, config.script);
        } else {
            form = this.make_generic_form(config);
        }
        var wrapper = this.make_form_wrapper(form, config);
        return wrapper;
    }

    /**
     * @typedef {object} SubFormConfig
     *
     * @augments FieldConfig
     * @property {FieldConfig[]} fields - array of fields. The order is the
     *     order in which they appear in the resulting subform.
     */

    /**
     * Return a new subform.
     *
     * @param {SubFormConfig} config - the configuration of the subform.
     * @return {HTMLElement}
     */
    this.make_subform = function (config) {
        this.logger.trace("enter make_subform");
        caosdb_utils.assert_type(config, "object", "param `config`");
        caosdb_utils.assert_string(config.name, "`config.name` of param `config`");
        caosdb_utils.assert_array(config.fields, "`config.fields` of param `config`");

        const name = config.name;
        var form = $('<div data-subform-name="' + name + '" class="caosdb-f-form-elements-subform"/>');

        for (let field of config.fields) {
            this.logger.trace("add subform field", field);
            let elem = this.make_form_field(field);
            form.append(elem);
        }

        this.logger.trace("leave make_subform", form[0]);
        return form[0];
    }

    this.dismiss_form = function (form) {
        if (form.tagName === "FORM") {
            form.dispatchEvent(this.cancel_form_event);
        }
        var _form = $(form).find("form");
        if (_form.length > 0) {
            _form[0].dispatchEvent(this.cancel_form_event);
        }
    }

    this.enable_group = function (form, group) {
        this.enable_fields(this.get_group_fields(form, group));
    }

    this.disable_group = function (form, group) {
        this.disable_fields(this.get_group_fields(form, group));
    }

    this.get_group_fields = function (form, group) {
        return $(form).find(".caosdb-f-field[data-groups*='(" + group + ")']").toArray();
    }

    /**
     * Return an array of field with name
     *
     * @param {string} name - the field name
     * @return {HTMLElement[]} array of fields
     */
    this.get_fields = function (form, name) {
        caosdb_utils.assert_html_element(form, "parameter `form`");
        caosdb_utils.assert_string(name, "parameter `name`");
        return $(form).find(".caosdb-f-field[data-field-name='" + name + "']").toArray();
    }

    this.add_field_to_group = function (field, group) {
        this.logger.trace("enter add_field_to_group", field, group);
        var groups = ($(field).attr("data-groups") ? $(field).attr("data-groups") : "") + "(" + group + ")";
        $(field).attr("data-groups", groups);
    }

    this.disable_fields = function (fields) {
        $(fields).toggleClass("caosdb-f-field-disabled", true).hide();
        $(fields).find(":input").prop("required", false);
        for (const field of $(fields)) {
            field.dispatchEvent(this.field_disabled_event);
        }
    }

    this.enable_fields = function (fields) {
        $(fields).toggleClass("caosdb-f-field-disabled", false).show();
        $(fields).filter(".caosdb-f-form-field-required").find("input.caosdb-f-property-single-raw-value, select.selectpicker").prop("required", true);
        for (const field of $(fields)) {
            field.dispatchEvent(this.field_enabled_event);
        }
    }

    this.enable_name = function (form, name) {
        this.enable_fields($(form).find(".caosdb-f-field[data-field-name='" + name + "']").toArray());
    }

    this.disable_name = function (form, name) {
        this.disable_fields($(form).find(".caosdb-f-field[data-field-name='" + name + "']").toArray());
    }

    this.make_script_form = function (config, script) {
        this.logger.trace("enter make_script_form");

        const submit_callback = async function (form) {
            form = $(form);


            // actually submit the form
            var response = await form_elements._run_script(script, form);
            var result = [];

            if (response.code === "0") {
                // handle success
                result.push(form_elements.make_success_message(response.stdout));
                return result;

            } else {
                // handle scripting error
                result.push(form_elements.make_error_message(response.call));
                result.push(form_elements.make_error_message(response.stderr));
                throw result;
            }
        };

        this.logger.trace("leave make_script_form");
        const new_config = $.extend({}, {
            name: script,
            submit: submit_callback
        }, config);
        return this.make_generic_form(new_config);
    }

    /**
     * Return a generic form, bind the config.submit to the submit event
     * of the form.
     *
     * The `config.fields` array may contain `form_elements.field_config`
     * objects or HTMLElements.
     *
     * @return {HTMLElement}
     */
    this.make_generic_form = function (config) {
        this.logger.trace("enter make_generic_form");

        caosdb_utils.assert_type(config, "object", "param `config`");
        caosdb_utils.assert_string(config.name, "`config.name` of param `config`", true);
        caosdb_utils.assert_array(config.fields, "`config.fields` of param `config`");

        const form = $('<form class="form-horizontal" action="#" method="post" />');

        // set name
        if (config.name) {
            form.attr("name", config.name);
        }

        // add fields
        for (let field of config.fields) {
            this.logger.trace("add field", field);
            if (field instanceof HTMLElement) {
                form.append(field);
            } else {
                let elem = this.make_form_field(field);
                form.append(elem);
            }
        }

        // set groups
        if (config.groups) {
            for (let group of config.groups) {
                this.logger.trace("add group", group);
                for (let fieldname of group.fields) {
                    let field = form.find(".caosdb-f-field[data-field-name='" + fieldname + "']");
                    this.logger.trace("set group", field, group);
                    this.add_field_to_group(field, group.name)

                }
                // disable if necessary
                if (typeof group.enabled === "undefined" || group.enabled) {
                    this.enable_group(form, group.name);
                } else {
                    this.disable_group(form, group.name);
                }
            }
        }

        const footer = this.make_footer();
        form.append(footer);

        if (!(typeof config.submit === 'boolean' && config.submit === false)) {
            // add submit button unless config.submit is false
            footer.append(this.make_submit_button());
        }
        form[0].addEventListener("submit", (e) => {
            e.preventDefault();
            e.stopPropagation();
            if (form.find(".caosdb-f-form-submitting").length > 0) {
                // do not submit twice
                return;
            }

            this.logger.debug("submit form", e);

            form[0].dispatchEvent(this.submit_form_event);

            form.find(":input").prop("disabled", true);
            var submitting = form_elements.make_submitting_info();
            form.find(".caosdb-f-form-elements-footer").before(submitting);


            form[0].addEventListener(this.form_success_event.type, (e) => {
                submitting.remove();
            }, true);
            form[0].addEventListener(this.form_error_event.type, (e) => {
                submitting.remove();
            }, true);


            // remove old messages
            const error_handler = config.error;
            const success_handler = config.success;
            const submit_callback = config.submit;
            form.find(".caosdb-f-form-elements-message").remove();
            if (typeof config.submit === "function") {
                // wrap callback in async function
                const _wrap_callback = async function () {
                    try {
                        var results = await submit_callback(form[0]);

                        // success_handler
                        if (typeof success_handler === "function") {
                            var processed = await success_handler(form[0], results);
                            if (typeof processed !== "undefined") {
                                form_elements.show_results(form[0], processed);
                            }
                        } else {
                            form_elements.show_results(form[0], results);
                        }

                        form[0].dispatchEvent(form_elements.form_success_event);
                    } catch (err) {

                        // error_handler
                        if (typeof error_handler === "function") {
                            var processed = await error_handler(form[0], err);
                            if (typeof processed !== "undefined") {
                                form_elements.show_results(form[0], processed);
                            }
                        } else {
                            form_elements.show_errors(form[0], err);
                        }

                        form[0].dispatchEvent(form_elements.form_error_event);
                    }

                }();
            }
            return false;


        }, true);

        form[0].addEventListener(this.form_success_event.type, function (e) {
            // remove submit button, show ok button
            form.find("button[type='submit']").remove();
            form.find("button:contains('Cancel')").text("Ok").prop("disabled", false);
        }, true);
        form[0].addEventListener(this.form_error_event.type, function (e) {
            // reenable inputs
            form.find(":input").prop("disabled", false);
        }, true);

        // add cancel button
        $(footer).append(this.make_cancel_button(form[0]));

        // init caching for this form
        form_elements.init_form_caching(config, form[0]);

        // init validation
        form_elements.init_validator(form[0]);

        this.logger.trace("leave make_generic_form");
        return form[0];
    }

    this.init_form_caching = function (config, form) {
        var default_config = {
            "cache_event": form_elements.submit_form_event.type,
            "cache_storage": localStorage
        };
        var lconfig = $.extend({}, default_config, config);

        this.logger.trace("init_form_caching", lconfig, form);

        form.addEventListener(lconfig.cache_event, (e) => {
            form_elements.cache_form(lconfig.cache_storage, form);
        }, true);
        form_elements.load_cached(lconfig.cache_storage, form);
    }

    this.show_results = function (form, results) {
        $(form).append(results);
    }

    this.show_errors = function (form, errors) {
        $(form).append(errors);
    }

    this.make_footer = function () {
        return $('<div class="text-end caosdb-f-form-elements-footer"/>')
            .css({
                "margin": "20px",
            }).append(this.make_required_marker())
            .append('<span class="caosdb-f-form-required-label">required field</span>')[0];
    }

    this.make_error_message = function (message) {
        return this.make_message(message, "error");
    }

    this.make_success_message = function (message) {
        return this.make_message(message, "success");
    }

    this.make_submitting_info = function () {
        // TODO styling
        return $(this.make_message("Submitting... please wait. This might take some time.", "info"))
            .toggleClass("h3", true)
            .toggleClass("caosdb-f-form-submitting", true)
            .toggleClass("text-right", true)[0];
    }

    this.make_message = function (message, type) {
        var ret = $('<div class="caosdb-f-form-elements-message"/>');
        if (type) {
            ret.addClass("caosdb-f-form-elements-message-" + type);
        }
        return ret.append(markdown.textToHtml(message))[0];
    }

    /**
     * @typedef {object} RangeFieldConfig
     *
     * @augments FieldConfig
     * @property {FieldConfig} from - the start point of the range. This is
     *     usually an integer or double input field.
     * @property {FieldConfig] to -  the end point of the range. This is
     *     usually an integer or a double input field.
     */

    /**
     * Return a new form field representing a range of numbers.
     *
     * @param {RangeFieldConfig} config
     * @return {HTMLElement}
     */
    this.make_range_input = function (config) {

        // TODO
        // 1. wrapp both inputs to separate it from the label into a container
        // 2. make two rows for each input
        // 3. make inline-block for all included elements
        const from_config = $.extend({}, {
            cached: config.cached,
            required: config.required,
            type: "double"
        }, config.from);
        const to_config = $.extend({}, {
            cached: config.cached,
            required: config.required,
            type: "double"
        }, config.to);

        const from_input = this.make_form_field(from_config);
        $(from_input).toggleClass("form-control", false);

        const to_input = this.make_form_field(to_config);
        $(to_input).toggleClass("form-control", false);

        const ret = $(this._make_field_wrapper(config.name));
        if (config.label) {
            ret.append(this._make_input_label_str(config));
        }

        ret.append(from_input);
        ret.append(to_input);

        // styling
        $(from_input).toggleClass("col-sm-4", true);
        $(to_input).toggleClass("col-sm-4", true);

        return ret[0];
    }

    /**
     * Return a DIV with class `caosdb-f-field` and a data attribute
     * `data-field-name` which contains the name.
     *
     * The DIV is used to wrap LABEL and INPUT elements of a form together.
     *
     * @param {string} name - the name of the field.
     * @returns {HTMLElement} a DIV.
     */
    this._make_field_wrapper = function (name) {
        caosdb_utils.assert_string(name, "param `name`");
        return $('<div class="row caosdb-f-field caosdb-v-field" data-field-name="' + name + '" />')[0];
    }

    /**
     * Return a new date field.
     *
     * @param {FieldConfig} config
     * @return {HTMLElement}
     */
    this.make_date_input = function (config) {
        return this._make_input(config);
    }

    /**
     * @typedef {object} TextFieldConfig
     *
     * Field config for a text field (config.type = 'text').
     *
     * @augments {FieldConfig}
     *
     * @property {string} pattern - a regex pattern which validates the value of the text field.
     */

    /**
     * Return a new text field.
     *
     * @param {TextFieldConfig} config
     * @return {HTMLElement}
     */
    this.make_text_input = function (config) {
        const input = this._make_input(config);
        if (config.pattern) {
            $(input).find("input").attr("pattern", config.pattern);
        }
        return input;
    }


    /**
     * Return an input field which accepts double values.
     *
     * `config.type` is set to "number" and overrides any other type.
     *
     * @param {FieldConfig} config.
     * @returns {HTMLElement} a double form field.
     */
    this.make_double_input = function (config) {
        const _config = $.extend({}, config, {
            type: "number"
        });
        const ret = $(this._make_input(_config))
        if (typeof config.min !== "undefined") {
            ret.find("input").attr("min", config.min);
        }
        if (typeof config.max !== "undefined") {
            ret.find("input").attr("max", config.max);
        }
        ret.find("input").attr("step", "any");
        return ret[0];
    }


    /**
     * Return an input field which accepts integers.
     *
     * `config.type` is set to "number" and overrides any other type.
     *
     * @param {FieldConfig} config.
     * @returns {HTMLElement} an integer form field.
     */
    this.make_integer_input = function (config) {
        var ret = $(this.make_double_input(config));
        ret.find("input").attr("step", "1");
        return ret[0];
    }

    /**
     * @typedef {object} FileFieldConfig
     *
     * @augments FieldConfig
     * @property {boolean} [multiple=false] - whether to accept multiple files.
     * @property {string} [accept] - a comma separated list of file extensions
     *     which are accepted (exclusively).
     */

    /**
     * Return a new form field for a file upload.
     *
     * @param {FileFieldConfig} config - configuration for this form field.
     * @return {HTMLElement}
     */
    this.make_file_input = function (config) {
        const ret = this._make_input(config);
        $(ret)
            .find(":input")
            .prop("multiple", !!config.multiple)
            .css({
                "display": "block"
            });
        if (config.accept) {
            $(ret)
                .find(":input")
                .attr("accept", config.accept);
        }

        return ret;
    }

    /**
     * @typedef {object} SelectOptionConfig
     *
     * @property {string} value - the value of the select option.
     * @property {string} [label] - a visible representation (think:
     *     description) of the value of the select option. defaults to the
     *     value itself.
     */

    /**
     * @typedef {object} SelectFieldConfig
     *
     * @augments {FieldConfig}
     * @property {SelectOptionConfig} - options
     */

    /**
     * Return a select field.
     *
     * IMPORTANT: The select picker has to be initialized by the client by
     * calling ``form_elements.init_select_picker(ret, config.value)`` (see
     * below and https://gitlab.com/linkahead/linkahead-webui/-/issues/208).
     *
     * @param {SelectFieldConfig} config
     * @returns {HTMLElement} a select field.
     */
    this.make_select_input = function (config) {
        const options = config.options;
        const select = $(form_elements._make_select(config.multiple, config.name));

        for (let option of options) {
            select.append(form_elements._make_option(option.value, option.label));
        }
        const ret = form_elements._make_input(config, select[0]);
        // Here, the bootstrap-select features should be activated for the new
        // select element. However, up until now, this only works when the
        // select element is already a part of the dom tree - which is not the
        // case when this method is called and is controlled by the client. So
        // there is currently no other work-around than to call
        // init_select_picker after the form creation explicitely :(
        // form_elements.init_select_picker(ret, config.value);

        return ret;
    }


    /**
     * Return a checkbox input field.
     *
     * @param {form_elements.checkbox_config} config.
     * @returns {HTMLElement} a checkbox form field.
     */
    this.make_checkbox_input = function (config) {
        var clone = $.extend({}, config, {
            type: "checkbox"
        });
        var ret = $(this._make_input(clone));
        ret.find("input:checkbox").prop("checked", false);
        ret.find("input:checkbox").toggleClass("form-control", false);
        if (config.checked) {
            ret.find("input:checkbox").prop("checked", true);
            ret.find("input:checkbox").attr("checked", "checked");
        }
        if (config.value) {
            ret.find("input:checkbox").attr("value", config.value);
        }
        return ret[0];
    }


    /**
     * Add `caosdb-f-form-field-required` class to form field.
     *
     * @param {HTMLElement} field - the required form field.
     */
    this.set_required = function (field) {
        $(field).toggleClass("caosdb-f-form-field-required", true);
        $(field).find(":input").prop("required", true);
        $(field).find("label").prepend(this.make_required_marker());
    }

    /**
     * Return a span which is to be inserted before a field's label text
     * and which marks that field as required.
     *
     * @returns {HTMLElement} span element.
     */
    this.make_required_marker = function () {
        return $('<span class="caosdb-f-form-required-marker">*</span>')[0];
    }


    this.get_enabled_required_fields = function (form) {
        return $(this.get_enabled_fields(form))
            .filter(".caosdb-f-form-field-required")
            .toArray();
    }


    this.get_enabled_fields = function (form) {
        return $(form)
            .find(".caosdb-f-field")
            .filter(function (idx) {
                // remove disabled fields from results
                return !$(this).hasClass("caosdb-f-field-disabled");
            })
            .toArray();
    }


    /**
     * Check form validity and return if the form is valid.
     *
     * @param {HTMLElement} form - the form elemeng.
     * @return {boolean} true iff the form is valid.
     */
    this.check_form_validity = function (form) {
        const is_valid = form.checkValidity();
        form.reportValidity();
        return is_valid;
    }


    this.all_required_fields_set = function (form) {
        const req = form_elements.get_enabled_required_fields(form);
        for (const field of req) {
            if (!form_elements.is_set(field)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param {HTMLElement} form - the form be validated.
     */
    this.is_valid = function (form) {
        return form_elements.all_required_fields_set(form) && form_elements.check_form_validity(form);
    }


    this.toggle_submit_button_form_valid = function (form, submit) {
        // TODO do not change the submit button directly. change the
        // `submittable` state of the form and handle the case where a form
        // is submitting when this function is called.
        if (form_elements.is_valid(form)) {
            $(submit).prop("disabled", false);
        } else {
            $(submit).prop("disabled", true);
        }
    }


    this.init_validator = function (form) {
        const submit = $(form).find(":input[type='submit']")[0];
        if (submit) {
            form.addEventListener("caosdb.field.changed", (e) => form_elements.toggle_submit_button_form_valid(form, submit), true);
            form.addEventListener("caosdb.field.enabled", (e) => form_elements.toggle_submit_button_form_valid(form, submit), true);
            form.addEventListener("input", (e) => form_elements.toggle_submit_button_form_valid(form, submit), true);
        }
    }

    /**
     * Return a modal HTML element containing the given form.
     *
     * @param {HTMLElement} form - the form to be shown in the modal
     * @param {string} title - the title of the form modal
     * @param {string} explanationText - An optional paragraph shown between
     *                                    modal title and form.
     */
    this.make_form_modal = function (form, title, explanationText) {
        const modal = $(`
              <div class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">${title}</h4>
                      <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close">
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>${explanationText}</p>
                    </div>
                  </div>
                </div>`);

        modal.find(".modal-body").append(form);
        return modal[0];
    }

    /**
     * Return a submission button that triggers a given server-side-script.
     *
     * @param {string} script - Name of the server-side script to be triggered
     * @param {string} buttonName - Display name of the submission button
     */
    this.make_scripting_submission_button = function (script, buttonName) {
        let button_name = buttonName || "Submit";
        const scripting_caller = $(`
            <form method="POST" action="${connection.getBasePath()}scripting">
              <input type="hidden" name="call" value="${script}"/>
              <input type="hidden" name="-p0" value=""/>
              <div class="form-group">
                <input type="submit"
                  class="form-control btn btn-primary" value="${button_name}"/>
              </div>
            </form>`);

        return scripting_caller
    }


    /**
     * Return an input and a label, wrapped in a div with class
     * `caosdb-f-field`.
     *
     * @param {object} config - config object with `name`, `type` and
     *      optional `label`
     * @param {string} input - optional specification of the HTML input element.
     *      `<input class="form-control caosdb-f-property-single-raw-value" type="' + type + '" name="' + name + '" />`
     *      is used as default where `name` and `type` stem from the config 
     *      object.
     * @returns {HTMLElement} a form field.
     */
    this._make_input = function (config, input) {
        caosdb_utils.assert_string(config.name, "the name of a form field");
        let ret = $(this._make_field_wrapper(config.name));
        let name = config.name;
        let label = this._make_input_label_str(config);
        let type = config.type || "text";
        let value = config.value;
        const _input = $(input ||
            '<input class="form-control caosdb-f-property-single-raw-value" type="' +
            type + '" name="' + name + '" />');
        _input.change(function () {
            ret[0].dispatchEvent(form_elements.field_changed_event);
        });
        let input_col = $('<div class="caosdb-f-property-value col-sm-9"/>');
        input_col.append(_input);
        if (value) {
            _input.val(value);
        }
        return ret.append(label, input_col)[0];
    }

    /**
     * Return a string representation of a LABEL element, ready for parsing.
     *
     * This function is used by other functions to generate a LABEL element.
     *
     * The config's `name` goes to the `for` attribute, the `label` is the
     * text node of the resulting LABEL element.
     *
     * @param {object} config - a config object with `name` and `label`.
     * @returns {string} a html string for a LABEL element.
     */
    this._make_input_label_str = function (config) {
        let name = config.name;
        let label = config.label;
        return label ? '<label for="' + name +
            '" data-property-name="' + name +
            '" class="col-form-label col-sm-3">' + label +
            '</label>' : "";
    }

    this._init_functions();
}

$(document).ready(function () {
    caosdb_modules.register(form_elements);
});
