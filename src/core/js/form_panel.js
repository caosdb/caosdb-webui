/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

/**
 * form_panel module for creating a panel below the navbar where forms can be
 * placed.
 */
var form_panel = new function() {
    const logger = log.getLogger("form_panel");
    this.version = "0.1";
    this.dependencies = ["log", "caosdb_utils", "markdown", "bootstrap"];

    /**
     * Return a the panel which shall contain the form.
     *
     * Side-effects:
     * 1. Creates the form panel if it does not exist.
     * 2. Removes the welcome panel if present.
     */
    this.get_form_panel = function(panel_id, title) {
        // remove welcome
        $(".caosdb-f-welcome-panel").remove();
        $(".caosdb-v-welcome-panel").remove();

        var existing = $("#" + panel_id);
        if (existing.length > 0) {
            return existing[0];
        }
        const panel = $('<div id="' + panel_id + '" class="caosdb-f-form-panel bg-light container mb-1"/>');
        const header = $('<h2 class="text-center">' + title + '</h2>');
        panel.append(header);

        // add to main panel
        $('nav').after(panel);

        return panel[0];
    };

    /**
     * Remove the form panel from the DOM tree.
     */
    this.destroy_form_panel = function(panel) {
        $(panel).remove();
    };

    /**
     * Creates a callback function that creates the form inside 
     * the form panel when called for the first time.
     *
     * You may supply 'undefined' as form_config and use form_creator
     * instead which is supposed to be a function without argument that
     * return the form to be added to the panel
     *
     * The first input element will be focused unless you specify `false` as
     * fifths parameter.
     */
    this.create_show_form_callback = function(
        panel_id, title, form_config, form_creator = undefined, auto_focus = true
    ) {
        return (e) => {
            logger.trace("enter show_form_panel", e);

            const panel = $(form_panel.get_form_panel(panel_id, title));
            if (panel.find("form").length === 0) {
                if (form_config != undefined && form_creator != undefined) {
                    throw new Error("create_show_form_callback takes either a FormConfig or a function that creates the form");
                }

                var form;
                if (form_config != undefined) {
                    form = form_elements.make_form(form_config);
                } else if (form_creator != undefined) {
                    form = form_creator();
                } else {
                    throw new Error("create_show_form_callback takes a FormConfig or a function that creates the form");
                }
                panel.append(form);
                $(form).find(".selectpicker").selectpicker();

                form.addEventListener("caosdb.form.cancel",
                    (e) => form_panel.destroy_form_panel(panel),
                    true
                );
            }
            if (typeof auto_focus === "undefined" || !!auto_focus === true) {
                if (panel.find("form").length > 0 && panel.find("form")[0].length > 0) {
                    panel.find("form")[0][0].focus();
                } else {
                    logger.debug("create_show_form_callback was called with auto_focus = true (default), but no form is given.");
                }
            }
        }
    };

    this.init = function() {}
}

$(document).ready(function() {
    caosdb_modules.register(form_panel);
});
