/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';

/**
 * Core functionality of the LinkAhead web interface.
 *
 * @module webcaosdb
 * @global
 */

window.addEventListener('error', (e) => globalError(e.error));

var globalError = function (error) {
    var stack = error.stack;

    // ignore this particular error. It is caused by bootstrap-select which is
    // probably misusing the bootstrap 5 API.
    if (error.toString().startsWith("TypeError: this._element is undefined")) {
        if (error && error.stack)
            stack = "" + error.stack;
        if (stack.indexOf("dataApiKeydownHandler") > 0) {
            return;
        }
    }

    var message = "Error! Please help to make LinkAhead better! Copy this message and send it via email to info@indiscale.com.\n\n";
    message += error.toString();

    if (stack) {
        message += '\n' + stack;
    }

    window.alert(message);
    console.log(error);
    throw error;
}

var globalClassNames = new function () {
    this.NotificationArea = "caosdb-preview-notification-area";
    this.WaitingNotification = "caosdb-preview-waiting-notification";
    this.ErrorNotification = "caosdb-preview-error-notification";
}

/**
 * navbar module contains convenience functions for the navbar.
 *
 * @module navbar
 * @global
 */
this.navbar = new function () {

    var logger = log.getLogger("navbar");
    this.logger = logger;
    /**
     * Add a button to the navbar.
     *
     * If the param `button` is a string then a suitable BUTTON element will be
     * created. If the button is otherwise an HTMLElement or an array of
     * HTMLElements it will just be wrapped, styled and appended to the navbar.
     *
     * The optional `options` object knows the following optional keys:
     *     @property {function} callback - a function which will be bound to the
     *         click event of the button.
     *     @property {string} title - the title attribute (a kind of tooltip)
     *         of the button.
     *     @property {HTMLElement} menu - where to append the button. Defaults
     *         to the navbar itself.
     *
     * If the `options["menu"] parameter is not set, the button is appended
     * to the navbar directly and appears left of all previously appended
     * children.
     *
     *
     * @param {String|HTMLElement|HTMLElement[]} button - the button
     * @param {object} [options] - further options
     * @return {HTMLElement} wrapper of the new button
     */
    this.add_button = function (button, options) {
        logger.trace("enter add_button", button, options);

        // assure that button parameter is {String|HTMLElement|HTMLElement[]}
        if (typeof button === "undefined") {
            throw new TypeError("button is expected to be a string, a single HTMLElement or HTMLElement[], was " + typeof button);
        } else if (Array.isArray(button)) {
            for (const element of button) {
                if (!(element instanceof HTMLElement)) {
                    throw new TypeError("button is expected to be a string, a single HTMLElement or HTMLElement[], element was " + typeof element);
                }
            }
        } else if (!(typeof button === "string" || button instanceof String || button instanceof HTMLElement)) {
            throw new TypeError("button is expected to be a string, a single HTMLElement or HTMLElement[], was " + typeof button);
        }

        // default: empty options
        const _options = options || {};

        var button_elem = button;
        if (typeof button === "string" || button instanceof String) {
            // create button element from string
            button_elem = $('<button>' + button + '</button>');
        }

        // set title
        const title = _options["title"];
        if (typeof _options !== "undefined") {
            $(button_elem).attr("title", title);
        }


        // bind click
        const click_callback = _options["callback"]
        if (typeof click_callback === "function") {
            $(button_elem).click(click_callback);
        }

        // wrapp button
        let wrapper = $("<li class='nav-item'></li>").append(button_elem);


        // menu defaults to the navbar
        const menu = _options["menu"] || this.get_navbar();

        logger.debug("add", wrapper, "to", menu);
        $(menu).append(wrapper);

        logger.trace("leave add_button", wrapper[0]);
        return wrapper[0];
    }

    this.init = function () {
        $("nav.navbar-fixed-top")
            .on("shown.bs.collapse", function (e) {
                logger.trace("navbar expands", e);
            })
            .on("invisible.bs.collapse", function (e) {
                logger.trace("navbar shrinks", e);
            });
        this.init_login_show_button();
    }


    /**
     * Initialize the hiding/showing of the login form.
     *
     * If the viewport is xs (width <= 768px) the login form is hidden in the
     * a menu anyways.
     *
     * If the viewport is greater then the folloging applies:
     *
     * When loading the page, the login form is hidden behind a "Login" button.
     * When the user clicks the Login button the form appears and the other
     * button disappears. After a timeout of 10 seconds of inactivity the form
     * hides again. When the user starts typing, the timeout is canceled.
     *
     * If the user leaves the input fields ("blur" event) the timeout is
     * reestablished and the form hides after 20 seconds.
     */
    this.init_login_show_button = function () {
        const form = $("#caosdb-f-login-form");
        const show_button = $("#caosdb-f-login-show-button");
        var timeout = undefined;

        // show form and hide the show_button
        const _in = () => {
            // xs means viewport <= 768px
            form.removeClass("d-none");
            form.addClass("d-xs-inline-block");
            show_button.removeClass("d-inline-block");
            show_button.addClass("d-none");
        }
        // hide form and show the show_button
        const _out = () => {
            // xs means viewport <= 768px
            form.removeClass("d-xs-inline-block");
            form.addClass("d-none");
            show_button.removeClass("d-none");
            show_button.addClass("d-inline-block");
        }
        show_button.on("click", () => {
            // show form...
            _in();

            // and hide it after ten seconds if nothing happens
            timeout = setTimeout(_out, 10000)
        });
        form.find("input,button").on("blur", () => {
            if (timeout) {
                // cancel existing timeout (e.g. from showing)
                clearTimeout(timeout);
            }
            // hide after 20 seconds if nothing happens
            timeout = setTimeout(_out, 20000)
        });
        form.find("input,button").on("input", () => {
            // something happens!
            if (timeout) {
                clearTimeout(timeout);
            }
        });
    }


    /**
     * Create initially empty tool box dropdown and append to navbar.
     *
     * The returned element is the drop-down menu which will eventually contain
     * the tool buttons. That means, the buttons can be added directly to the
     * returned element to appear in the drop-down menu.
     *
     * @return {HTMLElement} the dropdown-menu.
     */
    this.init_toolbox = function (name) {
        var button = $(`<a class="nav-link dropdown-bs-toggle"
            data-bs-toggle="dropdown" href="#">${name}
            </a>`)[0];

        var menu = $(`<ul
            class="caosdb-v-navbar-toolbox
                caosdb-f-navbar-toolbox
                dropdown-menu"
            data-toolbox-name="${name}"/>`)[0];

        const wrapper = this.add_button([button, menu]);
        $(wrapper).toggleClass("dropdown", true)
            .children()
            .toggleClass("btn", false)
            .toggleClass("btn-link", false)
            .toggleClass("navbar-btn", false);

        return menu;
    }

    /**
     * Add a tool to a toolbox.
     *
     * If the button is a string a new button element is created showing the
     * string as its label. Otherwise the button should be an HTMLElement which
     * is directly added to the toolbox.
     * toolbox.
     *
     * The `callback` is a function which will be bound to the click event of
     * the button.
     *
     * The passed or created button is wrapped in a LI element and added to the
     * toolbox. The wrapper of the button is returned
     *
     * The optional `options` object knows the following optional keys:
     *     @property {function} callback - a function which will be bound to the
     *         click event of the button.
     *     @property {string} title - the title attribute (a kind of tooltip)
     *         of the button.
     *
     * @param {string|HTMLElement} button
     * @param {string} [toolbox] - the name of the toolbox
     * @param {object} [options] further options.
     * @return {HTMLElement} the button wrapper.
     */
    this.add_tool = function (button, toolbox, options) {
        const toolbox_element = this.get_toolbox(toolbox);

        // put toolbox_element as menu into the options for `add_button`
        const _options = $.extend({
            menu: toolbox_element
        }, options);

        const wrapper = this.add_button(button, _options);
        return wrapper;
    }

    this.get_navbar = function () {
        return $('#top-navbar')
            .find("ul.caosdb-navbar")[0];
    }

    this.get_toolbox = function (name) {
        var toolbox = $(this.get_navbar()).find(".caosdb-f-navbar-toolbox[data-toolbox-name='" + name + "']");
        if (toolbox.length > 0) {
            return toolbox[0];
        } else {
            return this.init_toolbox(name);
        }
    }

    /**
     * Hide the given element for certain roles
     *
     * @param {string|object} element - The HTML element to hide. May be either
     *     a string to be used in a jquery, or the result of a jquery.
     * @param {array} roles - List of roles for which the given element is
     *     hidden.
     */
    this.hideElementForRoles = function (element, roles) {
        var elt = element;
        if (typeof (element) === "string") {
            elt = $(elt);
        }
        const userRoles = getUserRoles();
        if (userRoles.some(role => roles.includes(role))) {
            elt.addClass("d-none");
        } else {
            elt.removeClass("d-none");
        }
    }

}


/**
 * @module caosdb_utils
 * @global
 */
this.caosdb_utils = new function () {
    this.assert_string = function (obj, name, optional = false) {
        if (typeof obj === "undefined" && optional) {
            return obj;
        }
        if (typeof obj == "string" || obj instanceof String) {
            return obj;
        }
        throw new TypeError(name + " is expected to be a string, was " + typeof obj);
    }

    this.assert_type = function (obj, type, name, optional = false) {
        if (typeof obj === "undefined" && optional) {
            return obj;
        }
        if (typeof obj !== type) {
            throw new TypeError(name + " is expected to be a " + type + ", was " + typeof obj);
        }
        return obj;
    }

    this.assert_not_undefined = function (obj, name) {
        if (typeof obj == "undefined" || obj == null) {
            throw new TypeError(name + " must not be undefined")
        }
        return obj;
    }

    this.assert_html_element = function (obj, name) {
        if (typeof obj === "undefined" || !(obj instanceof HTMLElement)) {
            throw new TypeError(name + " is expected to be an HTMLElement, was " + typeof obj);
        }
        return obj;
    }

    this.assert_array = function (obj, name, wrap_if_not_array) {
        if (Array.isArray(obj)) {
            return obj;
        } else if (wrap_if_not_array) {
            return [obj];
        }
        throw new TypeError(name + " is expected to be an array, was " + typeof obj);
    }

    /**
     * Create a tsv table as string.
     *
     * The data must be appropriately encoded (e.g. urlencoded).
     *
     * With `tab=","` it is also possible to create csv tables.
     *
     * @param {string[][]} data - An array of rows which contain arrays of
     *     cells.
     * @param {string} [preamble="data:text/csv;charset=utf-8,"] - a prefix for
     *     the the resulting string. The default is suitable for creating
     *     downloadable href attributes of links.
     * @param {string} [tab="%09"] - the cell separator.
     * @param {string} [newline="%0A"] - the row separator.
     * @return {string} a tsv table as a string.
     */
    this.create_tsv_table = function (data, preamble, tab, newline) {
        preamble = ((typeof preamble == 'undefined') ? "data:text/csv;charset=utf-8," : preamble);
        tab = tab || "%09";
        newline = newline || "%0A";
        const rows = data.map(x => x.join(tab))
        return `${preamble}${rows.join(newline)}`;
    }
}

/**
 * connection module contains all ajax calls.
 *
 * @module connection
 * @global
 */
this.connection = new function () {
    const logger = log.getLogger("connection");

    this._init = function () {
        /**
         * Send a get request.
         */
        this.get = async function _get(uri) {
            if (typeof uri === "undefined" || uri === "undefined") {
                throw new Error("uri was undefined");
            }
            try {
                return await $.ajax({
                    url: connection.getBasePath() + uri,
                    dataType: "xml",
                });
            } catch (error) {
                if (error.status == 414) {
                    throw new Error("UriTooLongException for GET " + uri);
                } else if (error.status == 0) {
                    logger.error(error);
                } else if (error.status != null) {
                    throw new Error("GET " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a put (i.e. update) request.
         */
        this.put = async function _put(uri, data) {
            try {
                return await $.ajax({
                    url: connection.getBasePath() + uri,
                    method: 'PUT',
                    dataType: "xml",
                    processData: false,
                    data: xml2str(data),
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status == 0) {
                    logger.error(error);
                } else if (error.status != null) {
                    throw new Error("PUT " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Run a script in the backend.
         * @scriptname is the name of the scriptfile without leading scriptpath, e.g. test.py.
         * @params is an associative array with keys being the options and values being the values.
         */
        this.runScript = async function _runScript(scriptname, params) {
            var formData = new FormData();
            formData.append("call", scriptname);
            for (var key in params) {
                if (params[key]["filename"]) {
                    formData.append(key, params[key]["blob"], params[key]["filename"]);
                } else {
                    formData.append(key, params[key]);
                }
            }
            try {
                return await $.ajax({
                    url: connection.getBasePath() + "scripting",
                    method: 'POST',
                    dataType: "xml",
                    contentType: false,
                    processData: false,
                    data: formData,
                });
            } catch (error) {
                if (error.status == 0) {
                    logger.error(error);
                } else if (error.status != null) {
                    throw new Error(
                        "POST scripting returned with HTTP status " + error.status +
                        " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a post (i.e. insert) request.
         */
        this.post = async function _post(uri, data) {
            try {
                return await $.ajax({
                    url: connection.getBasePath() + uri,
                    method: 'POST',
                    dataType: "xml",
                    processData: false,
                    data: xml2str(data),
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status == 0) {
                    logger.error(error);
                } else if (error.status != null) {
                    throw new Error("POST " + uri + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Send a delete request.
         * idline: 124&256
         */
        this.deleteEntities = async function _deleteEntities(idline) {
            try {
                return await $.ajax({
                    url: connection.getBasePath() + "Entity/" + idline,
                    method: 'DELETE',
                    dataType: "xml",
                    processData: false,
                    data: "",
                    contentType: 'text/xml',
                });
            } catch (error) {
                if (error.status == 0) {
                    logger.error(error);
                } else if (error.status != null) {
                    throw new Error("DELETE " + "Entity/" + idline + " returned with HTTP status " + error.status + " - " + error.statusText);
                } else {
                    throw error;
                }
            }
        }

        /**
         * Return the base path of the server.
         */
        this.getBasePath = function () {
            var base = window.location.origin + "/";
            if (typeof window.sessionStorage.caosdbBasePath !== "undefined") {
                base = window.sessionStorage.caosdbBasePath;
            }
            return base;
        }

        /**
         * Return the root path of the file system resource.
         */
        this.getFileSystemPath = function () {
            return connection.getBasePath() + "FileSystem/";
        }

        /**
         * Return a full URI for these entities.
         *
         * @param {string[]|number[]} ids
         * @return {string} the URI.
         */
        this.getEntityUri = function (ids) {
            return this.getBasePath() + transaction.generateEntitiesUri(ids);
        }
    }
    this._init();
}

/**
 * transformation module contains all code for tranforming xml into html via
 * xslt.
 *
 * @module transformation
 * @global
 */
this.transformation = new function () {
    /**
     * remove all permission information from a server's response.
     *
     * @param {XMLDocument} xml
     * @return {XMLDocument} without <Permissions> tags.
     */
    this.removePermissions = function (xml) {
        $(xml).find('Permissions').remove();
        return xml
    }

    /**
     * Retrieve a certain xslt script by name.
     *
     * @param {String} name
     * @return {XMLDocument} xslt document.
     */
    this.retrieveXsltScript = async function _rXS(name) {
        return await connection.get("webinterface/${BUILD_NUMBER}/xsl/" + name);
    }

    /**
     * Transform the server's response with multiple entities into their
     * html representation. The parameter `xml` may also be a Promise.
     *
     * @async
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformEntities = async function _tME(xml) {
        let xsl = await transformation.retrieveEntityXsl();
        let html = await asyncXslt(xml, xsl);
        return $(html).find('div.root > div.caosdb-entity-panel').toArray();
    }

    /**
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformParent = async function _tME(xml) {
        var xsl = await transformation.retrieveXsltScript("parent.xsl");
        insertParam(xsl, "entitypath", connection.getBasePath() + "Entity/");
        // TODO the following line should not have any effect. nevertheless: it
        // does not work without
        xsl = str2xml(xml2str(xsl));
        let html = await asyncXslt(xml, xsl);
        return html;
    }

    /**
     * @param {XMLDocument} xml
     * @return {HTMLElement[]} an array of HTMLElements.
     */
    this.transformProperty = async function _tME(xml) {
        var xsl = await transformation.retrieveXsltScript("property.xsl");
        insertParam(xsl, "filesystempath", connection.getBasePath() + "FileSystem/");
        insertParam(xsl, "entitypath", connection.getBasePath() + "Entity/");
        var entityXsl = await transformation.retrieveXsltScript('entity.xsl');
        var messageXsl = await transformation.retrieveXsltScript('messages.xsl');
        var commonXsl = await transformation.retrieveXsltScript('common.xsl');
        var xslt = transformation.mergeXsltScripts(xsl, [messageXsl, commonXsl, entityXsl]);
        let html = await asyncXslt(xml, xslt);
        return html;
    }

    /**
     * Retrieve the entity.xsl script and modify it such that we can use it
     * without the context of the main.xsl.
     *
     * @param {string} [root_template] a string to be injected as a root
     *     template. Optional. If none is given a default template is injected
     *     which calls the entity templates which perform the transformation of
     *     xml entities to the canocical HTML representation.
     * @return {XMLDocument} xslt script
     */
    this.retrieveEntityXsl = async function _rEX(root_template) {
        const _root = root_template || '<xsl:template match="/" xmlns="http://www.w3.org/1999/xhtml"><div class="root"><xsl:apply-templates select="Response/child::*" mode="entities"/></div></xsl:template>';
        var entityXsl = await transformation.retrieveXsltScript("entity.xsl");
        var commonXsl = await transformation.retrieveXsltScript("common.xsl");
        var errorXsl = await transformation.retrieveXsltScript('messages.xsl');
        var xslt = transformation.mergeXsltScripts(entityXsl, [errorXsl, commonXsl]);
        insertParam(xslt, "filesystempath", connection.getBasePath() + "FileSystem/");
        insertParam(xslt, "entitypath", connection.getBasePath() + "Entity/");
        xslt = injectTemplate(xslt, _root);
        return xslt;
    }

    /**
     * Merges several xsl style sheets into one. All template rules are
     * appended to the main style sheet.
     *
     * @param {XMLDocument} xslMain, the main style sheet
     * @param {XMLDocument[]} xslIncludes, array of style sheets which are to
     *          be included.
     * @return {XMLDocument} a new style sheets with all template rules;
     */
    this.mergeXsltScripts = function (xslMain, xslIncludes) {
        let ret = getXSLScriptClone(xslMain);
        for (var i = 0; i < xslIncludes.length; i++) {
            $(xslIncludes[i].firstElementChild).find('xsl\\:template').each(function (index) {
                $(ret.firstElementChild).append(this);
            });
        }
        return ret;
    }
}

/**
 * transaction module contains all code for insertion, update and deletion of 
 * entities. Currently, only updates are implemented.
 *
 * @module transaction
 * @global
 */
this.transaction = new function () {
    this.classNameUpdateForm = "caosdb-update-entity-form";

    /**
     * Retrieve a single entity and return its XML representation.
     * 
     * TODO merge with retrieve() in caosdb.js
     *
     * @param {String} entityId
     * @return {Element} an xml element.
     */
    this.retrieveEntityById = async function _rEBI(entityId) {
        let entities = await this.retrieveEntitiesById([entityId]);
        return entities[0];
    }

    /**
     * Retrieve multiple entities and return their XML representation.
     *
     * @param {String[]} entityIds, array of IDs.
     * @return {Element[]} array of xml elements.
     */
    this.retrieveEntitiesById = async function _rEBIs(entityIds) {
        const response = await connection.get(this.generateEntitiesUri(entityIds));
        return $(response).find('Response > [id]').toArray();
    }

    /** Sends a PUT request with an xml representation of entities and
     * returns the xml document with the server response.
     * 
     * @param {XMLDocument} xml, the entity elements surrounded by an
     *          <Update> tag.
     * @return {XMLDocument} the server response.
     */
    this.updateEntitiesXml = async function _uE(xml) {
        return await connection.put("Entity/", xml);
    }

    /** Sends a POST request with an xml representation of entities and
     * returns the xml document with the server response.
     * 
     * @param {XMLDocument} xml, the entity elements surrounded by an
     *          <Insert> tag.
     * @return {XMLDocument} the server response.
     */
    this.insertEntitiesXml = async function _iE(xml) {
        return await connection.post("Entity/", xml);
    }

    this.deleteEntities = async function _dE(idlist) {
        return await connection.deleteEntities(idlist.join("&"));
    }

    /**
     * Generate the URI for the retrieval of a list of entities.
     *
     * TODO merge this with connection.getEntityUri
     *
     * @param {String[]} entityIds - An array of entity ids..
     * @return {String} The uri.
     */
    this.generateEntitiesUri = function (entityIds) {
        return "Entity/" + entityIds.join("&");
    }

    /**
     * Submodule for update transactions.
     */
    this.update = new function () {
        /**
         * Create a form for updating entities. It has only a textarea and a
         * submit button.
         *
         * The 'entityXmlStr' contains the entity in it's current (soon-to-be
         * old) version which can then be modified by the user.
         *
         * The 'putCallback' is a function which accepts one parameter, a
         * string representation of an xml document.
         *
         * @param {String} entityXmlStr, the old entity
         * @param {function} putCallback, the function which sends a put request.
         */
        this.createUpdateForm = function (entityXmlStr, putCallback) {
            // check the parameters
            if (putCallback == null) {
                throw new Error("putCallback function must not be null.");
            }
            if (typeof putCallback !== "function" || putCallback.length !== 1) {
                throw new Error("putCallback is to be a function with one parameter.");
            }
            if (entityXmlStr == null) {
                throw new Error("entityXmlStr must not be null");
            }

            // create the form element by element
            let textarea = $('<div class="form-control"><textarea rows="8" style="width: 100%;" name="updateXml"/></div>');
            textarea.find('textarea').val(entityXmlStr);
            let submitButton = $('<button class="btn btn-secondary" type="submit">Update</button>');
            let resetButton = $('<button class="btn btn-secondary" type="reset">Reset</button>');
            let form = $('<form class="card-body"></form>');
            form.toggleClass(transaction.classNameUpdateForm, true);
            form.append(textarea);
            form.append(submitButton);
            form.append(resetButton);

            // reset restores the original xmlStr
            form.on('reset', function (e) {
                textarea.find('textarea').val(entityXmlStr);
                return false;
            });

            // submit calls the putCallback
            form.submit(function (e) {
                putCallback(e.target.updateXml.value);
                return false;
            });

            return form[0];
        }

        /**
         * Start the update of a single entity. Returns a state machine which 
         * 1) removes entity from page. show waiting notification instead. 
         * 2) retrieves the old version's xml
         * 3) shows update form with old xml
         * 4) submits new xml and handles success and failure of the update. 
         *
         * @param {HTMLElement} entity, the div which represent the entity.
         * @return {Object} a state machine.
         */
        this.updateSingleEntity = function (entity) {
            let updatePanel = transaction.update.createUpdateEntityPanel(transaction.update.createUpdateEntityHeading($(entity).find('.caosdb-entity-panel-heading')[0]));
            var app = new StateMachine({
                transitions: [{
                    name: "init",
                    from: 'none',
                    to: "waitRetrieveOld"
                }, {
                    name: "openForm",
                    from: ['waitRetrieveOld', 'waitPutEntity'],
                    to: 'modifyEntity'
                }, {
                    name: "submitForm",
                    from: 'modifyEntity',
                    to: 'waitPutEntity'
                }, {
                    name: "showUpdatedEntity",
                    from: 'waitPutEntity',
                    to: 'final'
                }, {
                    name: "resetApp",
                    from: '*',
                    to: 'final'
                }, ],
            });
            app.errorHandler = function (fn) {
                try {
                    fn();
                } catch (e) {
                    setTimeout(() => app.resetApp(e), 1000);
                }
            }
            app.onInit = function (e, entity) {
                // remove entity
                $(entity).hide();
                app.errorHandler(() => {
                    // show updatePanel instead
                    $(updatePanel).insertBefore(entity);
                    // create and add waiting notification
                    updatePanel.appendChild(transaction.update.createWaitRetrieveNotification());
                    let entityId = getEntityID(entity);
                    transaction.update.retrieveOldEntityXmlString(entityId).then(xmlstr => {
                        app.openForm(xmlstr);
                    }, err => {
                        app.resetApp(err);
                    });
                });
                // retrieve old xml, trigger state change when response is ready
            };
            app.onOpenForm = function (e, entityXmlStr) {
                app.errorHandler(() => {
                    // create and show Form
                    let form = transaction.update.createUpdateForm(entityXmlStr, (xmlstr) => {
                        app.submitForm(xmlstr);
                    });
                    updatePanel.append(form);
                });
            };
            app.onResetApp = function (e, error) {
                $(entity).show();
                $(updatePanel).remove();
                if (error != null) {
                    globalError(error);
                }
            };
            app.onShowUpdatedEntity = function (e, newentity) {
                // remove updatePanel
                updatePanel.remove();
                // show new version of entity
                $(newentity).insertBefore(entity);
                // remove old version
                $(entity).remove();
            };
            app.onSubmitForm = function (e, xmlstr) {
                // remove form
                $(updatePanel).find('form').remove();

                $(updatePanel).find('.' + globalClassNames.ErrorNotification).remove();

                // send HTTP PUT to update entity
                app.errorHandler(() => {
                    transaction.updateEntitiesXml(str2xml('<Update>' + xmlstr + '</Update>')).then(
                        xml => {
                            if ($(xml.firstElementChild).find('Error').length) {
                                // if there is an <Error> tag in the response, show
                                // the response in a new form.
                                app.openForm(xml2str(xml));
                                transaction.update.addErrorNotification($(updatePanel).find('.card-header'), transaction.update.createErrorInUpdatedEntityNotification());
                            } else {
                                // if there are no errors show the XSL-transformed
                                // updated entity.
                                transformation.transformEntities(xml).then(entities => {
                                    app.showUpdatedEntity(entities[0]);
                                }, err => {
                                    // errors from the XSL transformation
                                    app.resetApp(err);
                                });
                            }
                        }, err => {
                            // errors from the HTTP PUT request
                            app.resetApp(err);
                        }
                    );
                });
            };
            app.onLeaveWaitPutEntity = function () {
                // remove waiting notifications
                removeAllWaitingNotifications(updatePanel);
            };
            app.onLeaveWaitRetrieveOld = app.onLeaveWaitPutEntity;

            app.init(entity);
            app.updatePanel = updatePanel;

            let closeButton = transaction.update.createCloseButton('.card', () => {
                app.resetApp();
            });
            $(updatePanel).find('.card-header').prepend(closeButton);
            return app;
        }

        /**
         * Retrieves an entity and returns it's untransformed xml representation.
         *
         * @param {String} entityId, an entity id.
         * @return {String} the xml of the entity.
         */
        this.retrieveOldEntityXmlString = async function _retrieveOEXS(entityId) {
            let xml = await transaction.retrieveEntityById(entityId);;
            return xml2str(transformation.removePermissions(xml));
        }

        this.createWaitRetrieveNotification = function () {
            return createWaitingNotification("Retrieving xml and loading form. Please wait.");
        }

        this.createWaitUpdateNotification = function () {
            return createWaitingNotification("Sending update to the server. Please wait.");
        }

        this.createErrorInUpdatedEntityNotification = function () {
            return createErrorNotification("The update was not successful.");
        }

        this.addErrorNotification = function (elem, err) {
            $(elem).append(err);
            return elem;
        }

        /**
         * Create a panel where the waiting notifications and the update form
         * is to be shown. This panel will be inserted before the old entity.
         *
         * @param {HTMLElement} heading, the heading of the panel.
         * @return {HTMLElement} A div.
         */
        this.createUpdateEntityPanel = function (heading) {
            let panel = $('<div class="card" style="border-color: blue;"/>');
            panel.append(heading);
            return panel[0];
        };

        /**
         * Create a heading for the update panel from the heading of an entity.
         * Basically, this copies the heading, removes everything but the first
         * element, and adds a span that indicates that one is currently
         * updating something.
         *
         * @param {HTMLElement} entityHeading, the heading of the entity.
         * @return {HTMLElement} the heading for the update panel.
         */
        this.createUpdateEntityHeading = function (entityHeading) {
            let heading = entityHeading.cloneNode(true);
            let update = $('<span><h3>Update</h3></span>')[0];
            $(heading).children().slice(1).remove();
            $(heading).prepend(update);
            $(heading).find('.caosdb-backref-link').remove();
            return heading;
        }

        /**
         * Get the heading from an entity panel.
         *
         * @param {HTMLElement} entityPanel, the entity panel.
         * @return {HTMLElement} the heading.
         */
        this.getEntityHeading = function (entityPanel) {
            return $(entityPanel).find('.caosdb-entity-panel-heading')[0];
        }

        this.initUpdate = function (button) {
            transaction.update.updateSingleEntity(
                $(button).closest('.caosdb-entity-panel')[0]
            );
        }

        this.createCloseButton = function (close, callback) {
            let button = $('<button title="Cancel update" class="btn btn-link btn-close" aria-label="Cancel update">&times;</button>');
            button.bind('click', function () {
                $(this).closest(close).hide();
                callback();
            });
            return button[0];
        }
    }
}

/**
 * @module paging
 * @global
 */
var paging = new function () {

    this.defaultPageLen = 10;
    /**
     * It reads the current page from the current uri's query segment (e.g.
     * '?P=0L10') and sets the correct links of the PREV/NEXT buttons or
     * hides them.
     * 
     * This is called in main.xsl
     * 
     * @param totalEntities,
     *            the total number of entities in a response (which is >=
     *            the number of entities which are currently shown on this
     *            page.
     */
    this.initPaging = function (href, totalEntities) {

        if (totalEntities == null) {
            return false;
        }

        // get links for the next/prev page
        var oldPage = paging.getPSegmentFromUri(href);
        if (oldPage == null) {
            // no paging is to be done -> this is a success after all.
            return true;
        }

        var nextHref = paging.getPageHref(href, paging.getNextPage(oldPage, totalEntities));
        var prevHref = paging.getPageHref(href, paging.getPrevPage(oldPage));

        if (nextHref != null) {
            // set href and show next button
            $('.caosdb-next-button').attr("href", nextHref);
        }
        if (prevHref != null) {
            // set href and show prev button
            $('.caosdb-prev-button').attr("href", prevHref);
        }

        paging.toggle_paging_panel(!!prevHref || !!nextHref);

        return true;

    }

    this.toggle_paging_panel = function (on) {
        $(".caosdb-f-main").toggleClass("caosdb-f-show-paging-panel", on);
    }

    /**
     * Replace the old page string in the given uri or concat it if there was no
     * page string. If page is null return null.
     * 
     * @param uri_old,
     *            the old uri
     * @param page,
     *            the page the new uri shall point to
     * @return a string uri which points to the page denotes by the parameter
     */
    this.getPageHref = function (uri_old, page) {
        if (uri_old == null) {
            throw new Error("uri was null.");
        }
        if (page == null) {
            return null;
        }

        // remove fragment
        uri_old = uri_old.split("#")[0];

        var pattern = /(\?(.*&)?)P=([^&]*)/;
        if (pattern.test(uri_old)) {
            // replace existing P=...
            return uri_old.replace(pattern, "$1P=" + page);
        } else if (/\?/.test(uri_old)) {
            // append P=... to existing query segment
            return uri_old + "&P=" + page;
        } else {
            // no query segment so far
            return uri_old + "?P=" + page;
        }
    }

    /**
     * This function takes an URI and returns the first ocurrence of a paging pattern.
     * If the pattern is not found, null will be returned.
     *
     * @param uri An arbitrary URI, usually the current URI of the window.
     * @return The first part of the query segment which begins with 'P='
     */
    this.getPSegmentFromUri = function (uri) {
        if (uri == null) {
            throw new Error("uri was null.");
        }
        var pattern = /\?(.*&)?P=([^&#]*)/;
        var ret = pattern.exec(uri);
        return (ret != null ? ret[2] : null);
    }

    /**
     * Construct a page string for the previous page. Returns null, if there is
     * no previous page. Throws exceptions if P is null or has a wrong format.
     * 
     * @param P,
     *            a paging string
     * @return a String
     */
    this.getPrevPage = function (P) {
        if (P == null) {
            throw new Error("P was null");
        }

        var pattern = /^([0-9]+)L([0-9]+)$/
        var match = pattern.exec(P);
        if (match == null) {
            throw new Error("P didn't match " + pattern.toString());
        }

        var index_old = parseInt(match[1]);
        if (index_old == 0) {
            // no need for a prev button
            return null;
        }

        var length = parseInt(match[2]);
        var index = Math.max(index_old - length, 0);

        return index + "L" + length;
    }

    /**
     * Construct a page string for the next page. Returns null if there is no
     * next page. Throws exceptions if n or P is null or if n isn't an integer >=
     * 0 or if P doesn't have the correct format.
     * 
     * @param P,
     *            a paging string
     * @param n,
     *            total numbers of entities.
     * @return a String
     */
    this.getNextPage = function (P, n) {
        // check n and P for null values and correct formatting
        if (n == null) {
            throw new Error("n was null");
        }
        if (P == null) {
            throw new Error("P was null");
        }
        n = parseInt(n);
        if (isNaN(n) || !(typeof n === 'number' && (n % 1) === 0 && n >= 0)) {
            throw new Error("n is to be an integer >= 0!");
        }
        var pattern = /^([0-9]+)L([0-9]+)$/
        var match = pattern.exec(P);
        if (match == null) {
            throw new Error("P didn't match " + pattern.toString());
        }

        var index_old = parseInt(match[1]);
        var length = parseInt(match[2]);
        var index = index_old + length;

        if (index >= n) {
            // no need for a next button
            return null;
        }
        return index + "L" + length;
    }

    this.init = function () {
        var response_count = document.body.getAttribute("data-response-count");
        if (parseInt(response_count) >= 0) {
            paging.initPaging(window.location.href, response_count);
        }
    }
};


/*
 * Small module containing only a converter from markdown to html.
 *
 * @module markdown
 * @global
 */
this.markdown = new function () {
    this.dependencies = ["showdown", "caosdb_utils"];
    this.init = function () {
        this.converter = new showdown.Converter();
    };

    /**
     * Convert the content (the so-called innerHtml) of an element to HTML,
     * interpreting it as markdown.
     *
     * @param {HTMLElement} textElement - an element with text which is to be
     * converted to html.
     */
    this.toHtml = function (textElement) {
        let text = $(textElement).html();
        let html = this.textToHtml(text);
        $(textElement).html(html);
    };

    /**
     * Convert a markdown text to HTML
     */
    this.textToHtml = function (text) {
        caosdb_utils.assert_string(text, "param `text`");
        return this.converter.makeHtml(text.trim());
    };

    $(document).ready(function () {
        caosdb_modules.register(markdown);
    });
}

/**
 * @module hintMessages
 * @global
 */
var hintMessages = new function () {
    this.init = function () {
        for (var entity of $('.caosdb-entity-panel')) {
            this.hintMessages(entity);
        }
    }

    this.removeMessages = function (entity) {
        $(entity).find(".alert").remove();
    }

    this.unhintMessages = function (entity) {
        $(entity).find(".caosdb-f-message-badge").remove();
        $(entity).find(".alert").show();
    }

    /**
     * Replace all (too-big) message divs with (tiny) badges.
     *
     * When an entity has many errors, warnings and info messages they will
     * likely litter up the entity panel (and the property rows). This function
     * replaces all message divs with tiny, clickable badges showing just the
     * message type (error, warning, info). On click they are replaced with the
     * original message div again.
     *
     * This method can be called on an entity panel, but also on any element
     * which contains message divs at any depth in the DOM tree.
     *
     * @param {HTMLElement} entity - the element where to replace the
     *     messages.
     */
    this.hintMessages = function (entity) {

        // TODO refactor such that the function can detect whether a message is
        // replaced yet instead of "unhintMessage"ing all of them first and do
        // all the work again.
        this.unhintMessages(entity);

        // dictionary for mapping bootstrap classes to message types
        const messageType = {
            "info": "info",
            "warning": "warning",
            "danger": "error"
        };
        for (let alrt in messageType) {

            // find all message divs
            $(entity).find(".alert.alert-" + alrt).each(function (index) {
                var messageElem = $(this);

                // this way only one badge is shown, even if there are more
                // than one message of the given type.
                if (messageElem.parent().find(".caosdb-f-message-badge.alert-" + alrt).length == 0) {

                    // TODO why is the message badge added to the .caosdb-v-property-row here? shouldn't .caosdb-messages suffice?
                    messageElem.parent('.caosdb-messages, .caosdb-v-property-row').prepend('<button title="Click here to show the ' + messageType[alrt] + ' messages of the last transaction." class="btn caosdb-v-message-badge caosdb-f-message-badge badge alert-' + alrt + '">' + messageType[alrt] + '</button>');
                    messageElem.parent().find(".caosdb-f-message-badge.alert-" + alrt).on("click", function (e) {

                        // TODO use remove here instead of hide?
                        $(this).hide();

                        // TODO use messageElem.show() here? or even append
                        // here and remove in the next statement?
                        messageElem.parent().find('.alert.alert-' + alrt).show()
                    });
                } else {
                    // badge found
                    // TODO add counter to the badge for each message type
                }

                messageElem.hide();
            });
        }

        // moves all badges into one div with text-end
        if ($(entity).find(".caosdb-messages > .caosdb-f-message-badge").length > 0) {
            var div = $('<div class="text-end" style="padding: 5px 16px;"/>');
            div.prependTo($(entity).find(".caosdb-messages"));
            var messageBadges = $(entity).find(".caosdb-messages > .caosdb-f-message-badge");
            messageBadges.detach();
            div.append(messageBadges);
        }

        // see the other TODO above: why is there a .caosdb-v-message-badge inside the caosdb-v-property-row without a div.caosdb-message
        $(entity).find(".caosdb-v-property-row > .caosdb-v-message-badge").addClass("float-end");
    }
}


function createErrorNotification(msg) {
    return $('<div class="' + globalClassNames.ErrorNotification + '">' + msg + '<div>')[0];
}

/**
 * Create a waiting notification with a informative message for the waiting user. 
 *
 * @param {String} info, a message for the user
 * @param {String} id, optional, the id of the message div.  Default is empty
 * @return {HTMLElement} A div with class `caosdb-preview-waiting-notification`.
 */
function createWaitingNotification(info, id) {
    id = id ? `id="${id}"` : "";
    return $(`<div class="${globalClassNames.WaitingNotification}" ${id}>${info}</div>`)[0];
}

/**
 * Remove all waiting notifications from an element.
 *
 * @param {HTMLElement} elem
 * @return {HTMLElement} The parameter `elem`.
 */
function removeAllWaitingNotifications(elem) {
    $(elem).find(`.${globalClassNames.WaitingNotification}`).remove();
    return elem;
}

// TODO remove and use connection.post
/**
 * Post an xml document to basepath/Entity
 * 
 * @param xml,
 *            XML document
 * @param basepath,
 *            string
 * @param querySegment,
 *            string
 * @param timeout,
 *            integer (in milliseconds)
 * @return Promise object
 */
function postXml(xml, basepath, querySegment, timeout) {
    return $.ajax({
        type: 'POST',
        contentType: 'text/xml',
        url: basepath + "Entity/" + (querySegment == null ? "" : querySegment),
        processData: false,
        data: xml,
        dataType: 'xml',
        timeout: timeout,
        statusCode: {
            401: function () {
                throw new Error("unauthorized");
            },
        },
    });
}

/**
 * Serialize an xml document into plain string.
 * 
 * @param xml
 * @return string representation of xml
 */
function xml2str(xml) {
    return new XMLSerializer().serializeToString(xml);
}

/**
 * Load a json configuration.
 *
 * If the file cannot be found, an empty array is returned.
 * 
 * @param filename The filename of the configuration file residing in conf/.
 * @return object containing the configuration
 */
async function load_config(filename) {
    const uri = connection.getBasePath() + "webinterface/${BUILD_NUMBER}/conf/" + filename;
    try {
        var data = await $.ajax({
            url: uri,
            dataType: "json",
        });
    } catch (error) {
        if (error.status == 0) {
            console.log(error);
        } else if (error.status == 404) {
            return [];
        } else {
            throw new Error("loading '" + uri + "' failed.", error);
        }
    }
    return data;
}

/**
 * Convert a string into an xml document.
 * 
 * @param s,
 *            a string representation of an xml document.
 * @return an xml document.
 */
function str2xml(s) {
    var parser = new DOMParser();
    return parser.parseFromString(s, "text/xml");
}

/**
 * Asynchronously transform an xml into html via xslt.
 * 
 * @param xmlPromise,
 *            resolves to an input xml document.
 * @param xslPromise,
 *            resolves to a xsl script.
 * @param paramsPromise,
 *            resolves to parameters for the xsl script.
 * @return A promise which resolves to a generated HTML document.
 */
async function asyncXslt(xmlPromise, xslPromise, paramsPromise) {
    var xml, xsl, params;
    [xml, xsl, params] = await Promise.all([xmlPromise, xslPromise, paramsPromise]);
    return xslt(xml, xsl, params);
}

/**
 * transform a xml into html via xslt
 * 
 * @param xml,
 *            the input xml document
 * @param xsl,
 *            the transformation script
 * @param params,
 *            xsl parameter to be set (optionally).
 * @return html
 */
function xslt(xml, xsl, params) {
    var xsltProcessor = new XSLTProcessor();
    if (params) {
        for (var k in params) {
            if (params.hasOwnProperty(k)) {
                xsltProcessor.setParameter(null, k, params[k]);
            }
        }
    }
    var result = null;
    try {
        if (typeof xsltProcessor.transformDocument == 'function') {
            // old FFs
            var retDoc = document.implementation.createDocument("", "", null);
            xsltProcessor.transformDocument(xml, xsl, retDoc, null);
            result = retDoc.documentElement;
        } else {
            // modern browsers
            xsltProcessor.importStylesheet(xsl);
            result = xsltProcessor.transformToFragment(xml, document);
        }
    } catch (error) {
        throw new Error(`XSL Transformation terminated with error: ${error.message}`);
    }
    if (!result) {
        throw new Error("XSL Transformation did not return any results");
    }
    return result;
}

/**
 * TODO
 */
function getXSLScriptClone(source) {
    return str2xml(xml2str(source))
}

/**
 * Add a template rule to a XSL style sheet.
 *
 * The original document is cloned (copy-on-change) before the template rule is
 * appended.
 *
 * @param {XMLDocument} orig_xsl - the original xsl style sheet
 * @param {string} templateStr - the new template rule (an xml string)
 * @return {XMLDocument} new xsl style sheet with one more rule.
 */
function injectTemplate(orig_xsl, templateStr) {
    var xsl = getXSLScriptClone(orig_xsl);
    xsl.documentElement.insertAdjacentHTML("beforeend", templateStr);
    return xsl;
}

/**
 * TODO
 */
function insertParam(xsl, name, value = null) {
    var param = xsl.createElement("xsl:param");
    param.setAttribute("name", name);
    if (value != null) {
        param.setAttribute("select", "'" + value + "'");
    }
    xsl.firstElementChild.append(param);
}


this.user_management = function ($, connection, createWaitingNotification, createErrorNotification) {

    const set_new_password = function (realm, username, password) {
        return $.ajax({
            type: "PUT",
            url: connection.getBasePath() + `User/${realm}/${username}`,
            dataType: "xml",
            data: {
                password: password,
            },
        });
    }

    /**
     * Get the modal with the password form, if present.
     *
     * @return {HTMLElement}
     */
    const get_change_password_form = function () {
        const modal = $("#caosdb-f-change-password-form");
        return modal[0];
    }

    const init_change_password_form = function () {
        var modal = get_change_password_form();
        if (typeof modal == "undefined") {
            return;
        }
        modal = $(modal);

        const form = modal.find("form");
        const password_input = form[0]["password"];
        const password_input2 = form[0]["password2"];
        const realm_input = form[0]["realm"];
        const username_input = form[0]["username"];
        const checkbox = form.find("[type='checkbox']");
        const reset_button = modal.find("[type='reset']");

        reset_button.click(() => {
            // hide form
            modal.modal("hide");
            $(password_input).attr("type", "password");
            $(password_input2).attr("type", "password");
            checkbox.checked = false;
        });

        checkbox.change((e) => {
            if (checkbox[0].checked) {
                $(password_input).attr("type", "text");
                $(password_input2).attr("type", "text");
            } else {
                $(password_input).attr("type", "password");
                $(password_input2).attr("type", "password");
            }
        })

        form[0].onsubmit = function (e) {
            e.preventDefault();
            if (password_input.value == password_input2.value) {
                const password = password_input.value;
                const username = username_input.value;
                const realm = realm_input.value;
                form[0].reset();
                form.find(".modal-body > *, .modal-footer > *").hide()
                const wait = createWaitingNotification("Please wait...");
                form.find(".modal-body").append(wait);
                user_management.set_new_password(realm, username, password)
                    .then((result) => {
                        wait.remove();

                        const msg = $('<p>Success! The new password has been stored.</p>');
                        form.find(".modal-body")
                            .append(msg);

                        const ok_button = $('<button type="reset">Ok</button>');
                        form.find(".modal-footer")
                            .append(ok_button);

                        ok_button.click(() => {
                            ok_button.remove();
                            msg.remove();
                            form.find(".modal-body > *, .modal-footer > *").show(1000);
                            modal.modal("hide");
                        });
                    })
                    .catch((err) => {
                        wait.remove();
                        console.error(err);

                        var msg_text;
                        if (err.status == 403) {
                            msg_text = "You are not allowed to do this.";
                        } else if (err.status == 422) {
                            msg_text = "Your password was too weak.";
                        } else {
                            msg_text = "An unknown error occurred.";
                        }
                        const msg = createErrorNotification(msg_text)
                        form.find(".modal-body").append(msg);

                        const ok_button = $('<button type="reset">Ok</button>');
                        form.find(".modal-footer")
                            .append(ok_button);

                        ok_button.click(() => {
                            ok_button.remove();
                            msg.remove();
                            form.find(".modal-body > *, .modal-footer > *").show(1000);
                            modal.modal("hide");
                        });
                    })
                    .catch(globalError);

                return false;
            } else {
                password_input2.setCustomValidity('The second password must match the first one.');
                password_input2.reportValidity();
                password_input2.setCustomValidity('');
            }
            return false;
        };

    }

    var init = function () {
        init_change_password_form();
    }

    return {
        init: init,
        set_new_password: set_new_password,
        get_change_password_form: get_change_password_form,
    };
}($, connection, createWaitingNotification, createErrorNotification);


/**
 * Initialize all the submodules.
 */
function initOnDocumentReady() {
    paging.init();
    hintMessages.init();

    // show image 100% width
    $(".entity-image-preview").click(function () {
        $(this).css('max-width', '100%');
        $(this).css('max-height', "");
    });

    if (typeof _caosdb_modules_auto_init === "undefined") {
        // the test index.html sets this to false, 
        // unset -> no tests
        caosdb_modules.auto_init = true;
    } else {
        caosdb_modules.auto_init = _caosdb_modules_auto_init;
    }
    caosdb_modules.init();
    navbar.init();

    if ("${BUILD_MODULE_USER_MANAGEMENT}" == "ENABLED") {
        caosdb_modules.register(user_management);
    }

    if ("${BUILD_MODULE_SHOW_ID_IN_LABEL}" == "ENABLED") {
        // Remove the "display: none;" rule from the caosdb-label-id class
        [...document.styleSheets]
        .map(s => {
                return [...s.cssRules].find(x => x.selectorText == '.caosdb-label-id')
            })
            .filter(Boolean)
            .forEach(rule => rule.style.removeProperty("display"));
    }
}


/**
 * Responsible for module inititialization.
 *
 * Singleton which is globally available under caosdb_modules.
 *
 * @class _CaosDBModules
 * @property {boolean} auto_init - if modules are initialized automatically
 *   after beeing registered, or when `this.init_all()` is being called.
 */
class _CaosDBModules {
    constructor() {
        this.auto_init = undefined;
        this.modules = [];
    }

    /**
     * Delegate the responsibility of initialization to this instance.
     *
     * @param {object} module - an object with an `init()` method.
     * @throws TypeError - if module has no `init` method.
     */
    register(module) {
        if (!(typeof module.init === "function")) {
            throw new TypeError("modules must define an init function");
        }

        this.modules.push(module);
        if (this.auto_init) {
            this._init_module(module);
        }
    }

    _init_module(module) {
        if (!module.__initialized) {
            module.__initialized = true;
            module.init();
        }
    }

    /**
     * Initialize this instance.
     *
     * If this.auto_init is true, all registered, uninitialized modules are
     * initialized by this function.
     */
    init() {
        if (this.auto_init) {
            for (const module of this.modules) {
                this._init_module(module);
            }
        }
    }
}

var caosdb_modules = new _CaosDBModules()

$(document).ready(initOnDocumentReady);
