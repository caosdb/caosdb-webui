<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 * Copyright (C) 2019 Daniel Hornung (d.hornung@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />

  <xsl:include href="xsl/jsheader.xsl" />
  <xsl:include href="xsl/main.xsl" />
  <xsl:include href="xsl/navbar.xsl" />
  <xsl:include href="xsl/messages.xsl" />
  <xsl:include href="xsl/query.xsl" />
  <xsl:include href="xsl/entity.xsl" />
  <xsl:include href="xsl/filesystem.xsl" />
  <xsl:include href="xsl/footer.xsl" />
  <xsl:include href="xsl/common.xsl"/>
  <xsl:include href="xsl/welcome.xsl"/>

  <xsl:template name="caosdb-tour-toc">
    <div class="caosdb-v-tour-toc-sidebar" id="tour-toc">
    <div class="caosdb-v-tour-toc-show"></div>
    <button class="caosdb-v-tour-toc-show caosdb-f-tour-toc-toggle btn"></button>
    <div class="caosdb-v-tour-toc-header">
      <h3>Tour</h3>
    </div>
    <div class="caosdb-f-tour-toc-body"></div>
    </div>
  </xsl:template>

  <xsl:template match="/">
    <html lang="en">
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <xsl:element name="link">
          <xsl:attribute name="rel">icon</xsl:attribute>
          <xsl:attribute name="type">image/png</xsl:attribute>
          <xsl:attribute name="href">
              <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/${BUILD_FAVICON}')"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:call-template name="caosdb-head-title" />
        <xsl:call-template name="caosdb-head-css" />
        <xsl:call-template name="caosdb-head-js" />
      </head>
      <body>
        <xsl:choose>
          <xsl:when test="'${BUILD_MODULE_EXT_PROPERTY_DISPLAY}'='ENABLED'">
            <xsl:attribute name="data-hidden-properties">true</xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="data-hidden-properties">false</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:attribute name="data-response-count">
          <xsl:value-of select="/Response/@count"/>
        </xsl:attribute>
        <xsl:if test="count(/Response/*)&lt;3 and not(/Response/Error|/Response/Info|/Response/Warning)">
            <xsl:attribute name="class">caosdb-welcome</xsl:attribute>
        </xsl:if>
        <div class="background d-flex flex-column">
          <xsl:call-template name="caosdb-tour-toc" />
          <xsl:call-template name="caosdb-top-navbar" />
          <xsl:call-template name="caosdb-data-container" />
          <xsl:if test="count(/Response/*)&lt;3 and not(/Response/Error|/Response/Info|/Response/Warning)">
            <xsl:call-template name="welcome"/>
          </xsl:if>
        </div>
        <footer class="py-5">
          <xsl:call-template name="caosdb-footer"/>
        </footer>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
