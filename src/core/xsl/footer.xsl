<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Daniel Hornung (d.hornung@indiscale.com)
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>

  <xsl:template name="caosdb-footer">
    <div class="container d-flex flex-lg-row flex-column justify-content-around">
      <div class="caosdb-footer-element" id="caosdb-footer-element-custom-1">
        ${BUILD_FOOTER_CUSTOM_ELEMENT_ONE}
      </div>
      <div class="caosdb-footer-element" id="caosdb-footer-element-custom-2">
        ${BUILD_FOOTER_CUSTOM_ELEMENT_TWO}
      </div>
    </div>
    <div class="container d-flex flex-md-row flex-column justify-content-center">
      <a href="${BUILD_FOOTER_CONTACT_HREF}">Contact</a>
      <span class="caosdb-bulletsep d-none d-md-inline">•</span>
      <a href="${BUILD_FOOTER_IMPRINT_HREF}">Imprint/Impressum</a>
      <span class="caosdb-bulletsep d-none d-md-inline">•</span>
      <a href="${BUILD_FOOTER_DATA_POLICY_HREF}">Data Policy</a>
      <span class="caosdb-bulletsep d-none d-md-inline">•</span>
      <a href="${BUILD_FOOTER_LICENCE_HREF}" target="_blank">License (AGPL-v3)</a>
      <span class="caosdb-bulletsep d-none d-md-inline">•</span>
      <a href="${BUILD_FOOTER_SOURCES_HREF}" target="_blank">Sources</a>
      <span class="caosdb-bulletsep d-none d-md-inline">•</span>
      <a href="${BUILD_FOOTER_DOCS_HREF}" target="_blank">Documentation</a>
    </div>
  </xsl:template>
</xsl:stylesheet>
