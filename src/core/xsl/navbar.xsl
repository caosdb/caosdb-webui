<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH (info@indiscale.com)
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template name="make-retrieve-all-link">
    <xsl:param name="entity"/>
    <xsl:param name="display"/>
    <xsl:param name="paging" select="'0L10'"/>
    <li>
      <a class="dropdown-item">
        <xsl:attribute name="href">
          <xsl:value-of select="concat($entitypath, '?query=find%20', normalize-space($entity))"/>
          <xsl:if test="$paging">
            <xsl:value-of select="concat('&amp;P=',$paging)"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:value-of select="$display"/>
      </a>
    </li>
  </xsl:template>
  <xsl:template name="caosdb-top-navbar">
    <xsl:if test="/Response/@realm='${BUILD_MODULE_USER_MANAGEMENT_CHANGE_OWN_PASSWORD_REALM}'">
      <xsl:call-template name="change-password-modal">
          <xsl:with-param name="realm"><xsl:value-of select="/Response/@realm"/></xsl:with-param>
          <xsl:with-param name="username"><xsl:value-of select="/Response/@username"/></xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top mb-2 flex-column" id="caosdb-navbar-full">
      <noscript>Please enable JavaScript!</noscript>
      <div class="container-fluid">
        <a class="navbar-brand d-lg-none d-inline">
          <xsl:attribute name="href">
            <xsl:value-of select="$basepath"/>
          </xsl:attribute>
          <xsl:element name="img">
            <xsl:if test="'${BUILD_NAVBAR_BRAND_NAME}' != ''">
              <xsl:attribute name="class">caosdb-logo</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="src">
                <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/${BUILD_NAVBAR_LOGO}')"/>
            </xsl:attribute>
          </xsl:element>
          ${BUILD_NAVBAR_BRAND_NAME}
        </a>
        <button class="navbar-toggler" data-bs-target="#top-navbar" data-bs-toggle="collapse" type="button" aria-controls="top-navbar" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="top-navbar">
          <a class="navbar-brand d-none d-lg-inline">
            <xsl:attribute name="href">
              <xsl:value-of select="$basepath"/>
            </xsl:attribute>
            <xsl:element name="img">
              <xsl:if test="'${BUILD_NAVBAR_BRAND_NAME}' != ''">
                <xsl:attribute name="class">caosdb-logo</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="src">
                  <xsl:value-of select="concat($basepath,'webinterface/${BUILD_NUMBER}/${BUILD_NAVBAR_LOGO}')"/>
              </xsl:attribute>
            </xsl:element>
            ${BUILD_NAVBAR_BRAND_NAME}
          </a>
          <ul class="navbar-nav caosdb-navbar me-auto">
            <li class="nav-item dropdown" id="caosdb-navbar-entities">
              <a class="nav-link dropdown-toggle" role="button" id="navbarEntitiesMenuLink" data-bs-toggle="dropdown" aria-expanded="false" href="#">
                Entities
              </a>
              <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarEntitiesMenuLink">
                <li class="dropdown-header">Retrieve all:</li>
                <xsl:call-template name="make-retrieve-all-link">
                  <xsl:with-param name="entity">
                                      Entity
                                  </xsl:with-param>
                  <xsl:with-param name="display">
                                      Entities
                                  </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="make-retrieve-all-link">
                  <xsl:with-param name="entity">
                                      Record
                                  </xsl:with-param>
                  <xsl:with-param name="display">
                                      Records
                                  </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="make-retrieve-all-link">
                  <xsl:with-param name="entity">
                                      RecordType
                                  </xsl:with-param>
                  <xsl:with-param name="display">
                                      RecordTypes
                                  </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="make-retrieve-all-link">
                  <xsl:with-param name="entity">
                                      Property
                                  </xsl:with-param>
                  <xsl:with-param name="display">
                                      Properties
                                  </xsl:with-param>
                </xsl:call-template>
                <xsl:call-template name="make-retrieve-all-link">
                  <xsl:with-param name="entity">
                                      File
                                  </xsl:with-param>
                  <xsl:with-param name="display">
                                      Files
                                  </xsl:with-param>
                </xsl:call-template>
              </ul>
            </li>
            <li id="caosdb-navbar-filesystem" class="nav-item">
              <a class="nav-link" role="button">
                <xsl:call-template name="make-filesystem-link">
                  <xsl:with-param name="href" select="'/'"/>
                  <xsl:with-param name="display" select="'File System'"/>
                </xsl:call-template>
              </a>
            </li>
            <li id="caosdb-navbar-query" class="nav-item">
              <a class="nav-link" role="button" data-bs-target="#caosdb-query-panel-collapsible" data-bs-toggle="collapse">
                Query
              </a>
            </li>
          </ul>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" role="button" id="navbarBookmarkMenuLink" data-bs-toggle="dropdown" aria-expanded="false" href="#">
                <span id="caosdb-f-bookmarks-collection-counter" class="badge bg-secondary">0</span>
                  Bookmarks
                </a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-light" aria-labelledby="navbarBookmarkMenuLink">
                <li class="disabled" id="caosdb-f-bookmarks-collection-link"
                    title="Show all bookmarked entities.">
                  <a class="dropdown-item">Show all</a></li>
                <li class="disabled" id="caosdb-f-bookmarks-export-link"
                    title="Export all bookmarks to a file. The exported file is a spread sheet with columns for the id, the version, the complete URI of the bookmarked entities and the path, if the entity is a file.">
                  <a class="dropdown-item">Export to file</a></li>
                <li class="disabled" id="caosdb-f-bookmarks-clear"
                    title="Empty the list of bookmarks.">
                  <a class="dropdown-item">Clear</a></li>
              </ul>
            </li>
          <xsl:call-template name="caosdb-user-menu"/>
          </ul>
      </div>
      </div>
      <!-- global messages -->
      <xsl:apply-templates select="/Response/Error">
        <xsl:with-param name="class" select="'alert-danger'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="/Response/Warning">
        <xsl:with-param name="class" select="'alert-warning'"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="/Response/Info">
        <xsl:with-param name="class" select="'alert-info'"/>
      </xsl:apply-templates>
      <!-- query panel -->
      <div class="collapse" id="caosdb-query-panel-collapsible" style="width: 100%">
      <div class="container py-2 py-sm-3 py-lg-4 py-xl-5 flex-column caosdb-query-panel" id="caosdb-query-panel">
        <xsl:call-template name="caosdb-query-panel"/>
      </div>
      </div>
    </nav>
  </xsl:template>
  <xsl:template match="Role" name="caosdb-user-roles">
    <div class="caosdb-user-role">
      <xsl:value-of select="text()"/>
    </div>
  </xsl:template>
  <xsl:template match="UserInfo" name="caosdb-user-info">
    <div class="caosdb-user-info" style="display: none;">
      <div class="caosdb-user-name">
        <xsl:value-of select="@username"/>
      </div>
      <div class="caosdb-user-realm">
        <xsl:value-of select="@realm"/>
      </div>
      <xsl:apply-templates select="Roles/Role"/>
    </div>
  </xsl:template>
  <xsl:template name="change-password-modal">
    <xsl:param name="realm"/>
    <xsl:param name="username"/>
    <div id="caosdb-f-change-password-form" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form class="modal-content" method="PUT">
          <input type="hidden" name="realm"><xsl:attribute name="value"><xsl:value-of select="$realm"/></xsl:attribute></input>
          <input type="hidden" name="username"><xsl:attribute name="value"><xsl:value-of select="$username"/></xsl:attribute></input>
          <div class="modal-header">
            <h4 class="modal-title">Set a new password</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label>New Password
              <input class="form-control" type="password" name="password" required="required">
                <xsl:attribute name="pattern">(?=.*[_\W])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}</xsl:attribute>
                <xsl:attribute name="title">The new password must contain at least 8 characters, an uppercase letter (A-Z), a lowercase letter (a-z), a number (0-9), and a special character (!#$%'*+,-./:;?^_{|}~)</xsl:attribute>
              </input>
              </label>
            </div>
            <div class="form-group">
              <label>Repeat
                <input class="form-control" type="password" name="password2" required="required"/>
              </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"/>
                    Show password
                </label>
            </div>
          </div>
          <div class="modal-footer">
            <button type="reset" class="btn btn-default" >Cancel</button>
            <button type="submit" class="btn btn-default" >Submit</button>
          </div>
        </form>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="caosdb-user-menu">
    <xsl:choose>
      <xsl:when test="/Response/@username">
        <li class="nav-item dropdown my-auto" id="user-menu">
          <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" href="#">
            <xsl:value-of select="concat(/Response/@username,' ')"/>
            <i class="bi-person-fill"></i>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-light">
            <xsl:if test="/Response/@realm='${BUILD_MODULE_USER_MANAGEMENT_CHANGE_OWN_PASSWORD_REALM}'">
              <li>
                <a class="dropdown-item" title="Change your password." href="#" data-bs-toggle="modal" data-bs-target="#caosdb-f-change-password-form">Change Password</a>
              </li>
            </xsl:if>
            <li>
              <a class="dropdown-item" title="Click to logout.">
                <xsl:attribute name="href">
                  <xsl:value-of select="concat($basepath, 'logout')"/>
                </xsl:attribute>
                Logout <i class="bi-box-arrow-right"></i></a>
            </li>
          </ul>
        </li>
      </xsl:when>
      <xsl:otherwise>
        <li id="user-menu" class="nav-item my-auto">
          <form id="caosdb-f-login-form" class="d-none" method="POST">
            <xsl:attribute name="action">
              <xsl:value-of select="concat($basepath, 'login')"/>
            </xsl:attribute>
            <div class="row">
            <div class="col">
            <input class="form-control" id="username" name="username" placeholder="username" type="text"/>
            </div>
            <div class="col">
            <input class="form-control" id="password" name="password" placeholder="password" type="password"/>
            </div>
            <div class="col-auto">
            <button class="btn btn-primary" type="submit">Login</button>
            </div>
            </div>
          </form>
          <form class="my-auto">
          <button style="margin-right: 15px" class="btn btn-secondary navbar-btn d-inline-block" id="caosdb-f-login-show-button" type="button">Login</button>
          </form>
        </li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="paging-panel">
    <div class="caosdb-f-paging-panel mb-2">
      <a type="button" class="caosdb-prev-button btn btn-light">Previous Page</a>
      <a type="button" class="caosdb-next-button btn btn-light">Next Page</a>
    </div>
  </xsl:template>
</xsl:stylesheet>
