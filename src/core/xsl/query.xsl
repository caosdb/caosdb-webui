<?xml version="1.0" encoding="utf-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>
  <xsl:template match="ParseTree" mode="query-results">
    <xsl:apply-templates select="ParsingError"/>
  </xsl:template>
  <xsl:template match="ParseTree/ParsingError" mode="query-results">
    <div class="card-body">
      <div class="caosdb-overflow-box">
        <div class="caosdb-overflow-content">
          <span>ParseTree:</span>
          <xsl:value-of select="text()"/>
        </div>
        <div class="caosdb-overflow-contents">
          <span>
            <xsl:value-of select="@type"/>
            <xsl:text>, line </xsl:text>
            <xsl:value-of select="@line"/>
            <xsl:text>, character </xsl:text>
            <xsl:value-of select="@character"/>
            <xsl:text>:</xsl:text>
          </span>
          <xsl:value-of select="text()"/>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template match="Query" mode="query-results">
    <div class="card caosdb-query-response mb-2">
      <div class="card-header caosdb-query-response-heading">
        <div class="row">
          <div class="col-sm-10 caosdb-overflow-box">
            <div class="caosdb-overflow-content">
              <span>Query: </span>
              <span class="caosdb-f-query-response-string">
                <xsl:value-of select="@string"/>
              </span>
            </div>
          </div>
          <div class="col-sm-2 text-end">
            <span>Results: </span>
            <span class="caosdb-query-response-results">
              <xsl:value-of select="@results"/>
            </span>
          </div>
        </div>
      </div>
      <xsl:if test="@results=0">
        <div class="card caosdb-no-results">
          <div class="alert alert-warning" role="alert">
                There were no results for this query.
              </div>
        </div>
      </xsl:if>
      <xsl:apply-templates mode="query-results" select="./ParseTree"/>
    </div>
    <xsl:if test="@results!=0">
      <xsl:apply-templates mode="select-table" select="./Selection"/>
    </xsl:if>
  </xsl:template>
  <xsl:template match="Selection" mode="select-table">
    <div class="card caosdb-select-table">
      <div class="card-header">
        <div class="container-fluid panel-container">
          <div class="col-xs-6">
            <h5>Table of selected fields</h5>
          </div>
          <div class="col-xs-6 text-end">
            <!-- Trigger the modal with a button -->
            <button class="btn btn-info btn-sm caosdb-v-btn-select" data-bs-target="#downloadModal" data-bs-toggle="modal" type="button">Export</button>
            <!-- Modal -->
            <div class="modal fade text-left" id="downloadModal" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header align-middle">
                      <h4 class="modal-title">Download this table</h4>
                    <button class="btn-close" data-bs-dismiss="modal" type="button"></button>
                  </div>
                  <div class="modal-body text-start">
                    <p>
                      <a id="caosdb-f-query-select-data-tsv" onclick="downloadTSV(this)" href="#selected_data.tsv" download="selected_data.tsv">
                        Download table as TSV File
                      </a>
                      <span class="form-check" style="margin-top: 0; display: inline; position: absolute; right: 1rem"><label><input type="checkbox" class="me-1" name="raw" id="caosdb-table-export-raw-flag-tsv" title="Export raw entity ids instead of the visible page content."/>raw</label></span>
                    </p>
                    <p>
                      <a class="caosdb-v-query-select-data-xsl" onclick="downloadXLS(this)" href="#selected_data.xsl" download="">
                        Download table as XLS File
                      </a>
                      <span class="form-check" style="margin-top: 0; display: inline; position: absolute; right: 1rem"><label><input type="checkbox" class="me-1" name="raw" id="caosdb-table-export-raw-flag-xls" title="Export raw entity ids instead of the visible page content."/>raw</label></span>
                    </p>
                    <p>
                      <a id="caosdb-f-query-select-files" onclick="ext_file_download.download_files(this)" href="#selected_data.tsv" download="files.zip" title="Collects file entities listed in the table in a zip file. If the entity belonging to a row is a file entity, it will be included.">
                          Download files referenced in the table
                      </a>
                    </p>
                    <hr/>
                    <p>
                      <small>Download this dataset in Python with:</small>
                    </p>
                    <p>
                      <code>
                        data = caosdb.execute_query('<xsl:value-of select="//Response/Query/@string"/>')
                      </code>
                    </p>
                  </div>
                  <div class="modal-footer">
                    <div class="row" style="margin:0px">
                      <div class="col-xs-6 caosdb-f-modal-footer-left">
                      </div>
                      <div class="col-xs-6">
                        <button class="btn btn-secondary" data-bs-dismiss="modal" type="button">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="caosdb-select-table-actions-panel text-end btn-group-sm"></div>
      <div class="card-body">
        <table class="table ttable-responsive able-hover">
          <thead>
            <tr>
              <th></th>
              <xsl:for-each select="Selector">
                <th>
                  <xsl:value-of select="@name"/>
                </th>
              </xsl:for-each>
            </tr>
          </thead>
          <tbody>
            <xsl:for-each select="/Response/*[@id]">
              <xsl:call-template name="select-table-row">
                <xsl:with-param name="entity-id" select="@id"/>
                <xsl:with-param name="version-id" select="Version/@id"/>
                <xsl:with-param name="ishead" select="Version/@head"/>
              </xsl:call-template>
            </xsl:for-each>
          </tbody>
        </table>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="entity-link">
    <xsl:param name="entity-id"/>
    <xsl:param name="version-id"/>
    <xsl:param name="ishead"/>
    <a class="btn btn-secondary btn-sm caosdb-select-id" title="Go to this entity.">
      <xsl:attribute name="href">
        <xsl:value-of select="concat($entitypath, $entity-id)"/>
        <xsl:if test="$version-id and not($ishead)">
          <xsl:value-of select="concat('@', $version-id)"/>
        </xsl:if>
      </xsl:attribute>
      <!-- <xsl:value-of select="$entity-id" /> -->
      <span class="caosdb-select-id-target">
          <i class="bi bi-box-arrow-up-right"></i>
      </span>
    </a>
  </xsl:template>

  <xsl:template name="select-table-row">
    <xsl:param name="entity-id"/>
    <xsl:param name="version-id"/>
    <xsl:param name="ishead"/>
    <tr>
      <xsl:attribute name="data-entity-id">
        <xsl:value-of select="$entity-id"/>
      </xsl:attribute>
      <xsl:attribute name="data-version-id">
        <xsl:value-of select="$version-id"/>
      </xsl:attribute>
      <td>
        <xsl:call-template name="entity-link">
          <xsl:with-param name="entity-id" select="$entity-id"/>
          <xsl:with-param name="version-id" select="$version-id"/>
          <xsl:with-param name="ishead" select="$ishead"/>
        </xsl:call-template>
      </td>
      <xsl:for-each select="/Response/Query/Selection/Selector">
        <xsl:call-template name="select-table-cell">
          <xsl:with-param name="entity-id" select="$entity-id"/>
          <xsl:with-param name="version-id" select="$version-id"/>
          <xsl:with-param name="field-name" select="translate(@name, $uppercase, $lowercase)"/>
        </xsl:call-template>
      </xsl:for-each>
    </tr>
  </xsl:template>

  <xsl:template name="select-table-cell">
    <xsl:param name="entity-id"/>
    <xsl:param name="version-id"/>
    <xsl:param name="field-name"/>
    <td class="caosdb-f-entity-property">
      <xsl:attribute name="data-property-name">
        <xsl:value-of select="$field-name"/>
      </xsl:attribute>
      <div class="caosdb-f-property-value caosdb-v-property-value">
        <xsl:choose>
          <xsl:when test="$version-id!=''">
            <xsl:apply-templates select="/Response/*[@id=$entity-id and Version/@id=$version-id]" mode="walk-select-segments">
              <xsl:with-param name="first-segment">
                <xsl:value-of select="substring-before(concat($field-name, '.'), '.')"/>
              </xsl:with-param>
              <xsl:with-param name="next-segments">
                <xsl:value-of select="substring-after($field-name, '.')"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="/Response/*[@id=$entity-id]" mode="walk-select-segments">
              <xsl:with-param name="first-segment">
                <xsl:value-of select="substring-before(concat($field-name, '.'), '.')"/>
              </xsl:with-param>
              <xsl:with-param name="next-segments">
                <xsl:value-of select="substring-after($field-name, '.')"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </div>
    </td>
  </xsl:template>

  <xsl:template match="Property" mode="walk-select-segments">
    <!-- handle properties -->
    <xsl:param name="first-segment"/>
    <xsl:param name="next-segments"/>

    <xsl:choose>
      <xsl:when test="@*[translate($first-segment, $uppercase, $lowercase)=name()]">
        <!--handle attributes-->
        <xsl:call-template name="single-value">
          <xsl:with-param name="value">
            <xsl:value-of select="@*[translate(name(), $uppercase, $lowercase)=$first-segment]"/>
          </xsl:with-param>
          <xsl:with-param name="reference" select="'false'"/>
          <xsl:with-param name="boolean" select="'false'"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:when test="translate($first-segment, $uppercase, $lowercase)='version'">
        <!--handle version-->
        <xsl:call-template name="single-value">
          <xsl:with-param name="value" select="Version/@id"/>
          <xsl:with-param name="reference" select="'false'"/>
          <xsl:with-param name="boolean" select="'false'"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:when test="$next-segments='value'">
        <!--handle value-->
        <xsl:apply-templates mode="property-value" select="."/>
      </xsl:when>

      <xsl:when test="translate($next-segments, $uppercase, $lowercase)='unit'">
        <!--handle unit-->
        <xsl:call-template name="single-value">
          <xsl:with-param name="value" select="@unit"/>
          <xsl:with-param name="reference" select="'false'"/>
          <xsl:with-param name="boolean" select="'false'"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:when test="$next-segments!=''">
        <!--walk to next level of nested entities-->
        <xsl:apply-templates select="*[@id]" mode="walk-select-segments">
          <xsl:with-param name="first-segment">
            <xsl:value-of select="substring-before(concat($next-segments, '.'), '.')"/>
          </xsl:with-param>
          <xsl:with-param name="next-segments">
            <xsl:value-of select="substring-after($next-segments, '.')"/>
          </xsl:with-param>
        </xsl:apply-templates>
      </xsl:when>

      <xsl:otherwise>
        <!--next is empty. handle complete property-->
        <xsl:apply-templates mode="property-value" select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="walk-select-segments">
    <!-- handle anything but attributes and properties -->
    <xsl:param name="first-segment"/>
    <xsl:param name="next-segments"/>
    <xsl:choose>
      <xsl:when test="@*[translate($first-segment, $uppercase, $lowercase)=name()]">
        <!--handle attributes-->
        <xsl:call-template name="single-value">
          <xsl:with-param name="value">
            <xsl:value-of select="@*[translate(name(), $uppercase, $lowercase)=$first-segment]"/>
          </xsl:with-param>
          <xsl:with-param name="reference" select="'false'"/>
          <xsl:with-param name="boolean" select="'false'"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:when test="translate($first-segment, $uppercase, $lowercase)='version'">
        <!--handle version-->
        <xsl:call-template name="single-value">
          <xsl:with-param name="value" select="Version/@id"/>
          <xsl:with-param name="reference" select="'false'"/>
          <xsl:with-param name="boolean" select="'false'"/>
        </xsl:call-template>
      </xsl:when>

      <xsl:when test="$next-segments">
        <!-- when there is a next-segmenst -->
        <xsl:apply-templates select="Property[translate(@name, $uppercase, $lowercase)=$first-segment]" mode="walk-select-segments">
          <xsl:with-param name="next-segments" select="$next-segments"/>
        </xsl:apply-templates>
      </xsl:when>


      <xsl:otherwise>
        <!-- otherwise, this is the final segment and the reference can be printed. -->
        <xsl:value-of select="@id"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="caosdb-query-panel">
    <!-- query panel, this is the area which contains the query form and other related stuff (e.g. query short cuts). -->
      <form class="card caosdb-query-form" id="caosdb-query-form" method="GET">
        <xsl:attribute name="action">
          <xsl:value-of select="$entitypath"/>
        </xsl:attribute>
        <input id="caosdb-query-paging-input" name="P" type="hidden" value="0L10"/>
        <div class="input-group">
          <input class="form-control caosdb-f-query-textarea" id="caosdb-query-textarea" name="query" placeholder="E.g. 'FIND Experiment'" rows="1" style="resize: vertical;" type="text"></input>
            <a class="btn btn-secondary caosdb-search-btn" href="#" title="Click to execute the query.">
              <i class="bi-search"></i>
            </a>
        </div>
      </form>
  </xsl:template>
</xsl:stylesheet>
