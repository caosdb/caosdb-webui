<?xml version="1.0" encoding="UTF-8"?>
<!--
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" />

    <xsl:template match="/Properties">
        <html lang="en">
            <head>
                <meta charset="utf-8" />
            </head>
            <body>
                <form method="POST">
                    <xsl:apply-templates select="./*"/>
                    <input type="submit"/>
                </form>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="/Properties/*">
        <div>
            <xsl:value-of select="name()"/>
            <input>
                <xsl:choose>
                  <xsl:when test="contains(name(), 'PASS')">
                      <xsl:attribute name="type">password</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:attribute name="type">text</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="name()"/></xsl:attribute>
            </input>
        </div>
    </xsl:template>

</xsl:stylesheet>
