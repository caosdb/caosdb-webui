The comments feature of the LinkAhead webui
===========================================

WebUI contains a feature that allows users to add comments to existing
records.

The feature is not enabled by default.

You can manually activate it using the following steps: - Add a new
RecordType (e.g. using the Edit Mode) called “Annotation” - Add a new
RecordType called “CommentAnnotation” with parent “Annotation” - Add a
new TEXT Property called “comment” - Add a new REFERENCE Property called
“annotationOf”

or using the following XML:

.. code:: xml

   <Property id="-1" name="comment" description="A comment on something." datatype="TEXT">
   </Property>

   <Property id="-2" name="annotationOf" description="The core property of the [Annotation] denoting which entity the annotation is annotating." datatype="REFERENCE">
   </Property>

    <RecordType id="-3" name="Annotation" description="Annotations annotate other entities in order to represent information about these entities without changing them. Mostly this will be comments by users on these entities or flags used by third party programs.">
       <Property id="-2" name="annotationOf" description="The core property of the [Annotation] denoting which entity the annotation is annotating." datatype="REFERENCE" importance="OBLIGATORY">
       </Property>
   </RecordType>

   <RecordType name="CommentAnnotation" description="CommentAnnotations represent user comments on other entities. As they are entities themselves they can be 'responded' by just annotating them with another CommentAnnotation.">
       <Parent id="-3" name="Annotation" description="Annotations annotate other entities in order to represent information about these entities without changing them. Mostly this will be comments by users on these entities or flags used by third party programs." />
       <Property id="-1" name="comment" description="A comment on something." datatype="TEXT" importance="OBLIGATORY">
       </Property>
   </RecordType>

Additionally, on some servers the comment button might be disabled using
CSS.

E.g. on the demo server you would have to comment out the following
lines in ``demoserver.css``:

.. code:: css

   .caosdb-new-comment-button {
       visibility: hidden;
   }

Using the YAML-Datamodel-Interface
----------------------------------

It’s even easier to add the model using the yaml interface. Use the
following yaml file:

.. code:: yaml


   Annotation:
     description: Annotations annotate other entities in order to represent information about these entities without changing them. Mostly this will be comments by users on these entities or flags used by third party programs.
     obligatory_properties:
       annotationOf:
         description: The core property of the [Annotation] denoting which entity the annotation is annotating.
         datatype: REFERENCE

   CommentAnnotation:
     description: CommentAnnotations represent user comments on other entities. As they are entities themselves they can be 'responded' by just annotating them with another CommentAnnotation.
     inherit_from_obligatory:
       - Annotation
     obligatory_properties:
       comment:
         description: A comment on something.
         datatype: TEXT

Save this file under “datamodel.yaml”.

Make sure you have installed linkahead-advancedtools.

Then sync the model:

.. code:: python

   import linkahead as db
   from linkaheadadvancedtools.models.parser import parse_model_from_json_schema

   model = parse_model_from_yaml("datamodel.yaml")
   model.sync_data_model(noquestion=True)
