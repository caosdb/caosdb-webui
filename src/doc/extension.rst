
Extending the LinkAhead Web Interface
==================================

Here we collect information on how to extend the web interface as a developer.

.. toctree::
   :maxdepth: 1
   :glob:

   extension/*


