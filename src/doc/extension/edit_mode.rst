
Customizing the Edit Mode
=========================


Customizing the number of candidates retrieved for references in the Edit Mode
------------------------------------------------------------------------------

To prevent the retrieval of huge numbers of entities just to create candidates
for the drop down of a reference during editing of an Entity, the number of
candidates can be limited.

Set the ``BUILD_MAX_EDIT_MODE_DROPDOWN_OPTIONS`` build variable. -1 means no
limit; every other number is the limit for each query sent (The number of
candidates can be at most twice this number).
