Customizing the display of referenced entities
=============================================

LinkAhead WebUI supports the customized display of referenced entities
using the :doc:`ext_references <../api/module-resolve_references>`
module. The ``BUILD_MODULE_EXT_RESOLVE_REFERENCES`` build variable has
to be set to ``ENABLED`` (see :doc:`/getting_started`) in order to use
this module.

You may then define your own JavaScript module to define how
references to specific Records should be resolved. The module has to
be located at a directory which is known to LinkAhead WebUI; we recommend
``linkahead-webui/src/ext/js``. Set the value of the
``BUILD_EXT_REFERENCES_CUSTOM_RESOLVER`` build variable to the name of
this module. The module has to have a ``resolve`` function which takes
an entity id as its only parameter and returns a ``reference_info``
object with the resolved custom reference as a ``text`` property. So
the basic structure of the module should look like

.. code-block:: javascript

   var my_reference_resolver = new function () {
       // Has to be called ``resolve`` and has to take exactly one
       // string parameter: the id of the referenced entity.
       this.resolve = async function (id) {
           /*
            * find the string that the reference should be resolved to,
            * e.g., from the value of the entity's properties.
            */
           return {"text": new_reference_text}
       }
   }

An example is located in
``linkahead-webui/src/core/js/reference_resolver/linkahead_default_person.js``. It
resolves any reference to a ``Person`` Record to the value of its ``firstname``
and ``lastname`` properties separated by a space and is active by default.
