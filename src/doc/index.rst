
Welcome to the documentation of LinkAhead's web UI!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Manual installation <install>
   Tutorials <tutorials/index>
   administration/index.rst
   Extending the UI <extension>
   API <api/index>
   Related Projects <related_projects/index>
   Back to overview <https://docs.indiscale.com/>

.. note::

   Go to `demo.indiscale.com <https://demo.indiscale.com>`__ and take the interactive tour to learn
   how to use LinkAhead's web interface.

This documentation helps you to :doc:`install the web UI <install>`, explains
the most important concepts and offers a range of
:doc:`tutorials<tutorials/index>`.


Indices and tables
==================

* :ref:`search`
