First Steps
===========

Before using or even manipulating data stored in LinkAhead, it is important to 
understand the way data is structured. Here, we will briefly look at this 
structure. You can find more details here_. In LinkAhead data is stored in objects called 
`Records`. A `Record` can have multiple `Properties`, like numbers, text or references
to other `Records`. `RecordTypes` are kind of blue prints for `Records` and 
provide a structure to the data. Let's look at an example:

.. image:: model.svg

.. The image is not good yet. Children should have properties of parents.

This illustrates a simple data model used in the `demo instance`_ provided by `IndiScale`_.
It shows that the `RecordType` Analysis has among others the `Properties` 
`quality_factor`, a number, and `date`, you guessed it... a date. The `Property`
`MusicalInstrumet` illustrates that a `Record` that has `Analysis` as a parent 
`RecordType` should reference a `Record` that has the `MusicalInstrumet` `RecordType` as a parent.

We recommend that you connect to the demo instance in order to try out the following
examples (see :doc:`Getting Started secton</getting_started>`.). However, you
can also translate the examples to the data model that you have at hand.



Main Menu (WIP)
---------------


.. note::  
   By default only 10 Entities are shown on one page. You can get to
   other pages with the “Next Page” and “Previous Page” buttons.

:math:`\Rightarrow` What are the differences between the options of the
“Entities” menu?

Entities, Records, Properties…What?


-  semantic data modeling

-  entries in LinkAhead are like Objects

-  RecordType: blue print for data

-  Record: actual data


See also the
`wiki <https://gitlab.com/caosdb/caosdb/wikis/Concepts/Data%20Model>`__
or the `paper <https://www.mdpi.com/2306-5729/4/2/83>`__

|image|

References in two directions

-  | References in LinkAhead are directed:
   | A Record A references another Record B

-  The referencing Record A has a corresponding Property.

-  The referenced Record B does not.

-  In order to get referencing Records in the Web Interface, click on the following button
    (or “Backref” on older systems).

|image1|

File System
-----------

-  Clicking on “File System” in the main menu allows you to browse files
   that LinkAhead knows about.

-  Typically, most files will be mounted from some file server.

.. note::   You will not find any Records in this view (that are not Files).



.. _here: https://gitlabio.something
.. _`demo instance`: https://demo.indiscale.com
.. _`IndiScale`: https://indiscale.com
.. |image| image:: model.svg
.. |image1| image:: References_button.png
   :width: 4em
