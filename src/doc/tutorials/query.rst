
Querying LinkAhead
===============

You should have the web interface of a LinkAhead instance at hand. If you do not
have one, you can visit https://demo.indiscale.com

Introduction
------------

The semantic data model of LinkAhead allows efficient data access. The
LinkAhead Query Language (CQL) is used to search data. Queries can be entered in
the webinterface under the respective menu entry.

Let's start with a simple one::

    FIND MusicalInstrument

Most queries simply start with the ``FIND`` keyword and describe what we are
looking for behind that. Then, we provided a RecordType name:
MusicalInstrument. This means that we will get all Records that have this
RecordType as parent. Try it out!

Let's look at::

    FIND ENTITY Guitar

When we add the ``ENTITY`` keyword we will get every entity that is a
Guitar -- also the RecordType, and even a Property with that name if there
exists one. Using  ``FIND RecordType Guitar`` would restrict the result to
only that RecordType. And ``FIND RECORD MusicalInstrument`` is just equivalent
to ``FIND MusicalInstrument``.

Note, that you can provide not only RecordType names after the ``FIND``, but names
in general: ``FIND "Nice Guitar"``. This will give you a Record with the
name "Nice Guitar" (if one exists... and there should be one in the demo instance).

While it does not matter whether you use capital letters or not, the names have to
be exact. There are two features that make it easy to use names for querying
in spite of this:
- You can use "*" to match any string. E.g. ``FIND Nice*``
- After typing three letters, names that start with those three are
suggested by the auto completion.

.. note::

   Train yourself by trying to guess what the result will be before
   actually executing the query.


Searching Data  Using Properties
--------------------------------

Looking for entities with certain names or such that have certain parents is
nice. However, the queries become really useful if we can impose further conditions
on the results. Let's start with an example again::

    FIND Guitar with price > 10000

This should list expensive guitars where are in the demo instance. Thus,
we are using a property (the price) of the Guitar Records to restrict the
result set. In general this looks like::

    FIND <Name> <Property Filter>

Typically, the filter has the form ``<Property> <Operator> <Value>``,
for example ``length >= 0.7mm``. Instead of the ``<Name>`` you can also use one
of the entity roles, namely ``RECORD``, ``RECORDTYPE``, ``FILE``, ``PROPERY``,
or ``ENTITY``.
There are many filters available. You can check the specification for a comprehensive description of
those. Here, we will only look at the most common examples.


If you only want to assure that Records have a certain Property, without imposing
constrains on the value, you can use::

   FIND MusicalInstrument WITH Manufacturer


Similarly, to what we saw above when using incomplete names, you can use a "*"
to match parts of text properties::

     FIND RECORD WITH serialNumber like KN*

There is large number of operators that can be used together with dates or
timestamps. One of the most useful is probably::

    FIND RECORD WITH date in 2019

A lot of valuable information is often stored in the relations among data, i.e. in
the references of entities. So how can we use those?::

    FIND RECORD WHICH REFERENCES A Guitar

This should be pretty self explanatory. And it is also possible to check for
references in the other direction::

     FIND RECORD WHICH IS REFERENCED BY A Analysis

You can also simply provide the ID of the entity::

   FIND RECORD WHICH IS REFERENCED BY 123``


Using Multiple Filters
----------------------

Often, one condition is not sufficient. Thus multiple filters/conditions can be combined.
This can for example be done using the following structure::

    FIND <Name> <Property Filter> (AND|OR) <Property Filter>

An example would be::

    FIND Guitar WITH price>48 AND electric=TRUE

Furthermore, reference conditions can be nested::

    FIND <Name> WHICH REFERENCES <Name> WHICH REFERENCES <Name>


For example::

    FIND Manufacturer WHICH IS REFERENCED BY Guitar WHICH IS REFERENCED BY Analysis


Restricting Result Information
------------------------------

Using ``COUNT`` instead of ``FIND`` will only return the number of
entities in the result set.

.. note::  This is often useful when experimenting with queries.

Using ``SELECT ... FROM`` instead of ``FIND`` returns specific
information in a table. A comma separated list of Property names can be provided behind the
``SELECT`` keyword::

   SELECT price, electric FROM Guitar

Or::

   SELECT quality_factor, report, date FROM Analysis WHICH REFERENCES A Guitar WITH electric=TRUE


