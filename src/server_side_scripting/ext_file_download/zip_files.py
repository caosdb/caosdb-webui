#!/usr/bin/env python3
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2019 IndiScale GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Creates a zip file from multiple file entities.  """

import argparse
import datetime
import io
import logging
import os
import sys
from tempfile import NamedTemporaryFile
from zipfile import ZipFile

import linkahead as db
import pandas as pd
from caosadvancedtools.serverside import helper
from linkahead import LinkAheadException, ConsistencyError, EntityDoesNotExistError


def _parse_arguments():
    """Parses the command line arguments.

    Takes into account defaults from the environment (where known).
    """
    parser = argparse.ArgumentParser(description='__doc__')
    parser.add_argument('-a', '--auth-token', required=False,
                        help=("An authentication token. If not provided LinkAhead's "
                              "pylib will search for other methods of "
                              "authentication if necessary."))
    parser.add_argument('ids', help="list of entity ids.")
    parser.add_argument('table', help="tsv table to be saved (as string).")

    return parser.parse_args()


def collect_files_in_zip(ids, table):
    # File output
    now = datetime.datetime.now()
    zip_name = "files.{time}.zip".format(
        time=now.strftime("%Y-%m-%dT%H_%M_%S"))
    zip_display_path, zip_internal_path = helper.get_shared_filename(zip_name)
    with ZipFile(zip_internal_path, 'w') as zf:
        nc = helper.NameCollector()

        # add the table which has been genereated by the webui table exporter
        with NamedTemporaryFile(delete=False) as table_file:
            # the file has been transmitted as string and has to be written to
            # a file first.
            table_file.write(table.encode())
        zf.write(table_file.name, "selected_table.tsv")

        # download and add all files
        for file_id in ids:
            try:
                tmp = db.get_entity_by_id(file_id, role="FILE")
            except EntityDoesNotExistError as e:
                # TODO
                # Current behavior: script terminates with error if just one
                # file cannot be retrieved.
                # Desired behavior: The script should go on with the other
                # ids, but the user should be informed about the missing files.
                # How should we do this?
                logger = logging.getLogger("caosadvancedtools")
                logger.error("Did not find File with ID={}.".format(
                    file_id))

                raise e
            savename = nc.get_unique_savename(os.path.basename(tmp.path))
            val_file = helper.get_file_via_download(
                tmp, logger=logging.getLogger("caosadvancedtools"))

            zf.write(val_file, savename)

    return zip_display_path


def main():
    args = _parse_arguments()

    if hasattr(args, "auth_token") and args.auth_token:
        db.configure_connection(auth_token=args.auth_token)

    id_list = [int(el) for el in args.ids.split(",")]

    zip_file = collect_files_in_zip(id_list, args.table)

    print(zip_file)


if __name__ == "__main__":
    main()
