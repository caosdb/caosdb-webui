/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* testing webcaosdb's javascript sources */

/* SETUP ext_references module */
QUnit.module("edit_mode.js", {
    before: function (assert) {
        this.form_elements_query = form_elements._query;
        this.edit_mode_query = edit_mode.query;
        var done = assert.async();
        retrieveTestEntities("edit_mode/getProperties_1.xml").then(entities => {
            this.testEntity_getProperties_1 = entities[0];
            this.testEntity_make_property_editable_1 = entities[0];
            done();
        }, err => {
            globalError(err);
            done();
        });
    },
    after: function (assert) {
        $('.modal.fade').has(".dropzone").remove();
    },
    afterEach: function (assert) {
        // remove mock up functions
        edit_mode.query = this.edit_mode_query;
        form_elements._query = this.form_elements_query;
    },
});

/**
 * Retrieve the test case xml and return an array of HTMLElement of entities in
 * the view mode representation.
 *
 * @return {HTMLElement[]}
 */
async function retrieveTestEntities(testCase, transform = true) {
    entities = await connection.get("xml/" + testCase);
    return transformation.transformEntities(entities);
}

QUnit.test("available", function (assert) {
    assert.ok(edit_mode);
});

QUnit.test("init", function (assert) {
    assert.ok(edit_mode.init);
});

QUnit.test("dragstart", function (assert) {
    assert.ok(edit_mode.dragstart);
});

QUnit.test("dragleave", function (assert) {
    assert.ok(edit_mode.dragleave);
});

QUnit.test("dragover", function (assert) {
    assert.ok(edit_mode.dragover);
});

function assert_throws(assert, cb, message, name, info) {
    try {
        cb();
        assert.ok(false, info);
    } catch (err) {
        assert.equal(err.message, message, "message ok: " + info);
        assert.equal(err.name, name, "name ok: " + info);
    }
}

QUnit.test("add_new_property", function (assert) {
    assert.ok(edit_mode.add_new_property);
    var done = assert.async(2);

    // test case setup
    var entity = $(`<div><ul class='caosdb-properties'/><li class="caosdb-f-entity-property"><ol><li>value1</li></ol></li><li class="caosdb-f-edit-mode-property-dropzone"></li></ul>`)[0];
    $(document.body).append(entity);
    var new_prop = $("<div class='test_new_prop'/>")[0];

    // test bad cases
    assert_throws(assert, () => {
            edit_mode.add_new_property(undefined, new_prop);
        },
        "entity must instantiate HTMLElement",
        "TypeError",
        "undefined entity param");
    assert_throws(assert, () => {
            edit_mode.add_new_property(entity, undefined);
        },
        "new_prop must instantiate HTMLElement",
        "TypeError",
        "undefined new_prop param");


    // test good cases
    assert.equal($(entity).find(".test_new_prop").length, 0, "no property");
    entity.addEventListener(edit_mode.property_added.type, function (e) {
        assert.ok(e.target === new_prop, "event fired on newprop");
        assert.ok(this === entity, "event detected on entity");
        done();
    }, true);
    edit_mode.add_new_property(entity, new_prop, (x) => {
        assert.ok(x === new_prop,
            "make_property_editable_cb called");
        done();
    });
    assert.equal($(entity).find(".test_new_prop").length, 1, "one property");
    // event has been fired and property has been added.

    $(entity).remove();

});

QUnit.test("property_added", function (assert) {
    assert.ok(edit_mode.property_added, "available");
    assert.ok(edit_mode.property_added instanceof Event, "is event");
});

QUnit.test("add_dropped_property", function (assert) {
    assert.ok(edit_mode.add_dropped_property);
});

QUnit.test("add_dropped_parent", function (assert) {
    assert.ok(edit_mode.add_dropped_parent);
});

QUnit.test("set_entity_dropable", function (assert) {
    assert.ok(edit_mode.set_entity_dropable);
});

QUnit.test("unset_entity_dropable", function (assert) {
    assert.ok(edit_mode.unset_entity_dropable);
});

QUnit.test("remove_save_button", function (assert) {
    assert.ok(edit_mode.remove_save_button);
});

QUnit.test("add_save_button", function (assert) {
    assert.ok(edit_mode.add_save_button);
});

QUnit.test("add_trash_button", function (assert) {
    assert.ok(edit_mode.add_trash_button);
});

QUnit.test("add_parent_trash_button", function (assert) {
    assert.ok(edit_mode.add_parent_trash_button);
});

QUnit.test("add_parent_delete_buttons", function (assert) {
    assert.ok(edit_mode.add_parent_delete_buttons);
});

QUnit.test("add_property_trash_button", function (assert) {
    assert.ok(edit_mode.add_property_trash_button);
});

QUnit.test("insert_entity", function (assert) {
    assert.ok(edit_mode.insert_entity);
});

QUnit.test("getProperties", function (assert) {
    assert.ok(edit_mode.getProperties);

    assert.equal(edit_mode.getProperties(undefined).length, 0, "undefined returns empty list");


    // test for correct parsing of datatypes
    var testEntity = this.testEntity_getProperties_1;

    var properties = edit_mode.getProperties(testEntity);
    assert.equal(properties.length, 6);

    var datatypes = ["TEXT", "DOUBLE", "INTEGER", "FILE", "DATETIME", "BOOLEAN"];
    for (var i = 0; i < properties.length; i++) {
        var p = properties[i];
        var datatype = datatypes[i];
        assert.equal(p.datatype, datatype, "test " + datatype);
    }

});

QUnit.test("update_entity", function (assert) {
    assert.ok(edit_mode.update_entity);
});

QUnit.test("add_edit_mode_button", function (assert) {
    assert.ok(edit_mode.add_edit_mode_button);

    var target = $(document.body)[0];
    assert.equal($(".caosdb-f-btn-toggle-edit-mode").length, 0, "initially no edit_mode_button");
    var button = edit_mode.add_edit_mode_button(target);
    assert.equal($(".caosdb-f-btn-toggle-edit-mode").length, 1, "1 edit_mode_button");

    $(button).remove();
});

QUnit.test("toggle_edit_mode", function (assert) {
    assert.ok(edit_mode.toggle_edit_mode);
});

QUnit.test("leave_edit_mode", function (assert) {
    assert.ok(edit_mode.leave_edit_mode);
});

QUnit.test("enter_edit_mode", function (assert) {
    assert.ok(edit_mode.enter_edit_mode);
});

QUnit.test("make_header_editable", function (assert) {
    assert.ok(edit_mode.make_header_editable);
});

QUnit.test("isListDatatype", function (assert) {
    assert.ok(edit_mode.unListDatatype);
});

QUnit.test("make_datatype_input_logic", function (assert) {
    assert.ok(edit_mode.make_datatype_input_logic);
});

QUnit.test("make_datatype_input", function (assert) {
    var done = assert.async(9);

    // mock query response of server
    form_elements._query = async function (q) {
        log.getLogger("edit_mode").trace(q);
        const entities = str2xml(`<Response>
            <RecordType name="Person"/></Response>`);
        return transformation.transformEntities(entities);
    };
    const form_wrapper = "<form/>";

    const no_dt_input = edit_mode.make_datatype_input(undefined);
    no_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(no_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "TEXT",
            "reference_scope": null,
        }, "No datatype (defaults to TEXT)");
        done();
    }, true);

    const text_dt_input = edit_mode.make_datatype_input("TEXT");
    text_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(text_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "TEXT",
            "reference_scope": null,
        }, "TEXT");
        done();
    }, true);

    const ref_dt_input = edit_mode.make_datatype_input("REFERENCE");
    ref_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(ref_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "REFERENCE",
            "reference_scope": null,
        }, "REF");
        done();
    }, true);

    const file_dt_input = edit_mode.make_datatype_input("FILE");
    file_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(file_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "FILE",
            "reference_scope": null,
        }, "FILE");
        done();
    }, true);

    const person_dt_input = edit_mode.make_datatype_input("Person");
    person_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(person_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "REFERENCE",
            "reference_scope": "Person",
        }, "Person");
        done();
    }, true);

    const list_text_dt_input = edit_mode.make_datatype_input("LIST<TEXT>");
    list_text_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(list_text_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "TEXT",
            "reference_scope": null,
            "is_list": "on",
        }, "LIST<TEXT>");
        done();
    }, true);

    const list_ref_dt_input = edit_mode.make_datatype_input("LIST<REFERENCE>");
    list_ref_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(list_ref_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "REFERENCE",
            "reference_scope": null,
            "is_list": "on",
        }, "LIST<REFERENCE>");
        done();
    }, true);

    const list_file_dt_input = edit_mode.make_datatype_input("LIST<FILE>");
    list_file_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(list_file_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "FILE",
            "reference_scope": null,
            "is_list": "on",
        }, "LIST<FILE>");
        done();
    }, true);

    const list_per_dt_input = edit_mode.make_datatype_input("LIST<Person>");
    list_per_dt_input.addEventListener("caosdb.field.ready", function (e) {
        var obj = form_elements
            .form_to_object($(form_wrapper).append(list_per_dt_input)[0])[0];
        assert.propEqual(obj, {
            "atomic_datatype": "REFERENCE",
            "reference_scope": "Person",
            "is_list": "on",
        }, "LIST<Person>");
        done();
    }, true);

});

QUnit.test("make_input", function (assert) {
    assert.ok(edit_mode.make_input);
});

QUnit.test("smooth_replace", function (assert) {
    assert.ok(edit_mode.smooth_replace);
});

QUnit.test("make_property_editable", function (assert) {
    assert.ok(edit_mode.make_property_editable);

    assert.throws(() => edit_mode.make_property_editable(undefined), /param 'element' is expected to be an HTMLElement, was undefined/, "undefined");


    // test for correct parsing of datatypes
    var testEntity = this.testEntity_make_property_editable_1;

    for (var element of $(testEntity).find('.caosdb-f-entity-property')) {
        edit_mode.make_property_editable(element);
    }

});

QUnit.test("create_new_record", function (assert) {
    assert.ok(edit_mode.create_new_record);
});

QUnit.test("init_edit_app", function (assert) {
    assert.ok(edit_mode.init_edit_app);
});

QUnit.test("has_errors", function (assert) {
    assert.ok(edit_mode.has_errors);
});

QUnit.test("freeze_but", function (assert) {
    assert.ok(edit_mode.freeze_but);
});

QUnit.test("unfreeze", function (assert) {
    assert.ok(edit_mode.unfreeze);
});

QUnit.test("retrieve_datatype_list", async function (assert) {
    assert.ok(edit_mode.retrieve_datatype_list);
    var query_done;

    edit_mode.query = function (query) {
        var re = /^(FIND|COUNT) (Record|File) "Ice Core"/g;
        assert.ok(query.match(re), `${query} should match ${re}`);
        query_done();
        return [];
    }
    query_done = assert.async(4);
    await edit_mode.retrieve_datatype_list("Ice Core");
});

QUnit.test("highlight", function (assert) {
    assert.ok(edit_mode.highlight);
});

QUnit.test("unhighlight", function (assert) {
    assert.ok(edit_mode.unhighlight);
});

QUnit.test("handle_error", function (assert) {
    assert.ok(edit_mode.handle_error);
});

QUnit.test("get_edit_panel", function (assert) {
    assert.ok(edit_mode.get_edit_panel);
});

QUnit.test("toggle_edit_panel", function (assert) {
    assert.ok(edit_mode.toggle_edit_panel);
});

QUnit.test("leave_edit_mode_template", function (assert) {
    assert.ok(edit_mode.leave_edit_mode_template);
});

QUnit.test("is_edit_mode", function (assert) {
    assert.ok(edit_mode.is_edit_mode);
});

QUnit.test("add_cancel_button", function (assert) {
    assert.ok(edit_mode.add_cancel_button);
});

QUnit.test("create_new_entity", function (assert) {
    assert.ok(edit_mode.create_new_entity);
});

QUnit.test("remove_cancel_button", function (assert) {
    assert.ok(edit_mode.remove_cancel_button);
});

QUnit.test("freeze_entity", function (assert) {
    assert.ok(edit_mode.freeze_entity);
});

QUnit.test("unfreeze_entity", function (assert) {
    assert.ok(edit_mode.unfreeze_entity);
});

QUnit.test("filter", function (assert) {
    assert.ok(edit_mode.filter);
});

QUnit.test("add_start_edit_button", function (assert) {
    assert.ok(edit_mode.add_start_edit_button);
});

QUnit.test("remove_start_edit_button", function (assert) {
    assert.ok(edit_mode.remove_start_edit_button);
});

QUnit.test("add_new_record_button", function (assert) {
    assert.ok(edit_mode.add_new_record_button);
});

QUnit.test("remove_new_record_button", function (assert) {
    assert.ok(edit_mode.remove_new_record_button);
});

QUnit.test("add_delete_button", function (assert) {
    assert.ok(edit_mode.add_delete_button);
});

QUnit.test("remove_delete_button", function (assert) {
    assert.ok(edit_mode.remove_delete_button);
});


edit_mode.query = async function(q) {
    return [];
}

QUnit.test("test case 1 - insert property", async function (assert) {

    // here lives the test tool box
    const test_tool_box = $('<div class="caosdb-f-edit-panel-body" > <div class="list-group list-group-flush"> <div class="list-group-item btn-group-vertical caosdb-v-editmode-btngroup caosdb-f-edit-mode-create-buttons"> <button type="button" class="btn btn-secondary caosdb-f-edit-panel-new-button new-property">Create Property</button> <button type="button" class="btn btn-secondary caosdb-f-edit-panel-new-button new-recordtype">Create RecordType</button> </div> </div>');

    // here live the entities
    const main_panel = $('<div class="caosdb-f-main-entities"/>');
    assert.equal($(".caosdb-f-main-entities").length, 0);

    $(document.body).append(test_tool_box).append(main_panel);


    // ENTER EDIT MODE
    assert.equal(edit_mode.is_edit_mode(), false, "edit_mode should not be active");
    // fake server response
    edit_mode.retrieve_data_model = async function () {
        return str2xml("<Response/>");
    }
    var app = await edit_mode.enter_edit_mode();
    assert.equal(edit_mode.is_edit_mode(), true, "now, edit_mode should be active");


    // NEW PROPERTY
    assert.equal($(".caosdb-f-edit-panel-new-button.new-property").length, 1, "one new-property button should be present");
    assert.equal($(".caosdb-entity-panel").length, 0, "no entities");
    assert.equal(app.state, "initial", "initial state");
    // click on "new property"
    $(".caosdb-f-edit-panel-new-button.new-property").first().click();

    while (app.state === "initial") {
        await sleep(500);
    }

    // EDIT PROPERTY
    assert.equal(app.state, "changed", "changed state");
    var entity = $(".caosdb-entity-panel");
    assert.equal(entity.length, 1, "entity added");
    // set name
    $(".caosdb-entity-panel .caosdb-f-entity-name").val("TestProperty");

    // SAVE
    var save_button = $(".caosdb-f-entity-save-button");
    assert.equal(save_button.length, 1, "save button available");
    // fake server response
    connection.post = async function (uri, data) {
        await sleep(500);
        assert.equal(xml2str(data), "<Request><Property name=\"TestProperty\" datatype=\"TEXT\"/></Request>");
        assert.equal(app.state, "wait", "in wait state");
        return str2xml("<Response><Property id=\"newId\" name=\"TestProperty\" datatype=\"TEXT\"/></Response>");
    }
    // click save button
    var updated_entity = main_panel.find(".caosdb-entity-panel .caosdb-id:contains('newId')");
    assert.equal(updated_entity.length, 0, "entity with id not yet in main panel");
    save_button.click();

    while (app.state === "changed" || app.state === "wait") {
        await sleep(500);
    }

    // SEE RESPONSE
    assert.equal(app.state, "initial", "initial state");

    var response = $("#newId");
    assert.equal(response.length, 1, "entity added");

    // entity has been added to main panel
    updated_entity = main_panel.find(".caosdb-entity-panel .caosdb-id:contains('newId')");
    assert.equal(updated_entity.length, 1, "entity with new id now in main panel");

    // tests for closed issue https://gitlab.com/caosdb/caosdb-webui/issues/47
    assert.equal(main_panel.find(".caosdb-entity-panel .caosdb-entity-actions-panel").length, 1, "general actions panel there");
    assert.equal(main_panel.find(".caosdb-entity-panel .caosdb-f-edit-mode-entity-actions-panel").length, 1, "edit_mode actions panel there (BUG linkahead-webui#47)");

    main_panel.remove();
    test_tool_box.remove();

    edit_mode.leave_edit_mode();
    assert.equal(edit_mode.is_edit_mode(), false, "edit_mode should not be active");

});


var transformProperty = async function (xml_str) {
    var xml = str2xml(`<Response>${xml_str}</Response>`);
    return (await transformation.transformProperty(xml)).firstElementChild;
}


QUnit.test("createElementForProperty", async function (assert) {
    assert.timeout(100000);
    // unique dummy values for each data type
    var data = {
        "TEXT": "Single",
        "DATETIME": "1996-12-15",
        "DOUBLE": "4.5",
        "INTEGER": "111",
        "BOOLEAN": "TRUE",
        "FILE": "1234",
        "REFERENCE": "3456",
        "Person": "5678",
    }

    for (let dt of ["BOOLEAN", "DATETIME", "TEXT", "Person", "DOUBLE", "FILE", "REFERENCE", "INTEGER"]) {
        var prop = {
            datatype: dt,
            reference: ["Person", "REFERENCE", "FILE"].includes(dt),
            value: data[dt],
        }

        var options = async () => {
          return [$(`<option>${data[dt]}</option>`)[0]];
        }
        var html = $(edit_mode.createElementForProperty(prop, options()));

        await sleep(100);

        if($(html).find("select").val()) {
            assert.equal($(html).find("select").val(), data[dt], `${dt} input has correct value`);

        } else {
            assert.equal($(html).val(), data[dt], `${dt} input has correct value`);
        }


    }
});

QUnit.test("getPropertyFromElement", async function (assert) {
    assert.timeout(100000);
    // unique dummy values for each data type
    var data = {
        "TEXT": "Single",
        "LIST<TEXT>": "One",
        "DATETIME": "1996-12-15",
        "LIST<DATETIME>": "2020-01-01T20:15:18",
        "DOUBLE": "4.5",
        "LIST<DOUBLE>": "5.5",
        "INTEGER": "111",
        "LIST<INTEGER>": "222",
        "BOOLEAN": "TRUE",
        "LIST<BOOLEAN>": "FALSE",
        "FILE": "1234",
        "LIST<FILE>": "2345",
        "REFERENCE": "3456",
        "LIST<REFERENCE>": "4567",
        "Person": "5678",
        "LIST<Person>": "6789",
    }
    for (let dt of ["BOOLEAN", "DATETIME", "TEXT", "Person", "DOUBLE", "FILE", "REFERENCE", "INTEGER"]) {
        var non_list_prop_xml_str = `<Property unit="m-${dt}" datatype="${dt}">${data[dt]}</Property>`;
        var list_prop_xml_str = `<Property unit="kg-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data["LIST<" + dt + ">"]}</Value></Property>`;
        var long_list_prop_xml_str = `<Property unit="s-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data[dt]}</Value><Value>${data[dt]}</Value></Property>`;

        var non_list_prop_html = await transformProperty(non_list_prop_xml_str);
        var list_prop_html = await transformProperty(list_prop_xml_str);
        var long_list_prop_html = await transformProperty(long_list_prop_xml_str);

        var non_list_pre = getPropertyFromElement(non_list_prop_html);
        assert.equal(non_list_pre.value, data[dt], "pre has value");

        edit_mode.make_property_editable(non_list_prop_html);
        edit_mode.make_property_editable(list_prop_html);
        edit_mode.make_property_editable(long_list_prop_html);

        var non_list_prop = edit_mode.getPropertyFromElement(non_list_prop_html);
        var list_prop = edit_mode.getPropertyFromElement(list_prop_html);
        var long_list_prop = edit_mode.getPropertyFromElement(long_list_prop_html);

        if (dt == "DOUBLE" || dt == "INTEGER") {
            assert.equal(non_list_prop.unit, `m-${dt}`, "non-list property has correct unit");
            assert.equal(list_prop.unit, `kg-${dt}`, "list property has correct unit");
            assert.equal(long_list_prop.unit, `s-${dt}`, "long list property has correct unit");
        }

        assert.equal(non_list_prop.datatype, dt, `${dt} non list has correct datatype`);
        assert.equal(list_prop.datatype, `LIST<${dt}>`, `${dt} list has correct datatype`);
        assert.propEqual(long_list_prop.datatype, `LIST<${dt}>`, `${dt} long list has correct datatype`);

        assert.equal(non_list_prop.value, data[dt], `${dt} non list has correct value`);
        assert.equal(list_prop.value, data[`LIST<${dt}>`], `${dt} list has correct value`);
        assert.propEqual(long_list_prop.value, [data[dt], data[dt]], `${dt} long list has correct values`);


    }

});


/**
 * The _toggle_list_property_object method converts a property object which represents
 * a non-list property into one that presents a list property and vice-versa.
 *
 * This test case performs a large set of different data types.
 */
QUnit.test("_toggle_list_property_object", async function (assert) {

    // unique dummy values for each data type
    var data = {
        "TEXT": "Single",
        "LIST<TEXT>": "One",
        "DATETIME": "1996-12-15",
        "LIST<DATETIME>": "2020-01-01T20:15:18",
        "DOUBLE": "4.5",
        "LIST<DOUBLE>": "5.5",
        "INTEGER": "111",
        "LIST<INTEGER>": "222",
        "BOOLEAN": "TRUE",
        "LIST<BOOLEAN>": "FALSE",
        "FILE": "1234",
        "LIST<FILE>": "2345",
        "REFERENCE": "3456",
        "LIST<REFERENCE>": "4567",
        "Person": "5678",
        "LIST<Person>": "6789",
    }
    for (let dt of ["DATETIME", "TEXT", "Person", "DOUBLE", "FILE", "REFERENCE", "INTEGER", "BOOLEAN"]) {
        var non_list_prop_xml_str = `<Property unit="m-${dt}" datatype="${dt}">${data[dt]}</Property>`;
        var list_prop_xml_str = `<Property unit="kg-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data["LIST<" + dt + ">"]}</Value></Property>`;
        var long_list_prop_xml_str = `<Property unit="s-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data[dt]}</Value><Value>${data[dt]}</Value></Property>`;

        var non_list_prop_html = await transformProperty(non_list_prop_xml_str);
        var list_prop_html = await transformProperty(list_prop_xml_str);
        var long_list_prop_html = await transformProperty(long_list_prop_xml_str);

        edit_mode.make_property_editable(non_list_prop_html);
        edit_mode.make_property_editable(list_prop_html);
        edit_mode.make_property_editable(long_list_prop_html);

        assert.notOk(edit_mode._toggle_list_property_object(non_list_prop_html, false), "no changes");
        assert.notOk(edit_mode._toggle_list_property_object(list_prop_html, true), "no changes");

        var list_inputs_property = edit_mode._toggle_list_property_object(non_list_prop_html, true);
        var single_input_property = edit_mode._toggle_list_property_object(list_prop_html, false);

        assert.equal(single_input_property.value, data[`LIST<${dt}>`], "list->single has correct value");
        if (dt == "INTEGER" || dt == "DOUBLE") {
            assert.equal(single_input_property.unit, `kg-${dt}`, "list -> single has correct unit");
        }
        assert.equal(single_input_property.list, false, "list->single is now a single property");
        assert.equal(single_input_property.datatype, dt, "list->single has correct datatype");
        assert.notOk(single_input_property.listDatatype, "list->single has correct listDatatype (undefined).");
        assert.propEqual(list_inputs_property.value, [data[dt]], "single->list has correct value");
        if (dt == "INTEGER" || dt == "DOUBLE") {
            assert.equal(list_inputs_property.unit, `m-${dt}`, "single -> list has correct unit");
        }
        assert.equal(list_inputs_property.list, true, "single->list is now a list");
        assert.equal(list_inputs_property.datatype, `LIST<${dt}>`, "single->list has correct datatype");
        assert.equal(list_inputs_property.listDatatype, dt, `single->list has correct listDatatype (${dt}).`);


        assert.throws(() => {
            edit_mode._toggle_list_property_object(long_list_prop_html, false)
        }, /Could not toggle to list=false with value=/, "long list throws on list->single");

    }
});


/**
 * _toggle_list_property converts a HTML representation (in edit_mode) of a non-list
 * property into a list property and vice versa. It internally uses
 * _toggle_list_property_object: HTML -> JS Object -> Converted JS Object ->
 * Converted HTML.
 */
QUnit.test("_toggle_list_property", async function (assert) {

    // unique dummy values for each data type
    var data = {
        "TEXT": "Single",
        "LIST<TEXT>": "One",
        "DATETIME": "1996-12-15",
        "LIST<DATETIME>": "2020-01-01T20:15:19",
        "DOUBLE": "4.5",
        "LIST<DOUBLE>": "5.5",
        "INTEGER": "111",
        "LIST<INTEGER>": "222",
        "BOOLEAN": "TRUE",
        "LIST<BOOLEAN>": "FALSE",
        "FILE": "1234",
        "LIST<FILE>": "2345",
        "REFERENCE": "3456",
        "LIST<REFERENCE>": "4567",
        "Person": "5678",
        "LIST<Person>": "6789",
    }
    for (let dt of ["DATETIME", "BOOLEAN", "TEXT", "DOUBLE", "FILE", "REFERENCE", "Person", "INTEGER"]) {
        var non_list_prop_xml_str = `<Property unit="m-${dt}" datatype="${dt}">${data[dt]}</Property>`;
        var list_prop_xml_str = `<Property unit="kg-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data["LIST<" + dt + ">"]}</Value></Property>`;
        var long_list_prop_xml_str = `<Property unit="s-${dt}" datatype="LIST&lt;${dt}&gt;"><Value>${data[dt]}</Value><Value>${data[dt]}</Value></Property>`;

        var non_list_prop_html = await transformProperty(non_list_prop_xml_str);
        var list_prop_html = await transformProperty(list_prop_xml_str);
        var long_list_prop_html = await transformProperty(long_list_prop_xml_str);

        edit_mode.make_property_editable(non_list_prop_html);
        edit_mode.make_property_editable(list_prop_html);
        edit_mode.make_property_editable(long_list_prop_html);

        assert.equal($(non_list_prop_html).find(".caosdb-f-property-value").length, 1);

        assert.notOk(edit_mode._toggle_list_property(non_list_prop_html, false), "no changes");
        assert.notOk(edit_mode._toggle_list_property(list_prop_html, true), "no changes");

        var list_inputs = edit_mode._toggle_list_property(non_list_prop_html, true);
        var single_input = edit_mode._toggle_list_property(list_prop_html, false);

        assert.throws(() => {
            edit_mode._toggle_list_property(long_list_prop_html, false)
        }, /Could not toggle to list=false with value=/, "long list throws on list->single");

        assert.equal($(list_inputs[0].parentElement).find("ol li :input:not(button)").is.length, 1, "one list input");
        assert.equal($(list_inputs[0].parentElement).find("ol li :input").val(), data[dt], `list ${dt} input has correct value`);
        assert.equal($(single_input.parentElement).find("ol :input").length, 0, "no list input");
        var val = $(single_input).val();
        if (dt == "DATETIME") {
            val = $(single_input).find("[type='date']").val() + "T" + $(single_input).find("[type='time']").val();
        } else if (["BOOLEAN", "Person", "FILE", "REFERENCE"].indexOf(dt) > -1) {
            val = $(single_input).find("select").val();
        }
        assert.equal(val, data[`LIST<${dt}>`], `single ${dt} input has correct value`);
    }


});


/**
 * Tests the buggy behavior of retrieve_datatype_list and
 * fill_reference_drop_down. The bug deleted the value of reference properties
 * and it had a two-fold cause:
 *
 * Firstly, properties with plain `REFERENCE` or `LIST<REFERENCE>` data types
 * (or 'FILE`) would no construct a correct query but the executed a `FIND
 * Record REFERENCE` which always returns an emtpy result set as options.
 *
 * Then, an empty set of options would produce a completely empty drop-down menu
 * (from which the user would normally choose a reference and the old reference
 * was pre-selected), effectively deleting the old value of the reference.
 */
QUnit.test("Bug #95", async function (assert) {
    var query_done;
    edit_mode.query = function (query) {
        var re = /^(COUNT|FIND) (Record|File)\s*/g;
        assert.ok(query.match(re), `${query} should match ${re}`);
        query_done();
        return [];
    }

    // retrieve_datatype_list calls edit_mode.query with correct query string.
    query_done = assert.async(4);
    await edit_mode.retrieve_datatype_list("REFERENCE");
    query_done = assert.async(2); // only called with file
    await edit_mode.retrieve_datatype_list("FILE");

    var options = async () => { await sleep(100); return [$("<option>1234</option>")[0]]; };
    var property = $("<div/>")
        .append(edit_mode.createElementForProperty({
            reference: true,
            datatype: "REFERENCE",
            list: false,
            value: "1234"
        }, options()));

    assert.equal($(property).find("select").val(), "1234", "old value before");
    assert.equal($(property).find("option").length, 1, "one option before");
    assert.equal($(property).find(":selected").text(), "1234", "old text before");

    await sleep(200);

    assert.equal($(property).find("select").val(), "1234", "old value after");
    assert.equal($(property).find("option").length, 2, "two options after (one empty)");
    assert.equal($(property).find(":selected").text(), "1234", "old text after");


    // now an integration test. A list of options is passed to 
    options = async () => {
        return edit_mode._create_reference_options(await transformation
            .transformEntities(str2xml(`
            <Response>
              <Record name="RName1" id="RID1"/>
              <Record name="RName2" id="RID2"/>
              <Record name="RName1234" id="1234"/>
            </Response>
            `))
        );
    }

    var fill_method_done = assert.async();
    var proxied = edit_mode.fill_reference_drop_down;
    edit_mode.fill_reference_drop_down = function (arg1, arg2, arg3) {
        proxied(arg1, arg2, arg3);

        assert.equal($(arg1).val(), "1234", "still old value after");
        assert.equal($(arg1).find("option").length, 4, "4 options after");
        assert.equal($(arg1).find(":selected").text(), "Name: RName1234, LinkAhead ID: 1234", "new text after");

        edit_mode.fill_reference_drop_down = proxied;
        fill_method_done();
    }

    property = $("<div/>")
        .append(edit_mode.createElementForProperty({
            reference: true,
            datatype: "REFERENCE",
            list: false,
            value: "1234"
        }, options()));

});


QUnit.test("fill_reference_drop_down", async function (assert) {
    var options = edit_mode._create_reference_options(await transformation
        .transformEntities(str2xml(`
        <Response>
          <Record name="RName1" id="RID1"/>
          <Record name="RName2" id="RID2"/>
          <Record name="RName1234" id="1234"/>
        </Response>
        `)));

    assert.equal(options.length, 3, "3 entities returned");
    assert.ok($(options).is("option"), "options contains options");

    var select = $('<select></select>');
    edit_mode.fill_reference_drop_down(select[0], options, "1234");

    assert.equal(select.find("option").length, 3, "3 options after");
    assert.equal(select.val(), "1234", "old value after");
    assert.equal(select.find(":selected").text(), "Name: RName1234, LinkAhead ID: 1234", "new text after");

});


/**
 * Test the inner logic of retrieve_datatype_list.
 */
QUnit.test("_create_reference_options", async function (assert) {
    var entities = await transformation
        .transformEntities(str2xml(`
    <Response>
      <Record name="RName" id="RID"/>
    </Response>
    `));
    assert.equal(edit_mode._create_reference_options(entities)[0].value, "RID");
    assert.equal(edit_mode._create_reference_options(entities)[0].innerHTML, "Name: RName, LinkAhead ID: RID");
});
