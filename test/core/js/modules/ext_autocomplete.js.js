/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("ext_autocomplete.js", {
    before: function (assert){
        ext_autocomplete.retrieve_names = async function () {
            return ['IceCore', 'Bag', 'IceSample', 'IceCream', 'Palette', 'Ice Core'];
        }
        ext_autocomplete.init();

    }
});

QUnit.test("availability", function(assert) {
    //assert.ok(bootstrap..init, "init available");
    assert.equal(ext_autocomplete.version, "0.1", "test version");
    assert.ok(ext_autocomplete.init, "init available");
});



QUnit.test("starts_with_filter", function(assert) {
    assert.equal(ext_autocomplete.starts_with_filter('IceCore','Ice'), true, 'test filter')
    assert.equal(ext_autocomplete.starts_with_filter('IceCore','iCe'), true, 'test filter')
    assert.equal(ext_autocomplete.starts_with_filter('IceCore','Core'), false, 'test filter')
    assert.equal(ext_autocomplete.starts_with_filter('Bag','Ice'), false, 'test filter')
});

QUnit.test("search", async function(assert) {

    var done = assert.async(2);
    var gcallback = function(expresults){
        return function (results) {
            assert.propEqual(
                results, 
                expresults, 
                "test list filter");
            done();
        };
    };
    await ext_autocomplete.search("Ice", 
        gcallback( ['IceCore', 'IceSample', 'IceCream', 'Ice Core'])
    );

    await ext_autocomplete.search("Core", gcallback([]));
});

QUnit.test("searchPost", async function(assert) {
  const resultsFromServer = ["Ice Core", "IceCore"];
  const origJQElement = [{
    selectionEnd: 8,
    value: "FIND Ice WHERE",
  }];

  const expected = [
      {
        "html": "Ice Core",
        "text": "FIND \"Ice Core\" WHERE"
      },
      {
        "html": "IceCore",
        "text": "FIND IceCore WHERE"
      }
  ];

  const result = ext_autocomplete.searchPost(resultsFromServer, origJQElement);
  assert.propEqual(result, expected);
});

QUnit.test("searchPost webui#170", async function(assert) {
  // https://gitlab.com/linkahead/linkahead-webui/-/issues/170
  // Autocompletion for "IS REFERENCED BY" leads to query syntax error
  const resultsFromServer = ["REFERENCED BY"];
  const origJQElement = [{
    selectionEnd: 24,
    value: "FIND Event WHICH IS REFE",
  }];

  const expected = [
      {
        "html": "REFERENCED BY",
        "text": "FIND Event WHICH IS REFERENCED BY"
      },
  ];

  const result = ext_autocomplete.searchPost(resultsFromServer, origJQElement);
  assert.propEqual(result, expected);
});

QUnit.test("class", function(assert) {
    assert.ok(ext_autocomplete.switch_on_completion , "toggle available");
    assert.ok(ext_autocomplete.switch_on_completion() , "toggle runs");
});
