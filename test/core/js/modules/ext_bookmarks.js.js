/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("ext_bookmarks.js", {
    before: function (assert) {
        // setup before module
        ext_bookmarks.set_collection_id("test");
        // otherwise tests would collide with actual bookmarks
    },
    beforeEach: function (assert) {
        // setup before each test
    },
    afterEach: function (assert) {
        // teardown after each test
        ext_bookmarks.clear_bookmark_storage();
        connection._init();
    },
    after: function (assert) {
        // teardown after module
    }
});

QUnit.test("parse_uri", function (assert) {
    assert.equal(typeof ext_bookmarks.parse_uri(""), "undefined");
    assert.equal(typeof ext_bookmarks.parse_uri("asdf"), "undefined");
    assert.equal(typeof ext_bookmarks.parse_uri("https://localhost:1234/Entity/sada?sadfasd#sdfgdsf"), "undefined");

    assert.propEqual(ext_bookmarks.parse_uri("safgsa/123&456&789?sadfasdf#_bm_1"), {
        bookmarks: ["123", "456", "789"],
        collection_id: "1"
    });
});

QUnit.test("get_bookmarks, clear_bookmark_storage", function (assert) {
    assert.propEqual(ext_bookmarks.get_bookmarks(), []);

    ext_bookmarks.bookmark_storage[ext_bookmarks.get_key("sdfg")] = "3456"
    assert.propEqual(ext_bookmarks.get_bookmarks(), ["3456"]);

    ext_bookmarks.clear_bookmark_storage();
    assert.propEqual(ext_bookmarks.get_bookmarks(), []);
});

QUnit.test("get_export_table", async function (assert) {
    connection.get = (id) => `<root><Response><File id="${id}" path="testpath_${id.split("/")[1]}"><Version id="abcHead"/></File></Response></root>`;
    const TAB = "%09";
    const NEWL = "%0A";
    const context_root = connection.getBasePath() + "Entity/";
    var table = await ext_bookmarks.get_export_table(
        ["123@ver1", "456@ver2", "789@ver3", "101112", "@131415"]);
    assert.equal(table,
        `data:text/csv;charset=utf-8,ID${TAB}Version${TAB}URI${TAB}Path${TAB}Name${TAB}RecordType${NEWL}123${TAB}ver1${TAB}${context_root}123@ver1${TAB}testpath_123@ver1${TAB}${TAB}${NEWL}456${TAB}ver2${TAB}${context_root}456@ver2${TAB}testpath_456@ver2${TAB}${TAB}${NEWL}789${TAB}ver3${TAB}${context_root}789@ver3${TAB}testpath_789@ver3${TAB}${TAB}${NEWL}101112${TAB}abcHead${TAB}${context_root}101112@abcHead${TAB}testpath_101112${TAB}${TAB}${NEWL}${TAB}131415${TAB}${context_root}@131415${TAB}testpath_@131415${TAB}${TAB}`);

});

QUnit.test("update_clear_button", function (assert) {
    const clear_button = $(`<div id="caosdb-f-bookmarks-clear"/>`);
    $("body").append(clear_button);

    assert.notOk(clear_button.is(".disabled"));
    ext_bookmarks.update_clear_button([]);
    assert.ok(clear_button.is(".disabled"));

    ext_bookmarks.update_clear_button(["asdf"]);
    assert.notOk(clear_button.is(".disabled"));

    ext_bookmarks.update_clear_button(["asdf"]);
    assert.notOk(clear_button.is(".disabled"));

    ext_bookmarks.update_clear_button([]);
    assert.ok(clear_button.is(".disabled"));

    clear_button.remove();
});

QUnit.test("update_export_link", function (assert) {
    const export_link = $(`<div id="caosdb-f-bookmarks-export-link"/>`);
    $("body").append(export_link);

    assert.notOk(export_link.is(".disabled"));
    ext_bookmarks.update_export_link([]);
    assert.ok(export_link.is(".disabled"));

    ext_bookmarks.update_export_link(["asdf"]);
    assert.notOk(export_link.is(".disabled"));

    ext_bookmarks.update_export_link(["asdf"]);
    assert.notOk(export_link.is(".disabled"));

    ext_bookmarks.update_export_link([]);
    assert.ok(export_link.is(".disabled"));

    export_link.remove();
});

QUnit.test("update_collection_link", function (assert) {
    const collection_link = $(
        `<div id="caosdb-f-bookmarks-collection-link"><a/></div>`);
    const a = collection_link.find("a")[0];
    $("body").append(collection_link);

    assert.notOk(collection_link.is(".disabled"));
    assert.notOk(a.href);

    ext_bookmarks.update_collection_link([]);
    assert.ok(collection_link.is(".disabled"));
    assert.notOk(a.href);

    ext_bookmarks.update_collection_link(["asdf"]);
    assert.notOk(collection_link.is(".disabled"));
    assert.equal(a.href, ext_bookmarks.get_collection_link(["asdf"]));

    ext_bookmarks.update_collection_link(["asdf", "sdfg"]);
    assert.notOk(collection_link.is(".disabled"));
    assert.equal(a.href, ext_bookmarks.get_collection_link(["asdf", "sdfg"]));

    ext_bookmarks.update_collection_link([]);
    assert.ok(collection_link.is(".disabled"));
    assert.notOk(a.href);

    collection_link.remove();
});

QUnit.test("bookmark buttons", function (assert) {
    const inactive_button = $(`<div data-bmval="id1"/>`);
    const active_button = $(`<div class="active" data-bmval="id2"/>`);
    const broken_button = $(`<div data-bmval=""/>`);
    const non_button = $(`<div data-bla="sadf"/>)`);
    const outside_button = $(`<div data-bmval="id3"/>`);
    const inside_buttons = $("<div/>").append([inactive_button, active_button,
        broken_button, non_button
    ]);

    // get_bookmark_buttons
    assert.equal(ext_bookmarks.get_bookmark_buttons("body").length, 0);

    $("body").append([outside_button, inside_buttons]);

    assert.equal(ext_bookmarks.get_bookmark_buttons("body").length, 4, "all but no_button");
    assert.equal(ext_bookmarks.get_bookmark_buttons(inside_buttons).length, 3, "all but non_button and outside_button");

    // get_value
    assert.equal(ext_bookmarks.get_value(inactive_button), "id1");
    assert.notOk(ext_bookmarks.get_value(non_button));
    assert.notOk(ext_bookmarks.get_value(broken_button));

    // init_button
    assert.ok(active_button.is(".active"));
    ext_bookmarks.init_button(active_button);
    assert.notOk(active_button.is(".active"));

    ext_bookmarks.bookmark_storage[ext_bookmarks.get_key("id1")] = "id1"
    assert.notOk(inactive_button.is(".active"));
    ext_bookmarks.init_button(inactive_button);
    assert.ok(inactive_button.is(".active"));

    ext_bookmarks.clear_bookmark_storage();

    inside_buttons.remove();
    outside_button.remove();
});

QUnit.test("select-query transformation", function (assert) {
    assert.equal(
        ext_bookmarks.get_select_id_query_string("FIND analysis"),
        "SELECT ID FROM analysis");
    assert.equal(
        ext_bookmarks.get_select_id_query_string(
            "FIND RECORD analysis WHICH HAS A date > 2012"),
        "SELECT ID FROM RECORD analysis WHICH HAS A date > 2012");
    assert.equal(
        ext_bookmarks.get_select_id_query_string(
            "SELECT name, date FROM analysis"),
        "SELECT ID FROM analysis");
    assert.equal(
        ext_bookmarks.get_select_id_query_string("COUNT analysis"),
        "SELECT ID FROM analysis");
    assert.equal(
        ext_bookmarks.get_select_id_query_string("fInD analysis"),
        "SELECT ID FROM analysis");
});

QUnit.test("select-query extraction", function (assert) {
    // Use response field copied from demo
    const response_field = $(`<div class="card caosdb-query-response mb-2">
    <div class="card-header caosdb-query-response-heading">
         <div class="row">
              <div class="col-sm-10 caosdb-overflow-box">
                   <div class="caosdb-overflow-content">
                        <span>Query: </span>
                        <span class = "caosdb-f-query-response-string">SELECT name, id FROM RECORD MusicalAnalysis</span>
                   </div>
              </div>
              <div class="col-sm-2 text-end">
                   <span>Results: </span>
                   <span class="caosdb-query-response-results">3</span>
              </div>
         </div>
    </div>
</div>`);
    $("body").append(response_field);

    assert.equal(ext_bookmarks.get_query_from_response(),
        "SELECT ID FROM RECORD MusicalAnalysis");
});