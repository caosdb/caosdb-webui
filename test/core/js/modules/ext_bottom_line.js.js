/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

var ext_bottom_line_test_suite = function ($, ext_bottom_line, QUnit) {

    var test_config = { "version": 0.1,
      "fallback": "blablabla",
      "creators": [
        { "id": "test.success",
          "is_applicable": "(entity) => getParents(entity).map(par => par.name).includes('TestPreviewRecordType') && getEntityName(entity) === 'TestPreviewRecord-success'",
          "create": "(entity) => 'SUCCESS'"
        },
        { "id": "test.error",
          "is_applicable": "(entity) => getParents(entity).map(par => par.name).includes('TestPreviewRecordType') && getEntityName(entity) === 'TestPreviewRecord-error'",
          "create": "(entity) => new Promise((res,rej) => {rej('Test Error');})"
        },
        { "id": "test.load-forever",
          "is_applicable": "(entity) => getParents(entity).map(par => par.name).includes('TestPreviewRecordType') && getEntityName(entity) === 'TestPreviewRecord-load-forever'",
          "create": "(entity) => new Promise((res,rej) => {})"
        },
        { "id": "test.success-2",
          "is_applicable": "(entity) => getParents(entity).map(par => par.name).includes('TestPreviewRecordType') && getEntityName(entity) !== 'TestPreviewRecord-fall-back'",
          "create": "(entity) => { return plotly_preview.create_plot([{x: [1,2,3,4,5], y: [1,2,4,8,16]}], { 'xaxis': {'title': 'time [samples]'}}, {displaylogo: false}); }"
        }
      ]
    };

    QUnit.module("ext_bottom_line.js", {
        before: async function (assert) {
            // setup before module
            await ext_bottom_line.configure(test_config);
        },
        beforeEach: function (assert) {
            // setup before each test
        },
        afterEach: function (assert) {
            // teardown after each test
        },
        after: function (assert) {
            // teardown after module
        }
    });

    QUnit.test("_creators", function (assert) {
        assert.equal(ext_bottom_line._creators.length, 9, "nine creators, 5 default ones, 4 from these tests.");
    });

    QUnit.test("add_preview_container", function(assert) {
        var entity = $("<div/>");
        var container = $(ext_bottom_line.add_preview_container(entity[0]));
        assert.ok(container.hasClass(ext_bottom_line._css_class_preview_container), `has class ${ext_bottom_line._css_class_preview_container}`);
        assert.ok(container.hasClass(ext_bottom_line._css_class_preview_container_resolvable), `has class ${ext_bottom_line._css_class_preview_container_resolvable}`);
    });

    QUnit.test("root_preview_handler", async function(assert) {
        for (let name_suffix of ["fall-back", "error", "load-forever", "success"]) {
            let name = "TestPreviewRecord-" + name_suffix;
            let entity_xml = `<Response><Record name="${name}"><Parent name='TestPreviewRecordType'/></Record></Response>`;
            let entity = (await transformation.transformEntities(str2xml(entity_xml)))[0];
            assert.equal(getEntityName(entity), name, "correct name");
            var container = $(ext_bottom_line.add_preview_container(entity));

            assert.ok(container.hasClass(ext_bottom_line._css_class_preview_container), `before has class ${ext_bottom_line._css_class_preview_container}`);
            assert.ok(container.hasClass(ext_bottom_line._css_class_preview_container_resolvable), `before has class ${ext_bottom_line._css_class_preview_container_resolvable}`);

            if (name_suffix === "load-forever"){
                ext_bottom_line.root_preview_handler(entity);
                await sleep(1000);
            } else {
                await ext_bottom_line.root_preview_handler(entity);
            }

            // ..._resolvable class removed
            assert.ok(container.hasClass(ext_bottom_line._css_class_preview_container), `after has class ${ext_bottom_line._css_class_preview_container}`);
            assert.notOk(container.hasClass(ext_bottom_line._css_class_preview_container_resolvable), `after missing class ${ext_bottom_line._css_class_preview_container_resolvable}`);


            switch(name_suffix) {
                case "fall-back":
                    assert.equal(container.text(), "blablabla");
                    break;
                case "error":
                    assert.equal(container.text(), "An error occured while loading this preview.Test Error");
                    break;
                case "load-forever":
                    assert.equal(container.text(), "Please wait...");
                    break;
                case "success":
                    assert.equal(container.text(), "SUCCESS");
                    break;
                default:
                    assert.ok(false);
            }

        }

    });

    QUnit.test("tiff converter", async function(assert) {
        let entity_xml = `<Response><File path="../pics/saturn.tif"/></Response>`;
        const entity = (await transformation.transformEntities(str2xml(entity_xml)))[0];
        const tiff_preview = await ext_bottom_line._creators.filter((c) => c.id == "_default_creators.tiff_images")[0].create(entity);

        assert.equal($(tiff_preview).find("img").attr("src").slice(0,21), "data:image/png;base64", "decoded tiff to png");
    });

}($, ext_bottom_line, QUnit);
