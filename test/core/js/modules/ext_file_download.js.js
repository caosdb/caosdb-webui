/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("ext_file_download.js", {
    before: function (assert) {
        // setup before module
    },
    beforeEach: function (assert) {
        // setup before each test
    },
    afterEach: function (assert) {
        // teardown after each test
    },
    after: function (assert) {
        // teardown after module
    }
});

QUnit.test("chunk_list ", function(assert) {
    const li = [1,2,3,4,5,6,7];
    const res = ext_file_download.chunk_list(li, 3);
    assert.equal(res.length, 3, "number of parts");
    assert.propEqual(res[2], [7], "number of parts");
});

QUnit.test("collect_ids ", function (assert) {
    const line = id => $(`<tr data-entity-id="${id}"/>`);
    const prop_val = x => $(`<div class="caosdb-f-property-value"/>`);
    const single_val =x =>  $(`<div class="caosdb-f-property-single-raw-value caosdb-id">${x}</div>`);


    const line1 = line("34");
    line1.append([prop_val().append(single_val("5")),prop_val().append(single_val("6"))])
    $("body").append([line1]);

    const res = ext_file_download.collect_ids()
    assert.ok(res.indexOf("5") > -1, "missing id");
    assert.ok(res.indexOf("6") > -1, "missing id");
    assert.ok(res.indexOf("34") > -1, "missing id");

    line1.remove();

});
