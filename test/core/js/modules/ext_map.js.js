/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("ext_map.js", {
    before: function (assert) {
        var lat = "latitude";
        var lng = "longitude";
        this.datamodel = {
            lat: lat,
            lng: lng
        };
        this.test_map_entity = `
<div class="caosdb-entity-panel caosdb-properties">
  <div class="caosdb-id">1234</div>
  <div class="list-group-item caosdb-f-entity-property">
    <div class="caosdb-property-name">` +
            lat + `</div>
    <div class="caosdb-f-property-value">1.23</div>
  </div>
  <div class="list-group-item caosdb-f-entity-property">
    <div class="caosdb-property-name">` +
            lng + `</div>
    <div class="caosdb-f-property-value">5.23</div>
  </div>
</div>`;
        // This one has lists of lat/lng.
        this.test_map_entity_2 = `
<div class="caosdb-entity-panel caosdb-properties">
  <div class="caosdb-id">1234</div>
  <div class="list-group-item caosdb-f-entity-property">
    <div class="caosdb-property-datatype">LIST</div>
    <div class="caosdb-property-name">` +
            lat + `</div>
    <div class="caosdb-f-property-value">
      <div class="caosdb-value-list"><div
           class="caosdb-f-property-single-raw-value">1.231</div>
      <div class="caosdb-f-property-single-raw-value">1.232</div></div>
    </div>
  </div>
  <div class="list-group-item caosdb-f-entity-property">
    <div class="caosdb-property-datatype">LIST</div>
    <div class="caosdb-property-name">` +
            lng + `</div>
    <div class="caosdb-f-property-value">
      <div class="caosdb-value-list"><div
           class="caosdb-f-property-single-raw-value">5.231</div>
      <div class="caosdb-f-property-single-raw-value">5.232</div></div>
    </div>
  </div>
</div>`;
    },
    beforeEach: function (assert) {
        sessionStorage.removeItem("caosdb_map.view");
    }
});

QUnit.test("availability", function (assert) {
    assert.equal(caosdb_map.version, "0.5.0", "test version");
    assert.ok(caosdb_map.init, "init available");
});

QUnit.test("default config", function (assert) {
    assert.ok(caosdb_map._default_config);
    assert.equal(caosdb_map._default_config.version, caosdb_map.version, "version");
});

QUnit.test("load_config", async function (assert) {
    assert.ok(caosdb_map.load_config, "available");
    var config = await caosdb_map.load_config("non_existing.json");
    assert.ok(config, "returns something");
    assert.equal(config.views.length, 1, "one view in default");
    assert.equal(config.views[0].id, "UNCONFIGURED", "view has id 'UNCONFIGURED'.");
});

QUnit.test("check_config", function (assert) {
    assert.ok(caosdb_map.check_config(caosdb_map._default_config), "default config ok");
    assert.throws(() => caosdb_map.check_config({
        "version": "wrong version",
    }), /The version of the configuration does not match the version of this implementation of the caosdb_map module. Should be '.*', was 'wrong version'./, "wrong version");
});

QUnit.test("check dependencies", function (assert) {
    assert.ok(caosdb_map.check_dependencies, "available");
    assert.propEqual(caosdb_map.dependencies, ["log", {
        "L": ["latlngGraticule", "Proj"]
    }, "navbar", "caosdb_utils"]);
    assert.ok(caosdb_map.check_dependencies(), "deps available");
});

QUnit.test("create_toggle_map_button", function (assert) {
    assert.ok(caosdb_map.create_toggle_map_button, "available");
    var button = caosdb_map.create_toggle_map_button();
    assert.equal(button.tagName, "A", "is A");
    assert.ok($(button).hasClass("caosdb-f-toggle-map-button"), "has caosdb-f-toggle-map-button class");
    assert.equal($(button).text(), "Map", "button says 'Map'");

    // set other content:
    button = caosdb_map.create_toggle_map_button("Karte");
    assert.equal(button.tagName, "A", "is A");
    assert.ok($(button).hasClass("caosdb-f-toggle-map-button"), "has caosdb-f-toggle-map-button class");
    assert.equal($(button).text(), "Karte", "button says 'Karte'");

});

QUnit.test("bind_toggle_map", function (assert) {
    let button = $("<button/>")[0];
    let done = assert.async();

    assert.ok(caosdb_map.bind_toggle_map, "available");
    assert.throws(() => caosdb_map.bind_toggle_map(button), /parameter 'toggle_cb'.* was undefined/, "no function throws");
    assert.throws(() => caosdb_map.bind_toggle_map("test", () => {}), /parameter 'button'.* was string/, "string button throws");
    assert.equal(caosdb_map.bind_toggle_map(button, done), button, "call returns button");

    // button click calls 'done'
    $(button).click();
});


QUnit.test("create_map", function (assert) {
    assert.equal(typeof caosdb_map.create_map_view, "function", "function available");

});

QUnit.test("create_map_panel", function (assert) {
    assert.ok(caosdb_map.create_map_panel, "available");
    let panel = caosdb_map.create_map_panel();
    assert.equal(panel.tagName, "DIV", "is div");
    assert.ok($(panel).hasClass("caosdb-f-map-panel"), "has class caosdb-f-map-panel");
    assert.ok($(panel).hasClass("container"), "has class container");
});

QUnit.test("init_map_panel", function (assert) {
    assert.ok(caosdb_map.init_map_panel, "available");
    const dummy_nav = $("<nav/>");
    $(document.body).append(dummy_nav);
    let panel = caosdb_map.init_map_panel(true);

    assert.ok($(panel).is(":visible"), "Panel is visible");

    panel = caosdb_map.create_map_panel(true);
    assert.notOk($(panel).is(":visible"), "Panel is not visible");

    dummy_nav.remove();
});

QUnit.test("create_map_view", function (assert) {
    var view_config = $.extend(true, {}, caosdb_map._unconfigured_views[0], {
        "select": true,
        "view_change": true
    });
    var map_panel = $("<div/>");

    var map = caosdb_map.create_map_view(map_panel[0], view_config);

    assert.ok(map instanceof L.Map, "map instance created");
    assert.equal($(map_panel).find(".leaflet-container").length, 1, "map_panel has .leaflet-container child");

    assert.notOk(map._crs, "no special crs");
    map.remove();
    map_panel = $("<div/>");

    // test with pre-defined crs
    view_config["crs"] = "Simple";
    map = caosdb_map.create_map_view(map_panel[0], view_config);

    assert.ok(map instanceof L.Map, "map instance created");
    assert.equal($(map_panel).find(".leaflet-container").length, 1, "map_panel has .leaflet-container child");
    assert.equal(map._crs, L.CRS.Simple, "map has SIMPLE crs");

    map.remove();
    map_panel = $("<div/>");

    // test with special crs:
    view_config["crs"] = {
        "code": "EPSG:3995",
        "proj4def": "+proj=stere +lat_0=90 +lat_ts=71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs",
        "options": {
            "resolutions": ["16384", "8192", "4096", "2048", "1024", "512", "256", "128", "64", "32", "16", "8", "4", "2", "1", "0.5"]
        }
    };

    map = caosdb_map.create_map_view(map_panel[0], view_config);

    assert.ok(map instanceof L.Map, "map instance created");
    assert.equal($(map_panel).find(".leaflet-container").length, 1, "map_panel has .leaflet-container child");
    assert.ok(map._crs instanceof L.Proj.CRS, "map has special crs");

    map.remove();

});

QUnit.test("get_map_entities", function (assert) {
    var datamodel = this.datamodel;
    var container = $('<div/>').append(this.test_map_entity);
    var map_objects = caosdb_map.get_map_entities(container[0], datamodel);
    assert.equal(map_objects.length, 1, "has one map object");
});


QUnit.test("create_entity_markers", function (assert) {
    var datamodel = this.datamodel;
    var entities = $(this.test_map_entity).toArray();

    // w/o popup
    var markers = caosdb_map.create_entity_markers(entities, datamodel);
    assert.equal(markers.length, 1, "has one marker");
    assert.ok(markers[0] instanceof L.Marker, "is marker");
    var latlng = markers[0]._latlng;
    assert.equal(latlng.lat, "1.23", "latitude set");
    assert.equal(latlng.lng, "5.23", "longitude set");
    assert.notOk(markers[0].getPopup(), "no popup");

    // with popup
    markers = caosdb_map.create_entity_markers(entities, datamodel, () => "popup");
    assert.ok(markers[0].getPopup(), "has popup");


    // test with list of lat/lng
    entities = $(this.test_map_entity_2).toArray();
    markers = caosdb_map.create_entity_markers(entities, datamodel);
    assert.equal(markers.length, 2, "has two marker");
    assert.ok(markers[0] instanceof L.Marker, "is marker");
    assert.ok(markers[1] instanceof L.Marker, "is marker");

    latlng = markers[0]._latlng;
    assert.equal(latlng.lat, "1.231", "latitude set");
    assert.equal(latlng.lng, "5.231", "longitude set");

    latlng = markers[1]._latlng;
    assert.equal(latlng.lat, "1.232", "latitude set");
    assert.equal(latlng.lng, "5.232", "longitude set");
});


QUnit.test("_add_current_page_entities", async function (assert) {
    var datamodel = this.datamodel;
    var layerGroup = L.layerGroup();
    var container = $('<div class="caosdb-f-main-entities"/>').append(this.test_map_entity);
    $("body").append(container);

    assert.equal(layerGroup.getLayers().length, 0, "no layer");
    var cpe = await caosdb_map._generic_get_current_page_entities(datamodel, undefined, undefined, undefined, undefined, undefined);

    assert.equal(cpe.length, 1, "has one entity");
    container.remove();
});


QUnit.test("make_layer_chooser_html", function (assert) {
    var test_conf = {
        "id": "test_id",
        "name": "test name",
        "description": "test description",
        "icon_options": {
            "html": "<span>ICON</span>",
        },
    };

    var layer_chooser = caosdb_map.make_layer_chooser_html(test_conf);
    assert.ok(layer_chooser, "available");
    assert.equal($(layer_chooser).attr("title"), "test description", "description set as title");
});

QUnit.test("_init_single_entity_layer", function (assert) {
    var test_conf = {
        "id": "test_id",
        "name": "test name",
        "description": "test description",
        "icon_options": {
            "html": "<span>ICON</span>",
        },
    }

    var entityLayer = caosdb_map._init_single_entity_layer(test_conf);
    assert.equal(entityLayer.id, test_conf.id, "id");
    assert.equal(entityLayer.active, true, "is active");
    assert.ok(entityLayer.chooser_html instanceof HTMLElement, "chooser_html is HTMLElement");
    assert.equal(entityLayer.layer_group.getLayers().length, 0, "empty layergroup");
});

QUnit.test("_get_with_POV ", function (assert) {
    assert.equal(caosdb_map._get_with_POV(
        []), "", "no POV");
    assert.equal(caosdb_map._get_with_POV(
        ["lol"]), " WITH \"lol\" ", "single POV");
    assert.equal(caosdb_map._get_with_POV(
        ["lol", "hi"]), " WITH \"lol\"  WITH \"hi\" ", "with two POV");
});


QUnit.test("_get_select_with_path  ", function (assert) {
    assert.throws(() => caosdb_map._get_select_with_path(), /Supply the datamodel./, "missing datamodel");
    assert.throws(() => caosdb_map._get_select_with_path(this.datamodel, []), /Supply at least a RecordType./, "missing value");
    assert.equal(caosdb_map._get_select_with_path(
        this.datamodel,
        ["RealRT"]), "SELECT parent,latitude,longitude FROM ENTITY \"RealRT\"  WITH ( \"latitude\" AND \"longitude\" ) ", "RT only");
    assert.equal(caosdb_map._get_select_with_path(
        this.datamodel,
        ["RealRT", "prop1"]), "SELECT parent,prop1.latitude,prop1.longitude FROM ENTITY \"RealRT\"  WITH \"prop1\"  WITH ( \"latitude\" AND \"longitude\" ) ", "RT with one prop");
    assert.equal(caosdb_map._get_select_with_path(
        this.datamodel,
        ["RealRT", "prop1", "prop2"]), "SELECT parent,prop1.prop2.latitude,prop1.prop2.longitude FROM ENTITY \"RealRT\"  WITH \"prop1\"  WITH \"prop2\"  WITH ( \"latitude\" AND \"longitude\" ) ", "RT with two props");
});


QUnit.test("_get_leaf_prop", async function (assert) {
    const test_response = str2xml(`
<Response>
  <Record id="6525" name="Test_IceCore_1">
    <Property datatype="Campaign" id="6430" name="Campaign">
      <Record id="6516" name="Test-2020_Camp1">
        <Property datatype="REFERENCE" id="168" name="responsible">
          <Record id="6515" name="Test_Scientist">
            <Property datatype="DOUBLE" id="151" name="latitude" importance="FIX">
              1.34
            </Property>
            <Property datatype="DOUBLE" id="151" name="longitude" importance="FIX">
              2
            </Property>
          </Record>
        </Property>
      </Record>
    </Property>
  </Record>
  <Record id="6526" name="Test_IceCore_2">
    <Property datatype="Campaign" id="6430" name="Campaign">
      <Record id="6516" name="Test-2020_Camp1">
        <Property datatype="REFERENCE" id="168" name="responsible">
          <Record id="6515" name="Test_Scientist">
            <Property datatype="DOUBLE" id="151" name="latitude" importance="FIX">
              3
            </Property>
            <Property datatype="DOUBLE" id="151" name="longitude" importance="FIX">
              4.8345
            </Property>
          </Record>
        </Property>
      </Record>
    </Property>
  </Record>
</Response>`);
    var leaves = caosdb_map._get_leaf_prop(test_response, 2, this.datamodel)

    assert.equal(Object.keys(leaves).length, 2, "number of records");
    assert.notEqual(typeof leaves["6525"], "undefined", "has entity id");
    assert.deepEqual(leaves["6525"], ["1.34", "2"]);
    assert.deepEqual(leaves["6526"], ["3", "4.8345"], "long/lat in second rec");

    assert.equal(
        caosdb_map._get_toplvl_rec_with_id(test_response, "6526")["id"],
        "6526",
        "number of records");

    caosdb_map._set_subprops_at_top(
        test_response, 2, this.datamodel, {
            "6526": [1.234, 5.67]
        })
    assert.equal($(test_response).find(`[name='longitude']`).length,
        4,
        "number lng props");
    assert.equal($(test_response).find(`[name='latitude']`).length,
        4,
        "number lat props");
    // after transforming, the long/lat props should be accessible
    var html_ents = await transformation.transformEntities(test_response);
    assert.equal(
        getProperty(html_ents[0], "longitude"),
        "2",
        "longitude of first rec");

});


QUnit.test("_get_leaf_prop - list of referenced map entities", async function (assert) {
    const test_response = str2xml(`
<Response>
  <Record id="7393">
    <Version id="7f04ebc3a09d43f8711371a1d62905e5fc6af80f" head="true" />
    <Parent id="7392" name="PathObject" />
    <Property datatype="LIST&lt;MapObject&gt;" id="7391" name="MapObject">
      <Value>
        <Record id="7394" name="Object-0">
          <Version id="4c3b4a7ef4abc4d3b6045968f3b5f028d82baab2" head="true" />
          <Property datatype="DOUBLE" id="7389" name="longitude" importance="FIX" unit="°">
            -44.840238182501864
          </Property>
          <Property datatype="DOUBLE" id="7390" name="latitude" importance="FIX" unit="°">
            83.98152416509532
          </Property>
        </Record>
      </Value>
      <Value>
        <Record id="7395" name="Object-1">
          <Version id="42fbe0c9be68c356f81f590cddbdd3d5fc17cba4" head="true" />
          <Property datatype="DOUBLE" id="7389" name="longitude" importance="FIX" unit="°">
            -35.60247552143245
          </Property>
          <Property datatype="DOUBLE" id="7390" name="latitude" importance="FIX" unit="°">
            73.86388403927366
          </Property>
        </Record>
      </Value>
      <Value>
        <Record id="7396" name="Object-2">
          <Version id="45b71028261061e94ae198eaaa66af0612004173" head="true" />
          <Property datatype="DOUBLE" id="7389" name="longitude" importance="FIX" unit="°">
            -42.429495631197724
          </Property>
          <Property datatype="DOUBLE" id="7390" name="latitude" importance="FIX" unit="°">
            74.95382063506622
          </Property>
        </Record>
      </Value>
    </Property>
  </Record>
</Response>
`);
    var leaves = caosdb_map._get_leaf_prop(test_response, 1, this.datamodel)

    assert.equal(Object.keys(leaves).length, 1, "number of records");
    var leave = leaves["7393"];
    assert.ok(leave, "has entity");
    assert.deepEqual(leave, [["83.98152416509532",
      "73.86388403927366", "74.95382063506622"], [ "-44.840238182501864",
        "-35.60247552143245", "-42.429495631197724" ]]);

    assert.equal(
        caosdb_map._get_toplvl_rec_with_id(test_response, "7393")["id"],
        "7393",
        "number of records");

    caosdb_map._set_subprops_at_top(
        test_response, 1, this.datamodel, {
            "7393": [["83.98152416509532", "73.86388403927366",
              "74.95382063506622"], [ "-44.840238182501864",
                "-35.60247552143245", "-42.429495631197724" ]] })
    assert.equal($(test_response).find(`[name='longitude']`).length,
        4,
        "number lng props");
    assert.equal($(test_response).find(`[name='latitude']`).length,
        4,
        "number lat props");
    // after transforming, the long/lat props should be accessible
    var html_ents = await transformation.transformEntities(test_response);
    assert.deepEqual(
        getProperty(html_ents[0], "longitude"), [ "-44.840238182501864",
          "-35.60247552143245", "-42.429495631197724" ],
        "longitude of first rec");

});

QUnit.test("_get_id_POV", function (assert) {
    assert.equal(caosdb_map._get_id_POV([]), "WITH ", "no POV");
    assert.equal(caosdb_map._get_id_POV([5]), "WITH id=5", "one id");
    assert.equal(caosdb_map._get_id_POV([5, 6]), "WITH id=5 or id=6", "two ids");
});
