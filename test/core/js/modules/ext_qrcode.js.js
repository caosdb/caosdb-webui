/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

'use strict';

QUnit.module("ext_qrcode.js", {
    before: function (assert) {
        // setup before module
    },
    beforeEach: function (assert) {
        // setup before each test
        $(document.body).append('<div data-entity-id="eid123" data-version-id="vid234" id="ext-qrcode-test-entity" class="caosdb-entity-panel"><div class="caosdb-v-entity-header-buttons-list"></div></div>');
    },
    afterEach: function (assert) {
        // teardown after each test
        const modal = bootstrap.Modal.getInstance($(".modal")[0]);
        if (modal) modal.dispose();
        $("#ext-qrcode-test-entity").remove();
        $(".modal").remove();
    },
    after: function (assert) {
        // teardown after module
    }
});

QUnit.test("init", function (assert) {
    assert.ok(ext_qrcode.init, "init available");
    assert.equal($(".caosdb-f-entity-qrcode-button").length, 0, "no button before.");
    ext_qrcode.init();
    assert.equal($(".caosdb-f-entity-qrcode-button").length, 1, "button has been added.");
    ext_qrcode.init();
    assert.equal($(".caosdb-f-entity-qrcode-button").length, 1, "still only one button.");

    ext_qrcode.remove_qrcode_button($("#ext-qrcode-test-entity")[0]);
    assert.equal($(".caosdb-f-entity-qrcode-button").length, 0, "no button after removal.");
});

QUnit.test("create_qrcode_button", function (assert) {
    assert.equal(ext_qrcode.create_qrcode_button("entityid", "versionid").tagName, "BUTTON", "create_qrcode_button creates a button");
});


QUnit.test("qrcode_button_click_handler", function (assert) {
    var done = assert.async();
    assert.equal($("#qrcode-modal-entityid-versionid").length, 0, "no modal before first click");
    ext_qrcode.qrcode_button_click_handler("entityid", "versionid")
    $("#qrcode-modal-entityid-versionid").on("shown.bs.modal", done);
    assert.equal($("#qrcode-modal-entityid-versionid").length, 1, "first click added the modal");
});

QUnit.test("update_qrcode", async function (assert) {
    // create modal
    const entity_id = "eid456";
    const entity_version = "vid3564";
    const modal_id = `qrcode-modal-${entity_id}-${entity_version}`;
    const modal_element = ext_qrcode.create_qrcode_modal(modal_id, entity_id, entity_version);
    $(document.body).append(modal_element);

    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode *").length, 0, "no qrcode.");
    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode-link *").length, 0, "no link.");

    // update adds qrcode
    ext_qrcode.update_qrcode(modal_element, entity_id, entity_version);

    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode-link a")[0].href, connection.getEntityUri([entity_id]), "link points to entity head.");
    // wait until qrcode is ready
    await sleep(500);
    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode canvas").length, 1, "qrcode is there.");

    $("#" + modal_id).find("canvas").remove();
    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode canvas").length, 0, "removed qrcode canvas for next test.");
    // select radio button for link to exact version: check both...
    $("#" + modal_id).find("input[name=entity-qrcode-versioned]").prop("checked", true);
    // ...then uncheck first
    $("#" + modal_id).find("input[name=entity-qrcode-versioned]").first().prop("checked", false);
    $("#" + modal_id).find("form").trigger("change");

    // check: uri has changed
    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode-link a")[0].href, connection.getEntityUri([entity_id]) + "@" + entity_version, "link changed to versioned entity.");
    // wait until qrcode is ready
    await sleep(500);
    assert.equal($("#" + modal_id).find(".caosdb-f-entity-qrcode canvas").length, 1, "qrcode there again.");

});