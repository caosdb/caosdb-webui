/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


/* MODULE ext_sss_markdown */
QUnit.module("ext_sss_markdown", {
    before: function (assert) {
        markdown.init();
        ext_sss_markdown.logger.setLevel("trace");

        const qunit_obj = this;
        const done = assert.async();

        // load test case
        $.ajax({
            cache: true,
            dataType: 'xml',
            url: "xml/test_sss_output.xml",
        }).done(function(data, textStatus, jdXHR) {
            // append the test case to the QUnit module object.
            qunit_obj.testCase1 = data;
        }).always(function() {
            done();
        });
    },
    after: function (assert) {
        // clean up
        $("#caosdb-stdout").remove();
    }
});


QUnit.test("availability", function(assert) {
    assert.ok(ext_sss_markdown, "available");
});

QUnit.test("test case 1", function(assert) {
    // setup
    const testCase1 = this.testCase1;
    const plainStdout = testCase1.evaluate("/Response/script/stdout", testCase1, null, XPathResult.STRING_TYPE, null).stringValue;

    const stdout = $('<div id="caosdb-stdout"/>').text(plainStdout);
    $("body").append(stdout);

    assert.equal($("#caosdb-stdout").length, 1);
    assert.equal($("#caosdb-stdout div.alert").length, 0, "no bootstrap alert");
    assert.equal($("#caosdb-stdout p").length, 0, "no html paragraphs");
    assert.equal($("#caosdb-stdout").text(), plainStdout, "only plain text");

    ext_sss_markdown.init();

    assert.equal($("#caosdb-stdout p").length, 3, "3 html paragraphs transformed");
    assert.equal($("#caosdb-stdout code").length, 1, "html code tag transformed");
    assert.equal($("#caosdb-stdout div.alert").text(),
        "this is a bootstrap alert", "bootstrap alert transformed");

});
