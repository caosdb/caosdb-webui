/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("form_elements.js", {
    before: function (assert) {
        markdown.init();
        var entities = [
            $('<div><div class="caosdb-id" data-entity-name="name12">id12</div></div>')[0],
            $('<div><div class="caosdb-id" data-entity-name="name13">id13</div></div>')[0],
            $('<div><div class="caosdb-id" data-entity-name="name14">id14</div></div>')[0],
            $('<div><div class="caosdb-id" data-entity-name="name15">id15</div></div>')[0],
        ];

        form_elements._query = async function (query) {
            return entities;
        };
        this.get_example_1 = async function () {
            return $(await $.ajax("html/form_elements_example_1.html"))[0];
        };
    },
    after: function (assert) {
        form_elements._init_functions();
    }
});

QUnit.test("availability", function (assert) {
    assert.equal(form_elements.version, "0.1", "test version");
    assert.ok(form_elements.init, "init available");
    assert.ok(form_elements.version, "version available");
});

QUnit.test("make_reference_option", function (assert) {
    assert.equal(typeof form_elements.make_reference_option, "function", "function available");
    assert.throws(() => form_elements.make_reference_option(), /is expected to be a string/, "noargs throws");
    var option = form_elements.make_reference_option("id15");
    assert.equal($(option).val(), "id15", "value");
    assert.equal($(option).text(), "id15", "text");
    option = form_elements.make_reference_option("id16", "desc16");
    assert.equal($(option).val(), "id16", "value");
    assert.equal($(option).text(), "desc16", "text");
});


QUnit.test("make_reference_select", async function (assert) {
    assert.equal(typeof form_elements.make_reference_select, "function", "function available");
    var select = await form_elements.make_reference_select([{
        dataset: {
            entityId: "id17"
        }
    }, {
        dataset: {
            entityId: "id18"
        }
    }, ]);
    assert.ok($(select).hasClass("selectpicker"), "selectpicker class from bootstrap-select");
    assert.notOk($(select).val(), "unselected");
    $(select).val(["id18"]);
    assert.equal($(select).val(), "id18", "select id18");
    $(select).val("id17");
    assert.equal($(select).val(), "id17", "select id17");


    // multiple
    $(select).prop("multiple", true);
    $(select).val(["id17", "id18"]);
    assert.propEqual($(select).val(), ["id17", "id18"], "select multiple");

});


QUnit.test("make_script_form", async function (assert) {
    assert.equal(typeof form_elements.make_script_form, "function", "function available");

    // TODO
    //assert.throws(()=>form_elements.make_script_form(), /param `script` is expected to be a string, was undefined/, "no param call throws");
    //assert.throws(()=>form_elements.make_script_form("test"), /param `config` is expected to be a object, was undefined/, "empty object config call throws");


    var done = assert.async(3);
    var config = {
        groups: [{
            name: "group1",
            fields: ["date"],
            enabled: false
        }, ],
        fields: [{
            type: "date",
            name: "baldate"
        }, {
            type: "select",
            name: "Sex",
            label: "Sex",
            value: "female",
            required: true,
            options: [{
                value: "female",
                label: "female"
            }, {
                value: "diverse",
                label: "diverse"
            }, {
                value: "male",
                label: "male"
            }]
        }],
    };

    var script_form = await form_elements.make_script_form(config, "test_script");
    $(document.body).append(script_form);
    assert.equal(script_form.tagName, "FORM", "is form");

    var submit_button = $(script_form).find("button[type='submit']");
    assert.equal(submit_button.length, 1, "has submit button");

    var cancel_button = $(script_form).find("button[type='button']:contains('Cancel')");
    assert.equal(cancel_button.length, 1, "has cancel button");

    var field = $(script_form).find(".caosdb-f-field");
    assert.equal(field.length, 2, "has two field");
    assert.equal(field.find("input[type='date']").length, 1, "has date input");
    assert.equal(field.find("select").length, 1, "has select input");

    script_form.addEventListener("caosdb.form.cancel", function (e) {
        done();
    }, true);
    cancel_button.click();


    script_form.addEventListener("caosdb.form.error", function (e) {
        assert.equal($(script_form).find(".caosdb-f-form-elements-message-error").length, 2, "error message there (call and stderr)");
        done();
        script_form.remove();
    });

    form_elements._run_script = async function (script, params) {
        done();
        return {
            code: "1",
            stderr: "Autsch!",
            call: "none"
        };
    };

    assert.equal($(script_form).find(".caosdb-f-form-error-message").length, 0, "no error message");
    submit_button.click();




});


QUnit.test("make_date_input", function (assert) {
    assert.equal(typeof form_elements.make_date_input, "function", "function available");

    var config = {
        type: "date",
        name: "thedate",
        label: "Birthday",
    }
    var field = form_elements.make_date_input(config);
    var input = $(field).find("input");
    assert.equal(input.length, 1, "has input");
    assert.equal(input.attr("name"), "thedate", "input has name");

    var label = $(field).find("label");
    assert.equal(label.length, 1, "has label");
    assert.equal(label.attr("for"), "thedate", "label has for");
    assert.equal(label.text(), "Birthday", "label has text");

});

QUnit.test("make_range_input", async function (assert) {
    assert.equal(typeof form_elements.make_range_input, "function", "function available");

    var config = {
        type: "range",
        from: {
            name: "from_bla",
        },
        to: {
            name: "to_bla",
        },
        name: "seats",
        label: "seat numbers",
    }
    var field = await form_elements.make_range_input(config);
    var input = $(field).find("input");
    assert.equal(input.length, 2, "has two inputs");
    assert.equal(input.first().attr("name"), "from_bla", "input 1 has name");
    assert.equal(input.last().attr("name"), "to_bla", "input 2 has name");

    var label = $(field).find("label");
    assert.equal(label.length, 1, "has label");
    assert.equal(label.attr("for"), "seats", "label has for");
    assert.equal(label.text(), "seat numbers", "label has text");


});

QUnit.test("make_form_field", async function (assert) {
    assert.equal(typeof form_elements.make_form_field, "function", "function available");

    var cached = false;
    for (var t of ["date", "range", "reference_drop_down", "subform", "checkbox", "double", "integer"]) {
        cached = !cached;
        var config = {
            help: {
                title: "HELP",
                content: "help me, help me, help me-e-e!"
            },
            type: t,
            cached: cached,
            name: "a name",
            label: "a label",
            from: {
                name: "from_bla"
            },
            to: {
                name: "to_bla"
            },
            query: "FIND something",
            make_desc: getEntityName,
            fields: [],
        };
        var field = await form_elements.make_form_field(config);
        assert.equal($(field).hasClass("caosdb-f-form-field-cached"), cached, t + " cache initialized");
        assert.ok(field instanceof HTMLElement, "returns HTMLElement");
        t === "subform" || assert.equal($(field).children("label").length, 1, t + " has label");
        t === "subform" || assert.equal($(field).find(".caosdb-f-form-help").length, 1, t + " has help");
    }

});


QUnit.test("make_subform", async function (assert) {
    assert.equal(typeof form_elements.make_subform, "function", "function available");

    const config = {
        type: "subform",
        name: "subbla",
        fields: [

        ],
    }

    var subform = $(await form_elements.make_subform(config));
    assert.ok(subform[0].tagName, "DIV", "is div");
    assert.ok(subform.hasClass("caosdb-f-form-elements-subform"), "has caosdb-f-form-elements-subform class");
    assert.equal(subform.data("subform-name"), "subbla", "has name");

});


QUnit.test("make_reference_drop_down", async function (assert) {
    assert.equal(typeof form_elements.make_reference_drop_down, "function", "function available");

    var config = {
        type: "reference_drop_down",
        name: "icecore",
        label: "IceCore",
        query: "FIND Record IceCore",
        make_desc: getEntityName,
    }
    var field = await form_elements.make_reference_drop_down(config);

    var label = $(field).find("label");
    assert.equal(label.length, 1, "has label");
    assert.equal(label.attr("for"), "icecore", "label has for");
    assert.equal(label.text(), "IceCore", "label has text");
});

QUnit.test("make_checkbox_input", function (assert) {
    assert.equal(typeof form_elements.make_checkbox_input, "function", "function available");


    var config = {
        type: "checkbox",
        name: "approved",
        label: "Approval",
        checked: true,
        value: "yes!!!",
    }
    var field = form_elements.make_checkbox_input(config);
    var input = $(field).find("input:checkbox");
    assert.equal(input.length, 1, "has checkbox input");
    assert.equal(input.attr("name"), "approved", "input has name");
    assert.ok(input.is(":checked"), "input is checked");

    var obj = form_elements.form_to_object(field)[0];
    assert.equal(obj["approved"], "yes!!!", "checked value");


    config["checked"] = false;
    config["value"] = "notherval";
    field = form_elements.make_checkbox_input(config);
    input = $(field).find("input:checkbox");
    assert.equal(input.length, 1, "has checkbox input");
    assert.equal(input.attr("name"), "approved", "input has name");
    assert.notOk(input.is(":checked"), "input is not checked");

    obj = form_elements.form_to_object(field)[0];
    assert.equal(typeof obj["approved"], "undefined", "no checked value");


});

QUnit.test("form_to_object", async function (assert) {
    assert.equal(typeof form_elements.form_to_object, "function", "function available");

    var config = {
        fields: [{
            type: "date",
            name: "the-date"
        }, {
            type: "reference_drop_down",
            name: "icecore",
            query: "FIND Record IceCore"
        }, {
            type: "range",
            name: "the-range",
            from: {
                name: "fromblla"
            },
            to: {
                name: "toblla"
            }
        }, {
            type: "subform",
            name: "subform1",
            fields: [{
                type: "date",
                name: "the-other-date",
            }, {
                type: "checkbox",
                name: "rectangular",
            }, ],
        }, {
            type: "select",
            required: true,
            cached: true,
            name: "sex",
            label: "Sex",
            value: "d",
            options: [{
                "value": "f",
                "label": "female"
            }, {
                "value": "d",
                "label": "diverse"
            }, {
                "value": "m",
                "label": "male"
            }, ],
        }, ],
    };

    var form = await form_elements.make_script_form(config, "bla");

    var json = form_elements.form_to_object(form)[0];

    assert.equal(typeof json["cancel"], "undefined", "cancel button not serialized");
    assert.equal(json["the-date"], "", "date");
    assert.equal(json["icecore"], null, "reference_drop_down");
    assert.equal(json["fromblla"], "", "range from");
    assert.equal(json["toblla"], "", "range to");
    assert.equal(json["sex"], "d", "select");
    assert.equal(typeof json["the-other-date"], "undefined", "subform element not on root level");

    var subform = json["subform1"];
    assert.equal(typeof subform, "object", "subform1");
    assert.equal(subform["the-other-date"], "", "subform date");
    assert.equal(typeof subform["rectangular"], "undefined", "subform checkbox not checked");
    console.log(json);

});


QUnit.test("make_double_input", function (assert) {
    assert.equal(typeof form_elements.make_double_input, "function", "function available");

    var config = {
        type: "double",
        name: "d"
    };
    var input = $(form_elements.make_double_input(config)).find("input");
    assert.ok(input.is("[type='number'][step='any']"), "double input");

    assert.ok(input.val("15").prop("validity").valid, "15 valid");
    assert.ok(input.val("1.5").prop("validity").valid, "1.5 valid");
    assert.equal(input.val("abc").val(), "", "abc not accepted");

});

QUnit.test("make_integer_input", function (assert) {
    assert.equal(typeof form_elements.make_integer_input, "function", "function available");

    var config = {
        type: "integer",
        name: "i"
    };
    var input = $(form_elements.make_integer_input(config)).find("input");
    assert.ok(input.is("[type='number'][step='1']"), "integer input");

    assert.ok(input.val("15").prop("validity").valid, "15 valid");
    assert.notOk(input.val("1.5").prop("validity").valid, "1.5 not valid");
    assert.equal(input.val("abc").val(), "", "abc not valid");
});

QUnit.test("make_form", function (assert) {
    assert.equal(typeof form_elements.make_form, "function", "function available");

    var form1 = form_elements.make_form({
        fields: []
    });
    assert.equal(form1.tagName, "DIV", "wrapper is div");
    assert.ok($(form1).hasClass("caosdb-f-form-wrapper"), "div has caosdb-f-form-wrapper class");
    assert.equal($(form1).find(".h3").length, 0, "no header");

    var form2 = form_elements.make_form({
        fields: [],
        header: "bla"
    });
    assert.equal(form2.tagName, "DIV", "wrapper is div");
    assert.equal($(form2).find(".h3").length, 1, "one header");
    assert.equal($(form2).find(".h3").text(), "bla", "header text set");
});

QUnit.test("enable/disable_group", function (assert) {
    assert.equal(typeof form_elements.disable_group, "function", "function available");
    assert.equal(typeof form_elements.enable_group, "function", "function available");

    var form = $('<form><div class="caosdb-f-field">test</div></form>');

    form_elements.disable_group(form, "group1");
    assert.notOk(form.find(".caosdb-f-field").hasClass("caosdb-f-field-disabled"), "not disabled");

    form = $('<form><div class="caosdb-f-field" data-groups="(group1)" >test</div></form>');

    form_elements.disable_group(form, "group1");
    assert.ok(form.find(".caosdb-f-field").hasClass("caosdb-f-field-disabled"), "disabled");
    form_elements.enable_group(form, "group1");
    assert.notOk(form.find(".caosdb-f-field").hasClass("caosdb-f-field-disabled"), "disabled again");

    form.append('<div class="caosdb-f-field caosdb-f-field-disabled">test2</div>');
    form.append('<div class="caosdb-f-field caosdb-f-field-disabled" data-groups="(group3)" >test3</div>');

    form_elements.disable_group(form, "group1");
    assert.equal(form.find(".caosdb-f-field").length, 3, "three fields");
    assert.equal(form.find(".caosdb-f-field[data-groups*='(group1)']").length, 1, "one field in group1");
    assert.equal(form.find(".caosdb-f-field[data-groups*='(group2)']").length, 0, "no fields in group2");
    assert.equal(form.find(".caosdb-f-field[data-groups*='(group3)']").length, 1, "one field in group3");
    form_elements.d
    assert.equal(form.find(".caosdb-f-field-disabled").length, 3, "three fields disabled");

    form_elements.enable_group(form, "group1");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 2, "two fields disabled");

    form_elements.enable_group(form, "group1");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 2, "still, two fields disabled");
    form_elements.enable_group(form, "group3");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 1, "one field disabled");

    form_elements.enable_group(form, "group2");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 1, "still, one field disabled");

    form_elements.disable_group(form, "group2");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 1, "still, one field disabled");

    form_elements.disable_group(form, "group3");
    assert.equal(form.find(".caosdb-f-field-disabled").length, 2, "two field disabled again");

});


QUnit.test("parse_script_result", function (assert) {
    assert.equal(typeof form_elements.parse_script_result, "function", "function available");

    var result = str2xml(
        `<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="https://localhost:10443/webinterface/webcaosdb.xsl" ?>
<Response username="admin" realm="PAM" srid="256c14970dac2b2b5649973d52e4c06a" timestamp="1570785591824" baseuri="https://localhost:10443">
  <UserInfo username="admin" realm="PAM">
    <Roles>
      <Role>administration</Role>
    </Roles>
  </UserInfo>
  <script code="127">
    <call>sample_creation.py .upload_files/form.json</call>
    <stdout />
    <stderr>/usr/bin/env: ‘python’: No such file or directory</stderr>
  </script>
</Response>
`
    );
    var parsed = form_elements.parse_script_result(result);
    assert.equal(parsed.code, "127", "code parsed");
    assert.equal(parsed.call, "sample_creation.py .upload_files/form.json", "call parsed");
    assert.equal(parsed.stderr, "/usr/bin/env: ‘python’: No such file or directory", "stderr parsed");
    assert.equal(parsed.stdout, "", "stdout parsed");


});


QUnit.test("disable_name", function (assert) {
    assert.equal(typeof form_elements.disable_name, "function", "function available");
});

QUnit.test("enable_name", function (assert) {
    assert.equal(typeof form_elements.enable_name, "function", "function available");
});

QUnit.test("add_field_to_group", function (assert) {
    assert.equal(typeof form_elements.add_field_to_group, "function", "function available");

    var field = $(form_elements._make_field_wrapper("field1"))[0];
    form_elements.add_field_to_group(field, "group1");
    assert.equal($(field).attr("data-groups"), "(group1)", "group1 added");
    form_elements.add_field_to_group(field, "group2");
    assert.equal($(field).attr("data-groups"), "(group1)(group2)", "group2 added");

});

QUnit.test("cache_form", async function (assert) {
    var form = await this.get_example_1();
    assert.equal($(form).find("form").length, 1, "example form available");

    var cache = {};
    var field = $(form_elements.get_fields(form, "ice_core"));
    field.find(":input").val("6345");


    form_elements.cache_form(cache, form);
    assert.equal(cache[form_elements.get_cache_key(form, field[0])], "6345");
});


QUnit.test("load_cached", async function (assert) {
    var done = assert.async();
    var form = await this.get_example_1();
    assert.equal($(form).find("form").length, 1, "example form available");


    var field = $(form_elements.get_fields(form, "ice_core"));
    var cache = {};
    cache[form_elements.get_cache_key(form, field[0])] = "6346";


    assert.equal(field.find(":input").val(), null, "field unset before");
    field[0].addEventListener(form_elements.field_changed_event.type,
        (e) => {
            done();
            assert.equal(field.find(":input").val(), "6346", "field set after");
        }, true);
    form_elements.load_cached(cache, form);

});


QUnit.test("field_ready", function (assert) {
    var done = assert.async(3);
    var field1 = $('<div id="f1"><div class="caosdb-f-field-not-ready"/></div>')[0];
    var field2 = $('<div id="f2" class="caosdb-f-field-not-ready"/>')[0];
    var field3 = $('<div id="f3"/>')[0];
    var field4 = $('<div id="f4"><div class="caosdb-f-field-not-ready"/></div>')[0];

    form_elements.field_ready(field1).then(field => {
        assert.equal($(field)
            .find(".caosdb-f-field-not-ready")
            .length, 0, "caosdb-f-field-not-ready class removed");
        assert.equal(field.id, "f1", "field1 ready");
        done();
    });
    // remove class and send field_ready
    $(field1).find(".caosdb-f-field-not-ready")
        .removeClass("caosdb-f-field-not-ready");
    field1.dispatchEvent(form_elements.field_ready_event);

    form_elements.field_ready(field2).then(field => {
        assert.equal($(field).find(".caosdb-f-field-not-ready")
            .length, 0, "caosdb-f-field-not-ready class removed");
        assert.equal(field.id, "f2", "field2 ready");
        done();
    });

    // send field_ready
    $(field2).removeClass("caosdb-f-field-not-ready");
    field2.dispatchEvent(form_elements.field_ready_event);

    form_elements.field_ready(field3).then(field => {
        assert.equal($(field).find(".caosdb-f-field-not-ready")
            .length, 0, "no caosdb-f-field-not-ready");
        assert.equal(field.id, "f3", "field3 ready");
        done();
    });
    // nothing to do for field3

    form_elements.field_ready(field4).then(field => {
        assert.ok(false, "field4 should never be ready!");
    });
});

QUnit.test("make_alert - cancel", async function (assert) {
    var cancel_callback = assert.async()
    var _alert = form_elements.make_alert({
        message: "message",
        proceed_callback: () => {
            assert.ok(false, "this should not be called");
        },
        cancel_callback: cancel_callback,
    });
    $("body").append(_alert);

    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 1, "alert is there");
    $(_alert).find("button.caosdb-f-btn-alert-cancel")[0].click();
    await sleep(500);

    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 0, "has been removed");


});

QUnit.test("make_alert - proceed", async function (assert) {
    var proceed_callback = assert.async();
    var _alert = form_elements.make_alert({
        message: "message",
        proceed_callback: proceed_callback,
    });
    assert.equal($(_alert).find("[type='checkbox']").length, 0, "no remember checkbox");

    $("body").append(_alert);
    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 1, "alert is there");
    $(_alert).find("button.caosdb-f-btn-alert-proceed")[0].click();
    await sleep(500);

    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 0, "has been removed");

});

QUnit.test("make_alert - remember", async function (assert) {
    form_elements._set_alert_decision("unittests", "");

    var proceed_callback = assert.async(3);
    var _alert = form_elements.make_alert({
        message: "message",
        proceed_callback: proceed_callback,
        remember_my_decision_id: "unittests",
    });
    assert.equal($(_alert).find("[type='checkbox']").length, 1, "has remember checkbox");

    // append for the first time
    $("body").append(_alert);
    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 1, "alert is there");

    // click proceed without "don't ask me again".
    $(_alert).find("button.caosdb-f-btn-alert-proceed")[0].click();
    await sleep(500);

    form_elements._set_alert_decision("unittests", "");

    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 0, "has been removed");

    // append 2nd time
    _alert = form_elements.make_alert({
        message: "message",
        proceed_callback: proceed_callback,
        remember_my_decision_id: "unittests",
    });
    assert.equal($(_alert).find("[type='checkbox']").length, 1, "has remember checkbox");
    $("body").append(_alert);
    assert.equal($("body").find(".caosdb-f-form-elements-alert").length, 1, "alert is there");
    $(_alert).find("[type='checkbox']").prop("checked", true);

    $(_alert).find("button.caosdb-f-btn-alert-proceed")[0].click();
    await sleep(500);
    form_elements._set_alert_decision("unittests", "proceed");

    // try 3rd time
    _alert = form_elements.make_alert({
        message: "message",
        proceed_callback: proceed_callback,
        remember_my_decision_id: "unittests",
    });
    assert.equal(typeof _alert, "undefined", "alert was not created, proceed callback was called third time");
});

QUnit.test("make_select_input", function (assert) {
    const config = {
        name: "sex",
        label: "Sex",
        multiple: true,
        options: [{
            "value": "f",
            "label": "female"
        }, {
            "value": "d",
            "label": "diverse"
        }, {
            "value": "m",
            "label": "male"
        }, ],
    }
    const select = $(form_elements.make_select_input(config));
    assert.equal(select.find("select").length, 1, "select input there");
    assert.equal(select.find("select").attr("name"), "sex", "has select with correct name");
    assert.equal(select.find("select option").length, 3, "three options there");
});

QUnit.test("select_input caching", async function (assert) {
    const config = {
        "name": "test-form",
        "fields": [{
            type: "select",
            required: true,
            cached: true,
            name: "sex",
            label: "Sex",
            options: [{
                "value": "f",
                "label": "female"
            }, {
                "value": "d",
                "label": "diverse"
            }, {
                "value": "m",
                "label": "male"
            }, ],
        }, ],
    }
    const form_wrapper = $(form_elements.make_form(config));
    await sleep(200);
    const form = form_wrapper.find("form");
    assert.equal(form.find("select").length, 1);


    // write to cache
    const cache = {};
    const field = $(form_elements.get_fields(form[0], "sex"));
    field.find("select").val("f");
    assert.equal(form_elements.get_cache_value(field[0]), "f", "initial value set");
    assert.equal(form_elements.get_cache_key(form[0], field[0]), "form_elements.cache.test-form.sex", "cache key correct");

    form_elements.cache_form(cache, form[0]);
    assert.equal(cache[form_elements.get_cache_key(form[0], field[0])], "f");

    // read from cache and set the value
    field.find("select").val("m");
    assert.equal(form_elements.get_cache_value(field[0]), "m", "different value set");

    form_elements.load_cached(cache, form[0]);
    await sleep(200);
    assert.equal(form_elements.get_cache_value(field[0]), "f", "value back to value from cache");
});

QUnit.test("make_file_input", function (assert) {
    const config = {
        name: "some_file",
        multiple: true,
        accept: ".tsv, .csv",
    }
    const file_input = $(form_elements.make_file_input(config));
    assert.equal(file_input.find(":input").length, 1, "file input there");
    assert.equal(file_input.find(":input").attr("name"), "some_file", "has file input with correct name");
    assert.ok(file_input.find(":input").prop("multiple"), "is multiple");
    assert.equal(file_input.find(":input").attr("accept"), ".tsv, .csv", "accept there");
});

QUnit.test("pattern", function (assert) {
    const config = {
        "name": "test-form",
        "fields": [{
            "required": true,
            "name": "text_field",
            "type": "text",
            "pattern": "[a-f]*"
        }, ],
    }
    const form_wrapper = $(form_elements.make_form(config));
    const form = form_wrapper.find("form")[0];

    assert.notOk(form_elements.check_form_validity(form), "empty -> invalid");
    form_wrapper.find(":input[name='text_field']").val("ghi")
    assert.notOk(form_elements.check_form_validity(form), "ghi -> invalid");

    form_wrapper.find(":input[name='text_field']").val("abc")
    assert.ok(form_elements.check_form_validity(form), "abc -> valid");
});
