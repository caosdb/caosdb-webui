/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

'use strict';

QUnit.module("form_panel.js", {
    before: function(assert) {

    },
    after: function(assert) {}
});

QUnit.test("availability", function(assert) {
    assert.ok(form_panel.init, "init available");
    assert.ok(form_panel.create_show_form_callback, "version available");
});

QUnit.test("create_show_form_callback ", function(assert) {
    const title = "Upload CSV File"; // title of the form and text in the toolbox
    const panel_id = "csv_upload_form_panel";
    const server_side_script = "csv_script.py";
    const tool_box = "Tools"; // Name of the drop-down menu where the button is added in the navbar
    const help_text = "something";
    const accepted_files_formats = [".csv", "text/tsv", ] // Mime types and file endings.

    const csv_form_config = {
        script: server_side_script,
        fields: [{
            type: "file",
            name: "csv_file",
            label: "CSV File", // label of the file selector in the form
            required: true,
            cached: false,
            accept: accepted_files_formats.join(","),
            help: help_text,
        }, ],
    };
    const cb = form_panel.create_show_form_callback(panel_id, title, csv_form_config);
    assert.equal(typeof cb, "function", "function created");
    cb()

    const creator = function() {
        return form_elements.make_form(csv_form_config);
    }
    const cb2 = form_panel.create_show_form_callback(
        panel_id, title, undefined, creator
    );
    assert.equal(typeof cb2, "function", "function created");
    cb2()
    assert.notOk(document.querySelector(`#${panel_id}`), "panel is being appended to nav, but there is no nav");
    document.body.appendChild(document.createElement("nav"));

    cb2()
    assert.ok(document.querySelector(`#${panel_id}`), "panel was appended to nav");
    console.log(document.activeElement, "here");
    assert.equal(document.activeElement.name, "csv_file", "first form field is being focussed");

    document.querySelector(`#${panel_id} form`).dispatchEvent(form_elements.cancel_form_event);
    assert.notOk(document.querySelector(`#${panel_id}`), "query panel removed");
});


QUnit.test("autofocus_without_form", function(assert) {
    const title = "My not-form title";
    const id = "not_form_id";
    // callback function that creates a "form" without HTML form
    // elements.  Trivial here, but could be e.g., the file upload
    // from LinkAhead WebUI Core Components.
    const init_not_form_field = () => {
        const container = $(`<div class="row"/>`);
        return container[0];
    };
    // This should always work
    const cb_without_autofocus = form_panel.create_show_form_callback(
        id,
        title,
        undefined,
        init_not_form_field,
        false
    );
    const nav = document.createElement("nav");
    document.body.appendChild(nav);
    cb_without_autofocus();
    assert.ok(document.querySelector(`#${id}`), "Callback was called without autofocus.");
    $(`#${id}`).remove();

    const cb_with_autofocus = form_panel.create_show_form_callback(
        id,
        title,
        undefined,
        init_not_form_field,
        true
    );
    cb_with_autofocus();
    assert.ok(document.querySelector(`#${id}`), "Callback was called with autofocus, bt still okay.");
});
