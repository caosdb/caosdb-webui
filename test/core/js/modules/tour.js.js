/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
'use strict';


/* SETUP */
QUnit.module("tour.js", {
});

QUnit.test("availability", function(assert){
    assert.ok(tour, "tour module available");
    assert.ok(typeof tour.init === "function", "tour module has init function")
});

//QUnit.test("create_tour_button", function(assert){
    //assert.ok(typeof tour.create_tour_button === "function", "function available");
    //assert.throws(()=>{tour.create_tour_button()}, "no params call throws");
    //assert.throws(()=>{tour.create_tour_button(undefined, "content")}, "name undefined call throws");
    //assert.throws(()=>{tour.create_tour_button("name", undefined)}, "content undefined call throws");

//});


//QUnit.test("apply_highlighters", function(assert){
    //assert.ok(typeof tour.apply_highlighters === "function", "function available");
    //assert.throws(()=>{tour.apply_highlighters()}, "no params call throws.");
    //assert.throws(()=>{tour.apply_highlighters(undefined, {})}, "button undefined call throws");
    //assert.throws(()=>{tour.apply_highlighters("asdf", {})}, "button was not HTMLElement");
    //assert.ok(!tour.apply_highlighters($('<div/>')[0], {}), "params ok");
//});

QUnit.test("tour.PageSet", function(assert){
    assert.ok(typeof tour.PageSet === "function", "class available");
    assert.throws(() => {new tour.PageSet()}, "no param instanciation throws");
    assert.throws(() => {new tour.PageSet("string")}, "string param instanciation throws");
});
