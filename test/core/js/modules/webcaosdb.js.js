/*
 * ** header v3.0
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH (info@indiscale.com)
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com), Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
/* testing webcaosdb's javascript sources */


/* SETUP general module */
QUnit.module("webcaosdb.js", {
    before: function (assert) {
        markdown.init();
        connection._init();
    },
    after: function (assert) {
        connection._init();
    },
});

/* TESTS */
QUnit.test("xslt", function (assert) {
    let xml_str = '<root/>';
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="root"><newroot>content</newroot></xsl:template></xsl:stylesheet>';
    xml = str2xml(xml_str);
    xsl = str2xml(xsl_str);
    broken_xsl = str2xml('<blabla/>');

    html = xslt(xml, xsl);
    assert.ok(html);
    assert.equal(html.firstChild.localName, "newroot", "<root/> transformed to <newroot/>");

    // broken xsl throws exception
    assert.throws(() => {
        xslt(xml, broken_xsl)
    }, "broken xsl throws exc.");

    // string throws exception
    assert.throws(() => {
        xslt(xml_str, xsl)
    }, "xml_str throws exc.");
    assert.throws(() => {
        xslt(xml, xsl_str)
    }, "xsl_str throws exc.");

    assert.throws(() => {
        xslt(undefined, xsl)
    }, "null xml throws exc.");
    assert.throws(() => {
        xslt(xml, undefined)
    }, "nu ll xsl throws exc.");
});

QUnit.test("markdown.textTohtml", function (assert) {
    const str = `\# header\n\#\# another header\nparagraph`;
    assert.equal(markdown.textToHtml(str), "<h1 id=\"header\">header</h1>\n<h2 id=\"anotherheader\">another header</h2>\n<p>paragraph</p>");

});

QUnit.test("injectTemplate", async function (assert) {
    const xml_str = '<root/>';
    const xsl_str = '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:output method="html" /></xsl:stylesheet>';
    const xml = str2xml(xml_str);
    const xsl = str2xml(xsl_str);

    const result_xsl = injectTemplate(xsl, '<xsl:template xmlns="http://www.w3.org/1999/xhtml" match="root"><newroot>content</newroot></xsl:template>');

    assert.equal(xml2str(result_xsl), "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\"><xsl:output method=\"html\"/><xsl:template xmlns=\"http://www.w3.org/1999/xhtml\" match=\"root\"><newroot>content</newroot></xsl:template></xsl:stylesheet>");
    var result_xml = xslt(xml, result_xsl);
    assert.equal(xml2str(result_xml), "<newroot xmlns=\"http://www.w3.org/1999/xhtml\">content</newroot>");

    result_xml = await asyncXslt(xml, result_xsl);
    assert.equal(xml2str(result_xml), "<newroot xmlns=\"http://www.w3.org/1999/xhtml\">content</newroot>");
});

QUnit.test("asyncXslt", function (assert) {
    let xml_str = '<root/>';
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="root"><newroot/></xsl:template></xsl:stylesheet>';
    xml = str2xml(xml_str);
    xsl = str2xml(xsl_str);
    xslProm = new Promise((resolve, reject) => {
        setTimeout(resolve, 1000, xsl);
    });
    broken_xsl = str2xml('<blabla/>');

    let done = assert.async(2);
    asyncXslt(xml, xslProm).then((html) => {
        assert.ok(html);
        assert.equal(html.firstChild.localName, "newroot", "<root/> transformed to <newroot/>");
        done();
    });

    // broken xsl throws exception
    asyncXslt(xml, broken_xsl).catch((error) => {
        assert.equal(/^XSL Transformation.*$/.test(error.message), true, "broken xsl thros exc.");
        done();
    });
});

QUnit.test("xml2str", function (assert) {
    xml = str2xml('<root/>');
    assert.equal(xml2str(xml), '<root/>');
});

QUnit.test("str2xml", function (assert) {
    xml = str2xml('<root/>');
    assert.ok(xml);

    // make sure this is a document:
    assert.ok(xml.contentType.endsWith("/xml"), "has contentType=*/xml");
    assert.ok(xml.documentElement, "has documentElement");
    assert.equal(xml.documentElement.outerHTML, '<root/>', "has outerHTML");

    // TODO: there is no mechanism to throw an error when the string is not
    // valid.
});

QUnit.test("postXml", function (assert) {
    assert.ok(postXml, "function exists.");
});

QUnit.test("createErrorNotification", function (assert) {
    assert.ok(createErrorNotification, "function available");
    let err = createErrorNotification("test");
    assert.ok($(err).hasClass(preview.classNameErrorNotification), "has class caosdb-preview-error-notification");
});

/* MODULE connection */
QUnit.module("webcaosdb.js - connection", {
    before: function (assert) {
        assert.ok(connection, "connection module is defined");
    }
});

QUnit.test("get", function (assert) {
    assert.expect(4);
    assert.ok(connection.get, "function available");
    let done = assert.async(2);
    connection.get("webinterface/${BUILD_NUMBER}/xsl/entity.xsl").then(function (resolve) {
        assert.equal(resolve.toString(), "[object XMLDocument]", "entity.xsl returned.");
        done();
    });
    connection.get("webinterface/non-existent").then((resolve) => resolve, function (error) {
        assert.equal(error.toString().split(" - ", 1)[0], "Error: GET webinterface/non-existent returned with HTTP status 404", "404 error thrown");
        done();
    });
});

/* MODULE transformation */
QUnit.module("webcaosdb.js - transformation", {
    before: function (assert) {
        assert.ok(transformation, "transformation module is defined");
    }
});

QUnit.test("removePermissions", function (assert) {
    assert.ok(transformation.removePermissions, "function available");
});

QUnit.test("retrieveXsltScript", function (assert) {
    assert.ok(transformation.retrieveXsltScript, "function available");
    let done = assert.async(2);
    transformation.retrieveXsltScript("entity.xsl").then(xsl => {
        assert.equal(xsl.toString(), "[object XMLDocument]", "entity.xsl returned");
        done();
    });
    transformation.retrieveXsltScript("asdfasdfasdf.xsl").then(xsl => xsl, err => {
        assert.equal(err.toString().split(" - ")[0], "Error: GET webinterface/${BUILD_NUMBER}/xsl/asdfasdfasdf.xsl returned with HTTP status 404", "not found.");
        done();
    });
});

QUnit.test("retrieveEntityXsl", function (assert) {
    assert.ok(transformation.retrieveEntityXsl, "function available");
    let done = assert.async();
    transformation.retrieveEntityXsl().then(xsl => {
        let xml = str2xml('<Response/>');
        let html = xslt(xml, xsl);
        assert.equal(html.firstElementChild.tagName, "DIV");
        done();
    });
});

QUnit.test("transformEntities", function (assert) {
    assert.ok(transformation.transformEntities, "function available");
    let done = assert.async();
    let xml = str2xml('<Response><Record id="142"><Warning description="asdf"/></Record></Response>');
    transformation.transformEntities(xml).then(htmls => {
        assert.ok($(htmls[0]).hasClass("caosdb-entity-panel"), "entity has been transformed");
        assert.equal($(htmls[0]).find('.caosdb-messages .alert-warning').length, 1, "entity has warning.");
        done();
    }, err => {
        globalError(err);
    });
});

QUnit.test("mergeXsltScripts", function (assert) {
    assert.ok(transformation.mergeXsltScripts, 'function available.');
    let xslMainStr = '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"/>';
    assert.equal(xml2str(transformation.mergeXsltScripts(str2xml(xslMainStr), [])), xslMainStr, 'no includes returns same as xslMain.');
    let xslIncludeStr = '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"><xsl:template name="bla"/></xsl:stylesheet>'
    let xslInclude = str2xml(xslIncludeStr);
    assert.ok($(transformation.mergeXsltScripts(str2xml(xslMainStr), [xslInclude])).find("[name='bla']")[0], 'template bla is there.');
});

/* MODULE transaction */
QUnit.module("webcaosdb.js - transaction", {
    before: function (assert) {
        assert.ok(transaction, "transaction module is defined");
    },
});

QUnit.test("generateEntitiesUri", function (assert) {
    assert.ok(transaction.generateEntitiesUri, "function available");

    assert.throws(() => {
        transaction.generateEntitiesUri()
    }, 'no param throws');
    assert.throws(() => {
        transaction.generateEntitiesUri(null)
    }, 'null param throws');
    assert.equal(transaction.generateEntitiesUri(["asdf", "qwer"]), "Entity/asdf&qwer", "works");
});

QUnit.test("updateEntitiesXml", function (assert) {
    assert.ok(transaction.updateEntitiesXml, "function available");
    var done = assert.async();
    connection.put = function (uri, data) {
        assert.equal(uri, 'Entity/', "updateEntitiesXml calls connection.put");
        assert.equal(xml2str(data), '<Update/>');
        done();
    };
    transaction.updateEntitiesXml(str2xml('<Update/>'));
});

QUnit.test("retrieveEntitiesById", function (assert) {
    assert.ok(transaction.retrieveEntitiesById, "function available");
    var done = assert.async();
    connection.get = function (uri) {
        assert.equal(uri, 'Entity/1234&2345', "retrieveEntitiesById calls connection.get");
        done();
    };
    transaction.retrieveEntitiesById(["1234", "2345"]);
});

QUnit.test("retrieveEntityById", function (assert) {
    assert.ok(transaction.retrieveEntityById, "function available");
    var done = assert.async();
    connection.get = function (uri) {
        assert.equal(uri, 'Entity/1234', "retrieveEntityById calls connection.get");
        return new Promise((ok, fail) => {
            setTimeout(() => ok(str2xml('<Response><Entity id="1234" name="new"/></Response>')), 200);
        });
    };
    transaction.retrieveEntityById("1234").then(ret => {
        done();
    });
});

/* MODULE transaction.update */
QUnit.module("webcaosdb.js - transaction.update", {
    before: function (assert) {
        assert.ok(transaction.update, "transaction.update module is defined");
    }
});

QUnit.test("createWaitRetrieveNotification", function (assert) {
    assert.ok(transaction.update.createWaitRetrieveNotification(), 'function available and returns non-null');
});

QUnit.test("createWaitUpdateNotification", function (assert) {
    assert.ok(transaction.update.createWaitUpdateNotification(), 'function available and returns non-null');
});

QUnit.test("createUpdateForm", function (assert) {
    let done = assert.async();
    let cuf = transaction.update.createUpdateForm;
    assert.ok(cuf, "function available");
    assert.throws(() => cuf(null, function (xml) {}), "null entityXmlStr throws");
    assert.throws(() => cuf("", null), "null putCallback throws");
    assert.throws(() => cuf("", ""), "non-function putCallback throws");
    assert.throws(() => cuf("", function () {}), "putCallback function without parameters throws");

    let form = cuf("<root/>", function (xml) {
        assert.equal(xml, '<newroot/>', "modified xml is submitted.");
        done();
    });
    assert.equal(form.tagName, "FORM", "returns form");
    assert.ok($(form).hasClass(transaction.classNameUpdateForm), "has correct class");
    assert.equal($(form).find('textarea').length, 1, "has one textarea");
    let textarea = $(form).find('textarea')[0];
    assert.equal(textarea.value, "<root/>", "textarea contains xml");
    assert.equal(textarea.name, "updateXml", "textarea has name updateXml");
    assert.equal($(form).find(':submit').length, 1, "has one submit button");
    assert.equal($(form).find(':reset').length, 1, "has one reset button");

    textarea.value = "<toberesetroot/>"
    $(form).trigger('reset');
    assert.equal(textarea.value, "<root/>", "after reset, old xml is there again");
    textarea.value = "<newroot/>";
    $(form).submit();

    //$(document.body).append(form);
});

QUnit.test("createUpdateEntityHeading", function (assert) {
    let cueh = transaction.update.createUpdateEntityHeading;
    assert.ok(cueh, "function available");
    let eh = $('<div class="card-header"><div class="1strow"></div><div class="2ndrow"></div></div>')[0];
    assert.equal($(eh).children('.1strow').length, 1, "eh has 1st row");
    assert.equal($(eh).children('.2ndrow').length, 1, "eh has 2nd row");
    let uh = cueh(eh);
    assert.equal($(uh).children('.2ndrow').length, 0, "uh has no 2nd row");
    assert.equal($(uh).children('.1strow').length, 1, "uh has 1st row");
});

QUnit.test("createUpdateEntityPanel", function (assert) {
    let cued = transaction.update.createUpdateEntityPanel;
    assert.ok(cued, "function available");
    let div = $(cued($('<div id="headingid">heading</div>')));
    assert.ok(div.hasClass("card"), "card has class card.");
    assert.equal(div.children(":first-child")[0].id, "headingid", "heading is first child element");
});

QUnit.test("updateSingleEntity - success", function (assert) {
    let done = assert.async();
    let use = transaction.update.updateSingleEntity;
    assert.ok(use, "function available");
    let entityPanel = $('<div class="card caosdb-entity-panel"><div class="card-header caosdb-entity-panel-heading"><div>heading<div class="caosdb-id">1234</div></div><div>other stuff in the heading</div></div>body</div>')[0];
    connection.get = function (uri) {
        assert.equal(uri, 'Entity/1234', 'get was called with correct uri');
        return new Promise((ok, fail) => {
            setTimeout(() => {
                ok(str2xml('<Response><Entity id="1234" name="old"><Permissions/></Entity></Response>'));
            }, 200);
        });
    };


    $(document.body).append(entityPanel);
    let app = use(entityPanel);

    assert.equal(app.state, 'waitRetrieveOld', "in waitRetrieveOld state");

    setTimeout(() => {
        connection._init();
        connection.put = function (uri, xml) {
            assert.equal(xml2str(xml), '<Update><Entity id="1234" name="new"/></Update>', "put was called with correct xml");
            return new Promise((ok, fail) => {
                setTimeout(() => ok(str2xml('<Response><Record id="1234" name="new"></Record></Response>')), 200);
            });
        };
        let form = $(document.body).find('form.' + transaction.classNameUpdateForm);
        assert.ok(form[0], "form is there.");
        assert.equal(form[0].updateXml.value, '<Entity id="1234" name="old"/>', "Permisions have been removed.");
        form[0].updateXml.value = '<Entity id="1234" name="new"/>';
        form.submit();
        assert.equal(app.state, 'waitPutEntity', "in waitPutEntity state");
        assert.equal($(document.body).find('form.' + transaction.classNameUpdateForm).length, 0, "form has been removed.");

    }, 400);



    app.onEnterFinal = function (e) {
        done();
        $(entityPanel).remove();
    };

});

QUnit.test("updateSingleEntity - with errors in the server's response", function (assert) {
    let done = assert.async();
    let use = transaction.update.updateSingleEntity;
    let entityPanel = $('<div class="card caosdb-entity-panel"><div class="card-header caosdb-entity-panel-heading"><div>heading<div class="caosdb-id">1234</div></div><div>other stuff in the heading</div></div>body</div>')[0];
    connection.get = function (uri) {
        return new Promise((ok, fail) => {
            setTimeout(() => {
                ok(str2xml('<Response><Entity id="1234" name="old"/></Response>'));
            }, 200);
        });
    };

    $(document.body).append(entityPanel);
    let app = use(entityPanel);

    // submit form -> server response contains error tag.
    setTimeout(() => {
        connection._init();
        connection.put = function (uri, xml) {
            return new Promise((ok, fail) => {
                setTimeout(() => ok(str2xml('<Response><Record id="1234" name="new"><Error description="This is an error."/></Record></Response>')), 200);
            });
        };
        $(document.body).find('form.' + transaction.classNameUpdateForm).submit();
    }, 400);

    app.onLeaveWaitPutEntity = function (e) {
        assert.equal(e.transition, "openForm", "app returns to form again due to errors.");
        assert.equal($(app.updatePanel).find('.card-header .' + preview.classNameErrorNotification).length, 0, "has no error notification before the response is processed.");

        setTimeout(() => {
            assert.equal($(app.updatePanel).find('.card-header .' + preview.classNameErrorNotification).length, 1, "has an error notification after the response is processed.");
            $(app.updatePanel).remove();
            entityPanel.remove();
            done();
        }, 200);
    };

});

QUnit.test("createErrorInUpdatedEntityNotification", function (assert) {
    assert.ok(transaction.update.createErrorInUpdatedEntityNotification, "function available.");
});

QUnit.test("addErrorNotification", function (assert) {
    assert.ok(transaction.update.addErrorNotification, "function available");
});

/* MODULE preview */
QUnit.module("webcaosdb.js - preview", {
    before: function (assert) {
        // load xmlTestCase
        var done = assert.async(2);
        var qunit_obj = this;
        $.ajax({
            cache: true,
            dataType: 'xml',
            url: "xml/test_case_preview_entities.xml",
        }).done(function (data, textStatus, jdXHR) {
            qunit_obj.testXml = data;
        }).always(function () {
            done();
        });
        // load entity.xsl
        preview.getEntityXsl("../").then(function (data) {
            insertParam(data, "entitypath", "/entitypath/");
            insertParam(data, "filesystempath", "/filesystempath/");
            qunit_obj.entityXSL = injectTemplate(data, '<xsl:template match="/"><root><xsl:apply-templates select="/Response/*" mode="top-level-data"/></root></xsl:template>');
            done();
        });

        assert.ok(preview, "preview module is defined");
    },
    afterEach: function (assert) {
        connection._init();
    }
});

QUnit.test("halfArray", function (assert) {
    assert.ok(preview.halfArray, "function available");
    assert.throws(() => {
        preview.halfArray([1]);
    }, "length < 2 throws.")
    assert.deepEqual(preview.halfArray([1, 2]), [
        [1],
        [2]
    ]);
    assert.deepEqual(preview.halfArray([1, 2, 3]), [
        [1],
        [2, 3]
    ]);
    assert.deepEqual(preview.halfArray([1, 2, 3, 4]), [
        [1, 2],
        [3, 4]
    ]);
});

QUnit.test("xslt file preview", function (assert) {
    let done = assert.async();
    let entityXSL = this.entityXSL;
    $.ajax({
        cache: true,
        dataType: 'xml',
        url: "xml/test_case_file_preview.xml",
    }).then((data) => {
        let html = xslt(data, entityXSL);
        assert.ok(html);
        done();
    }).catch((err) => {
        console.log(err);
        assert.ok(false, "error! see console.");
        done();
    });
});

QUnit.test("createShowPreviewButton", function (assert) {
    assert.ok(preview.createShowPreviewButton, "function available");
    let showPreviewButton = preview.createShowPreviewButton();
    assert.ok(showPreviewButton, "not null");
    assert.equal(showPreviewButton.tagName, "BUTTON", "is button element");
    assert.ok($(showPreviewButton).hasClass("caosdb-show-preview-button"), "has class caosdb-show-preview-button");
});

QUnit.test("createHidePreviewButton", function (assert) {
    assert.ok(preview.createHidePreviewButton, "function available");
    let hidePreviewButton = preview.createHidePreviewButton();
    assert.ok(hidePreviewButton, "not null");
    assert.equal(hidePreviewButton.tagName, "BUTTON", "is button element");
    assert.ok($(hidePreviewButton).hasClass("caosdb-hide-preview-button"), "has class 'caosdb-hide-preview-button'");
});

QUnit.test("addHidePreviewButton", function (assert) {
    assert.ok(preview.addHidePreviewButton, "function available");
    let okTestElem = $('<div><div class="caosdb-f-property-value"></div></div>')[0]
    let notOkTestElem = $('<div></div>')[0]

    assert.throws(() => {
        preview.addHidePreviewButton();
    }, "no argument throws.")
    assert.throws(() => {
        preview.addHidePreviewButton(null, null);
    }, "null arguments throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(okTestElem, null);
    }, "null button_elem parameter throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(null, $('<div/>')[0]);
    }, "null ref_property_elem parameter throws.");
    assert.throws(() => {
        preview.addHidePreviewButton(notOkTestElem, $('<div/>')[0]);
    }, "ref_property_elem w/o caosdb-value-list throws.");
    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addHidePreviewButton(okTestElem, preview.createHidePreviewButton()), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addShowPreviewButton", function (assert) {
    assert.ok(preview.addShowPreviewButton, "function available");
    let okTestElem = $('<div><div class="caosdb-f-property-value"></div></div>')[0]
    let notOkTestElem = $('<div></div>')[0]

    assert.throws(() => {
        preview.addShowPreviewButton();
    }, "no argument throws.")
    assert.throws(() => {
        preview.addShowPreviewButton(null, null);
    }, "null arguments throws.");
    assert.throws(() => {
        preview.addShowPreviewButton(okTestElem, null);
    }, "null button_elem parameter throws.");
    assert.throws(() => {
        preview.addShowPreviewButton(null, $('<div/>')[0]);
    }, "null ref_property_elem parameter throws.");
    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addShowPreviewButton(okTestElem, preview.createShowPreviewButton()), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addWaitingNotification", function (assert) {
    assert.ok(preview.addWaitingNotification, "function available");
    let testWaiting = $('<div>Waiting!</div>')[0];
    let okTestElem = $('<div><div class="caosdb-preview-notification-area"></div></div>')[0];
    let notOkTestElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.addWaitingNotification();
    }, "no arguments throws");
    assert.throws(() => {
        preview.addWaitingNotification(null, null);
    }, "null arguments throws");
    assert.throws(() => {
        preview.addWaitingNotification(null, testWaiting);
    }, "null first argument throws");
    assert.throws(() => {
        preview.addWaitingNotification(okTestElem, null);
    }, "null second argument throws");
    assert.throws(() => {
        preview.addWaitingNotification(notOkTestElem, testWaiting);
    }, "ref_property_elem w/o notification area throws");

    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addWaitingNotification(okTestElem, testWaiting), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("addErrorNotification", function (assert) {
    assert.ok(preview.addErrorNotification, "function available");
    let testError = $('<div>Error!</div>')[0];
    let okTestElem = $('<div><div class="caosdb-preview-notification-area"></div></div>')[0];
    let notOkTestElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.addErrorNotification();
    }, "no arguments throws");
    assert.throws(() => {
        preview.addErrorNotification(null, null);
    }, "null arguments throws");
    assert.throws(() => {
        preview.addErrorNotification(null, testError);
    }, "null first argument throws");
    assert.throws(() => {
        preview.addErrorNotification(okTestElem, null);
    }, "null second argument throws");
    assert.throws(() => {
        preview.addErrorNotification(notOkTestElem, testError);
    }, "ref_property_elem w/o notification area throws");

    assert.equal(okTestElem.firstChild.childNodes.length, 0, "before: test div has no children");
    assert.equal(okTestElem, preview.addErrorNotification(okTestElem, testError), "returns the first parameter");
    assert.equal(okTestElem.firstChild.childNodes.length, 1, "after: test div has new child");
});

QUnit.test("createWaitingNotification", function (assert) {
        assert.ok(preview.createWaitingNotification, "function available");
        let welem = preview.createWaitingNotification();
        assert.ok(welem, "not null");
        assert.ok($(welem).hasClass("caosdb-preview-waiting-notification"), "element has class 'caosdb-preview-waiting-notification'");
    }),

    QUnit.test("createNotificationArea", function (assert) {
        assert.ok(preview.createNotificationArea, "function available");
        let narea = preview.createNotificationArea();
        assert.ok(narea, "not null");
        assert.equal(narea.tagName, "DIV", "element is div");
        assert.ok($(narea).hasClass("caosdb-preview-notification-area"), "has class caosdb-preview-notification-area");
    });

QUnit.test("getHidePreviewButton", function (assert) {
    assert.ok(preview.getHidePreviewButton, "function available");
    let okElem = $('<div><button class="caosdb-hide-preview-button">click</button></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getHidePreviewButton();
    }, "no parameter throws.");
    assert.throws(() => {
        preview.getHidePreviewButton(null);
    }, "null parameter throws.");

    assert.notOk(preview.getHidePreviewButton(notOkElem), "parameter w/o button returns null");
    assert.equal(preview.getHidePreviewButton(okElem), okElem.firstChild, "button found");
});

QUnit.test("getRefLinksContainer", function (assert) {
    assert.ok(preview.getRefLinksContainer, "function available");
    // TODO: references or lists of references should have a special class, not just
    // caosdb-value-list. -> entity.xsl
    let okElem = $('<div><div class="caosdb-value-list"><a><span class="caosdb-id">1234</span></a></div></div>')[0];
    let okSingle = $('<div><div class="caosdb-f-property-value"><a class="btn"><span class="caosdb-id">1234</span></a></div><</div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getRefLinksContainer();
    }, "no parameter throws");
    assert.throws(() => {
        preview.getRefLinksContainer(null);
    }, "null parameter throws");

    assert.notOk(preview.getRefLinksContainer(notOkElem), "parameter w/o elem returns null");
    assert.equal(preview.getRefLinksContainer(okElem), okElem.firstChild, "links found");
    assert.equal(preview.getRefLinksContainer(okSingle), okSingle.firstChild.firstChild, "single link found");
});

QUnit.test("getPreviewCarousel", function (assert) {
    assert.ok(preview.getPreviewCarousel, "function available");
    let okElem = $('<div><div class="' + preview.classNamePreview + '"></div></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getPreviewCarousel();
    }, "no parameter throws");
    assert.throws(() => {
        preview.getPreviewCarousel(null);
    }, "null parameter throws");

    assert.notOk(preview.getPreviewCarousel(notOkElem), "parameter w/o carousel returns null");
    assert.equal(preview.getPreviewCarousel(okElem), okElem.firstChild, "carousel found");
});

QUnit.test("getShowPreviewButton", function (assert) {
    assert.ok(preview.getShowPreviewButton, "function available");
    let okElem = $('<div><button class="caosdb-show-preview-button">click</button></div>')[0];
    let notOkElem = $('<div></div>')[0];

    assert.throws(() => {
        preview.getShowPreviewButton();
    }, "no parameter throws.");
    assert.throws(() => {
        preview.getShowPreviewButton(null);
    }, "null parameter throws.");

    assert.notOk(preview.getShowPreviewButton(notOkElem), "parameter w/o button returns null");
    assert.equal(preview.getShowPreviewButton(okElem), okElem.firstChild, "button found");
});

QUnit.test("removeAllErrorNotifications", function (assert) {
    assert.ok(preview.removeAllErrorNotifications, "function available");
    let okElem = $('<div><div class="caosdb-preview-error-notification">Error1</div>' +
        '<div class="caosdb-preview-error-notification">Error2</div>' +
        '<div class="caosdb-preview-waiting-notification">Please wait!</div></div>')[0];
    let emptyElem = $('<div></div>')[0];


    assert.equal(okElem.childNodes.length, 3, "before: three children");
    assert.equal(okElem, preview.removeAllErrorNotifications(okElem), "return first parameter");
    assert.equal(okElem.childNodes.length, 1, "after: one child");
    assert.equal(okElem.firstChild.className, "caosdb-preview-waiting-notification", "waiting notification still there");

    assert.equal(emptyElem, preview.removeAllErrorNotifications(emptyElem), "empty elem works");
});

QUnit.test("removeAllWaitingNotifications", function (assert) {
    assert.ok(removeAllWaitingNotifications, "function available");
    let okElem = $('<div><div class="caosdb-preview-waiting-notification">Waiting1</div>' +
        '<div class="caosdb-preview-waiting-notification">Waiting2</div>' +
        '<div class="caosdb-preview-error-notification">Error!</div></div>')[0];
    let emptyElem = $('<div></div>')[0];

    assert.equal(okElem.childNodes.length, 3, "before: three children");
    assert.equal(okElem, removeAllWaitingNotifications(okElem), "return first parameter");
    assert.equal(okElem.childNodes.length, 1, "after: one child");
    assert.equal(okElem.firstChild.className, "caosdb-preview-error-notification", "error notification still there");

    assert.equal(emptyElem, removeAllWaitingNotifications(emptyElem), "empty elem works");
});

QUnit.test("getActiveSlideItemIndex", function (assert) {
    assert.ok(preview.getActiveSlideItemIndex, "function available");
    let okElem0 = $('<div><div class="carousel-inner">' +
        '<div class="carousel-item active"></div>' // index 0
        +
        '<div class="carousel-item"></div>' +
        '<div class="carousel-item"></div>' +
        '</div></div>')[0];
    let okElem1 = $('<div><div class="carousel-inner">' +
        '<div class="carousel-item"></div>' +
        '<div class="carousel-item active"></div>' // index 1
        +
        '<div class="carousel-item"></div>' +
        '</div></div>')[0];
    let okElem2 = $('<div><div class="carousel-inner">' +
        '<div class="carousel-item"></div>' +
        '<div class="carousel-item"></div>' +
        '<div class="carousel-item active"></div>' // index 2
        +
        '</div></div>')[0];
    let noInner = $('<div></div>')[0];
    let noActive = $('<div><div class="carousel-inner"><div class="carousel-item"></div></div></div>')[0];

    assert.throws(() => {
        preview.getActiveSlideItemIndex()
    }, "no params throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(null)
    }, "null param throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(noInner)
    }, "param w/o .carousel-inner throws");
    assert.throws(() => {
        preview.getActiveSlideItemIndex(noActive)
    }, "param w/o .active throws");

    assert.equal(0, preview.getActiveSlideItemIndex(okElem0));
    assert.equal(1, preview.getActiveSlideItemIndex(okElem1));
    assert.equal(2, preview.getActiveSlideItemIndex(okElem2));
});

QUnit.test("getEntityByIdVersion", function (assert) {
    assert.ok(preview.getEntityByIdVersion, "function available");
    let e1 = $('<div><div class="caosdb-id">1</div></div>')[0];
    let e2 = $('<div><div class="caosdb-id">2</div></div>')[0];

    let es = [e1, e2];

    assert.throws(() => {
        preview.getEntityByIdVersion()
    }, "no param throws.");
    assert.throws(() => {
        preview.getEntityByIdVersion(null, 1)
    }, "null first param throws.");
    assert.throws(() => {
        preview.getEntityByIdVersion("asdf", 1)
    }, "string first param throws.");
    assert.throws(() => {
        preview.getEntityByIdVersion(es, null)
    }, "null second param throws.");

    assert.equal(e1, preview.getEntityByIdVersion(es, "1"), "find 1");
    assert.equal(e2, preview.getEntityByIdVersion(es, "2"), "find 2");
    assert.equal(null, preview.getEntityByIdVersion(es, "3"), "find 3 -> null");
});

QUnit.test("createEmptyInner", function (assert) {
    assert.ok(preview.createEmptyInner, "function available");

    assert.throws(() => {
        preview.createEmptyInner();
    }, "no param throws");
    assert.throws(() => {
        preview.createEmptyInner(null);
    }, "null param throws");
    assert.throws(() => {
        preview.createEmptyInner("asdf");
    }, "NaN param throws");
    assert.throws(() => {
        preview.createEmptyInner(-5);
    }, "smaller 1 param throws");

    let inner = preview.createEmptyInner(3);
    assert.equal(inner.children.length, 3, "three items");
    assert.equal(inner.children[0].className, "carousel-item active", "first item is active");
    assert.equal(inner.children[1].className, "carousel-item", "second item is not active");
    assert.equal(inner.children[2].className, "carousel-item", "third item is not active");
});

QUnit.test("createCarouselNav", function (assert) {
    assert.ok(preview.createCarouselNav, "function available");
    let refLinks = $('<div style="display: none;" class="caosdb-value-list"><a class="caosdb-f-reference-value"><span class="caosdb-id">1234</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">2345</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">3456</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">4567</span></a></div>')[0];
    assert.throws(() => {
        preview.createCarouselNav();
    }, "no param throws");
    assert.throws(() => {
        preview.createCarouselNav(null, "asdf");
    }, "null 1st param throws");
    assert.throws(() => {
        preview.createCarouselNav(refLinks, null);
    }, "null 2nd param throws");

    let nav = preview.createCarouselNav(refLinks, "cid");
    assert.equal(nav.className, "caosdb-preview-carousel-nav", "caosdb-carousel-nav");
    assert.ok($(nav).find('[data-bs-slide="prev"][href="#cid"]')[0], "has prev button");
    assert.ok($(nav).find('[data-bs-slide="next"][href="#cid"]')[0], "has next button");
    let selectors = preview.getRefLinksContainer(nav);
    assert.equal(selectors.children.length, 4, '4 selctor buttons');
    $(document.body).append(nav);
    assert.equal($(selectors).is(':hidden'), false, "selectors not hidden.");
    $(nav).remove();
    $(selectors).find('a').each((index, button) => {
        assert.equal(button.getAttribute("data-bs-slide-to"), index, "buttons have correct data-bs-slide-to attribute");
        assert.equal(button.getAttribute("data-bs-target"), "#cid", "buttons have correct data-bs-target attribute");
        assert.notOk(button.getAttribute("href"), "button dont have href");
    });
    assert.equal($(selectors).find('a:first').hasClass('active'), true, "first button is active");
    assert.equal($(selectors).find('.active').length, 1, "no other bu tton is active");
});

{
    let refLinks = $('<div class="caosdb-value-list"><a class="caosdb-f-reference-value"><span class="caosdb-id">1234</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">2345</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">3456</span></a><a class="caosdb-f-reference-value"><span class="caosdb-id">4567</span></a></div>')[0];
    let e1 = $('<a class="caosdb-f-reference-value"><div class="caosdb-id">1234</div></a>')[0];
    let e2 = $('<a class="caosdb-f-reference-value"><div class="caosdb-id">2345</div></a>')[0];
    let e3 = $('<a class="caosdb-f-reference-value"><div class="caosdb-id">3456</div></a>')[0];
    let e4 = $('<a class="caosdb-f-reference-value"><div class="caosdb-id">4567</div></a>')[0];
    let entities = [e1, e3, e4, e2];
    let carousel = preview.createPreviewCarousel(entities, refLinks);
    let correct_order_id = ["1234", "2345", "3456", "4567"];
    let preview3Links = preview.createPreview(entities, refLinks);
    let preview1Link = preview.createPreview([e1], refLinks.children[0]);

    QUnit.test("createPreviewCarousel", function (assert) {
        assert.ok(preview.createPreviewCarousel, "function available");


        assert.throws(() => {
            preview.createPreviewCarousel()
        }, 'no param throws');
        assert.throws(() => {
            preview.createPreviewCarousel(null, refLinks)
        }, 'null 1st param throws');
        assert.throws(() => {
            preview.createPreviewCarousel(entities, null)
        }, 'null 2nd param throws');
        assert.throws(() => {
            preview.createPreviewCarousel([], null)
        }, 'missing entities in 1st param throws');

        assert.equal($(carousel).find("." + preview.classNamePreviewCarouselNav).length, 1, "carousel has nav");
        assert.equal($(carousel).find(".carousel-inner").length, 1, "carousel has inner");
        for (let i = 0; i < correct_order_id.length; i++) {
            assert.equal(getEntityID($(carousel).find('.carousel-item')[i]), correct_order_id[i], "entities ids are in order")
        }

        assert.ok(carousel.id, "has id");
        assert.equal($(carousel).attr("data-bs-interval"), "false", "no auto-sliding");
    });

    QUnit.test("getSelectorButtons", function (assert) {
        assert.ok(preview.getSelectorButtons, "function available");
        assert.equal(preview.getSelectorButtons($(carousel).find('.' + preview.classNamePreviewCarouselNav)[0])[0].getAttribute('data-bs-slide-to'), "0", "found selector button");
    });

    QUnit.test("setActiveSlideItemSelector", function (assert) {
        assert.ok(preview.setActiveSlideItemSelector, "function available");

        assert.throws(() => {
            preview.setActiveSlideItemSelector()
        }, "no param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(null, 1)
        }, "null 1st param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, null)
        }, "null 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, 15)
        }, "too big 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, "asdf")
        }, "NaN 2nd param throws");
        assert.throws(() => {
            preview.setActiveSlideItemSelector(carousel, -5)
        }, "negative 2nd param throws");

        assert.equal(preview.setActiveSlideItemSelector(carousel, 1), carousel, "returns carousel");
        for (let i = 0; i < correct_order_id.length; i++) {
            preview.setActiveSlideItemSelector(carousel, i);
            assert.equal($($(carousel).find('[data-bs-slide-to]')[i]).hasClass("active"), true, "button " + i + " is active");
            assert.equal($(carousel).find('.carousel-item.active').length, 1, "and none else");
        }
    });

    QUnit.test("triggerUpdateActiveSlideItemSelector", function (assert) {
        assert.ok(preview.triggerUpdateActiveSlideItemSelector, "function available");

        preview.setActiveSlideItemSelector(carousel, 1);
        assert.equal(preview.getActiveSlideItemIndex(carousel), 0, "before: active item is 0");
        assert.equal($(carousel).find('.' + preview.classNamePreviewCarouselNav).find('.active')[0].getAttribute('data-bs-slide-to'), 1, 'before: active selector is 1.');
        $(carousel).on('slid.bs.carousel', preview.triggerUpdateActiveSlideItemSelector);
        $(carousel).trigger('slid.bs.carousel');
        assert.equal(preview.getActiveSlideItemIndex(carousel), 0, "after: active item is 0");
        assert.equal($(carousel).find('.' + preview.classNamePreviewCarouselNav).find('.active')[0].getAttribute('data-bs-slide-to'), 0, 'after: active selector is 0.');
    });

    QUnit.test("createPreview", function (assert) {
        assert.ok($(preview3Links).hasClass(preview.classNamePreview), "3 links class name.");
        assert.ok($(preview1Link).hasClass(preview.classNamePreview), "1 links class name.");
        assert.ok($(preview1Link).hasClass(preview.classNamePreview), "1 links returns entity element");
    });

    let xmlResponse = str2xml('<Response><Record id="1234"/><Record id="2345"/><Record id="3456"/><Record id="4567"/></Response>');
    let ref_property_elem = $('<div><div class="caosdb-f-property-value"></div></div>');
    let original_get = connection.get;
    ref_property_elem.find('div').append(refLinks);

    QUnit.test("initProperty", async function (assert) {
        var done = assert.async(2);
        assert.ok(preview.initProperty, "function available");

        let app = preview.initProperty(ref_property_elem[0]);
        assert.equal(app.state, 'showLinks', 'app returned in showLinks state');

        let showPreviewButton = $(ref_property_elem).find('.' + preview.classNameShowPreviewButton);
        assert.equal(showPreviewButton.length, 1, 'one show preview button.');

        // test case for error in get method
        connection.get = async function (uri) {
            assert.equal(uri, "Entity/1234&2345&3456&4567", "get called with correct uri");
            await sleep(1000);
            done();
            throw "test error";
        };

        showPreviewButton.click();
        await sleep(200);
        assert.equal(app.state, 'waiting', 'app is now in waiting state');


        // reset after error was thrown
        await sleep(1200);
        assert.equal(app.state, 'showLinks', 'after reset in showLinks state');

        // test case for mockup-preview data
        connection.get = async function (uri) {
            if (uri.match(/webinterface/g)) {
                return await original_get(uri);
            }
            assert.equal(uri, "Entity/1234&2345&3456&4567", "get called with correct uri agai againn");
            await sleep(1000);
            done();
            return xmlResponse;
        };

        showPreviewButton.click();
        await sleep(200);
        assert.equal(app.state, 'waiting', 'app is now in waiting state again');

        await sleep(2000);
        // the preview data arrived
        assert.equal(app.state, 'showPreview', 'app is now in showPreview state');

        let hidePreviewButton = $(ref_property_elem).find('.' + preview.classNameHidePreviewButton);
        assert.equal(hidePreviewButton.length, 1, 'one hidePreviewButton');
        hidePreviewButton.click();
        assert.equal(app.state, 'showLinks', 'again in showLinks state.');

        connection.get = function (uri) {
            assert.ok(null, 'get was called: ' + uri);
        }
        showPreviewButton.click();
        assert.equal(app.state, 'showPreview', 'again in showPreview state but without calling connection.get again.');
        app.resetApp();
        assert.equal($(ref_property_elem).find('.' + preview.classNamePreview).length, 0, 'no carousel after reset');



    });


};

QUnit.test("preparePreviewEntity", function (assert) {
    assert.ok(preview.preparePreviewEntity, "function available");
    let e = $('<div><div class="caosdb-v-entity-header-buttons-list"><div class="caosdb-f-reference-value"><a class="caosdb-id">1234</a></div></div></div>')[0];
    let prepared = preview.preparePreviewEntity(e);
    assert.equal($(prepared).find("a[title='Load this entity in a new window.']")[0].href, connection.getBasePath() + "Entity/1234", "link is correct.");
});

QUnit.test("getEntityRef", function (assert) {
    assert.ok(preview.getEntityRef, 'function available');

    var html = $('<div class="caosdb-f-reference-value"><div class="caosdb-id">sdfg</div></div>')[0];
    assert.equal(preview.getEntityRef(html), "sdfg", "id extracted");

    html = $('<div class="caosdb-f-reference-value"><div class="caosdb-id"></div></div>')[0];
    assert.equal(preview.getEntityRef(html), "", "empty string extracted");

    html = $('<div class="caosdb-f-reference-value"></div>')[0];
    assert.throws(() => {
        preview.getEntityRef(html);
    }, "missing .caosdb-id throws");
});


QUnit.test("getAllEntityRefs", function (assert) {
    assert.ok(preview.getAllEntityRefs, 'function available');
    assert.throws(preview.getAllEntityRefs, "null param throws");

    // overwrite called methods
    const oldGetReferenceLinks = preview.getReferenceLinks;
    preview.getReferenceLinks = function (links) {
        assert.propEqual(links, ["bla"], "array is passed to getReferenceLinks");
        return links;
    }
    const oldGetEntityRef = preview.getEntityRef;
    preview.getEntityRef = function (link) {
        assert.equal(link, "bla", "array elements are passed to getEntityRef");
        return "asdf";
    }

    assert.propEqual(preview.getAllEntityRefs(["bla"]), ["asdf"], "returns array with refs");


    // cleanup
    preview.getReferenceLinks = oldGetReferenceLinks;
});

QUnit.test("retrievePreviewEntities", function (assert) {
    let done = assert.async(3);
    connection.get = function (url) {
        if (url.length > 15) {
            assert.equal(url, "Entity/1&2&3&4&5", "All five entities are to be retrieved.");
            done();
            throw new Error("UriTooLongException")
        } else {
            assert.equal(url, "Entity/1&2", "Only the first two entities are to be retrieved.");
            done();
            throw new Error("Terminate this test!");
        }
    }
    assert.ok(preview.retrievePreviewEntities, "function available");
    preview.retrievePreviewEntities([1, 2, 3, 4, 5]).catch(err => {
        assert.equal(err.message, "Terminate this test!", "The url had been split up.");
        done();
    });
});

QUnit.test("transformXmlToPreviews", function (assert) {
    assert.ok(preview.transformXmlToPreviews, "function available");
    assert.ok(this.entityXSL, "xsl there");
    assert.ok(this.testXml, "xml there");

    let done = assert.async();
    let asyncTestCase = function (resolve) {
        done();
    };
    let asyncErr = function (error) {
        console.log(error);
        done();
        done();
    }
    preview.transformXmlToPreviews(this.testXml, this.entityXSL).then(asyncTestCase).catch(asyncErr);
});

QUnit.test("init", function (assert) {
    assert.ok(preview.init, "function available");
});

QUnit.test("initEntity", function (assert) {
    assert.ok(preview.initEntity, "function available");
});

/* MODULE queryForm */
QUnit.module("webcaosdb.js - queryForm", {
    before: function (assert) {
        assert.ok(queryForm, "queryForm is defined");
        assert.notOk(queryForm.initFreeSearch(), "free search reset");
    }
});

QUnit.test("isSelectQuery", function (assert) {
    assert.ok(queryForm.isSelectQuery, "function available.");
    assert.throws(() => queryForm.isSelectQuery(), "null param throws.");
    assert.equal(queryForm.isSelectQuery("SELECT asdf"), true);
    assert.equal(queryForm.isSelectQuery("select asdf"), true);
    assert.equal(queryForm.isSelectQuery("SEleCt"), true);
    assert.equal(queryForm.isSelectQuery("FIND"), false);
    assert.equal(queryForm.isSelectQuery("asd"), false);
    assert.equal(queryForm.isSelectQuery("SEL ECT"), false);
});

QUnit.test("init", function (assert) {
    assert.ok(queryForm.init, "init available");
});

QUnit.test("restoreLastQuery", function (assert) {
    assert.ok(queryForm.restoreLastQuery, "available");

    let form = document.createElement("form");
    form.innerHTML = '<textarea name="query"></textarea><div class="caosdb-search-btn"></div>';

    assert.throws(() => queryForm.restoreLastQuery(form, null), "null getter throws exc.");
    assert.throws(() => queryForm.restoreLastQuery(null, () => "test"), "null form throws exc.");
    assert.throws(() => queryForm.restoreLastQuery(null, () => undefined), "null form throws exc.");

    assert.equal(form.query.value, "", "before1: field is empty");
    queryForm.restoreLastQuery(form, () => undefined);
    assert.equal(form.query.value, "", "after1: field is still empty");

    assert.equal(form.query.value, "", "before2: field is empty");
    queryForm.restoreLastQuery(form, () => "this is the old query");
    assert.equal(form.query.value, "this is the old query", "after2: field is not empty");
});

QUnit.test("bindOnClick", function (assert) {
    assert.ok(queryForm.bindOnClick, "available");
    var done = assert.async(4);
    queryForm.redirect = function (a, b) {
        done();
    };

    let form = document.createElement("form");
    let submitButton = $("<input type=\"submit\">");
    form.innerHTML = '<textarea name="query"></textarea><div class="caosdb-search-btn"></div>';

    assert.throws(() => queryForm.bindOnClick(form, null), "null setter throws exc.");
    assert.throws(() => queryForm.bindOnClick(form, "asdf"), "non-function setter throws exc.");
    assert.throws(() => queryForm.bindOnClick(form, () => undefined), "setter with zero params throws exc.");
    assert.throws(() => queryForm.bindOnClick(null, (set) => undefined), "null form throws exc.");
    assert.throws(() => queryForm.bindOnClick("asdf", (set) => undefined), "string form throws exc.");

    let storage = function () {
        let x = undefined;
        return function (set) {
            if (set) {
                x = set;
            }
            return x;
        }
    }();
    // test empty query, function should return silently
    queryForm.bindOnClick(form, storage);
    assert.equal(storage(), undefined, "before1: storage empty.");
    form.getElementsByClassName("caosdb-search-btn")[0].onclick();
    assert.equal(storage(), undefined, "after1: storage still empty.");


    // test the click handler of the button, first without spaces ...
    form.query.value = "freetext";
    assert.equal(storage(), undefined, "before2: storage empty.");
    form.getElementsByClassName("caosdb-search-btn")[0].onclick();

    assert.equal(storage(), "FIND RECORD WHICH HAS A PROPERTY LIKE '*freetext*'", "after2: storage not empty.");

    // test the form submit handler analogously
    form.query.value = "freetext2";
    $("body").append(form);
    $(form).append(submitButton);
    submitButton.click();
    assert.equal(storage(), "FIND RECORD WHICH HAS A PROPERTY LIKE '*freetext2*'", "after3: storage not empty.");

    $(form).remove();

    // ... then with spaces
    form.query.value = "free text 3";
    $("body").append(form);
    $(form).append(submitButton);
    submitButton.click();
    assert.equal(storage(), "FIND RECORD WHICH HAS A PROPERTY LIKE '*free*' AND A PROPERTY LIKE '*text*' AND A PROPERTY LIKE '*3*'", "after4: storage not empty.");

    $(form).remove();

    // ... then with quotation marks gone rogue
    form.query.value = "\"with double\" 'and single' \"what's wrong?\" ' \"nothin'\" \"'bla";
    $("body").append(form);
    $(form).append(submitButton);
    submitButton.click();
    assert.equal(storage(), `FIND RECORD WHICH HAS A PROPERTY LIKE '*with double*' AND A PROPERTY LIKE '*and single*' AND A PROPERTY LIKE '*what\\'s wrong?*' AND A PROPERTY LIKE '*\\'*' AND A PROPERTY LIKE '*nothin\\'*' AND A PROPERTY LIKE '*"\\'bla*'`, "after5: stuff with quotation marks");

    // ... then with empty quotation marks. this will not trigger the query execution at all
    storage("not triggered");
    form.query.value = '""'
    $("body").append(form);
    $(form).append(submitButton);
    submitButton.click();
    assert.equal(storage(), "not triggered", "not triggered");

    $(form).remove();
});

QUnit.test("splitSearchTerms", function (assert) {
    assert.ok(queryForm.splitSearchTerms, "available");
    const cases = [
        ["", []],
        ['"', ['"']],
        ["a", ["a"]],
        ["a b", ["a", "b"]],
        ["'a b'", ["a b"]],
        ['"a b"', ["a b"]],
        [`"with double" 'and single' "what's wrong?" ' "nothin'" "'bla`,
            ["with double", "and single", "what's wrong?", "'", "nothin'", `"'bla`]
        ],
    ];
    for (let testCase of cases) {
        assert.deepEqual(queryForm.splitSearchTerms(testCase[0]), testCase[1], `test case ${testCase[0]}`);
    }
});

QUnit.test("initFreeSearch", function (assert) {
    const form = $('<form></form>');
    const inputGroup = $('<div class="input-group"></div>');
    const textArea = $('<textarea class="caosdb-f-query-textarea" name="query"></textarea>');
    const submitButton = $('<input class="caosdb-search-btn" type="submit">');
    inputGroup.append(textArea);
    form.append(inputGroup).append(submitButton);
    $("body").append(form);

    // without initialization
    assert.ok(queryForm.initFreeSearch, "available");
    assert.notOk(queryForm.getRoleNameFacetSelect(), "role_name_facet_select is undefined 1");
    assert.notOk(queryForm.initFreeSearch(), "not initialized");

    assert.notOk(queryForm.getRoleNameFacetSelect(), "role_name_facet_select is undefined 2");

    assert.equal(form.find("select").length, 0, "no select present");
    assert.equal(queryForm.getRoleNameFacet(), "RECORD");


    window.localStorage["role_name_facet_option"] = "Sample";
    assert.ok(queryForm.initFreeSearch(form, "Person, Experiment, Sample"), "initialized");

    // after initialization
    assert.ok(queryForm.getRoleNameFacetSelect(), "role_name_facet_select is defined");
    assert.equal(queryForm.getRoleNameFacetSelect().tagName, "SELECT", "role_name_facet_select is SELECT");
    assert.equal(form.find("select").length, 1, "select is present");

    assert.equal(queryForm.getRoleNameFacet(), "Sample", "previously selected option is selected");
    form.find("select")[0].value = "Experiment";
    assert.equal(queryForm.getRoleNameFacet(), "Experiment");

    // clean up
    form.remove();
})

/* MODULE paging */
QUnit.module("webcaosdb.js - paging", {
    before: function (assert) {}
});

QUnit.test("initPaging", function (assert) {
    let initPaging = paging.initPaging;
    let getPageHref = paging.getPageHref;
    assert.ok(initPaging, "function exists.");
    assert.equal(initPaging(), false, "no parameter returs false.");
    assert.equal(initPaging(null), false, "null parameter returns false.");
    assert.equal(initPaging(null, null), false, "null,null parameter returns false.");
    assert.throws(() => {
        initPaging(getPageHref(window.location.href, "0L10"), "asdf")
    }, "string parameter throws exc.");
    assert.equal(initPaging(window.location.href, 1234), true, "no paging.");
    assert.equal(initPaging(getPageHref(window.location.href, "0L10"), 1234), true, "1234 returns true.");
    assert.equal(initPaging(getPageHref(window.location.href, "0L10"), '1234'), true, "'1234' returns true.");

});


QUnit.test("getNextPage", function (assert) {
    let getNextPage = paging.getNextPage;
    assert.ok(getNextPage, "function exists.");
    assert.throws(() => {
        getNextPage(null, 100)
    }, "null P throws exc.");
    assert.throws(() => {
        getNextPage("0L10", null)
    }, "null n throws exc.");
    assert.throws(() => {
        getNextPage("asdf", 100)
    }, "P with wrong format 1.");
    assert.throws(() => {
        getNextPage("1234l2345", 100)
    }, "P with wrong format 2.");
    assert.throws(() => {
        getNextPage("1234Lasdf", 100)
    }, "P with wrong format 3.");
    assert.throws(() => {
        getNextPage("0L10", -100)
    }, "n is negative.");
    assert.throws(() => {
        getNextPage("0L10", "asdf")
    }, "string n throws exc.");
    assert.equal(getNextPage("23L11", 30), null, "n is smaller than index+length -> no next page.");
    assert.equal(getNextPage("0L10", 10), null, "n equals index+length -> no next page.");
    assert.equal(getNextPage("0L22", 30), "22L22", "0L22 to 22L22.")
    assert.equal(getNextPage("5L22", 30), "27L22", "5L22 to 27L22.")
    assert.equal(getNextPage("5L10", 30), "15L10", "5L10 to 15L10.")

});

QUnit.test("getPrevPage", function (assert) {
    let getPrevPage = paging.getPrevPage;
    assert.ok(getPrevPage, "function exists.");
    assert.throws(() => {
        getPrevPage(null)
    }, "null P throws exc.");
    assert.throws(() => {
        getPrevPage("asdf")
    }, "P with wrong format 1.");
    assert.throws(() => {
        getPrevPage("1234l2345")
    }, "P with wrong format 2.");
    assert.throws(() => {
        getPrevPage("1234Lasdf")
    }, "P with wrong format 3.");
    assert.equal(getPrevPage("0L10"), null, "Begins with 0 -> No previous page.");
    assert.equal(getPrevPage("10L10"), "0L10", "Index 10 to index 0.");
    assert.equal(getPrevPage("5L10"), "0L10", "Index 5 to index 0.");
    assert.equal(getPrevPage("23L11"), "12L11", "Index 5 to index 0.");
});

QUnit.test("getPSegmentFromUri", function (assert) {
    let getPSegmentFromUri = paging.getPSegmentFromUri;
    assert.ok(getPSegmentFromUri, "function exists.");
    assert.throws(() => {
        getPSegmentFromUri(null)
    }, "null uri throws exc.");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla"), null, "asdf has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf"), null, "asdf?asdf has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?P"), null, "asdf?P has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blablaP=0L10"), null, "asdfP=0L10 has no P segment");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?P=0L10"), "0L10", "asdf?P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdfP=0L10"), null, "asdf?asdfP=0L10 has no P segment.");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10"), "0L10", "asdf?asdf&P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?&P=0L10"), "0L10", "asdf?&P=0L10 -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10&"), "0L10", "asdf?asdf&P=0L10& -> P=0L10");
    assert.equal(getPSegmentFromUri("https://example:1234/blabla?asdf&P=0L10&asdfasdf"), "0L10", "asdf?asdf&P=0L10&asdfasdf -> P=0L10");
});

QUnit.test("getPageHref", function (assert) {
    let getPageHref = paging.getPageHref;
    assert.ok(getPageHref, "function exists.");
    assert.throws(() => {
        getPageHref(null, "asdf")
    }, "null uri_old throws exc.");
    assert.equal(getPageHref("1234?P=1234", null), null, "null page returns null.");
    assert.equal(getPageHref("https://example:1234/blabla?P=page1&", "page2"), "https://example:1234/blabla?P=page2&", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?P=page1", "page2"), "https://example:1234/blabla?P=page2", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?asdf&P=page1", "page2"), "https://example:1234/blabla?asdf&P=page2", "replace page1 with page2");
    assert.equal(getPageHref("https://example:1234/blabla?asdfP=page1", "page2"), "https://example:1234/blabla?asdfP=page1&P=page2", "append page2");
    assert.equal(getPageHref("https://example:1234/blabla", "page2"), "https://example:1234/blabla?P=page2", "append page2");
});

/* MODULE annotation */
QUnit.module("webcaosdb.js - annotation", {
    before: function (assert) {
        markdown.init();
        // overwrite (we don't actually want to send any post requests)
        annotation.postCommentXml = function (xml) {
            return new Promise(resolve => setTimeout(resolve, 1000, str2xml("<Response/>")));
        }
    }
});

QUnit.test("loadAnnotationXsl", function (assert) {
    assert.ok(annotation.loadAnnotationXsl, "function exists");
});

QUnit.test("getAnnotationsForEntity", function (assert) {
    let xsl_str = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html" /><xsl:template match="annotation"><div><xsl:value-of select="@id"/></div></xsl:template><xsl:template match="Response"><root><xsl:apply-templates select="annotation"/></root></xsl:template></xsl:stylesheet>';
    let xslPromise = str2xml(xsl_str);
    let xml_str = '<Response><annotation id="1"/><annotation id="2"/><annotation id="3"/></Response>';
    let response = str2xml(xml_str);
    let databaseRequest = (id) => {
        return response;
    };

    let done = assert.async();
    let asyncTestCase = function (result) {
        assert.equal(result.length, 3, "3 divs");
        assert.equal(result[0].tagName, "DIV", "is DIV");
        assert.equal(result[0].childNodes[0].nodeValue, "1", "test is '1'");
        assert.equal(result[1].tagName, "DIV", "is DIV");
        assert.equal(result[1].childNodes[0].nodeValue, "2", "test is '2'");
        assert.equal(result[2].tagName, "DIV", "is DIV");
        assert.equal(result[2].childNodes[0].nodeValue, "3", "test is '3'");
        done();
    }
    annotation.getAnnotationsForEntity(1337, databaseRequest, xslPromise).then(asyncTestCase).catch((error) => {
        console.log(error);
        assert.ok(false, "failure!");
        done();
    });
});

QUnit.test("async/await behavior", (assert) => {
    let af = async function () {
        return await "returnval";
    };

    let done = assert.async(3);
    af().then((result) => {
        assert.equal(result, "returnval", "af() called once.");
        done();
    });
    af().then((result) => {
        assert.equal(result, "returnval", "af() called twice.");
        done();
    });

    let er = async function () {
        throw "asyncerror";
    };

    er().catch((error) => {
        assert.equal(error, "asyncerror", "er() called.");
        done();
    });
});

QUnit.test("convertNewCommentForm", function (assert) {
    assert.ok(annotation.convertNewCommentForm, "function exists.");
    assert.equal(xml2str(annotation.convertNewCommentForm(annotation.createNewCommentForm(2345))), "<Insert><Record><Parent name=\"CommentAnnotation\"/><Property name=\"comment\"/><Property name=\"annotationOf\">2345</Property></Record></Insert>", "conversion ok.");
});


QUnit.test("convertNewCommentResponse", function (assert) {
    let convertNewAnnotationResponse = annotation.convertNewCommentResponse;
    assert.ok(convertNewAnnotationResponse, "function exists.");
    let done = assert.async();
    let testResponse = '<Response><Record><Property name="annotationOf"/><Version head="true" date="2015-12-24T20:15:00" username="someuser"/><Property name="comment">This is a comment</Property></Record></Response>';
    let expectedResult = "<li xmlns=\"http://www.w3.org/1999/xhtml\" class=\"list-group-item markdowned\"><div class=\"d-flex\"><div class=\"d-shrink-0\">»</div><div class=\"flex-grow-1 ms-3\"><div class=\"caosdb-f-comment-header\">someuser<small><i> posted on 2015-12-24T20:15:00</i></small></div><div class=\"caosdb-f-comment-body\"><small><p class=\"caosdb-comment-annotation-text\"><p>This is a comment</p></p></small></div></div></div></li>";
    convertNewAnnotationResponse(str2xml(testResponse), annotation.loadAnnotationXsl("../../")).then(function (result) {
        assert.equal(result.length, 1, "one element returned.");
        assert.equal(xml2str(result[0]).replace(/\n/g, ""), expectedResult, "result converted correctly");
        done();
    }, function (error) {
        console.log(error);
        assert.ok(false, "see console.log");
        done();
    });
});

QUnit.test("getEntityId", function (assert) {
    let annotationSection = $('<div data-entity-id="dfgh"/>')[0];
    assert.ok(annotation.getEntityId, "function exists.");
    assert.equal(annotation.getEntityId($('<div/>')[0]), null, "no data-entity-id attribute returns null");
    assert.equal(annotation.getEntityId(annotationSection), "dfgh", "returns correct entityId.");
    assert.equal(annotation.getEntityId(), null, "no param returns null.");
});

QUnit.test("createNewCommentForm", function (assert) {
    let createNewCommentForm = annotation.createNewCommentForm;
    assert.ok(createNewCommentForm, "function exists.");
    assert.equal(createNewCommentForm(1234).tagName, "FORM", "returns form");
    assert.equal(createNewCommentForm(1234).elements["annotationOf"].value, "1234", "annotationOf there");
    assert.equal($(createNewCommentForm(1234)).find("button[type='submit']")[0].name, "submit", "has submit button");
    assert.equal($(createNewCommentForm(1234)).find("button[type='submit']")[0].innerHTML, "Submit", "is labled 'Submit'");
    assert.equal($(createNewCommentForm(1234)).find("button[type='reset']")[0].name, "cancel", "has cancel button");
    assert.equal($(createNewCommentForm(1234)).find("button[type='reset']")[0].innerHTML, "Cancel", "is labled 'Cancel'");
    assert.equal($(createNewCommentForm(1234)).find("button[type='asdf']")[0], null, "no asdf button");
});

QUnit.test("getNewCommentButton", function (assert) {
    assert.ok(annotation.getNewCommentButton, "function exists.");
    assert.equal(annotation.getNewCommentButton($('<div/>')[0]), null, "not present");
    assert.equal(annotation.getNewCommentButton($('<div><button class="otherclass"/></div>')[0]), null, "not present");
    assert.equal(annotation.getNewCommentButton($('<div><button id="asdf" class="caosdb-new-comment-button"/></div>')[0]).id, "asdf", "button found.");
    assert.equal(annotation.getNewCommentButton(), null, "no parameter");
    assert.equal(annotation.getNewCommentButton(null), null, "null parameter");
});

QUnit.test("createPleaseWaitNotification", function (assert) {
    assert.ok(annotation.createPleaseWaitNotification, "function exists.");
    assert.ok($(annotation.createPleaseWaitNotification()).hasClass("caosdb-please-wait-notification"), "has class caosdb-please-wait-notification");
});

QUnit.test("getNewCommentForm", function (assert) {
    let annotationSection = $('<div><form id="sdfg" class="caosdb-new-comment-form"></form></div>')[0];
    assert.ok(annotation.getNewCommentForm, "function exists");
    assert.equal(annotation.getNewCommentForm(annotationSection).id, "sdfg", "NewCommentForm found.");
    assert.equal(annotation.getNewCommentForm(), null, "no param returns null");
});

QUnit.test("validateNewCommentForm", function (assert) {
    assert.ok(annotation.validateNewCommentForm, "function exists.");
    let entityId = "asdf";
    let form = annotation.createNewCommentForm(entityId);

    assert.throws(annotation.validateNewCommentForm, "no param throws exc.");
    assert.equal(annotation.validateNewCommentForm(form), false, "empty returns false");
    form.newComment.value = "this is a new comment";
    assert.equal(annotation.validateNewCommentForm(form, 50), false, "to short returns false");
    assert.equal(annotation.validateNewCommentForm(form), true, "long enough returns true");
});

QUnit.test("getPleaseWaitNotification", function (assert) {
    assert.ok(annotation.getPleaseWaitNotification, "function exists");
    assert.equal(annotation.getPleaseWaitNotification(), null, "no param returns null");
    assert.equal(annotation.getPleaseWaitNotification($('<div><div class="blablabla" id="asdf"></div></div>')[0]), null, "does not exist");
    assert.equal(annotation.getPleaseWaitNotification($('<div><div class="caosdb-please-wait-notification" id="asdf"></div></div>')[0]).id, "asdf", "found.");
});

QUnit.test("NewCommentApp exception", function (assert) {
    try {
        var original = annotation.createNewCommentForm;
        annotation.createNewCommentForm = function () {
            throw new TypeError("This is really bad!");
        }

        let annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
        let app = annotation.initNewCommentApp(annotationSection);
        app.openForm();

        let done = assert.async();
        setTimeout(() => {
            assert.equal(app.state, "read");
            done();
        }, 2000);
    } finally {
        // clean up
        annotation.createNewCommentForm = original
    }
});

QUnit.test("convertNewCommentResponse error", function (assert) {
    let errorStr = '<Response username="tf" realm="PAM" srid="dc1df091045eca7bd6940b88aa6db5b6" timestamp="1499814014684" baseuri="https://baal:8444/mpidsserver" count="1">\
        <Error code="12" description="One or more entities are not qualified. None of them have been inserted/updated/deleted." />\
        <Record>\
        <Error code="114" description="Entity has unqualified properties." />\
        <Warning code="0" description="Entity has no name." />\
        <Parent name="CommentAnnotation">\
        <Error code="101" description="Entity does not exist." />\
        </Parent>\
        <Property name="comment" importance="FIX">\
        sdfasdfasdf\
        <Error code="101" description="Entity does not exist." />\
        <Error code="110" description="Property has no datatype." />\
        </Property>\
        <Property name="annotationOf" importance="FIX">\
        20\
        <Error code="101" description="Entity does not exist." />\
        <Error code="110" description="Property has no datatype." />\
        </Property>\
        </Record>\
        </Response>';

    let done = assert.async();
    let expectedResult = "<divxmlns=\"http://www.w3.org/1999/xhtml\"class=\"alertalert-dangercaosdb-new-comment-erroralert-dismissablemarkdowned\"><buttonclass=\"btn-close\"data-bs-dismiss=\"alert\"aria-label=\"close\">×</button><strong>Error!</strong>Thiscommenthasnotbeeninserted.<pclass=\"small\"><pre><code>&lt;record&gt;&lt;errorcode=\"114\"description=\"Entityhasunqualifiedproperties.\"&gt;&lt;/error&gt;&lt;warningcode=\"0\"description=\"Entityhasnoname.\"&gt;&lt;/warning&gt;&lt;parentname=\"CommentAnnotation\"&gt;&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;/parent&gt;&lt;propertyname=\"comment\"importance=\"FIX\"&gt;sdfasdfasdf&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;errorcode=\"110\"description=\"Propertyhasnodatatype.\"&gt;&lt;/error&gt;&lt;/property&gt;&lt;propertyname=\"annotationOf\"importance=\"FIX\"&gt;20&lt;errorcode=\"101\"description=\"Entitydoesnotexist.\"&gt;&lt;/error&gt;&lt;errorcode=\"110\"description=\"Propertyhasnodatatype.\"&gt;&lt;/error&gt;&lt;/property&gt;&lt;/record&gt;</code></pre></p></div>";
    annotation.convertNewCommentResponse(str2xml(errorStr), annotation.loadAnnotationXsl("../../")).then(function (result) {
        assert.equal(xml2str(result[0]).replace(/[\t\n\ ]/g, ""), expectedResult.replace(/[\t\n\ ]/g, ""), "transformed into an error div.");
        done();
    }, function (error) {
        console.log(error);
        assert.ok(false, "see console.log");
        done();
    });
})

QUnit.test("NewCommentApp convertNewCommentResponse", function (assert) {
    var done = assert.async(2);
    var original = annotation.convertNewCommentResponse;
    annotation.convertNewCommentResponse = function (xmlPromise, xslPromise) {
        done(1); // was called;
        return original(xmlPromise, xslPromise);
    }
    let originalPost = annotation.postCommentXml;
    annotation.postCommentXml = function (xml) {
        let testResponse = '<Response><Record><Property name="annotationOf"/><History transaction="INSERT" datetime="2015-12-24T20:15:00" username="someuser"/><Property name="comment">This is a comment</Property></Record></Response>';
        return new Promise(resolve => setTimeout(resolve, 1000, str2xml(testResponse)));
    }

    // prepare app
    var annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
    var app = annotation.initNewCommentApp(annotationSection);
    // prepare form
    app.openForm();
    var form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment qwerasdf.";

    app.observe("onEnterRead", () => {
        assert.equal(app.state, "read", "finally in read state");
        assert.equal($(annotationSection).find(".caosdb-comment-annotation-text")[0].innerHTML, "<p>This is a comment</p>", "new comment appended.");

        let button = annotation.getNewCommentButton(annotationSection);
        assert.ok(button.onclick, "button click is enabled.");
        assert.notOk($(button).hasClass('disabled'), "button is not visually disabled.");
        assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");
        assert.equal(annotation.getPleaseWaitNotification(annotationSection), null, "please wait is not there");

        done();
    });
    app.submitForm(form);

    // clean up
    annotation.convertNewCommentResponse = original;
    annotation.postCommentXml = originalPost;
});

QUnit.test("NewCommentApp convertNewCommentForm/postCommentXml", function (assert) {
    let done = assert.async(2);

    // do not actually post the comment, just wait 1 second and return
    // something.
    let originalPost = annotation.postCommentXml;
    annotation.postCommentXml = function (xml) {
        assert.equal(xml2str(xml), "<Insert><Record><Parent name=\"CommentAnnotation\"/><Property name=\"comment\">This is a new comment qwerasdf.</Property><Property name=\"annotationOf\">tzui</Property></Record></Insert>", "the conversion was sucessful");
        done(2); // postCommentXml was called
        return new Promise(resolve => setTimeout(resolve, 1000, str2xml("<Response/>")));
    }


    // prepare app
    let annotationSection = $('<div data-entity-id="tzui"><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeReceiveResponse = null; // because the response is empty.
    // prepare form
    app.openForm();
    let form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment qwerasdf.";

    let originalConvert = annotation.convertNewCommentForm;
    annotation.convertNewCommentForm = function (sendform) {
        assert.ok(sendform == form, "form is still the same");
        done(1); // convertNewCommentForm was called
        return originalConvert(sendform);
    }

    app.submitForm(form);
    assert.equal(app.state, "send", "in send state");


    // clean up
    annotation.convertNewCommentForm = originalConvert;
    annotation.postCommentXml = originalPost;
});

QUnit.test("NewCommentApp waitingNotification", function (assert) {
    // prepare app
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeReceiveResponse = null; // because the response is empty
    // prepare form
    app.openForm();
    let form = annotation.getNewCommentForm(annotationSection);
    form.newComment.value = "This is a new comment";


    $(form).submit();
    assert.equal(app.state, "send", "in send state");
    assert.equal(annotation.getPleaseWaitNotification(annotationSection).className, "caosdb-please-wait-notification", "please wait is there");
});


QUnit.test("NewCommentApp form.onsubmit", function (assert) {
    let done = assert.async(2);

    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    // add to body, otherwise click event would not be working
    $(document.body).append(annotationSection);
    let app = annotation.initNewCommentApp(annotationSection);
    app.onEnterSend = null; // remove form submission

    app.openForm();

    let form = annotation.getNewCommentForm(annotationSection);
    let submitButton = annotation.getSubmitNewCommentButton(annotationSection);

    // test with empty form -> rejected
    app.observe("onBeforeTransition", function (e) {
        done("1&2");
    });

    assert.equal(app.state, "write", "before in write state");
    submitButton.click();
    assert.equal(app.state, "write", "after emtpy submit still in write state");

    form.newComment.value = "This is a new comment";
    submitButton.click();
    assert.equal(app.state, "send", "after non-empty submit in send state");


    // clean up
    $(annotationSection).remove();
});

QUnit.test("NewCommentApp form.onreset", function (assert) {
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.openForm();

    let done = assert.async();
    assert.equal(annotation.getNewCommentForm(annotationSection).className, "caosdb-new-comment-form", "form is there");
    app.observe("onBeforeCancelForm", function (e) {
        assert.equal(e.from, "write", "leaving write state");
        done();
    });

    // add to body since the click event isn't working otherwise (damn!)
    $(document.body).append(annotationSection);

    annotation.getCancelNewCommentButton(annotationSection).click();
    assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");

    app.openForm();
    assert.equal($(annotationSection).find('form').length, 1, "form is not duplicated")

    // clean up
    $(annotationSection).remove();
});

QUnit.test("getCancelNewCommentButton", function (assert) {
    let annotationSection = $('<div><form class="caosdb-new-comment-form"><button id="fghj" type="reset"/></form></div>')[0];
    assert.ok(annotation.getCancelNewCommentButton, "function exists.");
    assert.equal(annotation.getCancelNewCommentButton(), null, "no param returns null");
    assert.equal(annotation.getCancelNewCommentButton(null), null, "null param returns null");
    assert.equal(annotation.getCancelNewCommentButton(annotationSection).id, "fghj", "returns correctly");
    assert.equal(annotation.getCancelNewCommentButton($('<div><form class="caosdb-new-comment-form"><button type="submit"/></form></div>')[0]), null, "button does not exist");
});

QUnit.test("getSubmitNewCommentButton", function (assert) {
    let annotationSection = $('<div><form class="caosdb-new-comment-form"><button id="fghj" type="submit"/></form></div>')[0];
    assert.ok(annotation.getSubmitNewCommentButton, "function exists.");
    assert.equal(annotation.getSubmitNewCommentButton(), null, "no param returns null");
    assert.equal(annotation.getSubmitNewCommentButton(null), null, "null param returns null");
    assert.equal(annotation.getSubmitNewCommentButton(annotationSection).id, "fghj", "returns correctly");
    assert.equal(annotation.getSubmitNewCommentButton($('<div><form class="caosdb-new-comment-form"><button type="reset"/></form></div>')[0]), null, "button does not exist");
});

QUnit.test("NewCommentApp newCommentButton.onclick", function (assert) {
    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    let button = annotation.getNewCommentButton(annotationSection);

    assert.equal(app.state, "read", "in read state");
    assert.ok(button.onclick, "button click is enabled.");
    assert.notOk($(button).hasClass('disabled'), "button is not visually disabled.");
    assert.equal(annotation.getNewCommentForm(annotationSection), null, "form is not there");

    $(button).click();
    assert.equal(app.state, "write", "after click in write state");
    assert.equal(button.onclick, null, "button click is disabled");
    assert.ok($(button).hasClass("disabled"), "button is visually disabled.")
    assert.ok(annotation.getNewCommentForm(annotationSection), "form is there");
    assert.equal(annotation.getNewCommentForm(annotationSection).parentNode.className, "list-group-item", "form is wrapped into list-group-item");
});

QUnit.test("NewCommentApp transitions", function (assert) {
    assert.throws(annotation.initNewCommentApp, "null parameter throws exc.");

    let annotationSection = $('<div><button class="caosdb-new-comment-button"></button></div>')[0];
    let app = annotation.initNewCommentApp(annotationSection);
    app.onBeforeSubmitForm = null; // remove validation
    app.onEnterSend = null; // remove form submission
    app.onLeaveWrite = null; // remove form deletion
    app.onBeforeReceiveResponse = null; // remove appending the result

    assert.ok(app, "app ok");
    assert.equal(app.state, "read", "initial state is read");
    app.openForm();
    assert.equal(app.state, "write", "open form -> state write");
    app.cancelForm();
    assert.equal(app.state, "read", "cancel -> state read");
    app.openForm();
    assert.equal(app.state, "write", "open form -> state write");
    app.submitForm();
    assert.equal(app.state, "send", "submit -> state send");
    app.receiveResponse();
    assert.equal(app.state, "read", "receiveRequest -> state read");
    app.resetApp("no error");
    assert.equal(app.state, "read", "reset -> state read");
});

QUnit.test("annotation module", function (assert) {
    assert.ok(annotation, "module exists.");
    assert.ok(annotation.createNewCommentForm, "createNewCommentForm exists.");
    assert.ok(annotation.initNewCommentApp, "initNewCommentApp exists.");
});


/* MODULE navbar */
QUnit.module("webcaosdb.js - navbar", {
    before: () => {
        $(document.body).append('<div id="top-navbar"><ul class="caosdb-navbar"/></div>');
    },
    beforeEach: () => {
        $(".caosdb-f-navbar-toolbox").remove();
    },
    after: () => {
        $("#top-navbar").remove();
    },
});

QUnit.test("get_navbar", function (assert) {
    assert.equal(navbar.get_navbar().className, "caosdb-navbar");
});

QUnit.test("add_button wrong parameters", function (assert) {
    assert.throws(() => {
        navbar.add_button(undefined)
    }, /button is expected/, "undefined throws");
    assert.throws(() => {
        navbar.add_button({
            "test": "an object"
        })
    }, "object throws");
    assert.throws(() => {
        navbar.add_button(["array of strings"])
    }, "array of string throws");
});

QUnit.test("test button classes", function (assert) {
    var result = $(navbar.add_button("TestButton")).children().first()
    assert.equal(result.text(), "TestButton", "text is correct");
});

QUnit.test("add_tool", function (assert) {
    assert.equal($(".caosdb-f-navbar-toolbox").length, 0, "no toolbox");
    navbar.add_tool("TestButton", "TestMenu");

    var toolbox = $("ul.caosdb-f-navbar-toolbox");
    assert.equal(toolbox.length, 1, "new toolbox");
    assert.equal(toolbox.find("button").length, 1, "new button");
    assert.equal(toolbox.find("button").text(), "TestButton", "Name correct")

    assert.notOk(toolbox.hasClass("btn"));
    assert.notOk(toolbox.hasClass("btn-link"));
    assert.notOk(toolbox.hasClass("navbar-btn"));

    assert.notOk(toolbox.siblings("a.dropdown-toggle").hasClass("btn"));
    assert.notOk(toolbox.siblings("a.dropdown-toggle").hasClass("btn-link"));
    assert.notOk(toolbox.siblings("a.dropdown-toggle").hasClass("navbar-btn"));
});

QUnit.test("toolbox example", function (assert) {
    // this is a kind of integration test and it uses the toolbox_example
    // module from toolbox_example.js. That example is also usefull for manual
    // testing.
    assert.equal($(".caosdb-f-navbar-toolbox").length, 0, "no toolbox");
    toolbox_example.init();
    assert.equal($(".caosdb-f-navbar-toolbox").length, 3, "three toolboxes");

    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Useful Links"]').length, 1, "one 'Useful Links' toolbox");
    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Useful Links"] a[href="https://indiscale.com"]').length, 1, "one external link");

    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Server-side Scripts"]').length, 1, "one 'Server-side Scripts' toolbox");
    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Server-side Scripts"] form input[type="submit"]').attr("value"), "Trigger Crawler", "one crawler trigger button");
    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Server-side Scripts"] form').attr("title"), "Trigger the crawler.", "form has tooltip");

    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Tools"]').length, 1, "one 'Tools' toolbox");
    assert.equal($('.caosdb-f-navbar-toolbox[data-toolbox-name="Tools"] button').length, 3, "three 'Tools' buttons");
});

QUnit.test("hide elements for roles", function (assert) {
    navbar.add_button("TestButton1");
    navbar.add_button("TestButton2");

    // mock user role
    $("#top-navbar").append(`<div class="caosdb-user-role">someRole</div>`);
    navbar.hideElementForRoles($("button:contains('TestButton1')"), ["someOtherRole"]);
    navbar.hideElementForRoles("button:contains('TestButton2')", ["someOtherRole"]);
    // I don't have the role, so nothing is hidden:
    assert.notOk($("button:contains('TestButton1')").hasClass("d-none"));
    assert.notOk($("button:contains('TestButton2')").hasClass("d-none"));

    // Add second role
    $("#top-navbar").append(`<div class="caosdb-user-role second-role">someOtherRole</div>`);
    // now I do
    navbar.hideElementForRoles($("button:contains('TestButton1')"), ["someOtherRole"]);
    navbar.hideElementForRoles("button:contains('TestButton2')", ["someOtherRole"]);
    assert.ok($("button:contains('TestButton1')").hasClass("d-none"));
    assert.ok($("button:contains('TestButton2')").hasClass("d-none"));

    // Remove second role again
    $(".second-role").remove();
    // now I don't any more
    navbar.hideElementForRoles($("button:contains('TestButton1')"), ["someOtherRole"]);
    navbar.hideElementForRoles("button:contains('TestButton2')", ["someOtherRole"]);
    assert.notOk($("button:contains('TestButton1')").hasClass("d-none"));
    assert.notOk($("button:contains('TestButton2')").hasClass("d-none"));
});

QUnit.module("webcaosdb.js - version_history", {
    before: function (assert) {
        connection._init();
    },
    after: function (assert) {
        connection._init();
    },
});

QUnit.test("available", function (assert) {
    assert.equal(typeof version_history.init, "function");
    assert.equal(typeof version_history.get_history_tsv, "function");
    assert.equal(typeof version_history.init_export_history_buttons, "function");
    assert.equal(typeof version_history.init_restore_version_buttons, "function");
    assert.equal(typeof version_history.init_load_history_buttons, "function");
    assert.equal(typeof version_history.retrieve_history, "function");
})

QUnit.test("init_load_history_buttons and init_load_history_buttons", async function (assert) {
    var xml_str = `<Response username="user1" realm="Realm1" srid="bc2f8f6b-71d6-49ca-890c-eebea3e38e18" timestamp="1606253365632" baseuri="https://localhost:10443" count="1">
  <UserInfo username="user1" realm="Realm1">
    <Roles>
      <Role>role1</Role>
    </Roles>
  </UserInfo>
  <Record id="8610" name="TestRecord1-6thVersion" description="This is the 6th version.">
    <Permissions>
      <Permission name="RETRIEVE:HISTORY" />
    </Permissions>
    <Version id="efa5ac7126c722b3f43284e150d070d6deac0ba6">
      <Predecessor id="f09114b227d88f23d4e23645ae471d688b1e82f7" />
      <Successor id="5759d2bccec3662424db5bb005acea4456a299ef" />
    </Version>
    <Parent id="8609" name="TestRT" />
  </Record>
</Response>
`;
    var done = assert.async(2);
    var xml = str2xml(xml_str);
    version_history._get = async function (entity) {
        assert.equal(entity, "Entity/8610@efa5ac7126c722b3f43284e150d070d6deac0ba6?H");
        done();
        $(xml).find("Version").attr("completeHistory", "true");
        return xml;
    }
    var html = await transformation.transformEntities(xml);
    var load_button = $(html).find(".caosdb-f-entity-version-load-history-btn");
    $("body").append(html);

    assert.notOk(load_button.is(":visible"), "load_button hidden");
    load_button.click(); // nothing happens

    version_history.init_load_history_buttons();
    assert.ok(load_button.is(":visible"), "load_button is not hidden anymore");

    // load_button triggers retrieval of history
    load_button.click();
    await sleep(500);

    var gone_button = $(html).find(".caosdb-f-entity-version-load-history-btn");
    assert.equal(gone_button.length, 0, "button is gone");

    export_button = $(html).find(".caosdb-f-entity-version-export-history-btn");
    assert.ok(export_button.is(":visible"), "export_button is visible");

    version_history._download_tsv = function (tsv) {
        assert.equal(tsv.indexOf("data:text/csv;charset=utf-8,Entity ID%09"), 0);
        done();
    }
    export_button.click();

    $(html).remove();
});

QUnit.test("available", function (assert) {
    assert.equal(typeof restore_old_version, "function");
})

QUnit.test("init_restore_version_buttons", async function (assert) {
    var xml_str = `<Response username="user1" realm="Realm1" srid="bc2f8f6b-71d6-49ca-890c-eebea3e38e18" timestamp="1606253365632" baseuri="https://localhost:10443" count="1">
  <UserInfo username="user1" realm="Realm1">
    <Roles>
      <Role>role1</Role>
    </Roles>
  </UserInfo>
  <Record id="8610" name="TestRecord1-6thVersion" description="This is the 6th version.">
    <Permissions>
      <Permission name="RETRIEVE:HISTORY" />
      <Permission name="UPDATE:*" />
    </Permissions>
    <Version id="efa5ac7126c722b3f43284e150d070d6deac0ba6" >
      <Predecessor id="f09114b227d88f23d4e23645ae471d688b1e82f7" />
      <Successor id="5759d2bccec3662424db5bb005acea4456a299ef" />
    </Version>
    <Parent id="8609" name="TestRT" />
  </Record>
</Response>
`;
    var done = assert.async(1);
    var xml = str2xml(xml_str);
    version_history._get = async function (entity) {
        assert.equal(entity, "Entity/8610@efa5ac7126c722b3f43284e150d070d6deac0ba6?H");
        done();
        $(xml).find("Version").attr("completeHistory", "true");
        return xml;
    }
    var html = await transformation.transformEntities(xml);
    var load_button = $(html).find(".caosdb-f-entity-version-load-history-btn");
    $("body").append(html);

    assert.notOk(load_button.is(":visible"), "load_button hidden");
    load_button.click(); // nothing happens

    version_history.init_load_history_buttons();
    assert.ok(load_button.is(":visible"), "load_button is not hidden anymore");

    //console.log(xml2str(restore_button[0]));
    //assert.ok(restore_button.hasClass("d-none"), "restore_button is hidden");


    // load_button triggers retrieval of history
    load_button.click();
    await sleep(500);

    //console.log(xml2str(restore_button[0]));
    //version_history.init_restore_version_buttons();

    var restore_button = $("body").find(".caosdb-f-entity-version-restore-btn");
    assert.ok(!restore_button.hasClass("d-none"), "restore_button is not hidden anymore");

    // restore_button triggers retrieval of history
    localStorage["form_elements.alert_decision.restore_entity"] = "proceed";
    restore_button.first().click();
    localStorage.removeItem("form_elements.alert_decision.restore_entity");
    await sleep(500);

    // restore is not possible in the unit test
    alertdiv = $(html).find(".alert-danger");
    assert.equal(alertdiv.length, 1, "on alert div");
    assert.ok(alertdiv.text().indexOf("Restore failed") > 0, "Restore failed");

    $(html).remove();
});


/* SETUP tests for user_management */
QUnit.module("webcaosdb.js - user_management", {
    afterEach: function (assert) {
        $(user_management.get_change_password_form()).remove();
    },
});


QUnit.test("get_change_password_form", async function (assert) {
    assert.equal(typeof user_management.get_change_password_form(), "undefined", "no password form");

    const modal = callTemplate(
        (await transformation.retrieveXsltScript("navbar.xsl")),
        "change-password-modal", {
            "username": "testuser",
            "realm": "testrealm"
        });

    $("body").append(modal);

    assert.ok(user_management.get_change_password_form(), "found password form");
});

QUnit.test("submit new password", async function (assert) {
    const modal = $(callTemplate(
        (await transformation.retrieveXsltScript("navbar.xsl")),
        "change-password-modal", {
            "username": "testuser",
            "realm": "testrealm"
        }).firstElementChild);

    $("body").append(modal);

    user_management.init();
    var done = assert.async();
    user_management.set_new_password = async (realm, user, password) => {
        assert.equal(realm, "testrealm", "realm correct");
        assert.equal(user, "testuser", "user correct");
        assert.equal(password, "newtestpassword1A!", "password correct");
        done();
    }

    const form = modal.find("form");
    assert.ok(form, "form there");

    form[0]["password"].value = "newtestpassword1A!";
    form[0]["password2"].value = "newtestpassword1A!";

    form.find(":submit").click();
});
