#!/bin/bash

# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2019-2023 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2023 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# Copy files which are currently edited or debugged to the LinkAhead server docker container

set -e

# Copy just the publicly accessible files
core_dir="$(dirname $0)/../src/core"
container="linkahead"
docker_webui_root="/opt/caosdb/git/linkahead-server/linkahead-webui"
docker_dir="${docker_webui_root}/src/core/"

docker cp "${core_dir}/." "$container:$docker_dir"
docker exec -ti -w "$docker_webui_root" "$container" make
